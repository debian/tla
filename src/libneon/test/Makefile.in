# Makefile for neon test suite.

SHELL = @SHELL@
CPPFLAGS = @CPPFLAGS@ -I. -I$(top_srcdir)/src -I$(top_srcdir)/test/common
CFLAGS = @CFLAGS@ @NEON_CFLAGS@
LDFLAGS = -L$(top_builddir)/../expat @LDFLAGS@
DEFS = @DEFS@

top_builddir = ..
top_srcdir = @top_srcdir@
srcdir = @srcdir@
VPATH = @srcdir@

AR = ar

RANLIB = @RANLIB@
LIBS = $(LIBTEST)
CC = @CC@
OPENSSL = @OPENSSL@

HELPERS = @HELPERS@
BASIC_TESTS = uri-tests util-tests string-tests session socket \
	request auth basic stubs redirect
ZLIB_TESTS = compress
ZLIB_HELPERS = file1.gz file2.gz trailing.gz badcsum.gz truncated.gz corrupt1.gz corrupt2.gz
DAV_TESTS = xml acl props lock
SSL_TESTS = socket-ssl ssl
SSL_HELPERS = ca-stamp
TESTS = @TESTS@
VALGRIND = valgrind --gdb-attach=yes --leak-check=yes

# Make every object depend on libneon.la to force a rebuild on any src/* changes
OBJDEPS = $(srcdir)/common/tests.h $(srcdir)/common/child.h $(srcdir)/utils.h \
	$(top_builddir)/config.h $(top_builddir)/src/libneon.la
# Test program just depends on libtest
DEPS = $(LIBTEST)

LIBTEST = libtest.la
LIBNEON = $(top_builddir)/src/libneon.la

LIBTOOL = @LIBTOOL@ --silent
LINK = $(LIBTOOL) --mode=link $(CC) $(LDFLAGS) -no-install
COMPILE = $(LIBTOOL) --mode=compile $(CC) $(CPPFLAGS) $(CFLAGS)

.SUFFIXES:
.SUFFIXES: .lo .c

# By default, compile but don't run the tests.
all: $(TESTS)

clean:
	rm -f $(TESTS) $(HELPERS) *.*o common/*.*o libtest.a *.log
	rm -rf ca .libs
	rm -f ca-stamp client.key *.csr ssigned.pem wrongcn.pem \
	   server.cert client.cert client.p12 *.cert

check: $(TESTS) $(HELPERS)
	@SRCDIR=$(srcdir) $(SHELL) $(srcdir)/run.sh $(TESTS)

grind: $(TESTS) $(HELPERS)
	@SRCDIR=$(srcdir) HARNESS="$(VALGRIND)" $(SHELL) $(srcdir)/run.sh $(TESTS)

NEWS = $(top_srcdir)/NEWS

file1.gz: $(NEWS)
	gzip -c --no-name $(NEWS) > $@

file2.gz: $(NEWS)
	gzip -c --name $(NEWS) > $@

# gzip file with trailing bytes.
trailing.gz: $(NEWS)
	gzip -c --no-name $(NEWS) > $@
	echo "hello, world" >> $@

truncated.gz: file1.gz
	dd if=file1.gz of=$@ bs=2048 count=2

badcsum.gz: file1.gz
	dd of=$@ if=file1.gz bs=1 count=`perl -e 'printf "%d", (stat("file1.gz"))[7] - 8;'`
	echo 'broken!' >> $@

corrupt1.gz: file1.gz
	dd of=$@ if=file1.gz bs=1 count=500
	cat $(NEWS) >> $@

corrupt2.gz: 
	cat $(NEWS) > $@

# Dummy target to create the CA keys etc.  makekeys stderr is redirected
# since it changes for every invocation; not helpful for regression
# testing.
ca-stamp: $(srcdir)/makekeys.sh $(srcdir)/openssl.conf
	rm -rf ca
	OPENSSL=$(OPENSSL) \
	 $(SHELL) $(srcdir)/makekeys.sh $(srcdir) 2>makekeys.out
	@echo timestamp > ca-stamp

Makefile: $(srcdir)/Makefile.in
	cd .. && ./config.status test/Makefile

LIBOBJS = common/tests.lo common/child.lo utils.lo

$(LIBTEST): $(LIBOBJS)
	$(LINK) -o $(LIBTEST) $(LIBOBJS) $(LIBNEON)

.c.lo:
	$(COMPILE) -c $< -o $@

.lo:
	$(LINK) -o $@ $< $(LIBS)

# Recompile socket.c with SOCKET_SSL defined
socket-ssl.lo: $(srcdir)/socket.c $(HDRS)
	$(COMPILE) -DSOCKET_SSL -c $(srcdir)/socket.c -o $@

socket-ssl: socket-ssl.lo $(LIBTEST)
	$(LINK) -o $@ socket-ssl.lo $(LIBS)

resolve: resolve.lo $(LIBNEON)
	$(LINK) -o $@ resolve.lo $(LIBNEON)

auth.lo: $(srcdir)/auth.c $(OBJDEPS)
uri-tests.lo: $(srcdir)/uri-tests.c $(OBJDEPS)
util-tests.lo: $(srcdir)/util-tests.c $(OBJDEPS)
string-tests.lo: $(srcdir)/string-tests.c $(OBJDEPS)
socket.lo: $(srcdir)/socket.c $(OBJDEPS)
server.lo: $(srcdir)/server.c $(OBJDEPS)
request.lo: $(srcdir)/request.c $(OBJDEPS)
regress.lo: $(srcdir)/regress.c $(OBJDEPS)
compress.lo: $(srcdir)/compress.c $(OBJDEPS)
acl.lo: $(srcdir)/acl.c $(OBJDEPS)
utils.lo: $(srcdir)/utils.c $(OBJDEPS)
stubs.lo: $(srcdir)/stubs.c $(OBJDEPS)
props.lo: $(srcdir)/props.c $(OBJDEPS)
session.lo: $(srcdir)/session.c $(OBJDEPS)
redirect.lo: $(srcdir)/redirect.c $(OBJDEPS)
basic.lo: $(srcdir)/basic.c $(OBJDEPS)
ssl.lo: $(srcdir)/ssl.c $(OBJDEPS)
cookies.lo: $(srcdir)/cookies.c $(OBJDEPS)

auth: auth.lo $(DEPS)
basic: basic.lo $(DEPS)
uri-tests: uri-tests.lo $(DEPS)
util-tests: util-tests.lo $(DEPS)
string-tests: string-tests.lo $(DEPS)
socket: socket.lo $(DEPS)
server: server.lo $(DEPS)
request: request.lo $(DEPS)
regress: regress.lo $(DEPS)
compress: compress.lo $(DEPS)
acl: acl.lo $(DEPS)
utils: utils.lo $(DEPS)
stubs: stubs.lo $(DEPS)
props: props.lo $(DEPS)
session: session.lo $(DEPS)
redirect: redirect.lo $(DEPS)
basic: basic.lo $(DEPS)
ssl: ssl.lo $(DEPS)
xml: xml.lo $(DEPS)
lock: lock.lo $(DEPS)
cookies: cookies.lo $(DEPS)
