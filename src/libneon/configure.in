dnl Require autoconf 2.53 for an AC_C_BIGENDIAN which supports 
dnl cross-compiling.
AC_PREREQ(2.53)

dnl Extract the version (sans LF) from .version, created at release-time.
m4_define(ne_version, [m4_translit(m4_include(.version), [
])])

AC_INIT(neon, ne_version, [neon@webdav.org])

AC_COPYRIGHT([Copyright (c) 2000, 2001, 2002 Joe Orton
This configure script may be copied, distributed and modified under the 
terms of the GNU Library General Public license; see COPYING for more details])

AC_CONFIG_HEADER(config.h)
AC_CONFIG_SRCDIR(src/ne_request.c)

NEON_WITH_LIBS

# Pass through initial LDFLAGS verbatim to neon-config, so that extra
# libraries which are detected (e.g. OpenSSL) can still be found when
# building using the --libs output of neon-config.
user_LDFLAGS=$LDFLAGS
AC_SUBST(user_LDFLAGS)

# By default, allow 'make install' to work.
ALLOW_INSTALL=yes
AC_SUBST(ALLOW_INSTALL)

# Always defined
AC_DEFINE([_GNU_SOURCE], 1, [Unconditionally define _GNU_SOURCE])
# Defined when neon is built as library
AC_DEFINE(NEON_IS_LIBRARY, 1, [Define when building neon as a library])

AC_PROG_INSTALL

dnl Avoid libtool 1.5 bug where configure fails if a C++ compiler
dnl is not available.
m4_ifdef([AC_LIBTOOL_TAGS], [AC_LIBTOOL_TAGS([])])

AC_DISABLE_SHARED
AC_PROG_LIBTOOL

AC_EXEEXT

top_builddir=`pwd`
AC_SUBST(top_builddir)

AC_ARG_ENABLE(webdav,
AC_HELP_STRING([--disable-webdav], [disable WebDAV support]))

if test "$enable_webdav" = "no"; then
  NEON_WITHOUT_WEBDAV
else
  # Yes, we do need an XML parser. The _BUNDLED macros handle
  # this normally.
  NEON_NEED_XML_PARSER=yes
fi

# The bundled macros also set this, which makes sure we recurse
# into the 'src' directory.
NEON_BUILD_BUNDLED=yes
AC_SUBST(NEON_BUILD_BUNDLED)

# Define NEON_VERSION* and make the appropriate substitutions.
NEON_VERSIONS

# Pass the interface version on to libtool when linking libneon.la
NEON_LINK_FLAGS="-version-info ${NEON_INTERFACE_VERSION}"

# Checks to compile test suite
NEON_TEST

LIBNEON_SOURCE_CHECKS

# Use the libtool-type build.
NEON_LIBTOOL_BUILD
# Find an XML parser

# NEON_XML_PARSER(expat)

AC_DEFINE(HAVE_EXPAT, 1, [Define if you have expat])
AC_DEFINE(HAVE_XMLPARSE_H, 1, [Define if using expat which includes xmlparse.h])
neon_xml_parser_message="expat"
NEON_LIBS="$NEON_LIBS -lexpat"
neon_xml_parser=expat
with_included_expat=yes

# Extra checks for debugging, compiler warnings
NEON_DEBUG
# Leave till last to prevent CFLAGS affecting checks.
NEON_WARNINGS

CFLAGS="$CFLAGS -I\${top_builddir}"

dnl Substitute NEON_VERSION for neon-config too.
AC_SUBST(NEON_VERSION)

AC_ARG_ENABLE(memleak, 
AC_HELP_STRING([--enable-memleak], [for test builds only: enable memory leak checking]))

dnl Have autoheader include the following template in config.h.in:
AH_VERBATIM([NEON_MEMLEAK], 
[/* Enable memory leak detection. */
#ifdef NEON_MEMLEAK
# include "memleak.h"
#endif])

if test "$enable_memleak" = "yes"; then
  CPPFLAGS="$CPPFLAGS -DNEON_MEMLEAK -I\$(top_srcdir)/src"
  # disable 'make install'
  ALLOW_INSTALL=memleak
fi

# Enable tests for optional features
TESTS="\$(BASIC_TESTS)"
HELPERS=""
if test "$NEON_SUPPORTS_SSL" = "yes"; then
   # Only enable SSL tests if an openssl binary is found (needed to make
   # certs etc).
   AC_PATH_PROG(OPENSSL, openssl, notfound)
   if test "$OPENSSL" != "notfound"; then
     TESTS="$TESTS \$(SSL_TESTS)"
     HELPERS="$HELPERS \$(SSL_HELPERS)"
   else
     AC_MSG_WARN([no openssl binary in \$PATH: SSL tests disabled])
   fi
fi
if test "$NEON_SUPPORTS_DAV" = "yes"; then
   TESTS="$TESTS \$(DAV_TESTS)"
fi
if test "$NEON_SUPPORTS_ZLIB" = "yes"; then
   TESTS="$TESTS \$(ZLIB_TESTS)"
   HELPERS="$HELPERS \$(ZLIB_HELPERS)"
fi
AC_SUBST(HELPERS)
AC_SUBST(TESTS)

AC_CONFIG_FILES([neon-config], [chmod +x neon-config])
AC_CONFIG_FILES([Makefile src/Makefile test/Makefile neon.pc])

AC_OUTPUT

# for VPATH builds:
test -d test/common || mkdir test/common

AC_MSG_NOTICE([Configured to build AC_PACKAGE_STRING:

  Install prefix:  ${prefix}
  Compiler:        ${CC}
  XML Parser:      ${neon_xml_parser_message}
  SSL library:     ${neon_ssl_message}
  zlib support:    ${neon_zlib_message}
  Build libraries: Shared=${enable_shared}, Static=${enable_static}
 
Now run 'make' to compile the neon library.
])

case $ALLOW_INSTALL in
memleak)
  AC_MSG_NOTICE([Configured with development-only flags:

WARNING: This copy of neon has been configured with memory leak checking
WARNING: enabled, which should only be used in a development copy of neon.
WARNING: This neon library should not be installed for use by applications.
]);;
esac
