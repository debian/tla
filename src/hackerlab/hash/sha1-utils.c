/* sha1-utils.c: 
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord
 * Adapted for SHA1:
 *  Copyright (C) 2004 Colin Walters <walters@verbum.org>
 * 
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */


#include "hackerlab/char/str.h"
#include "hackerlab/hash/sha1-utils.h"


/************************************************************************
 *(h1 "SHA1 Convenience Functions")
 * 
 * 
 * 
 */


/*(c sha1_ascii_for_str)
 * t_uchar * sha1_ascii_for_str (alloc_limits limits,
 *                               t_uchar * string);
 * 
 * Return a 0-terminated (41 bytes, total) string containing
 * a hexadecimal expression of the SHA1 hash of `string'.
 * 
 * This procedure internally allocates and frees memory using
 * `limits'.
 */
t_uchar *
sha1_ascii_for_str (alloc_limits limits,
                   t_uchar * string)
{
  return sha1_ascii_for_str_n (limits, string, str_length (string));
}


/*(c sha1_ascii_for_str_n)
 * t_uchar * sha1_ascii_for_str_n (alloc_limits limits,
 *                                 t_uchar * string,
 *                                 size_t length);
 * 
 * Return a 0-terminated (41 bytes, total) string containing a
 * hexadecimal expression of the SHA1 hash of the `length'-byte
 * `string'.
 * 
 * This procedure internally allocates and frees memory using
 * `limits'.
 */
t_uchar *
sha1_ascii_for_str_n (alloc_limits limits,
                     t_uchar * string,
                     size_t length)
{
  t_uchar binary[20];

  sha1_for_str_n (binary, limits, string, length);
  return sha1_alloc_ascii (limits, binary);
}


/*(c sha1_for_str)
 * void sha1_for_str (t_uchar * result,
 *                    alloc_limits limits,
 *                    t_uchar * string);
 * 
 * Fill the 20-byte array `result' with the (binary) SHA1
 * hash of `string'.
 * 
 * This procedure internally allocates and frees memory using
 * `limits'.
 */
void
sha1_for_str (t_uchar * result,
	      alloc_limits limits,
	      t_uchar * string)
{
  return sha1_for_str_n (result, limits, string, str_length (string));
}


/*(c sha1_for_str_n)
 * void sha1_for_str_n (t_uchar * result,
 *                      alloc_limits limits,
 *                      t_uchar * string,
 *                      size_t length);
 * 
 * Fill the 20-byte array `result' with the (binary) SHA1 
 * hash of the `length'-byte `string'.
 * 
 * This procedure internally allocates and frees memory using
 * `limits'.
 */
void
sha1_for_str_n (t_uchar * result,
		alloc_limits limits,
		t_uchar const * string,
		size_t length)
{
  sha1_context_t context;

  context = make_sha1_context (limits);
  sha1_scan (context, string, length);
  sha1_final (result, context);
}

/**
 * \brief get a sha1_t for a memory region
 */
sha1_t
sha1_for_bytes (t_uchar const * bytes, size_t length)
{
  sha1_t result;
  sha1_for_str_n ((t_uchar *)&result, lim_use_must_malloc, bytes, length);
  return result;
}



/* tag: Colin Walters Mon, 05 Jan 2004 20:11:18 -0500 (sha1-utils.c)
 */
