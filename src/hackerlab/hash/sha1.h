/* sha1.h:
 *
 ****************************************************************
 * Copyright (C) 2003 Colin Walters
 * 
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */

#ifndef INCLUDE__HASH__SHA1_H
#define INCLUDE__HASH__SHA1_H


#include "hackerlab/machine/types.h"
#include "hackerlab/mem/alloc-limits.h"



typedef struct sha1_
{
  t_uint32 A;
  t_uint32 B;
  t_uint32 C;
  t_uint32 D;
  t_uint32 E;
} sha1_t;

struct sha1_context;
typedef struct sha1_context * sha1_context_t;



/* automatically generated __STDC__ prototypes */
extern sha1_context_t make_sha1_context (alloc_limits limits);
extern void sha1_context_reset (sha1_context_t ctx);
extern void free_sha1_context (alloc_limits limits, sha1_context_t ctx);
extern void sha1_scan (sha1_context_t hd, const t_uchar * inbuf, size_t inlen);
extern void sha1_final (t_uchar * result, sha1_context_t hd);
extern t_uchar * sha1_alloc_ascii (alloc_limits limits, t_uchar * result);
extern void sha1_ascii (t_uchar * answer, t_uchar * result);
extern void sha1_to_ascii (t_uchar * answer, sha1_t * sha1);
extern int sha1_from_ascii (sha1_t * out, t_uchar const * ascii);
#define sha1_cmp(left, right) mem_cmp (&left, &right, 20)
#endif  /* INCLUDE__HASH__sha1_H */


/* tag: Colin Walters Sun, 04 Jan 2004 22:37:11 -0500 (sha1.h)
 */
