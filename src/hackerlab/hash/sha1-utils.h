/* sha1-utils.h:
 *
 ****************************************************************
 * Copyright (C) 2004 Colin Walters <walters@verbum.org>
 * 
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */

#ifndef INCLUDE__HASH__SHA1_UTILS_H
#define INCLUDE__HASH__SHA1_UTILS_H


#include "hackerlab/hash/sha1.h"



/* automatically generated __STDC__ prototypes */
extern t_uchar * sha1_ascii_for_str (alloc_limits limits,
				     t_uchar * string);
extern t_uchar * sha1_ascii_for_str_n (alloc_limits limits,
				       t_uchar * string,
				       size_t length);
extern void sha1_for_str (t_uchar * result,
			  alloc_limits limits,
			  t_uchar * string);
extern void sha1_for_str_n (t_uchar * result,
			    alloc_limits limits,
			    t_uchar const * string,
			    size_t length);
extern sha1_t sha1_for_bytes (t_uchar const * bytes, size_t length);
#endif  /* INCLUDE__HASH__SHA1_UTILS_H */


/* tag: Colin Walters Mon, 05 Jan 2004 20:09:21 -0500 (sha1-utils.h)
 */
