char* pathcompress_compress_path(char* path, char* compressed);
char* pathcompress_uncompress_path(char* path, char* uncompressed);
void pathcompress_add_path(char* path, char* abspath);
void pathcompress_add_pathid(char* path);
