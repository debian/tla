/* date-string.h:
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */

#ifndef INCLUDE__LIBDATE__DATE_STRING_H
#define INCLUDE__LIBDATE__DATE_STRING_H


#include "hackerlab/machine/types.h"



/* automatically generated __STDC__ prototypes */
extern t_uchar * pretty_date (time_t t);
extern t_uchar * standard_date (time_t t);
#endif  /* INCLUDE__LIBDATE__DATE_STRING_H */


/* tag: Tom Lord Sat Jan  5 15:26:10 2002 (date-string.h)
 */
