/* date-string.c:
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */


#include "hackerlab/bugs/panic.h"
#include "hackerlab/os/time.h"
#include "hackerlab/char/str.h"
#include "hackerlab/mem/alloc-limits.h"
#include "tla/libdate/date-string.h"



t_uchar *
pretty_date (time_t t)
{
  struct tm local;
  struct tm * localp;
  t_uchar buf[128];
  size_t len;
  t_uchar * answer = 0;

  localp = localtime (&t);
  invariant (!!localp);

  local = *localp;

  len = strftime (buf, sizeof (buf), "%a %b %e %H:%M:%S %Z %Y", &local);
  invariant (len);

  answer = str_save (0, buf);
  return answer;
}

t_uchar *
standard_date (time_t t)
{
  struct tm gm;
  struct tm * gmp;
  t_uchar buf[128];
  size_t len;
  t_uchar * answer = 0;

  gmp = gmtime (&t);
  invariant (!!gmp);

  gm = *gmp;

  len = strftime (buf, sizeof (buf), "%Y-%m-%d %H:%M:%S GMT", &gm);
  invariant (len);

  answer = str_save (0, buf);
  return answer;
}




/* tag: Tom Lord Sun May 25 11:41:15 2003 (date-string.c)
 */
