/* relassoc.h:
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */

#ifndef INCLUDE__LIBAWK__RELASSOC_H
#define INCLUDE__LIBAWK__RELASSOC_H


#include "tla/libawk/relational.h"
#include "tla/libawk/associative.h"


/* automatically generated __STDC__ prototypes */
extern assoc_table rel_to_assoc (rel_table t, int k, int v);
#endif  /* INCLUDE__LIBAWK__RELASSOC_H */


/* tag: Tom Lord Sat Jan  5 15:26:10 2002 (relassoc.h)
 */
