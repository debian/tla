/* relational.h:
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */

#ifndef INCLUDE__FILE_UTILS__RELATIONAL_H
#define INCLUDE__FILE_UTILS__RELATIONAL_H


#include "hackerlab/machine/types.h"
#include "hackerlab/char/dstr.h"



extern int rel_make_record;

struct rel_field
{
  t_dstr _d;
};

typedef struct rel_field rel_field;

struct rel_record
{
  rel_field * _c;
};
typedef struct rel_record rel_record;

extern const rel_record rel_record_null;

struct rel_table
{
  rel_record * _r;
};
typedef struct rel_table rel_table;

#ifndef REL_STATIC_DEFNS
#define REL_STATIC_DEFNS
static const rel_field rel_field_nil = { 0 };
static const rel_record rel_record_nil = { 0 };
static const rel_table rel_table_nil = { 0 };
#endif

struct rel_join_output_spec
{
  int table;
  int field;
};

typedef int * rel_cut_spec;



/* automatically generated __STDC__ prototypes */
extern int rel_n_records (rel_table r);
extern int rel_n_fields (rel_record r);
extern rel_record rel_peek_record (rel_table r, ssize_t row);
extern rel_field rel_get_field (rel_table r, ssize_t row, ssize_t col);
extern t_dstr rel_get_dstr (rel_table r, ssize_t row, ssize_t col);
extern t_dstr rel_peek_dstr (rel_table r, ssize_t row, ssize_t col);
extern const t_uchar * rel_peek_str (rel_table r, ssize_t row, ssize_t col);
extern t_dstr rel_rec_peek_dstr (rel_record rec, ssize_t col);
extern const t_uchar * rel_rec_peek_str (rel_record rec, ssize_t col);
extern int rel_set_taking (rel_table r,
                           ssize_t row,
                           ssize_t col,
                           rel_field d);
extern int rel_set_record (rel_table r, ssize_t row, rel_record rec);
extern rel_record rel_make_record_0 (void);
extern rel_record rel_make_record_1_taking (rel_field f0);
extern rel_record rel_make_record_2_taking (rel_field f0,
                                            rel_field f1);
extern rel_record rel_make_record_3_taking (rel_field f0,
                                            rel_field f1,
                                            rel_field f2);
extern rel_record rel_make_record_4_taking (rel_field f0,
                                            rel_field f1,
                                            rel_field f2,
                                            rel_field f3);
extern rel_record rel_make_record_5_taking (rel_field f0,
                                            rel_field f1,
                                            rel_field f2,
                                            rel_field f3,
                                            rel_field f4);
extern rel_record rel_make_record_6_taking (rel_field f0,
                                            rel_field f1,
                                            rel_field f2,
                                            rel_field f3,
                                            rel_field f4,
                                            rel_field f5);
extern rel_record rel_make_record_7_taking (rel_field f0,
                                            rel_field f1,
                                            rel_field f2,
                                            rel_field f3,
                                            rel_field f4,
                                            rel_field f5,
                                            rel_field f6);
extern rel_record rel_make_record_8_taking (rel_field f0,
                                            rel_field f1,
                                            rel_field f2,
                                            rel_field f3,
                                            rel_field f4,
                                            rel_field f5,
                                            rel_field f6,
                                            rel_field f7);
extern void rel_add_records (rel_table * table, ...);
  /* must terminate list with rel_record_null, NOT 0 or NULL ! */
extern void rel_add_field_taking (rel_record * r, rel_field field);
extern void rel_add_field_to_row_taking (rel_table table, ssize_t row, rel_field field);
extern rel_record rel_singleton_record_taking (rel_field field);
extern void rel_free_table (rel_table t);
extern void rel_free_record (rel_record r);
extern rel_field rel_field_ref (rel_field f);
extern void rel_field_unref (rel_field f);
extern const t_uchar * rel_field_str (rel_field f);
extern rel_field rel_make_field_str (const t_uchar * s);
extern rel_field rel_make_field_str_n (const t_uchar * s, ssize_t len);
extern rel_table rel_ws_split (const t_uchar * string);
extern rel_table rel_nl_split (const t_uchar * string);
extern rel_table rel_copy_table (rel_table t);
extern rel_record rel_copy_record (rel_record r);
extern void rel_append_x (rel_table * out, rel_table t);
extern void rel_reverse_table (rel_table t);
extern void rel_sort_table_by_field (int reverse_p,
                                     rel_table t,
                                     int field_n);
extern void rel_sort_table_by_field_fn (int reverse_p,
                                        rel_table t,
                                        int field_n, 
                                        int (*fn)(const t_uchar *, const t_uchar *));
extern void rel_sort_table_by_fields (int reverse_p,
                                      rel_table t,
                                      int * fields);
extern int * rel_sort_fields (int f, ...);
extern void rel_uniq_by_field (rel_table * table,
                               int field);
extern rel_table rel_cut (rel_cut_spec fields, rel_table t);
extern rel_record rel_cut_record (rel_cut_spec fields, rel_record r);
extern rel_cut_spec rel_cut_list (int field, ...);
extern rel_table rel_join (int absence_table,
                           struct rel_join_output_spec * output,
                           int table1_field,
                           int table2_field,
                           rel_table table1,
                           rel_table table2);
extern struct rel_join_output_spec * rel_join_output (int table,
                                                      int field, ...);
extern rel_table rel_read_table (int fd,
                                 int n_fields,
                                 const char * err_name,
                                 const char * err_src);
extern void rel_print_table (int fd, rel_table table);
extern void rel_print_pika_escape_iso8859_1_table (int fd, int escape_classes, rel_table table);
extern void rel_print_table_sp (int fd, rel_table file);
extern void rel_print_pika_escape_iso8859_1_table_sp (int fd, int escape_classes, rel_table file);
extern rel_table rel_table_substract (rel_table t, rel_table t2);
extern int rel_table_contains(rel_table t,  rel_record r);
#endif  /* INCLUDE__FILE_UTILS__RELATIONAL_H */


/* tag: Tom Lord Sat Jan  5 15:26:10 2002 (relational.h)
 */
