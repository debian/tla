/* relassoc.c:
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */


#include "tla/libawk/relassoc.h"



assoc_table
rel_to_assoc (rel_table t, int k, int v)
{
  int lim;
  int x;
  assoc_table answer = 0;

  lim = rel_n_records (t);
  for (x = 0; x < lim; ++x)
    assoc_set_taking (&answer, rel_get_field (t, x, k), rel_get_field (t, x, v));

  return answer;
}



/* tag: Tom Lord Thu May 15 14:41:16 2003 (relassoc.c)
 */
