/* associative.h:
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */

#ifndef INCLUDE__FILE_UTILS__ASSOCIATIVE_H
#define INCLUDE__FILE_UTILS__ASSOCIATIVE_H


#include "hackerlab/machine/types.h"
#include "tla/libawk/relational.h"



typedef struct assoc_table_incomplete_type * assoc_table;



/* automatically generated __STDC__ prototypes */
extern void assoc_set_taking (assoc_table * vtable,
                              rel_field key,
                              rel_field value);
extern rel_field assoc_get_taking (assoc_table vtable,
                                   rel_field key);
extern const t_uchar * assoc_get_str_taking (assoc_table vtable,
                                             rel_field key);
extern void assoc_del_taking (assoc_table vtable,
                              rel_field key);
extern void free_assoc_table (assoc_table table);
#endif  /* INCLUDE__FILE_UTILS__ASSOCIATIVE_H */


/* tag: Tom Lord Sat Jan  5 15:26:10 2002 (associative.h)
 */
