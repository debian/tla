GNU Arch 1.3.5

Copyright 2003, 2004, 2005, 2006 Free Software Foundation, Inc. and
contributors (see the file AUTHORS for a more complete list)

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version

This is GNU Arch version 1.3.5, a revision control system.  This
version of GNU Arch is also known as tla. 

GNU Arch was invented and mainly written by Tom Lord
<lord@emf.net>.  Significant parts of this program were written
by Robert Collins and James Blackwell as independent
contributors or employees at Canonical Ltd. 

Currently it is maintained by Andy Tai <atai@gnu.org>.

CHANGES IN 1.3.5

* Fixes for Bugs 11731, 11701, 7502, 16334, 16539, 16538, 16780
* Enhanced support for several different ssh implementations by 
  Ludovic Court�s <ludovic.courtes@laas.fr>
* SHA1 checksums 
* Undo command specific file selection,  by Ludovic
* Microsoft Windows NT FTP server support
* commit and undo commands allow file exclusion with the -x option
* library-dir as alias for my-revision-library
* archive-register as alias for register-archive
* Cygwin support, file name compression patches from Lode Leroy  
  <lode_leroy@hotmail.com>, incorporated into tla proper 
  (see notes below)
* apply-delta now supports diff3 style merge (via the -t option)
* honor --skip-present for replay, patch by Ludovic


32-bit Windows, Cygwin notes:

Now the Cygwin specific changes to tla by Lode Leroy
<lode_leroy@hotmail.com> are incorporated into the tla proper.  However,
to get tla to work on Cygwin requires more than changes to tla; changes
to GNU diff, tar and patch are necessary.  Since these tools are outside
the Arch Project's  control, here the patches to them are just
distributed together with tla in the cygwin/ directory; follow the
instructions in the cygwin/BUILD.txt file for installing tla on cygwin.

Thanks to Lode Leroy for his efforts in getting tla to work on Cygwin.

CHANGES IN 1.3.4 

Version 1.3.4 makes no changes to the underlying infrastructure
of tla; the main changes are in the user interface, the adaption
of many commands from bazaar, a fork of the earlier versions of
tla by Robert Collins and Canonical.  Specifically, tla now
supports the following new commands:

tla lint

   is an alias for tla tree-lint
   
tla add

   is an alias for tla add-id
   
tla branch [options] [SOURCE] BRANCH

   behaves like tla tag except that it can use the current
   directory as the source and then switch the current directory
   to the new branch

tla diff [options] [revisions] -- [files...]

   functions similar to tla what-changed --diffs  but supports
   the following additional usage for some common operations:

   tla diff -L
   
       compares the current tree with the latest versions in the
       repository (what is commonly called the 'HEAD' version in
       CVS)

   tla diff -s 
   
       shows the names of the changed files only (by
       default diff shows the whole changes)
   
   tla diff -- file1 file2 ...
   
       shows only the changes in the specified files
   
       
   tla diff -D <diff option 1> -D <diff option 2> ... 
       allows command line options to be passed to 'diff' when
       display differences between versions (note these options
       are not used for actual changeset computation)

tla export [options] [revision] DIR 

   allows the full source tree of  particular revision to be
   placed in a directory DIR excluding meta files used by
   Arch/tla.

tla switch  [options] [package]

   changes the current source directory to a particular revision
   with uncommitted changed preserved.

tla tree-id
tla tree-revision
    
   prints the tree revision name the current directory
corresponds to

   
In addition, a new "basic" help mode is provided:

tla help -b

only shows the "basic" commands necessary for using tla,
reducing the complexity of the help messages and making tla less
intimidating to new users.

ARCHIVE FORMAT NOTES

Note since tla version 1.3.2, tla creates "baz" format archives
by default.  If you want your archives to be readable by tla
versions earlier than 1.3.2, pass the  -t or --tla option to
'tla make-archive' 

For more information about GNU Arch (tla), see the Arch wiki at

http://wiki.gnuarch.org/

A mailing list for Arch users is available at

gnu-arch-users@gnu.org

(go to http://lists.gnu.org/mailman/listinfo/gnu-arch-users to
subscribe or to access the mailing list archives)

To report bugs, go to
http://savannah.gnu.org/bugs/?group=gnu-arch&func=additem
    
FINALLY, THE MEANING OF "TLA"
    
Some thought tla stands for "Tom Lord's Arch."  Accord to Tom: 

   ("Tom Lord's Arch") is  not what TLA stands for, although the
   coincidence that it could be read that way made for some
   amusing discussions on IRC,  way back when.

   TLA is a punning acronym that I chose because it could stand
   for "true love, always" and "three letter acronym".

   The former because of a misplaced faith in humanity.  The
   latter because it is a traditional hacker joke.

   -t
