#!/bin/sh
# -*- mode: sh; coding: utf-8 -*-
# Test archive mirror creation
# Copyright © 2003 Colin Walters <walters@verbum.org>
#
# See the file "COPYING" for further information about
# the copyright and warranty status of this work.

set -e

arg0="$0"
srcdir=`dirname "$arg0"`

. ${srcdir}/test-framework

setup_with_trivial_archives

test_class "Archive mirror"

begin_test "Archive mirror creation 1"
tla make-archive --baz --mirror jane@example.com--2003 ${ARCHIVEDIR}/jane-archive-mirror
test "$(tla whereis-archive jane@example.com--2003-MIRROR)" = ${ARCHIVEDIR}/jane-archive-mirror
test -f ${ARCHIVEDIR}/jane-archive-mirror/=meta-info/mirror
test $(cat ${ARCHIVEDIR}/jane-archive-mirror/=meta-info/mirror) = "jane@example.com--2003"
end_test

begin_test "Archive mirror creation 2 (with listing)"
tla make-archive --baz --listing --mirror foo@example.org--2003 ${ARCHIVEDIR}/foo-archive-mirror
test "$(tla whereis-archive foo@example.org--2003-MIRROR)" = ${ARCHIVEDIR}/foo-archive-mirror
test -f ${ARCHIVEDIR}/foo-archive-mirror/=meta-info/mirror
test $(cat ${ARCHIVEDIR}/foo-archive-mirror/=meta-info/mirror) = "foo@example.org--2003"
end_test 

begin_test "Archive-mirror shouldn't update cached revisions"
tla archive-mirror jane@example.com--2003
tla cacherev jane@example.com--2003/hello-world--mainline--1.0--base-0
test -f ${ARCHIVEDIR}/jane-archive/hello-world--mainline--1.0/base-0/hello-world--mainline--1.0--base-0.tar.gz
tla archive-mirror jane@example.com--2003
test ! -f ${ARCHIVEDIR}/jane-archive-mirror/hello-world--mainline--1.0/base-0/hello-world--mainline--1.0--base-0.tar.gz
end_test 

begin_test "Archive-mirror with revision spec should add cached revisions"
tla archive-mirror jane@example.com--2003 hello-world--mainline--1.0--base-0
test -f ${ARCHIVEDIR}/jane-archive-mirror/hello-world--mainline--1.0/base-0/hello-world--mainline--1.0--base-0.tar.gz
end_test

begin_test "Archive-mirror with revision spec should remove cached revisions"
tla uncacherev jane@example.com--2003/hello-world--mainline--1.0--base-0
test ! -f ${ARCHIVEDIR}/jane-archive/hello-world--mainline--1.0/base-0/hello-world--mainline--1.0--base-0.tar.gz
tla archive-mirror jane@example.com--2003 hello-world--mainline--1.0--base-0
test ! -f ${ARCHIVEDIR}/jane-archive-mirror/hello-world--mainline--1.0/base-0/hello-world--mainline--1.0--base-0.tar.gz
end_test

clean_workdir

# tag: Colin Walters Tue, 16 Sep 2003 23:03:55 -0400 (test-mirror-creation.sh)
#
