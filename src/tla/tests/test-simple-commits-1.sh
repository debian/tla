#!/bin/sh
# -*- mode: sh; coding: utf-8 -*-
# Simple commit tests
# Copyright © 2003 Colin Walters <walters@verbum.org>
#
# See the file "COPYING" for further information about
# the copyright and warranty status of this work.

set -e

arg0="$0"
srcdir=`dirname "$arg0"`

. ${srcdir}/test-framework

setup_with_trivial_archives

test_class "simple commits"

setup () {
  tla get jane@example.com--2003/hello-world--mainline--1.0 hello-world
  cd hello-world
}  

begin_test "single file modification"
setup
sed -e 's/Hello World/Hello, World/' < hello-world.c > hello-world.c.new
mv hello-world.c.new hello-world.c
tla commit -L 'correctly punctuate'
archive_has_revision_with_summary jane@example.com--2003 hello-world--mainline--1.0--patch-1 'correctly punctuate'
end_test 

begin_test "Adding new file"
setup
echo 'We follow the GNU coding standards' > HACKING
tla add-id HACKING
tla commit -L 'Add HACKING document'
archive_has_revision_with_summary jane@example.com--2003 hello-world--mainline--1.0--patch-2 'Add HACKING document'
end_test 

begin_test "Removing existing file"
setup
rm README
tla delete-id README
tla commit -L 'No README is necessary'
archive_has_revision_with_summary jane@example.com--2003 hello-world--mainline--1.0--patch-3 'No README is necessary'
end_test 

begin_test "Moving file"
setup
mv hello-world.c hello_world.c
tla move-id hello-world.c hello_world.c
tla commit -L 'Use UNIX separator'
archive_has_revision_with_summary jane@example.com--2003 hello-world--mainline--1.0--patch-4 'Use UNIX separator'
end_test 

clean_workdir

# tag: Colin Walters Wed, 17 Sep 2003 12:26:09 -0400 (test-simple-commits-1.sh)
#
