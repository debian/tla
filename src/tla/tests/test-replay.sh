#!/bin/sh
# -*- mode: sh; coding: utf-8 -*-
# Simple replay tests
# Copyright © 2006 Ludovic Courtès <ludovic.courtes@laas.fr>
#
# See the file "COPYING" for further information about
# the copyright and warranty status of this work.

set -e

arg0="$0"
srcdir=`dirname "$arg0"`

. ${srcdir}/test-framework

setup_with_trivial_archives

test_class "replay"

initialize_replay () {
  tla tag -S jane@example.com--2003/hello-world--mainline--1.0 \
             foo@example.org--2003/hello-world--fork--1.0
}

setup_replay () {
  tla get jane@example.com--2003/hello-world--mainline--1.0 hello-world--jane
  tla get foo@example.org--2003/hello-world--fork--1.0 hello-world--fork
}

cleanup () {
  rm -rf hello-world--jane hello-world--fork
}

MAINLINE_VERSION="jane@example.com--2003/hello-world--mainline--1.0"
FORK_VERSION="foo@example.org--2003/hello-world--fork--1.0"

initialize_replay

begin_test "replaying simple changeset"
setup_replay

cd hello-world--jane
echo 'This is a new file.' > the-new-file.txt
tla add-id the-new-file.txt
tla commit -L 'Added the new file.'
cd ..

cd hello-world--fork
tla replay "$MAINLINE_VERSION"
tla commit -L "Replayed Jane's changes."
cd ..

assert_working_tree_equivalence hello-world--fork hello-world--jane
cleanup
end_test


begin_test "replaying and skipping present patches"
setup_replay
cd hello-world--fork
echo 'Thanks Jane for this lovely file.' >> the-new-file.txt
tla commit -L "Improved Jane's new file."
cd ..
cd hello-world--jane
tla replay --skip-present "$FORK_VERSION"
tla commit -L "Merged new file improvements."
cd ..

assert_working_tree_equivalence hello-world--fork hello-world--jane
cleanup
end_test


begin_test "replaying what's new (nothing) and skipping present patches"
setup_replay
cd hello-world--jane
tla replay --skip-present --new "$FORK_VERSION"
cd ..

assert_working_tree_equivalence hello-world--fork hello-world--jane
cleanup
end_test


begin_test "replaying what's new and skipping present patches"
setup_replay
cd hello-world--fork
echo 'Really, it rocks.' >> README
tla commit -L "Appended personal opinion to the \`README' file."
cd ..

cd hello-world--jane
tla replay --skip-present --new "$FORK_VERSION"
cd ..

assert_working_tree_equivalence hello-world--fork hello-world--jane
cleanup
end_test


begin_test "replaying exact patch and skipping if already present"
setup_replay
copy_tree hello-world--jane hello-world--jane.good
cd hello-world--jane
tla replay --skip-present "$FORK_VERSION"--patch-1
cd ..

assert_working_tree_equivalence hello-world--jane hello-world--jane.good
rm -rf hello-world--jane.good
cleanup
end_test

clean_workdir

# tag: Ludovic Courtès Fri, 16 Jun 2006 15:01:42 +0200 (test-replay.sh)
#
