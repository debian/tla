#!/bin/sh
# -*- mode: sh; coding: utf-8 -*-
# Simple mv tests on explicitly tagged files/directories/symlinks
# Copyright © 2003 Colin Walters <walters@verbum.org>, Rob Weir <rweir@ertius.org>
#
# See the file "COPYING" for further information about
# the copyright and warranty status of this work.

set -e

arg0="$0"
srcdir=`dirname "$arg0"`

. ${srcdir}/test-framework

setup_with_somewhat_interesting_explicit_archives

test_class "mv"

setup () {
  tla get -s jane@example.com--2003/hello-world--mainline--1.0 hello-world
  copy_tree hello-world hello-world.good
  cd hello-world
}  

assert_output_changes () {
   tla what-changed -o ../,what-changed 1>../what-changed-output || test $? = 1
}  

verify_cleanup () {
  cd ..
  rm -rf ',what-changed'
  rm -f what-changed-output
  rm -rf hello-world
  mv hello-world.good hello-world
}

cleanup () {
  :
}
  
begin_test "move file to new file"
setup
tla mv hello-world.c hello_world.c
assert_output_changes
egrep '^=>[[:space:]]*hello-world\.c[[:space:]]*hello_world\.c' ../what-changed-output 1>/dev/null
egrep '^=>[[:space:]]*\.arch-ids/hello-world\.c\.id[[:space:]]*\.arch-ids/hello_world\.c\.id' ../what-changed-output 1>/dev/null
verify_cleanup
end_test 

begin_test "move file onto an existing file"
setup
tla mv hello-world.c goodbye-world.c
assert_output_changes
egrep '^D[[:space:]]*goodbye-world\.c' ../what-changed-output 1>/dev/null
egrep '^D[[:space:]]*.arch-ids/goodbye-world.c' ../what-changed-output 1>/dev/null
egrep '^=>[[:space:]]*hello-world\.c[[:space:]]*goodbye-world.c' ../what-changed-output 1>/dev/null
egrep '^=>[[:space:]]*\.arch-ids/hello-world\.c\.id[[:space:]]*\.arch-ids/goodbye-world\.c\.id' ../what-changed-output 1>/dev/null
verify_cleanup
end_test 

begin_test "move file into existing directory"
setup
tla mv hello-world.c old_directory
assert_output_changes
egrep '^=>[[:space:]]*hello-world\.c[[:space:]]*old_directory/hello-world\.c' ../what-changed-output 1>/dev/null
egrep '^=>[[:space:]]*\.arch-ids/hello-world\.c\.id[[:space:]]*old_directory/\.arch-ids/hello-world\.c\.id' ../what-changed-output 1>/dev/null
verify_cleanup
end_test 

begin_test "move file onto an existing symlink to a file"
setup
# echo "a file which will not change" > linked_file
tla mv hello-world.c link_to_goodbye-world.c
assert_output_changes
egrep '^=>[[:space:]]*hello-world\.c[[:space:]]*link_to_goodbye-world\.c' ../what-changed-output 1>/dev/null
egrep '^=>[[:space:]]*\.arch-ids/hello-world\.c\.id[[:space:]]*\.arch-ids/link_to_goodbye-world\.c\.id' ../what-changed-output 1>/dev/null
egrep -v '^M[[:space:]]*linked_to_goodbye-world\.c' ../what-changed-output 1>/dev/null
verify_cleanup
end_test 

begin_test "move file into an existing symlink to a directory"
setup
tla mv hello-world.c link_to_old_directory
assert_output_changes
egrep '^=>[[:space:]]*hello-world\.c[[:space:]]*old_directory/hello-world\.c' ../what-changed-output 1>/dev/null
egrep '^=>[[:space:]]*\.arch-ids/hello-world\.c\.id[[:space:]]*old_directory/\.arch-ids/hello-world\.c\.id' ../what-changed-output 1>/dev/null
verify_cleanup
end_test 

begin_test "move symlink to new file"
setup
tla mv link_to_hello-world.c hello_world.c
assert_output_changes
egrep '^=>[[:space:]]*link_to_hello-world\.c[[:space:]]*hello_world\.c' ../what-changed-output 1>/dev/null
egrep '^=>[[:space:]]*\.arch-ids/link_to_hello-world\.c\.id[[:space:]]*\.arch-ids/hello_world\.c\.id' ../what-changed-output 1>/dev/null
verify_cleanup
end_test 

begin_test "move symlink onto an existing file"
setup
tla mv link_to_hello-world.c goodbye-world.c
assert_output_changes
egrep '^D[[:space:]]*goodbye-world\.c' ../what-changed-output 1>/dev/null
egrep '^D[[:space:]]*.arch-ids/goodbye-world.c' ../what-changed-output 1>/dev/null
egrep '^=>[[:space:]]*link_to_hello-world\.c[[:space:]]*goodbye-world.c' ../what-changed-output 1>/dev/null
egrep '^=>[[:space:]]*\.arch-ids/link_to_hello-world\.c\.id[[:space:]]*\.arch-ids/goodbye-world\.c\.id' ../what-changed-output 1>/dev/null
verify_cleanup
end_test 

begin_test "move symlink into existing directory"
setup
tla mv link_to_hello-world.c old_directory
assert_output_changes
egrep '^=>[[:space:]]*link_to_hello-world\.c[[:space:]]*old_directory/link_to_hello-world\.c' ../what-changed-output 1>/dev/null
egrep '^=>[[:space:]]*\.arch-ids/link_to_hello-world\.c\.id[[:space:]]*old_directory/\.arch-ids/link_to_hello-world\.c\.id' ../what-changed-output 1>/dev/null
verify_cleanup
end_test 

begin_test "move symlink onto an existing symlink to a file"
setup
tla mv link_to_hello-world.c link_to_goodbye-world.c
assert_output_changes
egrep '^=>[[:space:]]*link_to_hello-world\.c[[:space:]]*link_to_goodbye-world\.c' ../what-changed-output 1>/dev/null
egrep '^=>[[:space:]]*\.arch-ids/link_to_hello-world\.c\.id[[:space:]]*\.arch-ids/link_to_goodbye-world\.c\.id' ../what-changed-output 1>/dev/null
egrep -v '^M[[:space:]]*goodbye-world\.c' ../what-changed-output 1>/dev/null
verify_cleanup
end_test 

begin_test "move symlink onto an existing symlink to a directory"
setup
tla mv link_to_hello-world.c link_to_old_directory
assert_output_changes
egrep '^=>[[:space:]]*link_to_hello-world\.c[[:space:]]*old_directory/link_to_hello-world\.c' ../what-changed-output 1>/dev/null
egrep '^=>[[:space:]]*\.arch-ids/link_to_hello-world\.c\.id[[:space:]]*old_directory/\.arch-ids/link_to_hello-world\.c\.id' ../what-changed-output 1>/dev/null
verify_cleanup
end_test 

begin_test "move directory to a previously non-existent name"
setup
tla mv old_directory renamed_directory
assert_output_changes
egrep '^D/[[:space:]]*old_directory/\.arch-ids' ../what-changed-output 1>/dev/null
egrep '^A/[[:space:]]*renamed_directory/\.arch-ids' ../what-changed-output 1>/dev/null
egrep '^/>[[:space:]]*old_directory[[:space:]]*renamed_directory' ../what-changed-output 1>/dev/null
egrep '^=>[[:space:]]*old_directory/.arch-ids/=id[[:space:]]*renamed_directory/.arch-ids/=id' ../what-changed-output 1>/dev/null
verify_cleanup
end_test 

begin_test "move directory into an existing directory"
setup
tla mv old_directory new_directory
assert_output_changes
egrep '^D/[[:space:]]*old_directory/\.arch-ids' ../what-changed-output 1>/dev/null
egrep '^A/[[:space:]]*new_directory/old_directory/\.arch-ids' ../what-changed-output 1>/dev/null
egrep '^/>[[:space:]]*old_directory[[:space:]]*new_directory/old_directory' ../what-changed-output 1>/dev/null
egrep '^=>[[:space:]]*old_directory/.arch-ids/=id[[:space:]]*new_directory/old_directory/.arch-ids/=id' ../what-changed-output 1>/dev/null
verify_cleanup
end_test 

begin_test "move directory into a symlink to an existing directory"
setup
tla mv old_directory link_to_new_directory
assert_output_changes
egrep '^D/[[:space:]]*old_directory/\.arch-ids' ../what-changed-output 1>/dev/null
egrep '^A/[[:space:]]*new_directory/old_directory/\.arch-ids' ../what-changed-output 1>/dev/null
egrep '^/>[[:space:]]*old_directory[[:space:]]*new_directory/old_directory' ../what-changed-output 1>/dev/null
egrep '^=>[[:space:]]*old_directory/.arch-ids/=id[[:space:]]*new_directory/old_directory/.arch-ids/=id' ../what-changed-output 1>/dev/null
verify_cleanup
end_test 

begin_test "move file into a directory tree"
setup
tla mv hello-world.c hierarchy/left/
assert_output_changes
egrep '^=>[[:space:]]*hello-world\.c[[:space:]]*hierarchy/left/hello-world\.c' ../what-changed-output 1>/dev/null
egrep '^=>[[:space:]]*\.arch-ids/hello-world\.c\.id[[:space:]]*hierarchy/left/\.arch-ids/hello-world\.c\.id' ../what-changed-output 1>/dev/null
tla commit -L"moved hello-world.c into the tree" 1>/dev/null
verify_cleanup
end_test 

begin_test "move file out of a directory tree"
setup
tla mv hierarchy/left/hello-world.c .
assert_output_changes
egrep '^=>[[:space:]]*hierarchy/left/hello-world\.c[[:space:]]*hello-world\.c' ../what-changed-output 1>/dev/null
egrep '^=>[[:space:]]*hierarchy/left/\.arch-ids/hello-world\.c\.id[[:space:]]*\.arch-ids/hello-world\.c\.id' ../what-changed-output 1>/dev/null
verify_cleanup
end_test 

begin_test "move file around a directory tree"
setup
cd hierarchy/left/
tla mv hello-world.c ../right/left/
cd ../..
assert_output_changes
egrep '^=>[[:space:]]*hierarchy/left/hello-world\.c[[:space:]]*hierarchy/right/left/hello-world\.c' ../what-changed-output 1>/dev/null
egrep '^=>[[:space:]]*hierarchy/left/\.arch-ids/hello-world\.c\.id[[:space:]]*hierarchy/right/left/\.arch-ids/hello-world\.c\.id' ../what-changed-output 1>/dev/null
verify_cleanup
end_test 

begin_test "move file into a directory tree and rename"
setup
tla mv hierarchy/left/hello-world.c hierarchy/left/renamed.c
assert_output_changes
egrep '^=>[[:space:]]*hierarchy/left/hello-world\.c[[:space:]]*hierarchy/left/renamed\.c' ../what-changed-output 1>/dev/null
egrep '^=>[[:space:]]*hierarchy/left/\.arch-ids/hello-world\.c\.id[[:space:]]*hierarchy/left/\.arch-ids/renamed\.c\.id' ../what-changed-output 1>/dev/null
tla commit -L"moved hello-world.c to renamed.c into the tree" 1> /dev/null
verify_cleanup
end_test 

begin_test "move file out of a directory tree and rename"
setup
tla mv hierarchy/left/renamed.c hello-world.c
assert_output_changes
egrep '^=>[[:space:]]*hierarchy/left/renamed\.c[[:space:]]*hello-world\.c' ../what-changed-output 1>/dev/null
egrep '^=>[[:space:]]*hierarchy/left/\.arch-ids/renamed\.c\.id[[:space:]]*\.arch-ids/hello-world\.c\.id' ../what-changed-output 1>/dev/null
verify_cleanup
end_test 

begin_test "move file around a directory tree and rename"
setup
pwd
cd hierarchy/left/
tla mv renamed.c ../right/left/hello-world.c
cd ../..
assert_output_changes
egrep '^=>[[:space:]]*hierarchy/left/renamed\.c[[:space:]]*hierarchy/right/left/hello-world\.c' ../what-changed-output 1>/dev/null
egrep '^=>[[:space:]]*hierarchy/left/\.arch-ids/renamed\.c\.id[[:space:]]*hierarchy/right/left/\.arch-ids/hello-world\.c\.id' ../what-changed-output 1>/dev/null
verify_cleanup
end_test 

clean_workdir
