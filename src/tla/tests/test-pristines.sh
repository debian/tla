#!/bin/sh
# -*- mode: sh; coding: utf-8 -*-
# Simple commit tests (2)
# Copyright © 2003 Colin Walters <walters@verbum.org>
# Copyright (C) 2004 Robert Collins <robertc@robertcollins.net>
#
# See the file "COPYING" for further information about
# the copyright and warranty status of this work.

set -e

arg0="$0"
srcdir=`dirname "$arg0"`

. ${srcdir}/test-framework

setup_with_trivial_archives

test_class "test pristine trees"

setup () {
  tla get jane@example.com--2003/hello-world--mainline--1.0 hello-world
  cd hello-world
}  

setup_pristine () {
  # purge and rebuild a pristine tree
  rm -rf \{arch\}/++pristine-trees
  tla add-pristine $(tla logs -f -r | head -n 1)
}

check_pristine_is_corrupt () {
  # trigger a pristine inode check
  # fail on ok pristines.
  # note on failed pristines.
  # usage: check_pristine_is_corrupt "failure description"
  (tla changes 2>&1 | grep "corrupt pristine") || test_fail "Did not detect $@ in pristine copy"
  echo "Detected $@" >&2
}


begin_test "modify date of file in pristine"
setup
touch  -t 200001010101.01 \{arch\}/++pristine-trees/unlocked/hello-world/hello-world--mainline/hello-world--mainline--1.0/jane\@example.com--2003/hello-world--mainline--1.0--base-0/README
check_pristine_is_corrupt "modified file date"
end_test 

begin_test "Add file to pristine"
setup
echo 'We follow the GNU coding standards' >  \{arch\}/++pristine-trees/unlocked/hello-world/hello-world--mainline/hello-world--mainline--1.0/jane\@example.com--2003/hello-world--mainline--1.0--base-0/HACKING
tla add-id  \{arch\}/++pristine-trees/unlocked/hello-world/hello-world--mainline/hello-world--mainline--1.0/jane\@example.com--2003/hello-world--mainline--1.0--base-0/HACKING
check_pristine_is_corrupt "new file added"
end_test 

begin_test "Removing file from pristine"
setup
rm \{arch\}/++pristine-trees/unlocked/hello-world/hello-world--mainline/hello-world--mainline--1.0/jane\@example.com--2003/hello-world--mainline--1.0--base-0/hello-world.c
check_pristine_is_corrupt "removed file"
end_test 

#tests to do:
# move a file in a pristine
# perform a limited commit (single file mod)
# perform a limited commit (multiple file modification)
# perform a limited commit (one file commited, other files modified)

clean_workdir

# tag: 2cf97d96-e663-4bab-a46a-1c912ae0ce82
#
