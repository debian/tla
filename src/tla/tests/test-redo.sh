#!/bin/sh
# -*- mode: sh; coding: utf-8 -*-
# Simple undo/redo tests
# Copyright © 2003 Colin Walters <walters@verbum.org>
#
# See the file "COPYING" for further information about
# the copyright and warranty status of this work.

set -e

arg0="$0"
srcdir=`dirname "$arg0"`

. ${srcdir}/test-framework

setup_with_trivial_archives

test_class "redo"

setup_redo () {
  tla get jane@example.com--2003/hello-world--mainline--1.0 hello-world
  cd hello-world
}  

undo_redo () {
  cd ..
  copy_tree hello-world hello-world.good
  cd hello-world
  tla undo
  tla redo
}

verify_cleanup () {
  for f in ,,undo*; do
    if dir_exists "$f"; then
      test_fail "directory $f should not exist!";
    fi
  done
  cd ..
  compare_trees_simple hello-world hello-world.good
  rm -rf hello-world.good
}

begin_test "undoing/redoing single file modification"
setup_redo
sed -e 's/Hello World/Hello, World/' < hello-world.c > hello-world.c.new
mv hello-world.c.new hello-world.c
undo_redo
verify_cleanup
end_test 

begin_test "undoing/redoing new file addition"
setup_redo
echo 'We follow the GNU coding standards' > HACKING
tla add-id HACKING
undo_redo
verify_cleanup
end_test 

begin_test "undoing/redoing removing existing file"
setup_redo
rm README
tla delete-id README
undo_redo
verify_cleanup
end_test 

begin_test "undoing/redoing moving file"
setup_redo
mv hello-world.c hello_world.c
tla move-id hello-world.c hello_world.c
undo_redo
verify_cleanup
end_test 

clean_workdir

# tag: Colin Walters Wed, 17 Sep 2003 13:07:42 -0400 (test-redo.sh)
#
