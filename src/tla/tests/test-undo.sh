#!/bin/sh
# -*- mode: sh; coding: utf-8 -*-
# Simple undo tests
# Copyright © 2003 Colin Walters <walters@verbum.org>
#
# See the file "COPYING" for further information about
# the copyright and warranty status of this work.

set -e

arg0="$0"
srcdir=`dirname "$arg0"`

. ${srcdir}/test-framework

setup_with_trivial_archives

test_class "undo"

setup_undo () {
  tla get jane@example.com--2003/hello-world--mainline--1.0 hello-world
  copy_tree hello-world hello-world.good
  cd hello-world
}  

verify_cleanup () {
  rm -rf ',,undo-1'
  cd ..
  compare_trees_simple hello-world hello-world.good
  rm -rf hello-world.good
}
  

begin_test "undoing single file modification"
setup_undo
sed -e 's/Hello World/Hello, World/' < hello-world.c > hello-world.c.new
mv hello-world.c.new hello-world.c
tla undo
verify_cleanup
end_test 

begin_test "undoing new file addition"
setup_undo
echo 'We follow the GNU coding standards' > HACKING
tla add-id HACKING
tla undo
verify_cleanup
end_test 

begin_test "Removing existing file"
setup_undo
rm README
tla delete-id README
tla undo
verify_cleanup
end_test 

begin_test "Moving file"
setup_undo
mv hello-world.c hello_world.c
tla move-id hello-world.c hello_world.c
tla undo
verify_cleanup
end_test 

begin_test "Undo to earlier patch (abbreviated syntax)"
setup_undo
sed -e 's/Hello World/Hello, World/' < hello-world.c > hello-world.c.new
mv hello-world.c.new hello-world.c
tla undo base-0
verify_cleanup
end_test 

clean_workdir

# tag: Colin Walters Wed, 17 Sep 2003 12:58:35 -0400 (test-undo.sh)
#
