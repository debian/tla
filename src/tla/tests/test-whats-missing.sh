#!/bin/sh
# -*- mode: sh; coding: utf-8 -*-
# Simple whats-missing tests
# Copyright © 2003 Colin Walters <walters@verbum.org>
#
# See the file "COPYING" for further information about
# the copyright and warranty status of this work.

set -e

arg0="$0"
srcdir=`dirname "$arg0"`

. ${srcdir}/test-framework

setup_with_trivial_archives

test_class "whats-missing"

setup () {
  :
}  

cleanup () {
  cd ${WORKDIR}
}  

check_missing_patches () {
  missing_patches_are jane@example.com--2003/hello-world--mainline--1.0 "$@"
}  

begin_test_savectx "get initial archives, commit change to wd 1"
setup
tla get jane@example.com--2003/hello-world--mainline--1.0 hello-world-1
tla get jane@example.com--2003/hello-world--mainline--1.0 hello-world-2
tla get jane@example.com--2003/hello-world--mainline--1.0 hello-world-3
cd hello-world-1
sed -e 's/Hello World/Hello, World/' < hello-world.c > hello-world.c.new
mv hello-world.c.new hello-world.c
tla commit -L 'correctly punctuate'
cd ../hello-world-2
check_missing_patches patch-1
cd ../hello-world-3
check_missing_patches patch-1
cleanup
end_test_savectx

begin_test_savectx "adding new file to wd 2"
setup
cd hello-world-2
tla update
check_missing_patches
echo 'We follow the GNU coding standards' > HACKING
tla add-id HACKING
tla commit -L 'Add HACKING document'
check_missing_patches
cd ../hello-world-1
check_missing_patches patch-2
cd ../hello-world-3
check_missing_patches patch-1 patch-2
cleanup
end_test_savectx

begin_test_savectx "renaming file in wd 2"
setup
cd hello-world-2
check_missing_patches
mv hello-world.c hello_world.c
tla move-id hello-world.c hello_world.c
tla commit -L 'rename hello-world.c to hello_world.c'
cd ../hello-world-1
check_missing_patches patch-2 patch-3
cd ../hello-world-3
check_missing_patches patch-1 patch-2 patch-3
cleanup
end_test_savectx

begin_test_savectx "modifying file in wd 3"
setup
cd hello-world-3
tla update
check_missing_patches
(echo '/* Copyright (C) 2003 Jane Doe */'; echo '/* This file is hereby placed into the public domain. */';
 cat hello_world.c) > hello_world.c.new
mv hello_world.c.new hello_world.c
tla commit -L 'add copyright header'
cd ../hello-world-1
check_missing_patches patch-2 patch-3 patch-4
cd ../hello-world-2
check_missing_patches patch-4
cleanup
end_test_savectx

clean_workdir

# tag: Colin Walters Thu, 18 Sep 2003 19:50:01 -0400 (test-whats-missing.sh)
#
