#!/bin/sh
# -*- mode: sh; coding: utf-8 -*-
# Simple what-changed tests
# Copyright © 2003 Colin Walters <walters@verbum.org>
#
# See the file "COPYING" for further information about
# the copyright and warranty status of this work.

set -e

arg0="$0"
srcdir=`dirname "$arg0"`

. ${srcdir}/test-framework

setup_with_trivial_archives

test_class "what-changed"

setup () {
  tla get jane@example.com--2003/hello-world--mainline--1.0 hello-world
  copy_tree hello-world hello-world.good
  cd hello-world
}  

assert_output_changes () {
   tla what-changed -o ../,what-changed "$@" 1>../what-changed-output || test $? = 1
}  

verify_cleanup () {
  cd ..
  rm -rf ',what-changed'
  rm -f what-changed-output
  rm -rf hello-world
  mv hello-world.good hello-world
}
  
# begin_test "null change output"
# setup
# tla what-changed > ../what-changed-output
# file_is_empty ../what-changed-output
# verify_cleanup
# end_test 

begin_test "null changeset"
setup
tla what-changed -o ../,what-changed
changeset_is_null_changeset ../,what-changed
verify_cleanup
end_test 

begin_test "single file modification"
setup
sed -e 's/Hello world/Hello, world/' < hello-world.c > hello-world.c.new
mv hello-world.c.new hello-world.c
assert_output_changes
file_matches '^M[[:space:]]*hello-world.c' ../what-changed-output
verify_cleanup
end_test 

begin_test "single file modification changeset"
setup
sed -e 's/Hello world/Hello, world/' < hello-world.c > hello-world.c.new
mv hello-world.c.new hello-world.c
assert_output_changes
changeset_patches_file ../,what-changed hello-world.c
verify_cleanup
end_test 

begin_test "new file addition output"
setup
echo 'We follow the GNU coding standards' > HACKING
tla add-id HACKING
assert_output_changes
file_matches '^A[[:space:]]*HACKING' ../what-changed-output
file_matches '^A[[:space:]]*.arch-ids/HACKING.id' ../what-changed-output
verify_cleanup
end_test 

begin_test "new file addition changeset"
setup
echo 'We follow the GNU coding standards' > HACKING
tla add-id HACKING
assert_output_changes
changeset_adds_file ../,what-changed HACKING
changeset_adds_file ../,what-changed .arch-ids/HACKING.id
verify_cleanup
end_test 

begin_test "removing existing file output"
setup
rm README
tla delete-id README
assert_output_changes
file_matches '^D[[:space:]]*README' ../what-changed-output
verify_cleanup
end_test 

begin_test "removing existing file changeset"
setup
rm README
tla delete-id README
assert_output_changes
changeset_deletes_file ../,what-changed README
verify_cleanup
end_test 

begin_test "moving file output"
setup
mv hello-world.c hello_world.c
tla move-id hello-world.c hello_world.c
assert_output_changes
file_matches '^=>[[:space:]]*hello-world.c.*hello_world.c' ../what-changed-output
verify_cleanup
end_test 

begin_test "moving file changeset"
setup
mv hello-world.c hello_world.c
tla move-id hello-world.c hello_world.c
assert_output_changes
changeset_renames_file ../,what-changed hello-world.c hello_world.c
verify_cleanup
end_test 

begin_test "diffing against version without patchlevel"
setup
assert_output_changes jane@example.com--2003/hello-world--mainline--1.0
verify_cleanup
end_test

clean_workdir

# tag: Colin Walters Wed, 17 Sep 2003 14:08:00 -0400 (test-what-changed.sh)
#
