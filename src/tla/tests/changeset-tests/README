
                       Bob's changeset Test Rig


These are (one version of) the scripts we've been using to do
"burn-in" testing on the changeset/do-changeset tools and larch's
mkpatch/dopatch.

They are not yet integrated into the configure/build system of
this package -- you have to run them by hand.

Briefly, these perform end-to-end checks on the changeset tools as
follows:

a) generate an ORIG tree with some random elements

b) copy ORIG to make a MOD tree.  perform a randomly selected
   bunch of edits on this tree (add and delete files, directories, and
   symlinks, change file contents, rename things, etc.).

c) generate a changeset from ORIG to MOD

d) copy ORIG to make TARGET

e) apply the changeset to TARGET

f) compare MOD and TARGET

g) assign ORIG = NEW-ORIG

h) goto step (b)

There are various parameters you can tweak in the source code (e.g.,
changing the relatively likelihood of various edit operations).   As
well, you test changeset/do-changeset or mkpatch/dopatch or
mkpatch/do-changeset or changeset/do-patch.

WARNING: skim the code a bit first.   There is a hard-wired path
in run-tests.sh script -- the script will create files under that
directory AND `rm -rf' files under that directory: so read first and
run with caution.

Thanks to Robert Anderson for writing this suite and spending a number
of long nights on IRC as he sent me failed test cases (often carefully
minimized by hand) and I pushed back fixes to my archive mirror.



                           WHAT YOU CAN DO

*) Play with the parameters and do some long test runs.

*) Look for ways to verify and/or improve test case coverage.

*) Work on making it portable among posix shells (e.g., don't rely
   on $RANDOM)

*) Generalize the tests to test other tagging methods.

*) Derive some new tests from this to test the archive transaction
   commands: imprev, cmtrev, tagrev, and getrev.

*) Generalize the tests to test auto-changelogs

*) Other clever and helpful things :-)


# tag: Tom Lord Sat Jun  7 11:30:20 2003 (changeset-tests/README)
#


