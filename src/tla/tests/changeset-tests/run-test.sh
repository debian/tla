#!/bin/sh
# run-test.sh
#
################################################################
# Copyright (C) 2003 Robert Anderson and Tom Lord
#
# See the file "COPYING" for further information about
# the copyright and warranty status of this work.
#


set -e

# echo '################################'
# echo '#'
# echo '# testing larch versions'
# echo '#'
# echo '################################'
# MKPATCH="larch mkpatch"
# DOPATCH="larch dopatch"

# echo '################################'
# echo '#'
# echo '# testing larch mkpatch with do-changeset'
# echo '#'
# echo '################################'
# MKPATCH="larch mkpatch"
# DOPATCH="do-changeset"

# echo '################################'
# echo '#'
# echo '# testing larch dopatch with changeset'
# echo '#'
# echo '################################'
# MKPATCH="changeset"
# DOPATCH="larch dopatch"


echo '################################'
echo '#'
echo '# testing file-utils versions'
echo '#'
echo '################################'
MKPATCH="tla mkpatch"
DOPATCH="tla dopatch"
ADD="tla add"
DEL="tla delete"
MV="tla move"


#    MKPATCH="valgrind changeset"
#    DOPATCH="valgrind do-changeset"



here=`pwd`

# source tree editing functions
. edit-tree-randomly

# This has to be outside surrounding trees not to have problems with
# init-tree.
testdir=/tmp/tla-changesets-tests
mkdir -p $testdir
cd $testdir

# Test trees are named tree-N, patchdirs are named patch-N
rm -rf tree-* patch-*

# Generate tree-0
mkdir tree-0
cd tree-0
tla init-tree tree-test--main--1.0
tla id-tagging-method explicit
sed -e "/^untagged-source/s/ .*/ unrecognized/" \{arch\}/\=tagging-method > \{arch\}/t
mv \{arch\}/t  \{arch\}/\=tagging-method 

# Initial state: 1 file
filename="./file$RANDOM"
echo "$RANDOM" > $filename
$ADD $filename

# Total number of consecutive trees in the test run
num_trees=50

tree_num=1
while [ $tree_num -le $num_trees ]; do

    cd $testdir

    tree_prev=tree-$(( $tree_num - 1 ))
    tree=tree-$tree_num

    printf "\n\n*\n* ORIG=%s MOD=%s\n*\n" "$tree_prev" "$tree"


    (cd $tree_prev ; tar cf - .) | (mkdir $tree ; cd $tree ; tar xf -) > /dev/null

    cd $tree

    #
    # Do edits (test the empty changeset first)
    #
    if [ $tree_num -ne 1 ] ; then
      edit_tree_randomly
    fi

    cd ..

    (cd $tree_prev ; tar cvf - . 2>/dev/null) | (mkdir $tree-TARGET ; cd $tree-TARGET ; tar xf -) > /dev/null

    printf "\n** mkpatch\n\n"
    $MKPATCH $tree_prev $tree patch-$tree_num
    printf "\n** dopatch\n\n"
    $DOPATCH patch-$tree_num $tree-TARGET

    printf "\n** compare\n\n"
    $here/dir_compare.sh $tree $tree-TARGET

    tree_num=$(( $tree_num + 1 ))
done


# tag: Robert Anderson Sat May 17 10:36:00 PDT 2003 (run-test.sh)

