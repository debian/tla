#!/bin/sh
# dir_compare.sh
#
################################################################
# Copyright (C) 2003 Robert Anderson and Tom Lord
#
# See the file "COPYING" for further information about
# the copyright and warranty status of this work.
#


dir1="$1"
dir2="$2"

# diff -r on files and directories
diff -r --exclude ,,\* --exclude ++\* --exclude link\* "$dir1" "$dir2"
filesdirs=$?

# make symlink target index
rm -f ,,link-index1 ,,link-index2
cd "$dir1"
find . -name "++*" -prune -o \
    -type l -printf "'%p' -> '%l'\n" | sort > ../,,link-index1
cd ../"$dir2"
find . -name "++*" -prune -o \
    -type l -printf "'%p' -> '%l'\n" | sort > ../,,link-index2
cd ..

set +e
diff ,,link-index1 ,,link-index2
links=$?
set -e

if [ $filesdirs -ne 0 ] || [ $links -ne 0 ]; then
    echo "$1 != $2"
    exit 1
else
    echo "dir cmp: ok."
    exit 0
fi

#set +x

# make metadata index
# cd tree-MOD
# find . -name \* -printf "%h/%f %m\n" > ../,,metadata
# sort ../,,metadata > ++metadata
# rm  ../,,metadata
# cd ../tree-TARGET
# find . -name \* -printf "%h/%f %m\n" > ../,,metadata
# sort ../,,metadata > ++metadata
# rm  ../,,metadata
# cd ..

# tag: Robert Anderson Sun May 18 18:13:10 PDT 2003 (dir_compare.sh)

