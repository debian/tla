#!/bin/sh
# -*- mode: sh; coding: utf-8 -*-
# Simple file-diffs tests
# Copyright © 2003 Colin Walters <walters@verbum.org>
#
# See the file "COPYING" for further information about
# the copyright and warranty status of this work.

set -e

arg0="$0"
srcdir=`dirname "$arg0"`

. ${srcdir}/test-framework

setup_with_trivial_archives

test_class "file-diffs"

setup () {
  tla get jane@example.com--2003/hello-world--mainline--1.0 hello-world
  cd hello-world
}  

cleanup () {
  :
}

do_file_diffs () {
  tla file-diffs "$@" || test "$?" = 1
}

begin_test "diffs for unmodified file"
setup
do_file_diffs hello-world.c > ../hello-world.patch
file_is_empty ../hello-world.patch
cleanup
end_test

begin_test "diffs for single file modification"
setup
sed -e 's/Hello world/Hello, world/' < hello-world.c > hello-world.c.new
mv hello-world.c.new hello-world.c
do_file_diffs hello-world.c > ../hello-world.patch
file_matches '^-.*Hello world' ../hello-world.patch
file_matches '^\+.*Hello, world' ../hello-world.patch
cleanup
end_test 

begin_test 'diff | patch -R => identity'
setup
cd ..
copy_tree hello-world hello-world.good
cd hello-world
sed -e 's/Hello world/Hello, world/' < hello-world.c > hello-world.c.new
mv hello-world.c.new hello-world.c
do_file_diffs hello-world.c | patch -p1 -R
cd ..
assert_trees_equal hello-world hello-world.good
cleanup
end_test 

clean_workdir
