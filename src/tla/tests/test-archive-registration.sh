#!/bin/sh
# -*- mode: sh; coding: utf-8 -*-
# Test archive registration
# Copyright © 2003 Colin Walters <walters@verbum.org>
#
# See the file "COPYING" for further information about
# the copyright and warranty status of this work.

set -e

arg0="$0"
srcdir=`dirname "$arg0"`

. ${srcdir}/test-framework

setup_workdir

test_class "Archive registration"

begin_test "Archive registration 1"
tla register-archive foo@bar.com--2003 http://foobar.net/arch
test $(tla whereis-archive foo@bar.com--2003) = "http://foobar.net/arch"
end_test

begin_test "Archive registration 2"
tla register-archive blah@baz.com--moo /var/www/baz/archives/spoon
test $(tla whereis-archive blah@baz.com--moo) = /var/www/baz/archives/spoon
test $(tla whereis-archive foo@bar.com--2003) = "http://foobar.net/arch"
end_test

begin_test "Archive re-registration"
tla register-archive -f foo@bar.com--2003 http://barfoo.com/ARCH
test $(tla whereis-archive foo@bar.com--2003) = http://barfoo.com/ARCH
end_test 

begin_test "Archive unregistration"
tla register-archive -d blah@baz.com--moo

if tla whereis-archive blah@baz.com--moo 2>/dev/null; then
  exit 1
fi    
end_test

clean_workdir

# tag: Colin Walters Tue, 16 Sep 2003 20:35:26 -0400 (test-register-archive.sh)
#
