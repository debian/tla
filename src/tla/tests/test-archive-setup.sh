#!/bin/sh
# -*- mode: sh; coding: utf-8 -*-
# Test archive setup
# Copyright © 2003 Colin Walters <walters@verbum.org>
#
# See the file "COPYING" for further information about
# the copyright and warranty status of this work.

set -e

arg0="$0"
srcdir=`dirname "$arg0"`

. ${srcdir}/test-framework

setup_with_empty_tla_archives

test_class "Archive setup"

begin_test "archive-setup"
tla archive-setup -A jane@example.com--2003 moo--cow--3
test -d ${ARCHIVEDIR}/jane-archive/moo/moo--cow/moo--cow--3
end_test 

begin_test "archive-setup"
tla archive-setup -A jane@example.com--2003 moo--cow--3.2
test -d ${ARCHIVEDIR}/jane-archive/moo/moo--cow/moo--cow--3.2
end_test 

begin_test "archive-setup"
tla archive-setup -A jane@example.com--2003 moo--blah--1.1
test -d ${ARCHIVEDIR}/jane-archive/moo/moo--blah/moo--blah--1.1
end_test 

begin_test "archive-setup"
tla archive-setup -A jane@example.com--2003 crack--whee--7.1
test -d ${ARCHIVEDIR}/jane-archive/crack/crack--whee/crack--whee--7.1
end_test 

clean_workdir

# tag: Colin Walters Tue, 16 Sep 2003 23:26:11 -0400 (test-archive-setup.sh)
#
