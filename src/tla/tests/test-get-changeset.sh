#!/bin/sh
# -*- mode: sh; coding: utf-8 -*-
# Simple update tests
# Written by Aaron Bentley <aaron.bentley@utoronto.ca>
# Copyright © 2004 Canonical Inc
#
# This file is licensed under the General Public License v2, or at your
# preference, any later version.

set -e

arg0="$0"
srcdir=`dirname "$arg0"`

. ${srcdir}/test-framework

test_class "get-changeset"
setup_with_patch_1 ()
{
  setup_with_trivial_archives
  tla get jane@example.com--2003/hello-world--mainline--1.0--base-0 hw-2
  tla my-default-archive jane@example.com--2003
  tla commit -d hw-2 -s "Need a changeset"
}
begin_test_savectx "get-changeset in a working-tree"

setup_with_patch_1
cd hw-2
tla get-changeset jane@example.com--2003/hello-world--mainline--1.0--patch-1

cd ..
end_test_savectx

begin_test_savectx "get-changeset outside of a working-tree"
setup_with_patch_1

tla get-changeset hello-world--mainline--1.0--patch-1
cd hw-2
tla update
cd ..
end_test_savectx

begin_test_savectx "get-changeset with no default archive"
setup_with_patch_1
tla my-default-archive -d
tla get-changeset jane@example.com--2003/hello-world--mainline--1.0--patch-1
cd hw-2
tla update
cd ..
end_test_savectx

# tag: b6682352-b119-4f7b-ad49-ca21aef11c56
