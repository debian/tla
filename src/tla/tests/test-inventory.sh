#!/bin/sh
# -*- mode: sh; coding: utf-8 -*-
# Test inventory/tagging method regexps
# Copyright © 2003 Colin Walters <walters@verbum.org>
#
# See the file "COPYING" for further information about
# the copyright and warranty status of this work.

set -e

arg0="$0"
srcdir=`dirname "$arg0"`

. ${srcdir}/test-framework

setup_with_trivial_archives

test_class "inventory"

inventory_has () {
  class=$1
  type=$2
  fname=$3
  file_matches '^'${class}'[[:space:]]+'${type}'[[:space:]]*'${fname} ../,inventory
}

begin_test_savectx "simple source inventory"
tla get jane@example.com--2003/hello-world--mainline--1.0 hello-world
cd hello-world
tla inventory > ../,inventory
inventory_has 'S' '' 'README'
inventory_has 'S' '' 'hello-world\.c'
end_test_savectx 

begin_test_savectx "simple junk inventory"
touch ',,test-junk'
touch ',other-junk'
tla inventory --junk > ../,inventory
file_matches ',,test-junk' ../,inventory
file_matches ',other-junk' ../,inventory
end_test_savectx

begin_test_savectx "simple junk and source inventory"
tla inventory --kind --source --junk > ../,inventory
inventory_has 'S' 'r' 'README'
inventory_has 'S' 'r' 'hello-world\.c'
inventory_has 'J' 'r' ',,test-junk'
inventory_has 'J' 'r' ',other-junk'
end_test_savectx

begin_test_savectx "default untagged source is precious"
touch blargh
test "$(tla inventory -p)" = "blargh"
end_test_savectx

begin_test_savectx "change junk regexp to match \"README\""
tla id-tagging-defaults | egrep -v '^junk' > '{arch}/=tagging-method'
echo 'junk ^(,.*)|(README)$' >> '{arch}/=tagging-method'
tla inventory --kind --source --junk > ../,inventory
inventory_has 'J' 'r' ',,test-junk'
inventory_has 'J' 'r' 'README'
inventory_has 'S' 'r' 'hello-world\.c'
end_test_savectx

begin_test_savectx "two junk regexps"
tla id-tagging-defaults > '{arch}/=tagging-method'
echo 'junk ^README$' >> '{arch}/=tagging-method'
tla inventory --kind --source --junk > ../,inventory
inventory_has 'J' 'r' ',,test-junk'
inventory_has 'J' 'r' 'README'
inventory_has 'S' 'r' 'hello-world\.c'
end_test_savectx

clean_workdir

# tag: Colin Walters Sun, 12 Oct 2003 12:43:30 -0400 (test-inventory.sh)
#
