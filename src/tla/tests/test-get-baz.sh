#!/bin/sh
# -*- mode: sh; coding: utf-8 -*-
# Test getting working trees
# Copyright © 2003 Colin Walters <walters@verbum.org>
#
# See the file "COPYING" for further information about
# the copyright and warranty status of this work.

set -e

arg0="$0"
srcdir=`dirname "$arg0"`

. ${srcdir}/test-framework

setup_with_trivial_archives

test_class "baz get"

begin_test "1-arg get"
tla get jane@example.com--2003/hello-world--mainline--1.0
test -d hello-world--mainline--1.0--base-0
test -f hello-world--mainline--1.0--base-0/README
test -f hello-world--mainline--1.0--base-0/hello-world.c
end_test 

begin_test "2-arg get"
tla get jane@example.com--2003/hello-world--mainline--1.0 hello-world
test -d hello-world
test -f hello-world/README
test -f hello-world/hello-world.c
end_test 

clean_workdir
