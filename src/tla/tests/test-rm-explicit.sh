#!/bin/sh
# -*- mode: sh; coding: utf-8 -*-
# Tests for tla rm.
# Copyright 2004 Stefanie Tellex <stefie10@alum.mit.edu>
#
# See the file "COPYING" for further information about
# the copyright and warranty status of this work.

set -e

arg0="$0"
srcdir=`dirname "$arg0"`

. ${srcdir}/test-framework

setup_with_somewhat_interesting_explicit_archives

test_class "rm"

setup () {
  tla get jane@example.com--2003/hello-world--mainline--1.0 hello-world
  cd hello-world
}  


begin_test "Removing existing file"
setup
tla rm hello-world.c
test ! -e hello-world.c
tla commit -s 'No README is necessary'
archive_has_revision_with_summary jane@example.com--2003 hello-world--mainline--1.0--patch-1 'No README is necessary'
end_test 


begin_test "Removing existing symlink"
setup
tla rm link_to_hello-world.c
test ! -e link_to_hello-world.c
tla commit -s 'No README is necessary'
archive_has_revision_with_summary jane@example.com--2003 hello-world--mainline--1.0--patch-1 'No README is necessary'
end_test 


begin_test "Doesn't remove directory"
setup
echo ${builddir}/../tla/tla rm old_directory ; true
! tla rm old_directory 
test -d old_directory
echo after test
end_test 


clean_workdir

# tag:
#
