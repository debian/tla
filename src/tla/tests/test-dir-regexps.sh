#!/bin/sh
#
# Per-directory inventory regexp tests.

set -e

arg0="$0"
srcdir=`dirname "$arg0"`

. ${srcdir}/test-framework

setup_with_trivial_archives

test_class "per-directory regexps"


begin_test "try all categories"
mkdir foo
cd foo
tla init-tree --nested foo@example.org--2003/hello-world--devo--1.3
tla id-tagging-method names
touch @rapca
touch attrapca
test "$(tla inventory -u)" = "@rapca"
test "$(tla inventory -s)" = "attrapca"
echo "junk ^@.*$" > .arch-inventory
tla add-id .arch-inventory
test "$(tla inventory -j)" = "@rapca"
test "$(tla inventory -s)" = "attrapca"
echo "backup ^@.*$" > .arch-inventory
test "$(tla inventory -b)" = "@rapca"
test "$(tla inventory -s)" = "attrapca"
echo "precious ^@.*$" > .arch-inventory
test "$(tla inventory -p)" = "@rapca"
test "$(tla inventory -s)" = "attrapca"
echo "unrecognized ^attrapca$" > .arch-inventory
test "$(echo $(tla inventory -u))" = "@rapca attrapca"
echo "source ^@.*$" > .arch-inventory
test "$(echo $(tla inventory -s --all | head -4))" = \
     ".arch-ids/.arch-inventory.id .arch-inventory @rapca attrapca"
end_test

begin_test "locality property"
mkdir foo
cd foo
tla init-tree --nested foo@example.org--2003/hello-world--devo--1.3
tla id-tagging-method names
mkdir 1
mkdir 1/@rapca
touch 1/@rapca/@rapca
touch @rapca
echo "source ^@.*$" > 1/.arch-inventory
tla add-id 1/.arch-inventory
test "$(echo $(tla inventory -s -d))" = "1 1/@rapca"
test "$(echo $(tla inventory -u))" = "1/@rapca/@rapca @rapca"
end_test

begin_test "commit with augmented source category"
tla get jane@example.com--2003/hello-world--mainline--1.0 hello-world
cd hello-world
echo "source ^@.*$" > .arch-inventory
tla add-id .arch-inventory
touch @rapca
tla add-id @rapca
tla commit -s 'add @rapca and locally augmented source category'
cd ..
tla get jane@example.com--2003/hello-world--mainline--1.0 hello-world.new
compare_trees_simple hello-world hello-world.new
end_test

begin_test "override a category"
mkdir bar
cd bar
tla init-tree --nested bar@example.org--2003/hello-world--devo--1.3
cat > {arch}/=tagging-method << EOF
names
exclude ^(.arch-ids|\{arch\}|\.arch-inventory)$
junk ^(,.*)$
precious ^(\+.*|\.gdbinit|\.#ckpts-lock|=build\.*|=install\.*|CVS|CVS\.adm|RCS|RCSLOG|SCCS|TAGS)$
backup ^.*(~|\.~[0-9]+~|\.bak|\.orig|\.rej|\.original|\.modified|\.reject)$
unrecognized ^(.*\.(o|a|so|core|so(\.[[:digit:]]+)*)|core)$
source ^[_=a-zA-Z0-9].*$
EOF

touch core
echo "source ^core$" > .arch-inventory
tla add-id .arch-inventory
test "$(tla inventory -u)" = ''
end_test

clean_workdir

# tag: rfarine e8bb544c-10ed-4094-8d65-db8b8004b374
