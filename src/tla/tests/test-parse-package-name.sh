#!/bin/sh
# -*- mode: sh; coding: utf-8 -*-
# Test parse-package-name
# Copyright © 2003 Colin Walters <walters@verbum.org>
#
# See the file "COPYING" for further information about
# the copyright and warranty status of this work.

set -e

arg0="$0"
srcdir=`dirname "$arg0"`

. ${srcdir}/test-framework

setup_workdir

test_class "parse-package-name"

begin_test "foo--bar--0--patch-1"
name="foo--bar--0--patch-1"
category=$(tla parse-package-name -c "${name}" $revision)
branch=$(tla parse-package-name -b "${name}" $revision)
version=$(tla parse-package-name -v "${name}" $revision)
patchlvl=$(tla parse-package-name -l "${name}" $revision)
test "${category}" = 'foo'
test "${branch}" = 'bar'
test "${version}" = '0'
test "${patchlvl}" = 'patch-1'
end_test

begin_test "foo-bar--baz--0--patch-1"
name="foo-bar--baz--0--patch-1"
category=$(tla parse-package-name -c "${name}" $revision)
branch=$(tla parse-package-name -b "${name}" $revision)
version=$(tla parse-package-name -v "${name}" $revision)
patchlvl=$(tla parse-package-name -l "${name}" $revision)
test "${category}" = 'foo-bar'
test "${branch}" = 'baz'
test "${version}" = '0'
test "${patchlvl}" = 'patch-1'
end_test

begin_test "foo-bar--baz-whee--0--patch-1"
name="foo-bar--baz-whee--0--patch-1"
category=$(tla parse-package-name -c "${name}" $revision)
branch=$(tla parse-package-name -b "${name}" $revision)
version=$(tla parse-package-name -v "${name}" $revision)
patchlvl=$(tla parse-package-name -l "${name}" $revision)
test "${category}" = 'foo-bar'
test "${branch}" = 'baz-whee'
test "${version}" = '0'
test "${patchlvl}" = 'patch-1'
end_test

begin_test "foo-bar--baz-whee--3.1--patch-1"
name="foo-bar--baz-whee--3.1--patch-1"
category=$(tla parse-package-name -c "${name}" $revision)
branch=$(tla parse-package-name -b "${name}" $revision)
version=$(tla parse-package-name -v "${name}" $revision)
patchlvl=$(tla parse-package-name -l "${name}" $revision)
test "${category}" = 'foo-bar'
test "${branch}" = 'baz-whee'
test "${version}" = '3.1'
test "${patchlvl}" = 'patch-1'
end_test

begin_test "foo-bar--baz-whee--3.1.3--patch-1"
name="foo-bar--baz-whee--3.1.3--patch-1"
category=$(tla parse-package-name -c "${name}" $revision)
branch=$(tla parse-package-name -b "${name}" $revision)
version=$(tla parse-package-name -v "${name}" $revision)
patchlvl=$(tla parse-package-name -l "${name}" $revision)
test "${category}" = 'foo-bar'
test "${branch}" = 'baz-whee'
test "${version}" = '3.1.3'
test "${patchlvl}" = 'patch-1'
end_test

begin_test "foo-bar--baz-whee--3.1.3--base-0"
name="foo-bar--baz-whee--3.1.3--base-0"
category=$(tla parse-package-name -c "${name}" $revision)
branch=$(tla parse-package-name -b "${name}" $revision)
version=$(tla parse-package-name -v "${name}" $revision)
patchlvl=$(tla parse-package-name -l "${name}" $revision)
test "${category}" = 'foo-bar'
test "${branch}" = 'baz-whee'
test "${version}" = '3.1.3'
test "${patchlvl}" = 'base-0'
end_test

begin_test "b33f--baz-whee-foo-bar--3.1.3--base-0"
name="b33f--baz-whee-foo-bar--3.1.3--base-0"
category=$(tla parse-package-name -c "${name}" $revision)
branch=$(tla parse-package-name -b "${name}" $revision)
version=$(tla parse-package-name -v "${name}" $revision)
patchlvl=$(tla parse-package-name -l "${name}" $revision)
test "${category}" = 'b33f'
test "${branch}" = 'baz-whee-foo-bar'
test "${version}" = '3.1.3'
test "${patchlvl}" = 'base-0'
end_test

clean_workdir

# tag: Colin Walters Tue, 16 Sep 2003 22:16:32 -0400 (test-parse-package-name.sh)
#
