#!/bin/sh
# -*- mode: sh; coding: utf-8 -*-
# Star merge tests
# Copyright © 2004 Aaron Bentley
# Copyright © 2003 Colin Walters
#
# See the file "COPYING" for further information about
# the copyright and warranty status of this work.

set -e

arg0="$0"
srcdir=`dirname "$arg0"`

. ${srcdir}/test-framework

setup_with_trivial_archives

test_class "star-merge"

begin_test "Merging when MINE's latest REFERENCE revision is older"
tla archive-setup jane@example.com--2003/hello-world--mainline--1.1
tla tag -A jane@example.com--2003 hello-world--mainline--1.0 hello-world--mainline--1.1
tla get jane@example.com--2003/hello-world--mainline--1.0 hello-world
tla commit -d hello-world -s "meaningless commit"
tla  tag -S jane@example.com--2003/hello-world--mainline--1.0 jane@example.com--2003/hello-world--mainline--1.2
tla get jane@example.com--2003/hello-world--mainline--1.1 hello-world11
tla star-merge -d hello-world11 --reference jane@example.com--2003/hello-world--mainline--1.0 jane@example.com--2003/hello-world--mainline--1.2
end_test 

clean_workdir

# tag: 6f36ba8a-51ff-4b18-a729-849b1e861781
