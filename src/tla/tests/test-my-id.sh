#!/bin/sh
# -*- mode: sh; coding: utf-8 -*-
# Test my-id
# Copyright © 2003 Colin Walters <walters@verbum.org>
#
# See the file "COPYING" for further information about
# the copyright and warranty status of this work.

set -e

arg0="$0"
srcdir=`dirname "$arg0"`

. ${srcdir}/test-framework

setup_workdir

test_class "my-id"

begin_test "Setting my-id: Jane Doe <jane@hacker.gnu.org>"
tla my-id 'Jane Doe <jane@hacker.gnu.org>'
test "$(tla my-id)" = 'Jane Doe <jane@hacker.gnu.org>'
end_test

begin_test "Getting my-id uid component"
test "$(tla my-id -u)" = 'jane@hacker.gnu.org'
end_test

begin_test "Setting my-id: Jane Doe <jane+patches@hacker.gnu.org>"
tla my-id 'Jane Doe <jane+patches@hacker.gnu.org>'
test "$(tla my-id)" = 'Jane Doe <jane+patches@hacker.gnu.org>'
end_test


clean_workdir

# tag: Colin Walters Tue, 16 Sep 2003 22:15:31 -0400 (test-my-id.sh)
#
