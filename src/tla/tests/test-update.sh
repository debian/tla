#!/bin/sh
# -*- mode: sh; coding: utf-8 -*-
# Simple update tests
# Written by James Blackwell <jblack@inframix.com>
# Copyright � 2004 Canonical Inc
#
# This file is licensed under the General Public License v2, or at your
# preference, any later version.

set -e

arg0="$0"
srcdir=`dirname "$arg0"`

. ${srcdir}/test-framework

test_class "update"
  
setup_hello_world ()
{
  setup_with_trivial_archives
  tla get jane@example.com--2003/hello-world--mainline--1.0 hello-world-1

  cd hello-world-1

  sed -e 's/Hello World/Hello, World/' < hello-world.c > hello-world.c.new
  mv hello-world.c.new hello-world.c
  tla commit -L 'correctly punctuate'

  cd ..
}

begin_test_savectx "solitary update"
setup_hello_world
tla get jane@example.com--2003/hello-world--mainline--1.0--base-0 hw-2
cd hw-2
tla update
cd ..
end_test_savectx


begin_test_savectx "update with -d dirname"
setup_hello_world
tla get jane@example.com--2003/hello-world--mainline--1.0--base-0 hw-3

tla update -d hw-3 
end_test_savectx "update with -d dirname"



# tag: DO_NOT_CHANGE James Blackwell Mon Aug 16 07:02:51 EDT 2004
