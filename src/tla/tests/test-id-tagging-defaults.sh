#!/bin/sh
# -*- mode: sh; coding: utf-8 -*-
# Test id-tagging-defaults 
# Copyright © 2003 Colin Walters <walters@verbum.org>
#
# See the file "COPYING" for further information about
# the copyright and warranty status of this work.

set -e

arg0="$0"
srcdir=`dirname "$arg0"`

. ${srcdir}/test-framework

setup_workdir

test_class "id-tagging-defaults"

begin_test "id-tagging-defaults"
tla id-tagging-defaults > id-tagging-defaults
file_matches '^junk ' id-tagging-defaults
file_matches '^backup ' id-tagging-defaults
file_matches '^precious ' id-tagging-defaults
file_matches '^source ' id-tagging-defaults
end_test

clean_workdir

# tag: Colin Walters Thu, 18 Sep 2003 15:05:12 -0400 (test-tagging-defaults.sh)
#
