#!/bin/sh
# -*- mode: sh; coding: utf-8 -*-
# Test import
# Copyright © 2003 Colin Walters <walters@verbum.org>
#
# See the file "COPYING" for further information about
# the copyright and warranty status of this work.

set -e

arg0="$0"
srcdir=`dirname "$arg0"`

. ${srcdir}/test-framework

setup_with_setup_tla_archives

test_class "import"

begin_test "Simple import"
make_hello_world hello-world--mainline--1.0
cd hello-world--mainline--1.0
tla init-tree --nested jane@example.com--2003/hello-world--mainline--1.0
tla id-tagging-method explicit
tla add-id hello-world.c README
tla import -L 'initial import'
test "$(tla categories -A jane@example.com--2003)" = "$(echo -e cat\\ncow\\nhello-world)"
test "$(tla branches jane@example.com--2003/hello-world)" = "hello-world--mainline"
test "$(tla versions jane@example.com--2003/hello-world--mainline)" = "hello-world--mainline--1.0"
test -d $(tla whereis-archive jane@example.com--2003)/hello-world/hello-world--mainline/hello-world--mainline--1.0/base-0
end_test 

clean_workdir

# tag: Colin Walters Wed, 17 Sep 2003 01:39:15 -0400 (test-import.sh)
#
