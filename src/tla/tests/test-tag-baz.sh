#!/bin/sh
# -*- mode: sh; coding: utf-8 -*-
# Simple tagging tests
# Copyright © 2003 Colin Walters <walters@verbum.org>
#
# See the file "COPYING" for further information about
# the copyright and warranty status of this work.

set -e

arg0="$0"
srcdir=`dirname "$arg0"`

. ${srcdir}/test-framework

setup_with_trivial_archives

test_class "tag baz archives"

begin_test "creating tag from new version"
tla archive-setup jane@example.com--2003/hello-world--mainline--1.1
tla tag jane@example.com--2003/hello-world--mainline--1.0 jane@example.com--2003/hello-world--mainline--1.1
tla get jane@example.com--2003/hello-world--mainline--1.1 hello-world
test -f hello-world/hello-world.c
test -f hello-world/README
end_test 

begin_test "creating tag from new branch"
tla archive-setup jane@example.com--2003/hello-world--foobranch--3
tla tag jane@example.com--2003/hello-world--mainline--1.0 jane@example.com--2003/hello-world--foobranch--3
tla get jane@example.com--2003/hello-world--foobranch--3 foobranch
test -f foobranch/hello-world.c
test -f foobranch/README
end_test 

begin_test "creating tag from new category"
tla archive-setup jane@example.com--2003/hi-world--mainline--1.3
tla tag jane@example.com--2003/hello-world--mainline--1.0 jane@example.com--2003/hi-world--mainline--1.3
tla get jane@example.com--2003/hi-world--mainline--1.3 hi-world
test -f hi-world/hello-world.c
test -f hi-world/README
end_test 

begin_test "creating tag from different archive"
tla tag -S jane@example.com--2003/hello-world--mainline--1.0 foo@example.org--2003/hello-world--devo--1.3
tla get foo@example.org--2003/hello-world--devo--1.3 foo-hello-world
test -f foo-hello-world/hello-world.c
test -f foo-hello-world/README
end_test 

clean_workdir
