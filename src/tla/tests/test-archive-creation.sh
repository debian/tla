#!/bin/sh
# -*- mode: sh; coding: utf-8 -*-
# Test archive creation
# Copyright © 2003 Colin Walters <walters@verbum.org>
#
# See the file "COPYING" for further information about
# the copyright and warranty status of this work.

set -e

arg0="$0"
srcdir=`dirname "$arg0"`

. ${srcdir}/test-framework

initial_setup

test_class "Simple Archive creation"

begin_test "Archive creation 1"
tla make-archive --baz jane@example.com--2003 $(pwd)/test-archive
test "$(tla whereis-archive jane@example.com--2003)" = $(pwd)/test-archive
test -d test-archive/=meta-info
test "Bazaar archive format 1 0" = "$(cat test-archive/.archive-version )"
end_test 

begin_test "Archive creation 2 (with listing)"
tla make-archive --baz --listing jane@example.com--2003b $(pwd)/test-archive2
test "$(tla whereis-archive jane@example.com--2003b)" = $(pwd)/test-archive2
test -d test-archive2/=meta-info
test -f test-archive2/.listing
test "Bazaar archive format 1 0" = "$(cat test-archive2/.archive-version )"
end_test 

begin_test "TLA Archive creation 1"
tla make-archive --tla jane@example.com--2003c $(pwd)/test-archive3
test "$(tla whereis-archive jane@example.com--2003c)" = $(pwd)/test-archive3
test -d test-archive3/=meta-info
test "Hackerlab arch archive directory, format version 2." = "$(cat test-archive3/.archive-version )"
end_test 

begin_test "TLA Archive creation 2 (with listing)"
tla make-archive --tla --listing jane@example.com--2003d $(pwd)/test-archive4
test "$(tla whereis-archive jane@example.com--2003d)" = $(pwd)/test-archive4
test -d test-archive4/=meta-info
test -f test-archive4/.listing
test "Hackerlab arch archive directory, format version 2." = "$(cat test-archive4/.archive-version )"
end_test 

begin_test "Archive creation 3 (email contains an underscore before @)"
tla make-archive jane_porter@example.com--2004 $(pwd)/test-archive
test "$(tla whereis-archive jane_porter@example.com--2004)" = $(pwd)/test-archive
test -d test-archive/=meta-info
end_test 


clean_workdir

# tag: Colin Walters Tue, 16 Sep 2003 22:31:21 -0400 (test-archive-creation.sh)
#
