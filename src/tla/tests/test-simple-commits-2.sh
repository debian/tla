#!/bin/sh
# -*- mode: sh; coding: utf-8 -*-
# Simple commit tests (2)
# Copyright © 2003 Colin Walters <walters@verbum.org>
#
# See the file "COPYING" for further information about
# the copyright and warranty status of this work.

set -e

arg0="$0"
srcdir=`dirname "$arg0"`

. ${srcdir}/test-framework

setup_with_trivial_archives

test_class "simple commits (pass 2)"

setup () {
  tla get jane@example.com--2003/hello-world--mainline--1.0 hello-world
  cd hello-world
}  

begin_test "single file modification"
setup
sed -e 's/Hello World/Hello, World/' < hello-world.c > hello-world.c.new
mv hello-world.c.new hello-world.c
tla commit -s 'correctly punctuate'
archive_has_revision_with_summary jane@example.com--2003 hello-world--mainline--1.0--patch-1 'correctly punctuate'
end_test 

begin_test "Adding new file"
setup
echo 'We follow the GNU coding standards' > HACKING
tla add-id HACKING
tla commit -s 'Add HACKING document'
archive_has_revision_with_summary jane@example.com--2003 hello-world--mainline--1.0--patch-2 'Add HACKING document'
end_test 

begin_test "Removing existing file"
setup
rm README
tla delete-id README
tla commit -s 'No README is necessary'
archive_has_revision_with_summary jane@example.com--2003 hello-world--mainline--1.0--patch-3 'No README is necessary'
end_test 

begin_test "Moving file"
setup
mv hello-world.c hello_world.c
tla move-id hello-world.c hello_world.c
tla commit -s 'Use UNIX separator'
archive_has_revision_with_summary jane@example.com--2003 hello-world--mainline--1.0--patch-4 'Use UNIX separator'
end_test 

begin_test "limited commit (single file modification)"
setup
sed -e 's/Hello, World/Hello World!!!/' < hello_world.c > hello_world.c.new
mv hello_world.c.new hello_world.c
tla commit -s 'add some emphasis' -- hello_world.c
archive_has_revision_with_summary jane@example.com--2003 hello-world--mainline--1.0--patch-5 'add some emphasis'
end_test 

begin_test "limited commit (multiple file modifications)"
setup
echo '/* c-file-style: gnu */' >> hello_world.c
tla commit -s 'punctuation in HACKING, add style to hello_world' -- HACKING hello_world.c
archive_has_revision_with_summary jane@example.com--2003 hello-world--mainline--1.0--patch-6 'punctuation in HACKING, add style to hello_world'
end_test 

begin_test "limited commit (one file when multiple file modifications)"
setup
echo 'arrgh' >> hello_world.c
echo 'gaarh' >> HACKING
tla commit -s 'scream a little' -- hello_world.c
archive_has_revision_with_summary jane@example.com--2003 hello-world--mainline--1.0--patch-7 'scream a little'
tla changes -q && test_fail "file HACKING not detected as modified after partial commit"
end_test

clean_workdir

# tag: Colin Walters Thu,  2 Oct 2003 17:39:21 -0400 (test-simple-commits-2.sh)
#
