#!/bin/sh
# -*- mode: sh; coding: utf-8 -*-
# Jane and foo hack on hello-world together
# Copyright © 2003 Colin Walters <walters@verbum.org>
#
# See the file "COPYING" for further information about
# the copyright and warranty status of this work.

set -e

arg0="$0"
srcdir=`dirname "$arg0"`

. ${srcdir}/test-framework

setup_with_trivial_tla_archives

setup () {
  :
}  

cleanup () {
  cd ${WORKDIR}
}

test_class "Jane and foo hack on hello-world"

begin_test_savectx "Jane checks out her tree"
setup
mkdir jane
cd jane
tla get jane@example.com--2003/hello-world--mainline--1.0 hello-world
cleanup
end_test_savectx

begin_test_savectx "Jane adds HACKING, fixes some grammatical issues"
setup
cd jane/hello-world
echo 'We follow the GNU coding standards' > HACKING
tla add-id HACKING
sed -e 's/Hello world/Hello, World/' < hello-world.c > hello-world.c.new
mv hello-world.c.new hello-world.c
tla commit -L 'add HACKING, fix some grammatical issues'
cleanup
end_test_savectx

begin_test_savectx "foo decides to create a branch from Jane"
setup
mkdir foo
cd foo
tla tag jane@example.com--2003/hello-world--mainline--1.0 foo@example.org--2003/hello-world--devo--1.3
tla get foo@example.org--2003/hello-world--devo--1.3 hello-world
cleanup
end_test_savectx 

begin_test_savectx "foo thinks hello-world.c is better named as hello_world.c"
setup
cd foo/hello-world
mv hello-world.c hello_world.c
tla move-id hello-world.c hello_world.c
tla commit -L 'rename hello-world.c to hello_world.c'
cleanup
end_test_savectx

begin_test_savectx "Jane adds a copyright header"
setup
cd jane/hello-world
(echo '/* Copyright (C) 2003 Jane Doe */'; echo '/* This file is hereby placed into the public domain. */';
 cat hello-world.c) > hello-world.c.new
mv hello-world.c.new hello-world.c
tla commit -L 'add copyright header'
cleanup
end_test_savectx

begin_test_savectx "foo adds AUTHORS"
setup
cd foo/hello-world
echo 'foo <foo@example.org>' > AUTHORS
echo 'Jane Doe <jane@example.com>' >> AUTHORS
tla add-id AUTHORS
tla commit -L 'add AUTHORS'
cleanup
end_test_savectx

begin_test_savectx "Checkpoint 1"
setup
tla get jane@example.com--2003/hello-world--mainline--1.0 jane-hello-world.tmp
cd jane-hello-world.tmp
file_matches 'Copyright.*Jane Doe' hello-world.c
file_matches 'Hello, World' hello-world.c
file_matches 'We follow.*GNU.*standards' HACKING
file_matches 'This is hello-world' README
cd ..
tla get foo@example.org--2003/hello-world--devo--1.3 foo-hello-world.tmp
cd foo-hello-world.tmp
file_matches 'Hello, World' hello_world.c
file_matches 'We follow.*GNU.*standards' HACKING
file_matches 'This is hello-world' README
file_matches 'foo' AUTHORS
file_matches 'Jane Doe' AUTHORS
cd ..
rm -rf jane-hello-world.tmp
rm -rf foo-hello-world.tmp
cleanup
end_test_savectx

begin_test_savectx "Jane thinks foo's changes are good, decides to merge"
setup
cd jane/hello-world
tla star-merge foo@example.org--2003/hello-world--devo--1.3
tla commit -L 'merge with foo'
file_matches 'Copyright.*Jane Doe' hello_world.c
file_matches 'Hello, World' hello_world.c
file_matches 'foo' AUTHORS
file_matches 'Jane Doe' AUTHORS
cleanup
end_test_savectx

begin_test_savectx "Jane puts her name first in AUTHORS"
setup
cd jane/hello-world
(grep 'Jane Doe' AUTHORS && cat AUTHORS | grep -v 'Jane Doe') > AUTHORS.new
mv AUTHORS.new AUTHORS
tla commit -L 'put my name first in AUTHORS'
cleanup
end_test_savectx

begin_test_savectx "foo decides to merge with Jane, despite her antics"
setup
cd foo/hello-world
tla star-merge jane@example.com--2003/hello-world--mainline--1.0
tla commit -L 'merge with jane'
cleanup
end_test_savectx

begin_test_savectx "Jane and foo's trees should be equivalent"
setup
tla get jane@example.com--2003/hello-world--mainline--1.0 jane-hello-world.tmp
tla get foo@example.org--2003/hello-world--devo--1.3 foo-hello-world.tmp
assert_working_tree_equivalence jane-hello-world.tmp foo-hello-world.tmp
rm -rf jane-hello-world.tmp
rm -rf foo-hello-world.tmp
cleanup
end_test_savectx


clean_workdir

# tag: Colin Walters Thu, 18 Sep 2003 13:37:39 -0400 (jane-and-foo-do-hello-world-1.sh)
#
