/* tla.c
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord, Stig Brautaset
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */


#include "config-options.h"
#include "hackerlab/bugs/panic.h"
#include "hackerlab/char/str.h"
#include "hackerlab/vu/safe.h"
#include "tla/libarch/cmds.h"
#include "tla/revision"


#if cfg__enable_pathcompress==1
#include "hackerlab/vu/vu-pathcompress.h"
#endif

int
main (int argc, char * argv[])
{
  int x;
  char * cmd;

  if (argc <= 0)
    panic ("tla invoked with argc <= 0 ?!?");

  if (argc == 1)
    {
    usage_error:
      safe_printfmt (2, "usage: tla command ...\n");
      safe_printfmt (2, "  (try 'tla -h')\n");
      exit (1);
    }

  cmd = argv[1];

  if (!str_cmp (cmd, "-h") || !str_cmp (cmd, "--help") || !str_cmp (cmd, "-H"))
    {
      safe_printfmt (1, "invoke arch commands\n");
      safe_printfmt (1, "usage: tla COMMAND [PARAMS ...]\n");
      safe_printfmt (1, "\n");
      safe_printfmt (1, "  -h, --help     Display a help message and exit.\n");
      safe_printfmt (1, "  -H             Display a verbose help message and exit.\n");
      safe_printfmt (1, "  -V, --version  Display a release identifier string\n");
      safe_printfmt (1, "                 and exit.\n");
      safe_printfmt (1, "\n");
      safe_printfmt (1, "\n");
      safe_printfmt (1, "To see a list of available subcommands, run:\n");
      safe_printfmt (1, "\n");
      safe_printfmt (1, "    tla help\n");
      safe_printfmt (1, "\n");
      exit (0);
    }

  if (!str_cmp (cmd, "-V") || !str_cmp (cmd, "--version"))
    {
      safe_printfmt (1, cfg__std__package "\n");
#ifdef TLA_REVISION
      safe_printfmt(1, "%s, ", TLA_REVISION);
#endif
      safe_printfmt (1, "built on %s\n", __DATE__);
      safe_printfmt (1, "\n");
      safe_printfmt (1, "Copyright 2003, 2004, 2005, 2006 Free Software Foundation, Inc. and contributors\n");
      safe_printfmt (1, "Written by Thomas Lord, et al. See the AUTHORS file for a list.\n");
      safe_printfmt (1, "\n");
      safe_printfmt (1, "This is free software; see the source for copying conditions.\n");
      safe_printfmt (1, "There is NO warranty; not even for MERCHANTABILITY or FITNESS FOR A\n");
      safe_printfmt (1, "PARTICULAR PURPOSE.\n");
      safe_printfmt (1, "\n");
      safe_printfmt (1, "Report problems to " cfg__tla_bug_mail "\n");
      safe_printfmt (1, "See: " cfg__tla_web_url "\n");
      safe_printfmt (1, "\n");
      exit (0);
    }

  if (cmd[0] == '-')
    goto usage_error;

#if cfg__enable_pathcompress==1
  if (1)
    {
      regex_t reg;
      char* pattern = "";

      regcomp(&reg, pattern, 0);
      vu_push_name_handler("pathcompress", NULL, &reg, 0, &pathcompress_fs_vtable, NULL, 0);
    }
#endif
  for (x = 0; arch_commands[x].name; ++x)
    {
      if (!str_cmp (cmd, arch_commands[x].name) && arch_commands[x].fn)
        {
          t_uchar * full_program_name = 0;
          int answer;

          full_program_name = str_alloc_cat (0, "tla ", arch_commands[x].name);

          answer = arch_commands[x].fn (full_program_name, argc - 1, argv + 1);

          lim_free (0, full_program_name);
          return answer;
        }
    }

  safe_printfmt (2, "tla: unrecognized command (%s)\n", cmd);
  safe_printfmt (2, "  (try 'tla help')\n");
  exit (1);

  return 1;
}





/* tag: Tom Lord Tue Jun 10 17:07:03 2003 (tla.c)
 */
