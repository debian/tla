/* link-target.c:
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */


#include "hackerlab/mem/mem.h"
#include "hackerlab/vu/safe.h"
#include "tla/libfsutils/link-target.h"



t_uchar *
link_target (const char * path)
{
  char * link_buf;
  int link_buf_size;
  int link_size;

  link_buf = lim_malloc (0, 1024);
  link_buf_size = 1024;

  while (1)
    {
      link_size = safe_readlink (path, link_buf, link_buf_size);

      if (link_size < link_buf_size)
        break;

      link_buf = lim_realloc (0, link_buf, link_buf_size * 2);
      link_buf_size *= 2;
    }

  link_buf = lim_realloc (0, link_buf, link_size + 1);
  link_buf[link_size] = 0;
  return (t_uchar *)link_buf;
}



/* tag: Tom Lord Wed May 14 18:35:14 2003 (link-target.c)
 */
