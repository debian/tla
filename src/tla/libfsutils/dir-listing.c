/* dir-listing.c:
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */


#include "hackerlab/vu/safe.h"
#include "tla/libfsutils/dir-listing.h"



rel_table
directory_files (const t_uchar * path)
{
  rel_table answer;
  DIR * dir;
  char * file;

  answer = rel_table_nil;

  safe_opendir (&dir, path);

  while (!safe_readdir (&file, dir))
    {
      rel_add_records (&answer, rel_singleton_record_taking (rel_make_field_str (file)), rel_record_null);
      lim_free (0, file);
    }

  safe_closedir (dir);

  return answer;
}


rel_table
maybe_directory_files (const t_uchar * path)
{
  int errn;
  rel_table answer;
  DIR * dir;
  char * file;

  answer = rel_table_nil;

  if (!vu_opendir (&errn, &dir, path))
    {
      while (!safe_readdir (&file, dir))
        {
          rel_add_records (&answer, rel_singleton_record_taking (rel_make_field_str (file)), rel_record_null);
          lim_free (0, file);
        }

      safe_closedir (dir);
    }

  return answer;
}




/* tag: Tom Lord Tue May 13 09:13:46 2003 (dir-listing.c)
 */
