/* same.c: 
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord
 * 
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */


#include "hackerlab/vu/vu.h"
#include "tla/libfsutils/same.h"



/*(c names_same_inode)
 * int names_same_inode (const t_uchar * a, const t_uchar * b);
 * 
 * Return non-0 iff A and B are paths naming the 
 * same inode.
 */
int
names_same_inode (const t_uchar * a, const t_uchar * b)
{
  int errn;
  struct stat a_stat;
  struct stat b_stat;

  if (vu_stat (&errn, a, &a_stat))
    return 0;

  if (vu_stat (&errn, b, &b_stat))
    return 0;

  return ((a_stat.st_ino == b_stat.st_ino) && (a_stat.st_dev == b_stat.st_dev));
}


/*(c on_same_device)
 * int on_same_device (const t_uchar * a, const t_uchar * b);
 * 
 * Return non-0 iff A and B are paths naming existing
 * files (of any kind) on the same device.
 */
int
on_same_device (const t_uchar * a, const t_uchar * b)
{
  int errn;
  struct stat a_stat;
  struct stat b_stat;

  if (vu_stat (&errn, a, &a_stat))
    return 0;

  if (vu_stat (&errn, b, &b_stat))
    return 0;

  return (a_stat.st_dev == b_stat.st_dev);
}



/* tag: Tom Lord Fri Dec  5 13:48:29 2003 (same.c)
 */
