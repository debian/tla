/* rmrf.h:
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */

#ifndef INCLUDE__LIBFSUTILS__RMRF_H
#define INCLUDE__LIBFSUTILS__RMRF_H



/* automatically generated __STDC__ prototypes */
extern void rmrf_file (t_uchar * path);
#endif  /* INCLUDE__LIBFSUTILS__RMRF_H */


/* tag: Tom Lord Sat Jan  5 15:26:10 2002 (rmrf.h)
 */
