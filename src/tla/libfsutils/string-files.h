/* string-files.h:
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */

#ifndef INCLUDE__LIBFSUTILS__STRING_FILES_H
#define INCLUDE__LIBFSUTILS__STRING_FILES_H



/* automatically generated __STDC__ prototypes */
extern int make_output_to_string_fd (void);
extern t_uchar * string_fd_close (int fd);
#endif  /* INCLUDE__LIBFSUTILS__STRING_FILES_H */


/* tag: Tom Lord Sat Jan  5 15:26:10 2002 (string-files.h)
 */
