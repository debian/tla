/* dir-as-cwd.c:
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */


#include "hackerlab/fs/file-names.h"
#include "hackerlab/fs/cwd.h"
#include "hackerlab/vu/safe.h"
#include "tla/libfsutils/dir-as-cwd.h"



t_uchar *
directory_as_cwd (const t_uchar * dir)
{
  int here_fd;
  t_uchar * answer = 0;

  here_fd = safe_open (".", O_RDONLY, 0);

  safe_chdir (dir);

  answer = safe_current_working_directory ();

  safe_fchdir (here_fd);
  safe_close (here_fd);
  return answer;
}




/* tag: Tom Lord Fri May 30 21:47:40 2003 (dir-as-cwd.c)
 */
