/* link-tree.h:
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */

#ifndef INCLUDE__LIBFSUTILS__LINK_TREE_H
#define INCLUDE__LIBFSUTILS__LINK_TREE_H


#include "tla/libawk/relational.h"


/* automatically generated __STDC__ prototypes */
extern void build_link_tree (t_uchar * from, t_uchar * to, int full_meta);
extern void build_partial_link_tree (t_uchar * from,
                                     t_uchar * to,
                                     rel_table inventory,
                                     int full_meta);
#endif  /* INCLUDE__LIBFSUTILS__LINK_TREE_H */


/* tag: Tom Lord Sat Jan  5 15:26:10 2002 (link-tree.h)
 */
