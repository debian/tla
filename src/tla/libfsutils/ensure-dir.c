/* ensure-dir.c:
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */


#include "hackerlab/char/str.h"
#include "hackerlab/vu/safe.h"
#include "hackerlab/fs/file-names.h"
#include "tla/libfsutils/ensure-dir.h"



void
ensure_directory_exists (const t_uchar * name)
{
  if (safe_file_is_directory_following (name))
    {
      return;
    }

  {
    t_uchar * parent = 0;

    parent = file_name_directory_file (0, name);
    if (parent)
      ensure_directory_exists (parent);

    if (!safe_file_is_directory_following (name))
      safe_mkdir (name, 0777);

    lim_free (0, parent);

    return;
  }
}



/* tag: Tom Lord Mon May 12 15:29:46 2003 (ensure-dir.c)
 */
