/* find-utils.h:
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */

#ifndef INCLUDE__LIBFSUTILS__FIND_UTILS_H
#define INCLUDE__LIBFSUTILS__FIND_UTILS_H


#include "tla/libawk/relational.h"


/* automatically generated __STDC__ prototypes */
extern void find_files (rel_table * out, t_uchar * path);
#endif  /* INCLUDE__LIBFSUTILS__FIND_UTILS_H */


/* tag: Tom Lord Sat Jan  5 15:26:10 2002 (find-utils.h)
 */
