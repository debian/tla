/* safety.c:
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */


#include "hackerlab/char/char-class.h"
#include "hackerlab/char/str.h"
#include "tla/libfsutils/safety.h"



int
is_non_upwards_relative_path (const t_uchar * path)
{

  if (path && (*path == '/'))
    return 0;

  while (path && *path)
    {
      if (!str_cmp_prefix ("../", path) || !str_cmp_prefix ("..", path))
        return 0;

      path = str_chr_index (path, '/');
      if (path)
        ++path;
    }

  return 1;
}





/* tag: Tom Lord Fri May 30 00:42:45 2003 (safety.c)
 */
