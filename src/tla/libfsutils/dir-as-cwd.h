/* dir-as-cwd.h:
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */

#ifndef INCLUDE__LIBFSUTILS__DIR_AS_CWD_H
#define INCLUDE__LIBFSUTILS__DIR_AS_CWD_H


#include "hackerlab/machine/types.h"


/* automatically generated __STDC__ prototypes */
extern t_uchar * directory_as_cwd (const t_uchar * dir);
#endif  /* INCLUDE__LIBFSUTILS__DIR_AS_CWD_H */


/* tag: Tom Lord Sat Jan  5 15:26:10 2002 (dir-as-cwd.h)
 */
