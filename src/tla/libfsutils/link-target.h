/* link-target.h:
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */

#ifndef INCLUDE__LIBFSUTILS__LINK_TARGET_H
#define INCLUDE__LIBFSUTILS__LINK_TARGET_H


#include "hackerlab/machine/types.h"


/* automatically generated __STDC__ prototypes */
extern t_uchar * link_target (const char * path);
#endif  /* INCLUDE__LIBFSUTILS__LINK_TARGET_H */


/* tag: Tom Lord Sat Jan  5 15:26:10 2002 (link-target.h)
 */
