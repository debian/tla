/* same.h:
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord
 * 
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */

#ifndef INCLUDE__LIBFSUTILS__SAME_H
#define INCLUDE__LIBFSUTILS__SAME_H



/* automatically generated __STDC__ prototypes */
extern int names_same_inode (const t_uchar * a, const t_uchar * b);
extern int on_same_device (const t_uchar * a, const t_uchar * b);
#endif  /* INCLUDE__LIBFSUTILS__SAME_H */


/* tag: Tom Lord Fri Dec  5 13:48:26 2003 (same.h)
 */
