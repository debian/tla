/* ensure-dir.h:
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */

#ifndef INCLUDE__LIBFSUTILS__ENSURE_DIR_H
#define INCLUDE__LIBFSUTILS__ENSURE_DIR_H


#include "hackerlab/machine/types.h"



/* automatically generated __STDC__ prototypes */
extern void ensure_directory_exists (const t_uchar * name);
#endif  /* INCLUDE__LIBFSUTILS__ENSURE_DIR_H */


/* tag: Tom Lord Sat Jan  5 15:26:10 2002 (ensure-dir.h)
 */
