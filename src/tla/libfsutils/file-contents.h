/* file-contents.h:
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */

#ifndef INCLUDE__LIBFSUTILS__FILE_CONTENTS_H
#define INCLUDE__LIBFSUTILS__FILE_CONTENTS_H


#include "hackerlab/machine/types.h"


/* automatically generated __STDC__ prototypes */
extern t_uchar * file_contents (const t_uchar * path);
extern t_uchar * file_contents_trimming (const t_uchar * path);
extern t_uchar * fd_contents (int in_fd);
#endif  /* INCLUDE__LIBFSUTILS__FILE_CONTENTS_H */


/* tag: Tom Lord Sat Jan  5 15:26:10 2002 (file-contents.h)
 */
