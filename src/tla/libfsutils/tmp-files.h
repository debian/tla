/* tmp-files.h:
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */

#ifndef INCLUDE__LIBFSUTILS__TMP_FILES_H
#define INCLUDE__LIBFSUTILS__TMP_FILES_H


#include "hackerlab/machine/types.h"


/* automatically generated __STDC__ prototypes */
extern t_uchar * tmp_file_name (const t_uchar * dir,
                                const t_uchar * basename);
extern t_uchar * tmp_seq_file (const t_uchar * dir,
                               const t_uchar * basename);
extern t_uchar * tmp_file_name_in_tmp (t_uchar const * const basename);

#
#endif  /* INCLUDE__LIBFSUTILS__TMP_FILES_H */


/* tag: Tom Lord Sat Jan  5 15:26:10 2002 (tmp-files.h)
 */
