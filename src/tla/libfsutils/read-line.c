/* read-line.c:
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */


#include "hackerlab/char/char-class.h"
#include "hackerlab/char/str.h"
#include "hackerlab/vu/safe.h"
#include "tla/libfsutils/read-line.h"



t_uchar *
read_line_from_fd (int in_fd)
{
  t_uchar * buf;
  long len;
  t_uchar * start;

  buf = 0;
  len = 0;

  safe_next_line (&buf, &len, in_fd);

  if (!len)
    {
      return 0;
    }
  else
    {
      while (len && char_is_space (buf[len - 1]))
        --len;

      start = buf;
      while (len && char_is_blank (*buf))
        {
          --len;
          ++buf;
        }
      return str_save_n (0, start, (size_t)len);
    }
}


t_uchar *
read_line_from_file (const t_uchar * path)
{
  int in_fd;
  t_uchar * answer;

  in_fd = safe_open (path, O_RDONLY, 0);
  answer = read_line_from_fd (in_fd);
  safe_close (in_fd);
  return answer;
}




/* tag: Tom Lord Fri May 16 16:49:28 2003 (read-line.c)
 */
