/* find-utils.c:
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */


#include "hackerlab/mem/mem.h"
#include "hackerlab/char/str.h"
#include "hackerlab/fs/file-names.h"
#include "hackerlab/vu/safe.h"
#include "tla/libfsutils/dir-listing.h"
#include "tla/libfsutils/find-utils.h"



void
find_files (rel_table * out, t_uchar * path)
{
  struct stat stat_buf;

  safe_lstat (path, &stat_buf);

  if (!S_ISDIR (stat_buf.st_mode))
    {
      rel_add_records (out, rel_singleton_record_taking (rel_make_field_str (path)), rel_record_null);
    }
  else
    {
      rel_table contents;
      int lim;
      int x;

      contents = directory_files (path);

      lim = rel_n_records (contents);
      for (x = 0; x < lim; ++x)
        {
          if (str_cmp (rel_peek_str (contents, x, 0), ".") && str_cmp (rel_peek_str (contents, x, 0), ".."))
            {
              t_uchar * sub_path;

              sub_path = file_name_in_vicinity (0, path, rel_peek_str (contents, x, 0));
              find_files (out, sub_path);
              lim_free (0, sub_path);
            }
        }

      rel_free_table (contents);
    }
}




/* tag: Tom Lord Thu May 15 14:55:04 2003 (find-utils.c)
 */
