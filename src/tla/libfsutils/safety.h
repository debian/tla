/* safety.h:
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */

#ifndef INCLUDE__LIBFSUTILS__SAFETY_H
#define INCLUDE__LIBFSUTILS__SAFETY_H



/* automatically generated __STDC__ prototypes */
extern int is_non_upwards_relative_path (const t_uchar * path);
#endif  /* INCLUDE__LIBFSUTILS__SAFETY_H */


/* tag: Tom Lord Sat Jan  5 15:26:10 2002 (safety.h)
 */
