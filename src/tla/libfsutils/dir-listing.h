/* dir-listing.h:
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */

#ifndef INCLUDE__LIBFSUTILS__DIR_LISTING_H
#define INCLUDE__LIBFSUTILS__DIR_LISTING_H


#include "tla/libawk/relational.h"


/* automatically generated __STDC__ prototypes */
extern rel_table directory_files (const t_uchar * path);
extern rel_table maybe_directory_files (const t_uchar * path);
#endif  /* INCLUDE__LIBFSUTILS__DIR_LISTING_H */


/* tag: Tom Lord Sat Jan  5 15:26:10 2002 (dir-listing.h)
 */
