/* read-line.h:
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */

#ifndef INCLUDE__LIBFSUTILS__READ_LINE_H
#define INCLUDE__LIBFSUTILS__READ_LINE_H



/* automatically generated __STDC__ prototypes */
extern t_uchar * read_line_from_fd (int in_fd);
extern t_uchar * read_line_from_file (const t_uchar * path);
#endif  /* INCLUDE__LIBFSUTILS__READ_LINE_H */


/* tag: Tom Lord Sat Jan  5 15:26:10 2002 (read-line.h)
 */
