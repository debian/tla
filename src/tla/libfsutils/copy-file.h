/* copy-file.h:
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */

#ifndef INCLUDE__LIBFSUTILS__COPY_FILE_H
#define INCLUDE__LIBFSUTILS__COPY_FILE_H


#include "tla/libawk/relational.h"


/* automatically generated __STDC__ prototypes */
extern void copy_fd (int in_fd, int out_fd);
extern void copy_file (const char * from, const char * to);
extern void copy_symlink (const char * from, const char * to);
extern void copy_file_or_symlink (const char * from, const char * to);
extern void copy_permissions (const char * from,
                              const char * to,
                              int full_meta);
extern void copy_file_list (const t_uchar * dest_dir,
                            const t_uchar * src_dir,
                            rel_table index,
                            int full_meta);
#endif  /* INCLUDE__LIBFSUTILS__COPY_FILE_H */


/* tag: Tom Lord Sat Jan  5 15:26:10 2002 (copy-file.h)
 */
