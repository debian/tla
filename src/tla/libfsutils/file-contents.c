/* file-contents.c:
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */


#include "hackerlab/char/str.h"
#include "hackerlab/vu/safe.h"
#include "tla/libfsutils/file-contents.h"



t_uchar *
file_contents (const t_uchar * path)
{
  int in_fd;
  t_uchar * buf = 0;
  size_t len = 0;

  in_fd = safe_open (path, O_RDONLY, 0);
  safe_file_to_string (&buf, &len, in_fd);
  safe_close (in_fd);

  buf = lim_realloc (0, buf, len + 1);
  buf[len] = 0;
  return buf;
}


t_uchar *
file_contents_trimming (const t_uchar * path)
{
  t_uchar * intermediate = 0;
  t_uchar * answer = 0;

  intermediate = file_contents (path);
  if (!intermediate)
    return 0;

  answer = str_save_trimming (0, intermediate);

  lim_free (0, intermediate);

  return answer;
}


t_uchar *
fd_contents (int in_fd)
{
  t_uchar * buf = 0;
  size_t len = 0;

  safe_file_to_string (&buf, &len, in_fd);

  buf = lim_realloc (0, buf, len + 1);
  buf[len] = 0;
  return buf;
}




/* tag: Tom Lord Thu May 22 19:46:48 2003 (file-contents.c)
 */
