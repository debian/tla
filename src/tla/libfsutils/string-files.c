/* string-files.c:
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */


#include "hackerlab/bugs/panic.h"
#include "hackerlab/mem/mem.h"
#include "hackerlab/char/str.h"
#include "hackerlab/vu/safe.h"
#include "hackerlab/vu/vu-virtual-null.h"
#include "tla/libfsutils/string-files.h"



int
make_output_to_string_fd (void)
{
  int errn;
  int fd;

  fd = vu_make_virtual_null_fd (&errn, O_WRONLY);
  invariant (fd >= 0);

  safe_buffer_fd (fd, 0, O_WRONLY, 0);
  safe_set_dont_flush (fd, 1);

  return fd;
}


t_uchar *
string_fd_close (int fd)
{
  t_uchar * bufbase;
  long bufsize;
  t_uchar * write_pos;
  long buffered;
  int has_zero_byte;
  t_uchar * answer = 0;

  safe_getbuf (&bufbase, &bufsize, &write_pos, &buffered, &has_zero_byte, fd);

  answer = lim_malloc (0, write_pos - bufbase + 1);
  mem_move (answer, bufbase, write_pos - bufbase);
  answer[write_pos - bufbase] = 0;
  safe_close (fd);

  return answer;
}




/* tag: Tom Lord Sun May 25 11:13:05 2003 (string-files.c)
 */
