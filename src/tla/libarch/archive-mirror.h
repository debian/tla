/* archive-mirror.h:
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */

#ifndef INCLUDE__LIBARCH__ARCHIVE_MIRROR_H
#define INCLUDE__LIBARCH__ARCHIVE_MIRROR_H



#include "hackerlab/machine/types.h"
#include "tla/libarch/archive.h"



enum arch_archive_mirror_cachedrevs
{
  arch_mirror_skip_cachedrevs,
  arch_mirror_foreign_continuation_cachedrevs,
  arch_mirror_all_cachedrevs
};

struct arch_archive_mirror_options
{
  enum arch_archive_mirror_cachedrevs cachedrevs;
  int print_summary; /* whether or not to print a summary of new patches */
};


/* automatically generated __STDC__ prototypes */
extern void arch_archive_mirror (int chatter_fd,
                                 struct arch_archive * arch,
                                 struct arch_archive * to_arch,
                                 t_uchar * limit,
                                 struct arch_archive_mirror_options * archive_mirror_opts);
extern void mirror_cached_revision (int is_cached,
                                    struct arch_archive_mirror_options * archive_mirror_opts,
                                    enum arch_revision_type type,
                                    struct arch_archive * arch,
                                    struct arch_archive * to_arch, 
                                    const t_uchar * revision, 
                                    const t_uchar * continuation_archive);
#endif  /* INCLUDE__LIBARCH__ARCHIVE_MIRROR_H */


/* tag: Tom Lord Sat Jan  5 15:26:10 2002 (archive-mirror.h)
 */
