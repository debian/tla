/* cmd-star-merge.h:
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */

#ifndef INCLUDE__LIBARCH__CMD_STAR_MERGE_H
#define INCLUDE__LIBARCH__CMD_STAR_MERGE_H


#include "hackerlab/machine/types.h"



extern t_uchar arch_cmd_star_merge_help[];


/* automatically generated __STDC__ prototypes */
extern int arch_cmd_star_merge (t_uchar * program_name, int argc, char * argv[]);
#endif  /* INCLUDE__LIBARCH__CMD_STAR_MERGE_H */


/* tag: Tom Lord Sat Jan  5 15:26:10 2002 (cmd-star-merge.h)
 */
