/* cmd-file-find.h:
 *
 ****************************************************************
 * Copyright (C) 2003, 2004 Tom Lord, Stig Brautaset
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */

#ifndef INCLUDE__LIBARCH__CMD_FILE_FIND_H
#define INCLUDE__LIBARCH__CMD_FILE_FIND_H


#include "hackerlab/machine/types.h"



extern t_uchar arch_cmd_file_find_help[];


/* automatically generated __STDC__ prototypes */
extern int arch_cmd_file_find (t_uchar * program_name, int argc, char * argv[]);
#endif  /* INCLUDE__LIBARCH__CMD_FILE_FIND_H */


/* tag: Jan Hudec Fr, 11 Jul 2003 12:34:30 +0200 (cmd-file-find.h)
 */
