/* make-changeset-files.h:
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */

#ifndef INCLUDE__LIBARCH__MAKE_CHANGESET_FILES_H
#define INCLUDE__LIBARCH__MAKE_CHANGESET_FILES_H


#include "tla/libarch/changeset-report.h"
#include "tla/libarch/make-changeset.h"


/* automatically generated __STDC__ prototypes */
extern void arch_make_files_changeset (struct arch_make_changeset_report * report,
                                       const t_uchar * dest,
                                       rel_table file_list,
                                       const t_uchar * orig,
                                       const t_uchar * mod,
                                       enum arch_id_tagging_method method,
                                       enum arch_inventory_category untagged_source_category,
                                       int escape_classes);
#endif  /* INCLUDE__LIBARCH__MAKE_CHANGESET_FILES_H */


/* tag: Tom Lord Sat Jan  5 15:26:10 2002 (make-changeset-files.h)
 */
