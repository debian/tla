/* arbdelta.h:
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */

#ifndef INCLUDE__LIBARCH__ARBDELTA_H
#define INCLUDE__LIBARCH__ARBDELTA_H


#include "tla/libarch/archive.h"


/* automatically generated __STDC__ prototypes */
extern void arch_arbitrary_delta (int chatter_fd, t_uchar * output_dir, t_uchar * tmp_dir, t_uchar * tree_root,
                                  struct arch_archive * orig_arch, t_uchar * orig_archive, t_uchar * orig_revision,
                                  struct arch_archive * mod_arch, t_uchar * mod_archive, t_uchar * mod_revision,
                                  int escape_classes);
#endif  /* INCLUDE__LIBARCH__ARBDELTA_H */


/* tag: Tom Lord Sat Jan  5 15:26:10 2002 (arbdelta.h)
 */
