/* cmd-help.c
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */


#include "config-options.h"
#include "hackerlab/cmd/main.h"
#include "tla/libarch/cmds.h"
#include "tla/libarch/cmd-help.h"



static t_uchar * usage = "[options]";
static t_uchar * version_string = (cfg__std__package " from regexps.com\n"
                                   "\n"
                                   "Copyright 2003 Tom Lord\n"
                                   "\n"
                                   "This is free software; see the source for copying conditions.\n"
                                   "There is NO warranty; not even for MERCHANTABILITY or FITNESS FOR A\n"
                                   "PARTICULAR PURPOSE.\n"
                                   "\n"
                                   "Report bugs to " cfg__tla_bug_mail ".\n"
                                   "\n"
                                   cfg__std__release_id_string
                                   "\n");

#define OPTS(OP) \
  OP (opt_help_msg, "h", "help", 0, \
      "Display a help message and exit.") \
  OP (opt_long_help, "H", 0, 0, \
      "Display a verbose help message and exit.") \
  OP (opt_basic, "b", "basic", 0, \
      "Display basic commands only") \
  OP (opt_version, "V", "version", 0, \
      "Display a release identifier string\n" \
      "and exit.")

t_uchar arch_cmd_help_help[] = ("provide help with arch\n"
                                "\n"
                                "This command prints a list of the available commands.\n"
                                "\n"
                                "To see just a list of the options to a particular command,\n"
                                "use:\n"
                                "\n"
                                "    tla $cmd -h\n"
                                "\n"
                                "(where $cmd is the name of the command).   For additional\n"
                                "explanation about a given command, use:\n"
                                "\n"
                                "    tla $cmd -H\n"
                                "\n");

enum options
{
  OPTS (OPT_ENUM)
};

static struct opt_desc opts[] =
{
  OPTS (OPT_DESC)
    {-1, 0, 0, 0, 0}
};



int
arch_cmd_help (t_uchar * program_name, int argc, char * argv[])
{
  int o;
  enum arch_command_level level = arch_level_advanced;
  struct opt_parsed * option;

  safe_buffer_fd (1, 0, O_WRONLY, 0);

  option = 0;

  while (1)
    {
      o = opt_standard (lim_use_must_malloc, &option, opts, &argc, argv, program_name, usage, version_string, arch_cmd_help_help, opt_help_msg, opt_long_help, opt_version);
      if (o == opt_none)
        break;
      switch (o)
        {
        default:
          safe_printfmt (2, "unhandled option `%s'\n", option->opt_string);
          panic ("internal error parsing arguments");

        usage_error:
          opt_usage (2, argv[0], program_name, usage, 1);
          exit (1);

          /* bogus_arg: */
          safe_printfmt (2, "ill-formed argument for `%s' (`%s')\n", option->opt_string, option->arg_string);
          goto usage_error;
        case opt_basic:
          {
            level = arch_level_basic;
            break;
          }
        }
    }

  if (argc != 1)
    goto usage_error;

  {
    int x;
    if (level != arch_level_basic)
        safe_printfmt(1, "use 'tla help -b' to display basic commands only; more suitable for beginners\n\n");
    if (level == arch_level_basic)
      {
        safe_printfmt (1, "                     basic tla sub-commands\n");
        safe_printfmt (1, "                     ----------------------");
      }      
    else
      {
        safe_printfmt (1, "                        tla sub-commands\n");
        safe_printfmt (1, "                        ----------------");
      }  
    for (x = 0; arch_commands[x].name; ++x)
      if (arch_commands[x].level!=arch_level_deprecated && level>=arch_commands[x].level)
        {
          if (!arch_commands[x].fn)
            {
              if (!arch_commands[x].name[0])
                safe_printfmt (1, "\n");
              else
                safe_printfmt (1, "\n\n* %s\n\n", arch_commands[x].name);
            }
          else if (arch_commands[x].alias_of)
            {
              int name_len;
              t_uchar * help_nl;

              name_len = (int)str_length (arch_commands[x].name);
              help_nl = str_chr_index (arch_commands[x].long_help, '\n');
              if (!help_nl)
                help_nl = arch_commands[x].long_help + str_length (arch_commands[x].long_help);
              safe_printfmt (1, "%*s%s : (alias for %s)\n", (28 - name_len), "", arch_commands[x].name, arch_commands[x].alias_of);
            }
          else
            {
              int name_len;
              t_uchar * help_nl;

              name_len = (int)str_length (arch_commands[x].name);
              help_nl = str_chr_index (arch_commands[x].long_help, '\n');
              if (!help_nl)
                help_nl = arch_commands[x].long_help + str_length (arch_commands[x].long_help);
              safe_printfmt (1, "%*s%s : %.*s\n", (28 - name_len), "", arch_commands[x].name, (int)(help_nl - arch_commands[x].long_help), arch_commands[x].long_help);
            }
        }

    safe_printfmt (1, "\n");
  }

  safe_printfmt (1, "\nUse 'tla command -h' for help on `command', or tla command -H for detailed help.\n");
    if (level != arch_level_basic)
        safe_printfmt(1, "use 'tla help -b' to display basic commands only; more suitable for beginners\n");
    else safe_printfmt(1, "use 'tla help' to display a full list of tla commands\n");

  return 0;
}




/* tag: Tom Lord Wed Jun 11 21:04:35 2003 (cmd-help.c)
 */
