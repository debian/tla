/* archive-version.h:
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */

#ifndef INCLUDE__LIBARCH__ARCHIVE_VERSION_H
#define INCLUDE__LIBARCH__ARCHIVE_VERSION_H


#include "hackerlab/machine/types.h"

extern const char arch_tree_format_1_str[];
extern const char arch_tree_format_2_str[];




enum arch_archive_access
{
  arch_archive_readable,
  arch_archive_writable,
  arch_archive_incompatible
};

enum arch_archive_type
{
  arch_archive_tla,
  arch_archive_baz
};



/* automatically generated __STDC__ prototypes */
extern t_uchar * arch_archive_version_for_new_archive (int tla_archive);
extern enum arch_archive_access arch_archive_access (t_uchar * version);
extern enum arch_archive_type arch_archive_type (t_uchar * version);
#endif  /* INCLUDE__LIBARCH__ARCHIVE_VERSION_H */


/* tag: Tom Lord Sat Jan  5 15:26:10 2002 (archive-version.h)
 */
