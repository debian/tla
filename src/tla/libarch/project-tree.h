/* project-tree.h:
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */

#ifndef INCLUDE__LIBARCH__PROJECT_TREE_H
#define INCLUDE__LIBARCH__PROJECT_TREE_H


#include "hackerlab/machine/types.h"



enum arch_tree_state
{
  arch_tree_in_ok_state = 0,
  arch_tree_in_resolve_conflicts,
  arch_tree_in_commit_definite,
  arch_tree_in_mid_commit,
};


/* automatically generated __STDC__ prototypes */
extern t_uchar * arch_tree_format_string (void);
extern void arch_init_tree (const t_uchar * tree_root);
extern t_uchar * arch_tree_root (enum arch_tree_state * state,
                                 const t_uchar * input_dir,
                                 int accurate);
extern t_uchar * arch_tree_ctl_dir (const t_uchar * tree_root);
extern int arch_tree_has_meta_flag (const t_uchar * const tree_root);
extern void arch_set_tree_version (const t_uchar * tree_root,
                                   const t_uchar * archive,
                                   const t_uchar * version);
extern t_uchar * arch_tree_version (const t_uchar * tree_root);
extern t_uchar * arch_try_tree_version (const t_uchar * cmd);
extern void arch_start_tree_commit (const t_uchar * tree_root,
                                    const t_uchar * log);
extern void arch_finish_tree_commit (const t_uchar * tree_root,
                                     const t_uchar * archive,
                                     const t_uchar * revision,
                                     const t_uchar * changelog_loc,
                                     int const full_meta);
extern void arch_abort_tree_commit (const t_uchar * tree_root,
                                    const t_uchar * archive,
                                    const t_uchar * revision);
extern t_uchar * arch_get_tree_fqrevision(const t_uchar *tree_root);
extern t_uchar * arch_get_fqrevision(const t_uchar *fqversion);

#endif  /* INCLUDE__LIBARCH__PROJECT_TREE_H */


/* tag: Tom Lord Sat Jan  5 15:26:10 2002 (project-tree.h)
 */
