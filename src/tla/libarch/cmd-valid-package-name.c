/* cmd-valid-package-name.c:
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */


#include "config-options.h"
#include "hackerlab/cmd/main.h"
#include "tla/libarch/namespace.h"
#include "tla/libarch/cmd-valid-package-name.h"



static t_uchar * usage = "[options] name";
static t_uchar * version_string = (cfg__std__package " from regexps.com\n"
                                   "\n"
                                   "Copyright 2003 Tom Lord\n"
                                   "\n"
                                   "This is free software; see the source for copying conditions.\n"
                                   "There is NO warranty; not even for MERCHANTABILITY or FITNESS FOR A\n"
                                   "PARTICULAR PURPOSE.\n"
                                   "\n"
                                   "Report bugs to " cfg__tla_bug_mail ".\n"
                                   "\n"
                                   cfg__std__release_id_string
                                   "\n");

#define OPTS(OP) \
  OP (opt_help_msg, "h", "help", 0, \
      "Display a help message and exit.") \
  OP (opt_long_help, "H", 0, 0, \
      "Display a verbose help message and exit.") \
  OP (opt_version, "V", "version", 0, \
      "Display a release identifier string\n" \
      "and exit.") \
  OP (opt_errname, "e", "errname", 1, \
      "specify program name for errors") \
  OP (opt_archive, 0, "archive", 0, \
      "require and explicit archive") \
  OP (opt_no_archive, 0, "no-archive", 0, \
      "prohibit and explicit archive") \
  OP (opt_category, "c", "category", 0, \
      "require a category") \
  OP (opt_package, "p", "package", 0, \
      "require category, permit branch") \
  OP (opt_vsn, "v", "vsn", 0, \
      "require a version number") \
  OP (opt_level, "l", "patch-level", 0, \
      "require a patch level") \
  OP (opt_lvl, 0, "lvl", 0, \
      "synonym for --patch-level") \
  OP (opt_tolerant, "t", "tolerant", 0, \
      "tolerate more specific names")

t_uchar arch_cmd_valid_package_name_help[] = ("test a package name for validity\n"
                                              "Exit with status 0 if PACKAGE is a valid package name,\n"
                                              "status 1 otherwise.\n"
                                              "\n"
                                              "By default, require a basename or basename plus branch label.\n"
                                              "\n"
                                              "Options -v and -l cancel -b and vice versa.\n"
                                              "\n"
                                              "Option -l implies -v.\n"
                                              "\n"
                                              "If an error name is specified (-e or --errname), then invalid\n"
                                              "names cause an error message on stdout.  Otherwise, the exit\n"
                                              "status is the only output.\n"
                                              "\n"
                                              "By default, validation is strict.  For example, -b checks\n"
                                              "for a valid basename and does not permit a branch label\n"
                                              "or version number.\n"
                                              "\n"
                                              "With -t, more specific names are permitted.  For example, -b\n"
                                              "will permit a branch name, version number, and patch level.\n");

enum options
{
  OPTS (OPT_ENUM)
};

static struct opt_desc opts[] =
{
  OPTS (OPT_DESC)
    {-1, 0, 0, 0, 0}
};



int
arch_cmd_valid_package_name (t_uchar * program_name, int argc, char * argv[])
{
  int o;
  struct opt_parsed * option;
  t_uchar * errname;
  enum arch_valid_package_name_archive archive_disposition;
  enum arch_valid_package_name_archive package_name_type;
  char * type_name;
  int tolerant;
  int result;

  errname = 0;
  archive_disposition = arch_maybe_archive;
  package_name_type = arch_req_package;
  type_name = "package";
  tolerant = 0;

  safe_buffer_fd (1, 0, O_WRONLY, 0);

  option = 0;

  while (1)
    {
      o = opt_standard (lim_use_must_malloc, &option, opts, &argc, argv, program_name, usage, version_string, arch_cmd_valid_package_name_help, opt_help_msg, opt_long_help, opt_version);
      if (o == opt_none)
        break;
      switch (o)
        {
        default:
          safe_printfmt (2, "unhandled option `%s'\n", option->opt_string);
          panic ("internal error parsing arguments");

        usage_error:
          opt_usage (2, argv[0], program_name, usage, 1);
          exit (1);

          /* bogus_arg: */
          safe_printfmt (2, "ill-formed argument for `%s' (`%s')\n", option->opt_string, option->arg_string);
          goto usage_error;


        case opt_errname:
          {
            errname = str_save (0, option->arg_string);
            break;
          }

        case opt_archive:
          {
            archive_disposition = arch_req_archive;
            break;
          }

        case opt_no_archive:
          {
            archive_disposition = arch_no_archive;
            break;
          }

        case opt_category:
          {
            package_name_type = arch_req_category;
            type_name = "category";
            break;
          }

        case opt_package:
          {
            package_name_type = arch_req_package;
            type_name = "package";
            break;
          }

        case opt_vsn:
          {
            package_name_type = arch_req_version;
            type_name = "version";
            break;
          }

        case opt_level:
        case opt_lvl:
          {
            package_name_type = arch_req_patch_level;
            type_name = "patch-level";
            break;
          }

        case opt_tolerant:
          {
            tolerant = 1;
            break;
          }
        }
    }

  if (argc != 2)
    goto usage_error;

  result = arch_valid_package_name (argv[1], archive_disposition, package_name_type, tolerant);

  if (!result)
    {
      if (errname)
        {
          safe_printfmt (2, "%s: invalid %s id (%s)\n", errname, type_name, argv[1]);
        }
      exit (1);
    }

  return 0;
}




/* tag: Tom Lord Mon May 12 11:13:10 2003 (valid-package-name.c)
 */
