/* cmd-my-default-archive.h:
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord, Stig Brautaset
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */

#ifndef INCLUDE__LIBARCH__CMD_MY_DEFAULT_ARCHIVE_H
#define INCLUDE__LIBARCH__CMD_MY_DEFAULT_ARCHIVE_H


#include "hackerlab/machine/types.h"



extern t_uchar arch_cmd_my_default_archive_help[];


/* automatically generated __STDC__ prototypes */
extern int arch_cmd_my_default_archive (t_uchar * program_name, int argc, char * argv[]);
#endif  /* INCLUDE__LIBARCH__CMD_MY_DEFAULT_ARCHIVE_H */


/* tag: Stig Brautaset Sat Jun  7 16:07:39 BST 2003 (cmd-my-default-archive.h)
 */
