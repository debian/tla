/* cmd-escape.c
 *
 ****************************************************************
 * Copyright (C) 2004  Christian Thaeter
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */



#include "config-options.h"
#include "hackerlab/cmd/main.h"
#include "tla/libarch/cmd.h"
#include "tla/libarch/cmd-escape.h"



static t_uchar * usage = "[--unescaped] string";
static t_uchar * version_string = (cfg__std__package " from regexps.com\n"
                                   "\n"
                                   "Copyright 2001, 2002, 2003 Tom Lord\n"
                                   "\n"
                                   "This is free software; see the source for copying conditions.\n"
                                   "There is NO warranty; not even for MERCHANTABILITY or FITNESS FOR A\n"
                                   "PARTICULAR PURPOSE.\n"
                                   "\n"
                                   "Report bugs to " cfg__tla_bug_mail ".\n"
                                   "\n"
                                   cfg__std__release_id_string
                                   "\n");

#define OPTS(OP) \
  OP (opt_help_msg, "h", "help", 0, \
      "Display a help message and exit.") \
  OP (opt_long_help, "H", 0, 0, \
      "Display a verbose help message and exit.") \
  OP (opt_version, "V", "version", 0, \
      "Display a release identifier string\n" \
      "and exit.") \
  OP (opt_unescaped, 0, "unescaped", 0, \
      "show filenames in unescaped form")

t_uchar arch_cmd_escape_help[] = ("print strings in escaped or unescaped form\n"
                                  "Using Pika escaping rules.\n");

enum options
{
  OPTS (OPT_ENUM)
};

static struct opt_desc opts[] =
{
  OPTS (OPT_DESC)
    {-1, 0, 0, 0, 0}
};

int i;



int
arch_cmd_escape (t_uchar * program_name, int argc, char * argv[])
{
  int o;
  struct opt_parsed * option;
  int escape_classes = arch_escape_classes;

  safe_buffer_fd (1, 0, O_WRONLY, 0);

  option = 0;

  while (1)
    {
      o = opt_standard (lim_use_must_malloc, &option, opts, &argc, argv, program_name, usage, version_string, arch_cmd_escape_help, opt_help_msg, opt_long_help, opt_version);
      if (o == opt_none)
        break;
      switch (o)
        {
        default:
          safe_printfmt (2, "unhandled option `%s'\n", option->opt_string);
          panic ("internal error parsing arguments");

        usage_error:
          opt_usage (2, argv[0], program_name, usage, 1);
          exit (1);

          /* bogus_arg: */
          safe_printfmt (2, "ill-formed argument for `%s' (`%s')\n", option->opt_string, option->arg_string);
          goto usage_error;

	case opt_unescaped:
          escape_classes = 0;
          break;

        }
    }


  if (escape_classes == 0)
    {
      t_uchar * unescaped_tmp;
      unescaped_tmp = pika_save_unescape_iso8859_1 (0, 0, argv[1]);
      if (!unescaped_tmp)
        {
          safe_printfmt (2, "Illegal escape sequence in '%s'\n", argv[1]);
          exit(2);
        }
      safe_printfmt (1, "%s", unescaped_tmp);
      lim_free (0, unescaped_tmp);
    }
  else
    {
      t_uchar * escaped_tmp;
      escaped_tmp = pika_save_escape_iso8859_1 (0, 0, escape_classes, argv[1]);
      safe_printfmt (1, "%s", escaped_tmp);
      lim_free (0, escaped_tmp);
    }

  return 0;
}



/* tag: Christian Thaeter Sun Mar  7 05:02:18 CET 2004 (cmd-escape.c)
 */
