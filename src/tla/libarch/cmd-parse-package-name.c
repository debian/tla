/* cmd-parse-package-name.c:
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */


#include "config-options.h"
#include "hackerlab/cmd/main.h"
#include "tla/libarch/namespace.h"
#include "tla/libarch/cmd-parse-package-name.h"



static t_uchar * usage = "[options] name";
static t_uchar * version_string = (cfg__std__package " from regexps.com\n"
                                   "\n"
                                   "Copyright 2003 Tom Lord\n"
                                   "\n"
                                   "This is free software; see the source for copying conditions.\n"
                                   "There is NO warranty; not even for MERCHANTABILITY or FITNESS FOR A\n"
                                   "PARTICULAR PURPOSE.\n"
                                   "\n"
                                   "Report bugs to " cfg__tla_bug_mail ".\n"
                                   "\n"
                                   cfg__std__release_id_string
                                   "\n");

#define OPTS(OP) \
  OP (opt_help_msg, "h", "help", 0, \
      "Display a help message and exit.") \
  OP (opt_long_help, "H", 0, 0, \
      "Display a verbose help message and exit.") \
  OP (opt_version, "V", "version", 0, \
      "Display a release identifier string\n" \
      "and exit.") \
  OP (opt_archive, "A", "archive", 1, \
      "Override `my-default-archive'") \
  OP (opt_arch, "a", "arch", 0, \
      "print the archive name") \
  OP (opt_non_arch, 0, "non-arch", 0, \
      "print the non-archive part of the name") \
  OP (opt_category, "c", "category", 0, \
      "print the category name") \
  OP (opt_branch, "b", "branch", 0, \
      "print the branch name") \
  OP (opt_package, "p", "package", 0, \
      "print the package name") \
  OP (opt_vsn, "v", "vsn", 0, \
      "print the version id") \
  OP (opt_pkg_version, 0, "package-version", 0, \
      "print the category--branch--version") \
  OP (opt_lvl, "l", "lvl", 0, \
      "print the patch level") \
  OP (opt_patch_lvl, 0, "patch-level", 0, \
      "synonym for --lvl")

t_uchar arch_cmd_parse_package_name_help[] = ("parse a package name\n"
                                              "Options -b, -B, -p, -v, and -l cancel each other.\n"
                                              "\n"
                                              "For -b, -v, and -l, just the id string is printed\n"
                                              "without \"--\".\n"
                                              "\n"
                                              "For -p, the output is \"<basename>--<branch>\".\n");

enum options
{
  OPTS (OPT_ENUM)
};

static struct opt_desc opts[] =
{
  OPTS (OPT_DESC)
    {-1, 0, 0, 0, 0}
};



int
arch_cmd_parse_package_name (t_uchar * program_name, int argc, char * argv[])
{
  int o;
  struct opt_parsed * option;
  t_uchar * default_archive;
  enum arch_parse_package_name_type part_wanted;
  t_uchar * spec;
  t_uchar * answer;

  default_archive = 0;
  part_wanted = arch_ret_package;

  safe_buffer_fd (1, 0, O_WRONLY, 0);

  option = 0;

  while (1)
    {
      o = opt_standard (lim_use_must_malloc, &option, opts, &argc, argv, program_name, usage, version_string, arch_cmd_parse_package_name_help, opt_help_msg, opt_long_help, opt_version);
      if (o == opt_none)
        break;
      switch (o)
        {
        default:
          safe_printfmt (2, "unhandled option `%s'\n", option->opt_string);
          panic ("internal error parsing arguments");

        usage_error:
          opt_usage (2, argv[0], program_name, usage, 1);
          exit (1);

          /* bogus_arg: */
          safe_printfmt (2, "ill-formed argument for `%s' (`%s')\n", option->opt_string, option->arg_string);
          goto usage_error;

        case opt_archive:
          {
            default_archive = str_save (0, option->arg_string);
            break;
          }

        case opt_arch:
          {
            part_wanted = arch_ret_archive;
            break;
          }

        case opt_non_arch:
          {
            part_wanted = arch_ret_non_archive;
            break;
          }

        case opt_category:
          {
            part_wanted = arch_ret_category;
            break;
          }

        case opt_branch:
          {
            part_wanted = arch_ret_branch;
            break;
          }

        case opt_package:
          {
            part_wanted = arch_ret_package;
            break;
          }

        case opt_vsn:
          {
            part_wanted = arch_ret_version;
            break;
          }

        case opt_pkg_version:
          {
            part_wanted = arch_ret_package_version;
            break;
          }

        case opt_lvl:
        case opt_patch_lvl:
          {
            part_wanted = arch_ret_patch_level;
            break;
          }
        }
    }

  if (argc != 2)
    goto usage_error;

  spec = argv[1];

  if (!arch_valid_package_name (spec, arch_maybe_archive, arch_req_package, 1))
    {
      safe_printfmt (2, "%s: invalid arch id (%s)\n",
                     argv[0], spec);
      exit (1);
    }

  answer = arch_parse_package_name (part_wanted, default_archive, spec);

  safe_printfmt (1, "%s\n", answer);

  return 0;
}




/* tag: Tom Lord Mon May 12 11:13:10 2003 (parse-package-name.c)
 */
