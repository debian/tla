/* pfs-fs.h:
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */

#ifndef INCLUDE__LIBARCH__PFS_FS_H
#define INCLUDE__LIBARCH__PFS_FS_H


#include "tla/libarch/pfs.h"



/* automatically generated __STDC__ prototypes */
extern struct arch_pfs_session * arch_pfs_fs_connect (const t_uchar * uri);
#endif  /* INCLUDE__LIBARCH__PFS_FS_H */


/* tag: Tom Lord Sat Jan  5 15:26:10 2002 (pfs-fs.h)
 */
