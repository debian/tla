/* cmd-delete-id.h:
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord, Stig Brautaset
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */

#ifndef INCLUDE__LIBARCH__CMD_DELETE_ID_H
#define INCLUDE__LIBARCH__CMD_DELETE_ID_H


#include "hackerlab/machine/types.h"



extern t_uchar arch_cmd_delete_id_help[];


/* automatically generated __STDC__ prototypes */
extern int arch_cmd_delete_id (t_uchar * program_name, int argc, char * argv[]);
#endif  /* INCLUDE__LIBARCH__CMD_DELETE_ID_H */


/* tag: Stig Brautaset Sat Jun  7 13:47:19 BST 2003 (cmd-delete-tag.h)
 */
