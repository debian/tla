/* cmd-library-branches.c
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */


#include "config-options.h"
#include "hackerlab/cmd/main.h"
#include "tla/libarch/namespace.h"
#include "tla/libarch/project-tree.h"
#include "tla/libarch/my.h"
#include "tla/libarch/libraries.h"
#include "tla/libarch/cmd-library-branches.h"



static t_uchar * usage = "[options] [category]";
static t_uchar * version_string = (cfg__std__package " from regexps.com\n"
                                   "\n"
                                   "Copyright 2003 Tom Lord\n"
                                   "\n"
                                   "This is free software; see the source for copying conditions.\n"
                                   "There is NO warranty; not even for MERCHANTABILITY or FITNESS FOR A\n"
                                   "PARTICULAR PURPOSE.\n"
                                   "\n"
                                   "Report bugs to " cfg__tla_bug_mail ".\n"
                                   "\n"
                                   cfg__std__release_id_string
                                   "\n");

#define OPTS(OP) \
  OP (opt_help_msg, "h", "help", 0, \
      "Display a help message and exit.") \
  OP (opt_long_help, "H", 0, 0, \
      "Display a verbose help message and exit.") \
  OP (opt_version, "V", "version", 0, \
      "Display a release identifier string\n" \
      "and exit.") \
  OP (opt_archive, "A", "archive", 1, \
      "Override `my-default-archive'")


t_uchar arch_cmd_library_branches_help[] = ("list the branches in a library category\n"
                                            "List all branches within CATEGORY in the revision library.\n");

enum options
{
  OPTS (OPT_ENUM)
};

static struct opt_desc opts[] =
{
  OPTS (OPT_DESC)
    {-1, 0, 0, 0, 0}
};



int
arch_cmd_library_branches (t_uchar * program_name, int argc, char * argv[])
{
  int o;
  struct opt_parsed * option;
  t_uchar * default_archive;

  default_archive = 0;

  safe_buffer_fd (1, 0, O_WRONLY, 0);

  option = 0;

  while (1)
    {
      o = opt_standard (lim_use_must_malloc, &option, opts, &argc, argv, program_name, usage, version_string, arch_cmd_library_branches_help, opt_help_msg, opt_long_help, opt_version);
      if (o == opt_none)
        break;
      switch (o)
        {
        default:
          safe_printfmt (2, "unhandled option `%s'\n", option->opt_string);
          panic ("internal error parsing arguments");

        usage_error:
          opt_usage (2, argv[0], program_name, usage, 1);
          exit (1);

          /* bogus_arg: */
          safe_printfmt (2, "ill-formed argument for `%s' (`%s')\n", option->opt_string, option->arg_string);
          goto usage_error;

        case opt_archive:
          {
            default_archive = str_save (0, option->arg_string);
            break;
          }
        }
    }

  if (argc > 2)
    goto usage_error;

  default_archive = arch_my_default_archive (default_archive);

  {
    t_uchar * cat_spec = 0;
    t_uchar * archive = 0;
    t_uchar * cat = 0;
    rel_table branches = rel_table_nil;

    if (argc == 2)
      {
        cat_spec = str_save (0, argv[1]);

        if (!arch_valid_package_name (cat_spec, arch_maybe_archive, arch_req_category, 0))
          {
            safe_printfmt (2, "%s: invalid category name (%s)\n",
                           argv[0], cat_spec);
            exit (2);
          }
      }
    else
      cat_spec = arch_try_tree_version (program_name);

    archive = arch_parse_package_name (arch_ret_archive, default_archive, cat_spec);
    cat = arch_parse_package_name (arch_ret_category, 0, cat_spec);

    branches = arch_library_branches (archive, cat);
    rel_print_table (1, branches);

    lim_free (0, cat_spec);
  }

  return 0;
}




/* tag: Tom Lord Wed May 21 14:49:30 2003 (library-branches.c)
 */
