/* archive-snapshot.h:
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */

#ifndef INCLUDE__LIBARCH__ARCHIVE_SNAPSHOT_H
#define INCLUDE__LIBARCH__ARCHIVE_SNAPSHOT_H


#include "hackerlab/machine/types.h"
#include "tla/libarch/archive.h"


/* automatically generated __STDC__ prototypes */
extern void archive_snapshot (struct arch_archive * arch, t_uchar * limit, t_uchar * dir);
#endif  /* INCLUDE__LIBARCH__ARCHIVE_SNAPSHOT_H */


/* tag: Tom Lord Sat Jan  5 15:26:10 2002 (archive-snapshot.h)
 */
