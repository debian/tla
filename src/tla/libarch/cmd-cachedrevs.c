/* cachedrevs.c
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */


#include "config-options.h"
#include "hackerlab/cmd/main.h"
#include "hackerlab/fs/file-names.h"
#include "hackerlab/fs/cwd.h"
#include "tla/libfsutils/tmp-files.h"
#include "tla/libfsutils/rmrf.h"
#include "tla/libarch/namespace.h"
#include "tla/libarch/project-tree.h"
#include "tla/libarch/my.h"
#include "tla/libarch/archive.h"
#include "tla/libarch/pristines.h"
#include "tla/libarch/build-revision.h"
#include "tla/libarch/cmd.h"
#include "tla/libarch/cmd-cachedrevs.h"



static t_uchar * usage = "[options] [version]";
static t_uchar * version_string = (cfg__std__package " from regexps.com\n"
                                   "\n"
                                   "Copyright 2003 Tom Lord\n"
                                   "\n"
                                   "This is free software; see the source for copying conditions.\n"
                                   "There is NO warranty; not even for MERCHANTABILITY or FITNESS FOR A\n"
                                   "PARTICULAR PURPOSE.\n"
                                   "\n"
                                   "Report bugs to " cfg__tla_bug_mail ".\n"
                                   "\n"
                                   cfg__std__release_id_string
                                   "\n");

#define OPTS(OP) \
  OP (opt_help_msg, "h", "help", 0, \
      "Display a help message and exit.") \
  OP (opt_long_help, "H", 0, 0, \
      "Display a verbose help message and exit.") \
  OP (opt_version, "V", "version", 0, \
      "Display a release identifier string\n" \
      "and exit.") \
  OP (opt_archive, "A", "archive", 1, \
      "Override `my-default-archive'")


t_uchar arch_cmd_cachedrevs_help[] = ("list cached revisions in an archive\n"
                                      "Report which revisions of VERSION have been cached\n"
                                      "as whole trees in the archive.\n"
                                      "\n"
                                      "See also \"tla cacherev -H\".\n");

enum options
{
  OPTS (OPT_ENUM)
};

static struct opt_desc opts[] =
{
  OPTS (OPT_DESC)
    {-1, 0, 0, 0, 0}
};



int
arch_cmd_cachedrevs (t_uchar *program_name, int argc, char * argv[])
{
  int o;
  struct opt_parsed * option;
  t_uchar * default_archive = 0;

  safe_buffer_fd (1, 0, O_WRONLY, 0);

  option = 0;

  while (1)
    {
      o = opt_standard (lim_use_must_malloc, &option, opts, &argc, argv, program_name, usage, version_string, arch_cmd_cachedrevs_help, opt_help_msg, opt_long_help, opt_version);
      if (o == opt_none)
        break;
      switch (o)
        {
        default:
          safe_printfmt (2, "unhandled option `%s'\n", option->opt_string);
          panic ("internal error parsing arguments");

        usage_error:
          opt_usage (2, argv[0], program_name, usage, 1);
          exit (1);

          /* bogus_arg: */
          safe_printfmt (2, "ill-formed argument for `%s' (`%s')\n", option->opt_string, option->arg_string);
          goto usage_error;

        case opt_archive:
          {
            lim_free (0, default_archive);
            default_archive = str_save (0, option->arg_string);
            break;
          }
        }
    }

  if (argc > 2)
    goto usage_error;

  default_archive = arch_my_default_archive (default_archive);

  {
    t_uchar * version_spec;
    t_uchar * archive = 0;
    t_uchar * version = 0;
    struct arch_archive * arch = 0;

    if (argc == 2)
      version_spec = str_save(0, argv[1]);
    else
      version_spec = arch_try_tree_version(program_name);

    if (!arch_valid_package_name (version_spec, arch_maybe_archive, arch_req_version, 0))
      {
        safe_printfmt (2, "%s: invalid version spec (%s)\n",
                       argv[0], version_spec);
        exit (2);
      }

    archive = arch_parse_package_name (arch_ret_archive, default_archive, version_spec);
    version = arch_parse_package_name (arch_ret_non_archive, 0, version_spec);
    arch = arch_archive_connect (archive, 0);

    {
      rel_table revisions = rel_table_nil;
      int x;

      revisions = arch_archive_revisions (arch, version, 2);

      for (x = 0; x < rel_n_records (revisions); ++x)
        {
          int is_cached;

          arch_revision_type (0, &is_cached, arch, rel_peek_str (revisions, x, 0));

          if (is_cached)
            safe_printfmt (1, "%s\n", rel_peek_str (revisions, x, 0));
        }

      rel_free_table (revisions);
    }

    arch_archive_close (arch);

    lim_free (0, archive);
    lim_free (0, version);
  }

  lim_free (0, default_archive);
  return 0;
}




/* tag: Tom Lord Thu May 29 23:36:53 2003 (cachedrevs.c)
 */
