/* apply-changeset.h:
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */

#ifndef INCLUDE__LIBARCH__APPLY_CHANGESET_H
#define INCLUDE__LIBARCH__APPLY_CHANGESET_H


#include "hackerlab/machine/types.h"
#include "tla/libarch/changeset-report.h"



#include "hackerlab/machine/types.h"
#include "hackerlab/os/stdarg.h"
#include "tla/libawk/relational.h"
#include "tla/libarch/inv-ids.h"



typedef void (*arch_apply_changeset_report_callback) (void * thunk, const char * fmt, va_list ap);

struct arch_apply_changeset_report
{
  arch_apply_changeset_report_callback callback;
  void * thunk;

  rel_table removed_files;                      /* [0] target-loc [1] id       (sort 0) */
  rel_table removed_dirs;                       /* [0] target-loc [1] id       (sort 0) */

  rel_table missing_removed_files;              /* [0] orig-loc [1] id         (sort 0) */
  rel_table missing_removed_dirs;               /* [0] orig-loc [1] id   (sort 0) */

  rel_table missing_renamed_files;              /* [0] orig-loc [1] mod-loc [2] id   (sort 0) */
  rel_table missing_renamed_dirs;               /* [0] orig-loc [1] mod-loc [2] id   (sort 0) */

  rel_table new_dirs;                           /* [0] target-loc [1] id  (sort 0) */
  rel_table renamed_dirs;                       /* [0] oldtgloc [1] loc [2] id (sort 0) */
  rel_table new_files;                          /* [0] loc [1] id (sort 0) */
  rel_table renamed_files;                      /* [0] oldtgloc [1] loc [2] id (sort 0) */


  rel_table modified_files;                     /* [0] loc [1] id   (sort 0) */
  rel_table modified_dirs;                      /* [0] loc [1] id   (sort 0) */
  rel_table missing_file_for_patch;             /* [0] mod-loc [1] id (sort 0) */
  rel_table missing_dir_for_patch;              /* [0] mod-loc [1] id (sort 0) */

  rel_table meta_modified_files;                /* [0] loc [1] id   (sort 0) */
  rel_table meta_modified_dirs;                 /* [0] loc [1] id   (sort 0) */
  rel_table missing_file_for_meta_patch;        /* [0] mod-loc [1] id (sort 0) */
  rel_table missing_dir_for_meta_patch;         /* [0] mod-loc [1] id (sort 0) */

  rel_table conflict_files;                     /* [0] loc [1] id   (sort 0) */
  rel_table conflict_dirs;                      /* [0] loc [1] id   (sort 0) */
  rel_table metadata_conflict_files;            /* [0] loc [1] id   (sort 0) */
  rel_table metadata_conflict_dirs;             /* [0] loc [1] id   (sort 0) */
};




/* automatically generated __STDC__ prototypes */
extern int arch_conflicts_occured (struct arch_apply_changeset_report * r);
extern void arch_free_apply_changeset_report_data (struct arch_apply_changeset_report * r);
extern void arch_apply_changeset (struct arch_apply_changeset_report * r,
                                  const t_uchar * changeset_spec,
                                  const t_uchar * target_spec,
                                  enum arch_id_tagging_method method,
                                  enum arch_inventory_category untagged_source_category,
                                  int reverse,
                                  int forward,
                                  int use_diff3,
                                  assoc_table older_table,
                                  assoc_table yours_table,
                                  int escape_classes);
#endif  /* INCLUDE__LIBARCH__APPLY_CHANGESET_H */


/* tag: Tom Lord Sat Jan  5 15:26:10 2002 (apply-changeset.h)
 */
