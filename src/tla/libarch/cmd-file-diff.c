/* cmd-file-diff.c
 *
 ****************************************************************
 * Copyright (C) 2003, 2004 Tom Lord
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */


#include "config-options.h"
#include "hackerlab/cmd/main.h"
#include "hackerlab/fs/file-names.h"
#include "tla/libfsutils/dir-as-cwd.h"
#include "tla/libarch/my.h"
#include "tla/libarch/libraries.h"
#include "tla/libarch/project-tree.h"
#include "tla/libarch/patch-logs.h"
#include "tla/libarch/inv-ids.h"
#include "tla/libarch/file-diffs.h"
#include "tla/libarch/namespace.h"
#include "tla/libfsutils/dir-as-cwd.h"
#include "tla/libarch/cmd.h"
#include "tla/libarch/cmd-file-diff.h"



static t_uchar * usage = "[options] file [revision]";
static t_uchar * version_string = (cfg__std__package " from regexps.com\n"
                                   "\n"
                                   "Copyright 2003 Tom Lord\n"
                                   "\n"
                                   "This is free software; see the source for copying conditions.\n"
                                   "There is NO warranty; not even for MERCHANTABILITY or FITNESS FOR A\n"
                                   "PARTICULAR PURPOSE.\n"
                                   "\n"
                                   "Report bugs to " cfg__tla_bug_mail ".\n"
                                   "\n"
                                   cfg__std__release_id_string
                                   "\n");

#define OPTS(OP) \
  OP (opt_help_msg, "h", "help", 0, \
      "Display a help message and exit.") \
  OP (opt_long_help, "H", 0, 0, \
      "Display a verbose help message and exit.") \
  OP (opt_version, "V", "version", 0, \
      "Display a release identifier string\n" \
      "and exit.") \
  OP (opt_archive, "A", "archive", 1, \
      "Override `my-default-archive'") \
  OP (opt_new_file, "N", "new-file", 0, \
      "Treat missing file as empty")

t_uchar arch_cmd_file_diff_help[] = ("show local changes to a file\n"
                                      "Print diffs between FILE and the corresponding file in a cached\n"
                                      "copy of REVISION.\n"
                                      "\n"
                                      "The default patch level for a given version is the latest level for\n"
                                      "which the project tree has a patch.  The default archive and version\n"
                                      "is as printed by \"tla tree-version\".\n");


enum options
{
  OPTS (OPT_ENUM)
};

static struct opt_desc opts[] =
{
  OPTS (OPT_DESC)
    {-1, 0, 0, 0, 0}
};



int
arch_cmd_file_diff (t_uchar * program_name, int argc, char * argv[])
{
  int o;
  struct opt_parsed * option;
  t_uchar * default_archive;
  int new_is_null = 0;
  int status = 2;

  default_archive = 0;

  safe_buffer_fd (1, 0, O_WRONLY, 0);

  option = 0;

  while (1)
    {
      o = opt_standard (lim_use_must_malloc, &option, opts, &argc, argv, program_name, usage, version_string, arch_cmd_file_diff_help, opt_help_msg, opt_long_help, opt_version);
      if (o == opt_none)
        break;
      switch (o)
        {
        default:
          safe_printfmt (2, "unhandled option `%s'\n", option->opt_string);
          panic ("internal error parsing arguments");

        usage_error:
          opt_usage (2, argv[0], program_name, usage, 1);
          exit (1);

          /* bogus_arg: */
          safe_printfmt (2, "ill-formed argument for `%s' (`%s')\n", option->opt_string, option->arg_string);
          goto usage_error;

        case opt_archive:
          {
            default_archive = str_save (0, option->arg_string);
            break;
          }
        case opt_new_file:
          {
            new_is_null = 1;
            break;
          }
        }
    }

  if ((argc < 2) || (argc > 3))
    goto usage_error;

  if (default_archive && !arch_valid_archive_name (default_archive))
    {
      safe_printfmt (2, "%s: invalid archive name (%s)\n",
                     argv[0], default_archive);
      exit (1);
    }

  {
    t_uchar * filespec;
    t_uchar * filedir = 0;
    t_uchar * filedir_path = 0;
    t_uchar * filedir_loc = 0;
    t_uchar * file_tail = 0;
    t_uchar * mod_loc = 0;
    t_uchar * tree_root = 0;
    t_uchar * revspec = 0;
    t_uchar * archive = 0;
    t_uchar * revision = 0;

    filespec = argv[1];

    filedir = file_name_directory_file (0, filespec);
    if (!filedir)
      filedir = str_save (0, ".");

    filedir_path = directory_as_cwd (filedir);

    tree_root = arch_tree_root (0, filedir, 0);
    if (!tree_root)
      {
        safe_printfmt (2, "%s: file is not in a project tree (%s)\n",
                       argv[0], filespec);
        exit (2);
      }

    invariant (!str_cmp_prefix (tree_root, filedir_path)
               && (!filedir_path[str_length (tree_root)] || ('/' == filedir_path[str_length (tree_root)])));

    if (!filedir_path[str_length (tree_root)])
      filedir_loc = str_save (0, "./");
    else
      filedir_loc = str_alloc_cat (0, "./", filedir_path + str_length (tree_root) + 1);

    file_tail = file_name_tail (0, filespec);
    mod_loc = file_name_in_vicinity (0, filedir_loc, file_tail);


    if (argc == 3)
      {
        revspec = str_save (0, argv[2]);
      }
    else
      {
        revspec = arch_tree_version (tree_root);
        if (!revspec)
          {
            safe_printfmt (2, "%s: tree has no default version (%s)\n",
                           argv[0], tree_root);
            exit (2);
          }
      }

    if (!arch_valid_package_name (revspec, arch_maybe_archive, arch_req_version, 1))
      {
        safe_printfmt (2, "%s: invalid revision specification (%s)\n",
                       argv[0], revspec);
        exit (2);
      }


    archive = arch_parse_package_name (arch_ret_archive, default_archive, revspec);
    if (!archive)
      {
        safe_printfmt (2, "%s: no archive specified\n", argv[0]);
        exit (2);
      }


    if (arch_valid_package_name (revspec, arch_maybe_archive, arch_req_version, 0))
      {
        t_uchar * version = 0;
        t_uchar * level = 0;

        version = arch_parse_package_name (arch_ret_package_version, 0, revspec);
        level  = arch_highest_patch_level (tree_root, archive, version);
        revision = str_alloc_cat_many (0, version, "--", level, str_end);

        lim_free (0, level);
        lim_free (0, version);
      }
    else
      {
        revision = arch_parse_package_name (arch_ret_non_archive, 0, revspec);
      }


    status = arch_file_get_or_diff (1, tree_root, mod_loc, archive, revision, 1, new_is_null, 0);


    lim_free (0, filedir);
    lim_free (0, filedir_path);
    lim_free (0, filedir_loc);
    lim_free (0, file_tail);
    lim_free (0, mod_loc);
    lim_free (0, tree_root);
    lim_free (0, revspec);
    lim_free (0, archive);
    lim_free (0, revision);
  }

  lim_free (0, default_archive);

  exit (status);
  return status;
}




/* tag: Tom Lord Fri May 30 21:37:57 2003 (file-diffs.c)
 */
