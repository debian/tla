/* make-changeset.h:
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */

#ifndef INCLUDE__LIBARCH__MAKE_CHANGESET_H
#define INCLUDE__LIBARCH__MAKE_CHANGESET_H


#include "hackerlab/machine/types.h"
#include "hackerlab/os/stdarg.h"
#include "tla/libawk/relational.h"
#include "tla/libawk/associative.h"
#include "tla/libarch/inv-ids.h"
#include "tla/libarch/changeset-utils.h"
#include "tla/libarch/changeset-report.h"



typedef void (*arch_changeset_report_callback) (void * thunk, const char * fmt, va_list ap);

struct arch_make_changeset_report
{
  arch_changeset_report_callback callback;
  void * thunk;

  struct arch_changeset_inventory orig_index;
  struct arch_changeset_inventory mod_index;

  assoc_table orig_dir_id_of;
  assoc_table orig_dir_loc_of;
  assoc_table orig_file_id_of;
  assoc_table orig_file_loc_of;
  assoc_table mod_dir_id_of;
  assoc_table mod_dir_loc_of;
  assoc_table mod_file_id_of;
  assoc_table mod_file_loc_of;
  assoc_table orig_container_dir_id_of_dir_id;
  assoc_table orig_container_dir_id_of_file_id;
  assoc_table mod_container_dir_id_of_dir_id;
  assoc_table mod_container_dir_id_of_file_id;

  rel_table renamed_dirs;
  rel_table renamed_files;
  rel_table modified_files;
  rel_table perms_changed_files;
  rel_table perms_changed_dirs;
  rel_table added_files;
  rel_table added_dirs;
  rel_table removed_files;
  rel_table removed_dirs;
};



/* automatically generated __STDC__ prototypes */
extern void arch_free_make_changeset_report_data (struct arch_make_changeset_report * report);
extern void arch_make_changeset (struct arch_make_changeset_report * report,
                                 const t_uchar * orig_spec,
                                 const t_uchar * mod_spec,
                                 const t_uchar * dest_spec,
                                 enum arch_id_tagging_method method,
                                 enum arch_inventory_category untagged_source_category,
                                 rel_table limits_spec,
                                 assoc_table inode_sig_shortcuts_of_mod,
                                 int link_same,
                                 int escape_classes);
extern void arch_make_empty_changeset (struct arch_make_changeset_report * make_report,
                                       struct arch_changeset_report * report,
                                       const t_uchar * changeset_path);
extern void arch_changeset_rewrite_indexes (const t_uchar * changeset_path,
                                            struct arch_changeset_report * report);
extern int arch_changeset_add_file (t_uchar ** path_ret,
                                    struct arch_changeset_report * report,
                                    struct arch_make_changeset_report * make_report,
                                    const t_uchar * changeset_path,
                                    const t_uchar * mod_loc,
                                    const t_uchar * id);
extern int arch_changeset_add_diffs (struct arch_changeset_report * report,
                                     struct arch_make_changeset_report * make_report,
                                     const t_uchar * changeset_path,
                                     const t_uchar * orig_loc,
                                     const t_uchar * mod_loc,
                                     const t_uchar * id);
extern void arch_make_changeset_compute_container_map (assoc_table * out,
                                                       assoc_table dir_id_of,
                                                       rel_table index);
#endif  /* INCLUDE__LIBARCH__MAKE_CHANGESET_H */


/* tag: Tom Lord Sat Jan  5 15:26:10 2002 (make-changeset.h)
 */
