/* whats-new.h:
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */

#ifndef INCLUDE__LIBARCH__WHATS_NEW_H
#define INCLUDE__LIBARCH__WHATS_NEW_H


#include "tla/libawk/relational.h"


/* automatically generated __STDC__ prototypes */
extern rel_table arch_whats_new (t_uchar * tree_root, struct arch_archive * arch, t_uchar * version, int skip_present);
#endif  /* INCLUDE__LIBARCH__WHATS_NEW_H */


/* tag: Tom Lord Sat Jan  5 15:26:10 2002 (whats-new.h)
 */
