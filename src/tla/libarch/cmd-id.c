/* cmd-id.c
 *
 ****************************************************************
 * Copyright (C) 2001, 2002, 2003  Tom Lord
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */


#include "config-options.h"
#include "hackerlab/cmd/main.h"
#include "hackerlab/char/pika-escaping-utils.h"
#include "tla/libarch/inv-ids.h"
#include "tla/libarch/cmd.h"
#include "tla/libarch/cmd-id.h"



static t_uchar * usage = "[options] file ...";
static t_uchar * version_string = (cfg__std__package " from regexps.com\n"
                                   "\n"
                                   "Copyright 2001, 2002, 2003 Tom Lord\n"
                                   "\n"
                                   "This is free software; see the source for copying conditions.\n"
                                   "There is NO warranty; not even for MERCHANTABILITY or FITNESS FOR A\n"
                                   "PARTICULAR PURPOSE.\n"
                                   "\n"
                                   "Report bugs to " cfg__tla_bug_mail ".\n"
                                   "\n"
                                   cfg__std__release_id_string
                                   "\n");

#define OPTS(OP) \
  OP (opt_help_msg, "h", "help", 0, \
      "Display a help message and exit.") \
  OP (opt_long_help, "H", 0, 0, \
      "Display a verbose help message and exit.") \
  OP (opt_version, "V", "version", 0, \
      "Display a release identifier string\n" \
      "and exit.") \
  OP (opt_implicit, 0, "implicit", 0, \
      "Use the implicit id tagging method.") \
  OP (opt_tagline, 0, "tagline", 0, \
      "Use the tagline id tagging method (default).") \
  OP (opt_explicit, 0, "explicit", 0, \
      "Use the explicit id tagging method.") \
  OP (opt_names, 0, "names", 0, \
      "Use the names id tagging method.") \
  OP (opt_silent, 0, "silent", 0, \
      "No output -- exit status only.") \
  OP (opt_unescaped, 0, "unescaped", 0, \
      "show filenames in unescaped form")

t_uchar arch_cmd_id_help[] = ("report the inventory id for a file\n"
                                   "Print a file's inventory id.\n");

enum options
{
  OPTS (OPT_ENUM)
};

static struct opt_desc opts[] =
{
  OPTS (OPT_DESC)
    {-1, 0, 0, 0, 0}
};



int
arch_cmd_id (t_uchar * program_name, int argc, char * argv[])
{
  int o;
  struct opt_parsed * option;
  int silent;
  enum arch_id_tagging_method method;
  int escape_classes = arch_escape_classes;

  safe_buffer_fd (1, 0, O_WRONLY, 0);

  option = 0;

  silent = 0;
  method = arch_tagline_id_tagging;

  while (1)
    {
      o = opt_standard (lim_use_must_malloc, &option, opts, &argc, argv, program_name, usage, version_string, arch_cmd_id_help, opt_help_msg, opt_long_help, opt_version);
      if (o == opt_none)
        break;
      switch (o)
        {
        default:
          safe_printfmt (2, "unhandled option `%s'\n", option->opt_string);
          panic ("internal error parsing arguments");

        usage_error:
          opt_usage (2, argv[0], program_name, usage, 1);
          exit (1);

          /* bogus_arg: */
          safe_printfmt (2, "ill-formed argument for `%s' (`%s')\n", option->opt_string, option->arg_string);
          goto usage_error;

        case opt_implicit:
          method = arch_implicit_id_tagging;
          break;

        case opt_tagline:
          method = arch_tagline_id_tagging;
          break;

        case opt_explicit:
          method = arch_explicit_id_tagging;
          break;

        case opt_names:
          method = arch_names_id_tagging;
          break;

        case opt_silent:
          silent = 1;
          break;

	case opt_unescaped:
          escape_classes = 0;
          break;

        }
    }

  if (argc < 2)
    goto usage_error;

  {
    int a;

    for (a = 1; a < argc; ++a)
      {
        t_uchar * answer;

        answer = arch_inventory_id (method, 0, argv[a], 0, 0, 0);
      
        if (!answer)
          {
            if (!silent)
              {
                t_uchar * escaped_tmp;
                escaped_tmp = pika_save_escape_iso8859_1 (0, 0, escape_classes, argv[a]);
                safe_printfmt (2, "\n");
                safe_printfmt (2, "%s: untagged file\n", argv[0]);
                safe_printfmt (2, "  %s\n", escaped_tmp);
                safe_printfmt (2, "\n");
                lim_free (0, escaped_tmp);
              }
            exit (1);
          }
        else
          {
            if (!silent)
              {
                t_uchar * escaped_tmp;
                escaped_tmp = pika_save_escape_iso8859_1 (0, 0, escape_classes, argv[a]);
                safe_printfmt (1, "%s\t%s\n", escaped_tmp, answer);
                lim_free (0, escaped_tmp);
              }
            lim_free (0, answer);
          }
      }
  }

  return 0;
}



/* tag: Tom Lord Wed May 14 08:06:44 2003 (inv-tag.c)
 */
