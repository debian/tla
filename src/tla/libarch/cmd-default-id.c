/* cmd-default-id.c
 *
 ****************************************************************
 * Copyright (C) 2001, 2002, 2003  Tom Lord
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */


#include "config-options.h"
#include "hackerlab/cmd/main.h"
#include "tla/libarch/inv-ids.h"
#include "tla/libarch/cmd.h"
#include "tla/libarch/cmd-default-id.h"



static t_uchar * usage = "[options] [TAG-PREFIX]";
static t_uchar * version_string = (cfg__std__package " from regexps.com\n"
                                   "\n"
                                   "Copyright 2001, 2002, 2003 Tom Lord\n"
                                   "\n"
                                   "This is free software; see the source for copying conditions.\n"
                                   "There is NO warranty; not even for MERCHANTABILITY or FITNESS FOR A\n"
                                   "PARTICULAR PURPOSE.\n"
                                   "\n"
                                   "Report bugs to " cfg__tla_bug_mail ".\n"
                                   "\n"
                                   cfg__std__release_id_string
                                   "\n");

#define OPTS(OP) \
  OP (opt_help_msg, "h", "help", 0, \
      "Display a help message and exit.") \
  OP (opt_long_help, "H", 0, 0, \
      "Display a verbose help message and exit.") \
  OP (opt_version, "V", "version", 0, \
      "Display a release identifier string\n" \
      "and exit.") \
  OP (opt_dir, "D", "dir DIR", 1, \
      "cd to DIR first") \
  OP (opt_delete, "d", "delete", 0, \
      "remove the default") \
  OP (opt_strong, "s", "strong", 0, \
      "use the strong default (default)") \
  OP (opt_weak, "w", "weak", 0, \
      "use the weak default") \
  OP (opt_dont_care, 0, "dont-care", 0, \
      "use the dont-care default")


t_uchar arch_cmd_default_id_help[] = ("print or modify default ids\n"
                                       "For files in this directory, use:\n"
                                       "\n"
                                       "     ID-PREFIX__BASENAME\n"
                                       "\n"
                                       "as the default explicit id for all files in this directory that\n"
                                       "do not have an explicit explicit id.\n"
                                       "\n"
                                       "With no arguments, print the previously set ID-PREFIX.\n"
                                       "\n"
                                       "By default, this command sets, prints or deletes a \"strong\n"
                                       "default\" -- a default explicit id which overrides implicit ids.\n"
                                       "\n"
                                       "With --weak, set (or print) a weak default id which is\n"
                                       "overridden by explicit ids.\n"
                                       "\n"
                                       "The --dont-care option sets (or with -d, clears) a flag for that\n"
                                       "directory that causes unidged files not to be reported as such\n"
                                       "in \"tla tree-lint\" reports.\n");

enum options
{
  OPTS (OPT_ENUM)
};

static struct opt_desc opts[] =
{
  OPTS (OPT_DESC)
    {-1, 0, 0, 0, 0}
};



enum which_default
{
  strong,
  weak,
  dont_care
};

enum op
{
  set,
  delete,
  print
};

int
arch_cmd_default_id (t_uchar * program_name, int argc, char * argv[])
{
  int o;
  t_uchar * dir;
  struct opt_parsed * option;
  enum op op;
  enum which_default which;
  t_uchar * set_value;


  safe_buffer_fd (1, 0, O_WRONLY, 0);

  option = 0;
  dir = ".";
  op = print;
  which = strong;
  set_value = 0;


  while (1)
    {
      o = opt_standard (lim_use_must_malloc, &option, opts, &argc, argv, program_name, usage, version_string, arch_cmd_default_id_help, opt_help_msg, opt_long_help, opt_version);
      if (o == opt_none)
        break;
      switch (o)
        {
        default:
          safe_printfmt (2, "unhandled option `%s'\n", option->opt_string);
          panic ("internal error parsing arguments");

        usage_error:
          opt_usage (2, argv[0], program_name, usage, 1);
          exit (1);

          /* bogus_arg: */
          safe_printfmt (2, "ill-formed argument for `%s' (`%s')\n", option->opt_string, option->arg_string);
          goto usage_error;

        case opt_dir:
          {
            dir = str_save (0, option->arg_string);
            break;
          }

        case opt_weak:
          {
            which = weak;
            break;
          }

        case opt_strong:
          {
            which = strong;
            break;
          }

        case opt_dont_care:
          {
            which = dont_care;
            break;
          }

        case opt_delete:
          {
            op = delete;
            break;
          }
        }
    }

  if (op == delete ? (argc > 1) : (argc > 2))
    goto usage_error;

  if (argc == 2)
    op = set;

  switch (op)
    {
    default:
      panic ("default-id hosed");
      exit (2);                 /* notreached */

    case set:
      {
        t_uchar * id_prefix;

        id_prefix = argv[1];

        switch (which)
          {
          default:
            panic ("default-id hosed");
            exit (2);                 /* notreached */

          case strong:
            {
              arch_set_strong_explicit_default (dir, id_prefix);
              break;
            }
          case weak:
            {
              arch_set_weak_explicit_default (dir, id_prefix);
              break;
            }
          case dont_care:
            {
              arch_set_dont_care_explicit_default (dir);
              break;
            }
          }
        break;
      }

    case delete:
      {
        switch (which)
          {
          default:
            panic ("default-id hosed");
            exit (2);                 /* notreached */

          case strong:
            {
              arch_delete_strong_explicit_default (dir);
              break;
            }
          case weak:
            {
              arch_delete_weak_explicit_default (dir);
              break;
            }
          case dont_care:
            {
              arch_delete_dont_care_explicit_default (dir);
              break;
            }
          }
        break;
      }

    case print:
      {
        t_uchar * type;
        t_uchar * file;

        switch (which)
          {
          default:
            panic ("default-id hosed");
            exit (2);                 /* notreached */

          case strong:
            {
              file = arch_strong_explicit_dflt_file (dir);
              type = "strong";

            print_id_file_contents:

              if (safe_access (file, F_OK))
                {
                  safe_printfmt (2, "%s explicit default not set\n", type);
                  exit (1);
                }
              else
                {
                  int in_fd;
                  t_uchar * buf;
                  size_t len;

                  in_fd = safe_open (file, O_RDONLY, 0);
                  buf = 0;
                  len = 0;
                  safe_file_to_string (&buf, &len, in_fd);
                  safe_close (in_fd);
                  safe_write_retry (1, buf, len);
                  exit (0);
                }
              break;
            }
          case weak:
            {
              file = arch_weak_explicit_dflt_file (dir);
              type = "weak";
              goto print_id_file_contents;
            }
          case dont_care:
            {
              file = arch_dont_care_explicit_dflt_file (dir);

              if (safe_access (file, F_OK))
                safe_printfmt (1, "dont-care flag NOT set\n");
              else
                safe_printfmt (1, "dont-care flag SET\n");
              break;
            }
          }
        break;
      }
    }
  exit (3);                     /* notreached */
  return 0;
}



/* tag: Tom Lord Wed May 14 13:56:06 2003 (default-tag.c)
 */
