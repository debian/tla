/* pfs-sftp.h:
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */

#ifndef INCLUDE__LIBARCH__PFS_SFTP_H
#define INCLUDE__LIBARCH__PFS_SFTP_H


#include "tla/libarch/pfs.h"



/* automatically generated __STDC__ prototypes */
extern int arch_pfs_sftp_supported_protocol (const t_uchar * uri);
extern struct arch_pfs_session * arch_pfs_sftp_connect (const t_uchar * uri);
extern int arch_pfs_sftp_parse_uri (t_uchar ** user,
                                    t_uchar ** hostname,
                                    t_uchar ** port,
                                    char ** path,
                                    const t_uchar * uri);
#endif  /* INCLUDE__LIBARCH__PFS_SFTP_H */


/* tag: Tom Lord Sat Jan  5 15:26:10 2002 (pfs-sftp.h)
 */
