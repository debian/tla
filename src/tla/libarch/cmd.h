/* cmd.h:
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */

#ifndef INCLUDE__LIBARCH__CMD_H
#define INCLUDE__LIBARCH__CMD_H


#include "hackerlab/machine/types.h"
#include "tla/libarch/arch.h"



typedef int (*arch_cmd_fn) (t_uchar * prog_name, int argc, char * argv[]);


/* automatically generated __STDC__ prototypes */
extern int arch_call_cmd (arch_cmd_fn fn, t_uchar * prog_name, ...);
  /* must terminate list with (char*)0, NOT 0 or NULL ! */
#endif  /* INCLUDE__LIBARCH__CMD_H */


/* tag: Tom Lord Sat Jan  5 15:26:10 2002 (cmd.h)
 */
