/* arbdelta.c:
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */


#include "hackerlab/vu/safe.h"
#include "hackerlab/mem/alloc-limits.h"
#include "tla/libarch/local-cache.h"
#include "tla/libarch/build-revision.h"
#include "tla/libarch/make-changeset.h"
#include "tla/libarch/inode-sig.h"
#include "tla/libarch/chatter.h"
#include "tla/libarch/arbdelta.h"


/* __STDC__ prototypes for static functions */
static void arbdelta_callback (void * vfd,
                               const char * fmt,
                               va_list ap);



void
arch_arbitrary_delta (int chatter_fd, t_uchar * output_dir, t_uchar * tmp_dir, t_uchar * tree_root,
                      struct arch_archive * orig_arch, t_uchar * orig_archive, t_uchar * orig_revision,
                      struct arch_archive * mod_arch, t_uchar * mod_archive, t_uchar * mod_revision,
                      int escape_classes)
{
  t_uchar * orig = 0;
  t_uchar * mod = 0;
  struct arch_make_changeset_report report = {0, };
  assoc_table inode_shortcut = 0;

  arch_chatter (chatter_fd, "* finding or building %s/%s\n", orig_archive, orig_revision);

  orig = arch_find_or_make_tmp_local_copy (chatter_fd, tmp_dir, tree_root, 0, orig_arch, orig_archive, orig_revision);

  arch_chatter (chatter_fd, "* finding or building %s/%s\n", mod_archive, mod_revision);

  mod = arch_find_or_make_tmp_local_copy (chatter_fd, tmp_dir, tree_root, 0, mod_arch, mod_archive, mod_revision);

  if (chatter_fd >= 0)
    {
      report.callback = arbdelta_callback;
      report.thunk = (void *)(long)chatter_fd;
      arch_chatter (chatter_fd, "* computing changeset\n");
    }

  arch_read_inode_sig (0, &inode_shortcut, mod, mod_archive, mod_revision);
  arch_make_changeset (&report, orig, mod, output_dir, arch_unspecified_id_tagging, arch_inventory_unrecognized, rel_table_nil, inode_shortcut, 0, escape_classes);


  lim_free (0, orig);
  lim_free (0, mod);
  free_assoc_table (inode_shortcut);
}


static void
arbdelta_callback (void * vfd,
                   const char * fmt,
                   va_list ap)
{
  safe_printfmt_va_list ((int)(t_ulong)vfd, fmt, ap);
  safe_flush ((int)(t_ulong)vfd);
}



/* tag: Tom Lord Wed Jun  4 01:19:04 2003 (arbdelta.c)
 */
