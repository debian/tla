/* cmd-rm.h:
 *
 ****************************************************************
 * Copyright (C) 2003, 2004  Tom Lord, Stefanie Tellex
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */

#ifndef INCLUDE__LIBARCH__CMD_RM_H
#define INCLUDE__LIBARCH__CMD_RM_H


#include "hackerlab/machine/types.h"



extern t_uchar arch_cmd_rm_help[];


/* automatically generated __STDC__ prototypes */
extern int arch_cmd_rm (t_uchar * program_name, int argc, char * argv[]);
#endif  /* INCLUDE__LIBARCH__CMD_RM_H */


/* tag: Rob Weir Sun Sep 21 15:22:29 EST 2003 (cmd-rm.h)
 */
