/* pristines.h:
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */

#ifndef INCLUDE__LIBARCH__PRISTINES_H
#define INCLUDE__LIBARCH__PRISTINES_H


#include "hackerlab/machine/types.h"
#include "tla/libawk/relational.h"
#include "tla/libarch/archive.h"



enum arch_pristine_types
{
  arch_unlocked_pristine = 1,   /* must be first */
  arch_locked_pristine = 2,
  arch_any_pristine = (arch_unlocked_pristine | arch_locked_pristine),
};

enum arch_pristine_search_scope
{
  arch_tree_pristine_search,
  arch_tree_and_sibling_pristine_search,
  arch_cache_dir_pristine_search,
};



/* automatically generated __STDC__ prototypes */
extern void arch_make_pristine (const t_uchar * tree_root,
                                const t_uchar * archive,
                                const t_uchar * revision);
extern void arch_add_pristine (int chatter_fd,
                               const t_uchar * tree_root,
                               struct arch_archive * arch,
                               const t_uchar * archive,
                               const t_uchar * revision);
extern t_uchar * arch_pristine_loc (const t_uchar * archive,
                                    const t_uchar * revision,
                                    int locked_p);
extern t_uchar * arch_pristine_path (const t_uchar * tree_root,
                                     const t_uchar * archive,
                                     const t_uchar * revision,
                                     int locked_p);
extern void arch_install_pristine (const t_uchar * tree_root,
                                   const t_uchar * archive,
                                   const t_uchar * revision,
                                   const t_uchar * source);
extern void arch_lock_pristine (const t_uchar * tree_root,
                                const t_uchar * archive,
                                const t_uchar * revision);
extern void arch_unlock_pristine (const t_uchar * tree_root,
                                  const t_uchar * archive,
                                  const t_uchar * revision);
extern rel_table arch_pristines (const t_uchar * tree_root,
                                 const t_uchar * archive_limit,
                                 const t_uchar * limit,
                                 int pristine_types);
extern t_uchar * arch_find_pristine (int * is_locked,
                                     const t_uchar * tree_root_or_root_dir,
                                     const t_uchar * archive,
                                     const t_uchar * revision,
                                     int pristine_types,
                                     enum arch_pristine_search_scope scope);
#endif  /* INCLUDE__LIBARCH__PRISTINES_H */


/* tag: Tom Lord Sat Jan  5 15:26:10 2002 (pristines.h)
 */
