/* cmd-logs.c
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */


#include "config-options.h"
#include "hackerlab/cmd/main.h"
#include "tla/libfsutils/file-contents.h"
#include "tla/libarch/namespace.h"
#include "tla/libarch/project-tree.h"
#include "tla/libarch/patch-logs.h"
#include "tla/libarch/cmd-logs.h"



static t_uchar * usage = "[options] [[archive]/version ...]";
static t_uchar * version_string = (cfg__std__package " from regexps.com\n"
                                   "\n"
                                   "Copyright 2003 Tom Lord\n"
                                   "\n"
                                   "This is free software; see the source for copying conditions.\n"
                                   "There is NO warranty; not even for MERCHANTABILITY or FITNESS FOR A\n"
                                   "PARTICULAR PURPOSE.\n"
                                   "\n"
                                   "Report bugs to " cfg__tla_bug_mail ".\n"
                                   "\n"
                                   cfg__std__release_id_string
                                   "\n");

#define OPTS(OP) \
  OP (opt_help_msg, "h", "help", 0, \
      "Display a help message and exit.") \
  OP (opt_long_help, "H", 0, 0, \
      "Display a verbose help message and exit.") \
  OP (opt_version, "V", "version", 0, \
      "Display a release identifier string\n" \
      "and exit.") \
  OP (opt_archive, "A", "archive", 1, \
      "Override `my-default-archive'") \
  OP (opt_dir, "d", "dir DIR", 1, \
      "cd to DIR first") \
  OP (opt_reverse, "r", "reverse", 0, \
      "sort from newest to oldest") \
  OP (opt_summary, "s", "summary", 0, \
      "print the summary of each patch") \
  OP (opt_creator, "c", "creator", 0, \
      "print the creator of each patch") \
  OP (opt_date, "D", "date", 0, \
      "print the date of each patch") \
  OP (opt_local_merges, 0, "local-merges", 0, \
      "list merges from the same archive") \
  OP (opt_foreign_merges, 0, "foreign-merges", 0, \
      "list merges from other archives") \
  OP (opt_all_merges, 0, "merges", 0, \
      "list all merges") \
  OP (opt_full, "f", "full", 0, \
      "print full patch level names")


t_uchar arch_cmd_logs_help[] = ("list patch logs for a version in a project tree\n"
                                  "Print the list of patches applied to a project tree\n"
                                  "DIR (or the current directory) from VERSION.\n");

enum options
{
  OPTS (OPT_ENUM)
};

static struct opt_desc opts[] =
{
  OPTS (OPT_DESC)
    {-1, 0, 0, 0, 0}
};



int
arch_cmd_logs (t_uchar * program_name, int argc, char * argv[])
{
  int o;
  struct opt_parsed * option;
  t_uchar * default_archive = 0;
  char * dir = 0;
  int reverse = 0;
  int summarized_headers = 0;
  int full = 0;

  default_archive = 0;
  dir = str_save (0, ".");

  safe_buffer_fd (1, 0, O_WRONLY, 0);

  option = 0;

  while (1)
    {
      o = opt_standard (lim_use_must_malloc, &option, opts, &argc, argv, program_name, usage, version_string, arch_cmd_logs_help, opt_help_msg, opt_long_help, opt_version);
      if (o == opt_none)
        break;
      switch (o)
        {
        default:
          safe_printfmt (2, "unhandled option `%s'\n", option->opt_string);
          panic ("internal error parsing arguments");

        usage_error:
          opt_usage (2, argv[0], program_name, usage, 1);
          exit (1);

          /* bogus_arg: */
          safe_printfmt (2, "ill-formed argument for `%s' (`%s')\n", option->opt_string, option->arg_string);
          goto usage_error;

        case opt_archive:
          {
            default_archive = str_save (0, option->arg_string);
            break;
          }

        case opt_dir:
          {
            dir = str_save (0, option->arg_string);
            break;
          }

        case opt_reverse:
          {
            reverse = 1;
            break;
          }

        case opt_summary:
          {
            summarized_headers |= arch_include_summary;
            break;
          }
        case opt_creator:
          {
            summarized_headers |= arch_include_creator;
            break;
          }
        case opt_date:
          {
            summarized_headers |= arch_include_date;
            break;
          }
        case opt_local_merges:
          {
            summarized_headers |= arch_include_local_merges;
            break;
          }
        case opt_foreign_merges:
          {
            summarized_headers |= arch_include_foreign_merges;
            break;
          }
        case opt_all_merges:
          {
            summarized_headers |= arch_include_local_merges;
            summarized_headers |= arch_include_foreign_merges;
            break;
          }

        case opt_full:
          {
            full = 1;
            break;
          }
        }
    }

  {
    t_uchar * tree_root = 0;
    char ** new_argv = 0;
    int a;

    tree_root = arch_tree_root (0, dir, 0);

    if (!tree_root)
      {
        safe_printfmt (2, "%s: not in project tree (%s)\n", argv[0], dir);
        exit (1);
      }

    if (argc < 2)
      {
        t_uchar * tree_version;

        tree_version = arch_tree_version (tree_root);

        if (!tree_version)
          {
            safe_printfmt (2, "%s: no default tree version\n  tree: %s\n",
                           argv[0], tree_root);
            exit (1);
          }

        *(char **)ar_push ((void **)&new_argv, 0, sizeof (char *)) = str_save (0, argv[0]);
        *(char **)ar_push ((void **)&new_argv, 0, sizeof (char *)) = str_save (0, tree_version);
        argc = 2;
        argv = new_argv;

        lim_free (0, tree_version);
      }

    if (default_archive)
      {
        if (!arch_valid_archive_name (default_archive))
          {
            safe_printfmt (2, "%s: invalid archive name (%s)\n",
                           argv[0], default_archive);
            exit (1);
          }
      }

    for (a = 1; a < argc; ++a)
      {
        t_uchar * version_spec;
        t_uchar * archive = 0;
        t_uchar * version = 0;
        t_uchar * fqversion = 0;
        rel_table log_ls = rel_table_nil;
        int x;

        version_spec = argv[a];

        if (!arch_valid_package_name (version_spec, arch_maybe_archive, arch_req_version, 0))
          {
            safe_printfmt (2, "%s: invalid version name (%s)\n",
                           argv[0], version_spec);
            exit (1);
          }

        archive = arch_parse_package_name (arch_ret_archive, default_archive, version_spec);
        version = arch_parse_package_name (arch_ret_non_archive, 0, version_spec);
        fqversion = arch_fully_qualify (archive, version);

        log_ls = arch_logs (tree_root, archive, version, 0);
        arch_sort_table_by_patch_level_field (reverse, log_ls, 0);

        for (x = 0; x < rel_n_records (log_ls); ++x)
          {
            if (!full)
              safe_printfmt (1, "%s\n", rel_peek_str (log_ls, x, 0));
            else
              safe_printfmt (1, "%s--%s\n", fqversion, rel_peek_str (log_ls, x, 0));

            if (summarized_headers)
              {
                t_uchar * log = 0;
                assoc_table headers = 0;
                t_uchar * body = 0;

                log = file_contents (rel_peek_str (log_ls, x, 1));
                arch_parse_log (0, &headers, (const t_uchar **)&body, log);
                arch_print_headers_summary (1, 4, headers, summarized_headers);

                lim_free (0, log);
                free_assoc_table (headers);
                lim_free (0, body);
              }
          }

        lim_free (0, archive);
        lim_free (0, version);
        lim_free (0, fqversion);
        rel_free_table (log_ls);
      }

    lim_free (0, tree_root);
    if (new_argv)
      {
        for (a = 0; a < argc; ++a)
          {
            lim_free (0, new_argv[a]);
          }
        ar_free ((void **)&new_argv, 0);
      }
  }

  lim_free (0, default_archive);
  lim_free (0, dir);

  return 0;
}




/* tag: Tom Lord Tue May 13 13:19:49 2003 (log-ls.c)
 */
