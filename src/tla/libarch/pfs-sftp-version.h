/* pfs-sftp-version.h:
 *
 ****************************************************************
 * Copyright (C) 2004 Johannes Berg
 * Copyright (C) 2006 Ludovic Court�s
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */

#ifndef INCLUDE__LIBARCH__PFS_SFTP_VERSION_H
#define INCLUDE__LIBARCH__PFS_SFTP_VERSION_H


#include "hackerlab/machine/types.h"



enum arch_ssh_type
  {
    ssh_type_unknown,
    ssh_type_lsh,
    ssh_type_openssh,
    ssh_type_fsecure3,
    ssh_type_psftp
  };


/* automatically generated __STDC__ prototypes */
extern enum arch_ssh_type arch_pfs_sftp_ssh_type (void);
extern const t_uchar * arch_pfs_sftp_ssh_binary (enum arch_ssh_type);
#endif  /* INCLUDE__LIBARCH__PFS_SFTP_H */


/* tag: pfs-sftp-version.h by Johannes Berg  (17:00 Feb 13 2004)
*/
