/* archive-mirror.c
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */


#include "config-options.h"
#include "hackerlab/cmd/main.h"
#include "tla/libarch/namespace.h"
#include "tla/libarch/archive.h"
#include "tla/libarch/archives.h"
#include "tla/libarch/archive-mirror.h"
#include "tla/libarch/my.h"
#include "tla/libarch/cmd.h"
#include "tla/libarch/cmd-archive-mirror.h"



static t_uchar * usage = "[options] [from [to] [limit]]";
static t_uchar * version_string = (cfg__std__package " from regexps.com\n"
                                   "\n"
                                   "Copyright 2003 Tom Lord\n"
                                   "\n"
                                   "This is free software; see the source for copying conditions.\n"
                                   "There is NO warranty; not even for MERCHANTABILITY or FITNESS FOR A\n"
                                   "PARTICULAR PURPOSE.\n"
                                   "\n"
                                   "Report bugs to " cfg__tla_bug_mail ".\n"
                                   "\n"
                                   cfg__std__release_id_string
                                   "\n");

#define OPTS(OP) \
  OP (opt_help_msg, "h", "help", 0, \
      "Display a help message and exit.") \
  OP (opt_long_help, "H", 0, 0, \
      "Display a verbose help message and exit.") \
  OP (opt_version, "V", "version", 0, \
      "Display a release identifier string\n" \
      "and exit.") \
  OP (opt_no_cachedrevs, 0, "no-cached", 0, \
      "don't copy cached revisions") \
  OP (opt_summary, "s", "summary", 0, \
      "print the summary of each patch") \
  OP (opt_id_cachedrevs, 0, "cached-tags", 0, \
      "copy only cachedrevs for tags to other archives")


t_uchar arch_cmd_archive_mirror_help[] = ("update an archive mirror\n"
                                          "If no arguments are given, update your `my-default-archive'-MIRROR\n"
                                          "archive with the contents of `my-default-archive'.\n"
                                          "\n"
                                          "If a [FROM] archive is given, update the [FROM]-MIRROR archive with\n"
                                          "the contents of the [FROM] archive\n"
                                          "\n"
                                          "If both [FROM] and [TO] archives are specified, update [TO] with\n"
                                          "the contents of [FROM]\n"
                                          "\n"
                                          "If LIMIT is provided, it should be a category, branch,\n"
                                          "version, or revision name.   Only the indicated part\n"
                                          "of FROM will be copied to TO. If LIMIT is a revision,\n"
					  "then cached revisions will be copied and deleted to TO.\n"
                                          "\n"
                                          "(see \"tla make-archive -H\".).\n");

enum options
{
  OPTS (OPT_ENUM)
};

static struct opt_desc opts[] =
{
  OPTS (OPT_DESC)
    {-1, 0, 0, 0, 0}
};



int
arch_cmd_archive_mirror (t_uchar * program_name, int argc, char * argv[])
{
  int o;
  struct opt_parsed * option;
  struct arch_archive_mirror_options * mirror_opts = 0;

  mirror_opts = lim_malloc (0, sizeof (*mirror_opts));
  mem_set0 ((t_uchar *)mirror_opts, sizeof (*mirror_opts));
  
  mirror_opts->print_summary = 0;
  mirror_opts->cachedrevs = arch_mirror_all_cachedrevs;
  
  safe_buffer_fd (1, 0, O_WRONLY, 0);

  option = 0;

  while (1)
    {
      o = opt_standard (lim_use_must_malloc, &option, opts, &argc, argv, program_name, usage, version_string, arch_cmd_archive_mirror_help, opt_help_msg, opt_long_help, opt_version);
      if (o == opt_none)
        break;
      switch (o)
        {
        default:
          safe_printfmt (2, "unhandled option `%s'\n", option->opt_string);
          panic ("internal error parsing arguments");

        usage_error:
          opt_usage (2, argv[0], program_name, usage, 1);
          exit (1);

          /* bogus_arg: */
          safe_printfmt (2, "ill-formed argument for `%s' (`%s')\n", option->opt_string, option->arg_string);
          goto usage_error;

        case opt_no_cachedrevs:
          {
            mirror_opts->cachedrevs = arch_mirror_skip_cachedrevs;
            break;
          }

        case opt_summary:
          {
            mirror_opts->print_summary = 1;
            break;
          }

        case opt_id_cachedrevs:
          {
            mirror_opts->cachedrevs = arch_mirror_foreign_continuation_cachedrevs;
            break;
          }
        }
    }

  if (argc > 4)
    goto usage_error;

  {
    int a;
    t_uchar * from_name = 0;
    t_uchar * ambiguous_param = 0;
    t_uchar * to_name = 0;
    t_uchar * limit_spec = 0;
    struct arch_archive * from = 0;
    t_uchar * official_from_name = 0;
    struct arch_archive * to = 0;


    a = 1;

    if (a >= argc)
      {
        from_name = arch_my_default_archive (0);
        if (!from_name)
          {
            safe_printfmt (2, "%s: no default archive set\n", argv[0]);
            exit (1);
          }
      }
    else
      {
        from_name = str_save (0, argv[a]);
        ++a;
      }

    if (a < argc)
      {
        ambiguous_param = str_save (0, argv[2]);
        ++a;
      }

    if (ambiguous_param && arch_valid_archive_name (ambiguous_param))
      {
        to_name = str_save (0, ambiguous_param);
        limit_spec = (argc > a ? str_save (0, argv[a]) : 0);
      }
    else
      {
        if (argc > (a + 1))
          goto usage_error;
        to_name = arch_mirrored_at (from_name);
        if (!to_name)
          {
            to_name = arch_mirrored_from (from_name);
            if (to_name)
              {
                t_uchar * t;
                t = from_name;
                from_name = to_name;
                to_name = t;
              }
            else
              {
                safe_printfmt (2, "%s: no mirror or source registered for %s\n",
                               argv[0], from_name);
                exit (1);
              }
          }
        if (ambiguous_param)
          {
            limit_spec = ambiguous_param;
            ambiguous_param = 0;
          }
      }

    if (limit_spec && !arch_valid_package_name (limit_spec, arch_no_archive, arch_req_category, 1))
      {
        safe_printfmt (2, "%s: invalid limit spec name (%s)\n",
                       argv[0], limit_spec);
        exit (2);
      }

    from = arch_archive_connect (from_name, 0);
    official_from_name = arch_get_meta_info_trimming (from, "name");

    to = arch_archive_connect (to_name, official_from_name);

    arch_archive_mirror (1, from, to, limit_spec, mirror_opts);

    arch_archive_close (to);
    lim_free (0, official_from_name);
    arch_archive_close (from);

    lim_free (0, from_name);
    lim_free (0, ambiguous_param);
    lim_free (0, to_name);
    lim_free (0, limit_spec);
  }

  lim_free (0, mirror_opts);

  return 0;
}



/* tag: Tom Lord Tue Jun 10 14:15:27 2003 (cmd-archive-mirror.c)
 */
