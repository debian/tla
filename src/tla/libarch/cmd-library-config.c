/* cmd-library-config.c
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */


#include "config-options.h"
#include "hackerlab/cmd/main.h"
#include "tla/libarch/namespace.h"
#include "tla/libarch/project-tree.h"
#include "tla/libarch/my.h"
#include "tla/libarch/libraries.h"
#include "tla/libarch/cmd-library-config.h"



static t_uchar * usage = "[options] library-dir";
static t_uchar * version_string = (cfg__std__package " from regexps.com\n"
                                   "\n"
                                   "Copyright 2003 Tom Lord\n"
                                   "\n"
                                   "This is free software; see the source for copying conditions.\n"
                                   "There is NO warranty; not even for MERCHANTABILITY or FITNESS FOR A\n"
                                   "PARTICULAR PURPOSE.\n"
                                   "\n"
                                   "Report bugs to " cfg__tla_bug_mail ".\n"
                                   "\n"
                                   cfg__std__release_id_string
                                   "\n");

#define OPTS(OP) \
  OP (opt_help_msg, "h", "help", 0, \
      "Display a help message and exit.") \
  OP (opt_long_help, "H", 0, 0, \
      "Display a verbose help message and exit.") \
  OP (opt_version, "V", "version", 0, \
      "Display a release identifier string\n" \
      "and exit.") \
  OP (opt_greedy, 0, "greedy", 0, \
      "make the library greedy") \
  OP (opt_non_greedy, 0, "non-greedy", 0, \
      "make the library not greedy") \
  OP (opt_sparse, 0, "sparse", 0, \
      "make the library sparse") \
  OP (opt_non_sparse, 0, "non-sparse", 0, \
      "make the library not sparse")


t_uchar arch_cmd_library_config_help[] = ("configure parameters of a revision library\n"
                                            "Set/show various parameters for a revision library.\n");

enum options
{
  OPTS (OPT_ENUM)
};

static struct opt_desc opts[] =
{
  OPTS (OPT_DESC)
    {-1, 0, 0, 0, 0}
};



int
arch_cmd_library_config (t_uchar * program_name, int argc, char * argv[])
{
  int o;
  struct opt_parsed * option;
  int make_greedy = 0;
  int make_non_greedy = 0;
  int make_sparse = 0;
  int make_non_sparse = 0;

  safe_buffer_fd (1, 0, O_WRONLY, 0);

  option = 0;

  while (1)
    {
      o = opt_standard (lim_use_must_malloc, &option, opts, &argc, argv, program_name, usage, version_string, arch_cmd_library_config_help, opt_help_msg, opt_long_help, opt_version);
      if (o == opt_none)
        break;
      switch (o)
        {
        default:
          safe_printfmt (2, "unhandled option `%s'\n", option->opt_string);
          panic ("internal error parsing arguments");

        usage_error:
          opt_usage (2, argv[0], program_name, usage, 1);
          exit (1);

          /* bogus_arg: */
          safe_printfmt (2, "ill-formed argument for `%s' (`%s')\n", option->opt_string, option->arg_string);
          goto usage_error;

        case opt_greedy:
          {
            make_greedy = 1;
            make_non_greedy = 0;
            break;
          }

        case opt_non_greedy:
          {
            make_non_greedy = 1;
            make_greedy = 0;
            break;
          }

        case opt_sparse:
          {
            make_sparse = 1;
            make_non_sparse = 0;
            break;
          }

        case opt_non_sparse:
          {
            make_non_sparse = 1;
            make_sparse = 0;
            break;
          }
        }
    }

  if (argc != 2)
    goto usage_error;

  {
    t_uchar * lib = argv[1];

    arch_verify_is_library (lib);

    if (make_greedy)
      arch_set_library_greediness (lib, 1);

    if (make_non_greedy)
      arch_set_library_greediness (lib, 0);

    if (make_sparse)
      arch_set_library_sparseness (lib, 1);

    if (make_non_sparse)
      arch_set_library_sparseness (lib, 0);

    safe_printfmt (1, "* library %s\n", lib);

    if (arch_library_is_greedy (lib))
      safe_printfmt (1, "greedy? yes\n");
    else
      safe_printfmt (1, "greedy? no\n");

    if (arch_library_is_sparse (lib))
      safe_printfmt (1, "sparse? yes\n");
    else
      safe_printfmt (1, "sparse? no\n");
  }

   return 0;
}




/* tag: Tom Lord Fri Dec  5 16:35:15 2003 (cmd-library-config.c)
 */
