/* cmd-apply-changeset.c
 *
 ****************************************************************
 * Copyright (C) 2001, 2002, 2003, 2004  Tom Lord
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */


#include "config-options.h"
#include "hackerlab/cmd/main.h"
#include "hackerlab/mem/mem.h"
#include "tla/libarch/apply-changeset.h"
#include "tla/libarch/cmd.h"
#include "tla/libarch/cmdutils.h"
#include "tla/libarch/cmd-apply-changeset.h"


/* __STDC__ prototypes for static functions */
static void changeset_callback (void * ign, const char * fmt, va_list ap);



static t_uchar * usage = "[options] CHANGESET [TARGET]";
static t_uchar * version_string = (cfg__std__package " from regexps.com\n"
                                   "\n"
                                   "Copyright 2001, 2002, 2003 Tom Lord\n"
                                   "\n"
                                   "This is free software; see the source for copying conditions.\n"
                                   "There is NO warranty; not even for MERCHANTABILITY or FITNESS FOR A\n"
                                   "PARTICULAR PURPOSE.\n"
                                   "\n"
                                   "Report bugs to " cfg__tla_bug_mail ".\n"
                                   "\n"
                                   cfg__std__release_id_string
                                   "\n");

#define OPTS(OP) \
  OP (opt_help_msg, "h", "help", 0, \
      "Display a help message and exit.") \
  OP (opt_long_help, "H", 0, 0, \
      "Display a verbose help message and exit.") \
  OP (opt_version, "V", "version", 0, \
      "Display a release identifier string\n" \
      "and exit.") \
  OP (opt_forward, "N", "forward", 0, \
      "pass the --forward option to `patch'") \
  OP (opt_reverse, "r", "reverse", 0, \
      "Apply the changeset in reverse") \
  OP (opt_unescaped, 0, "unescaped", 0, \
      "show filenames in unescaped form")

t_uchar arch_cmd_apply_changeset_help[] = ("apply a whole-tree changeset\n"
					   "Apply the changeset CHANGESET to the source tree TARGET (default `.').\n"
					   "\n"
					   "See also \"tla changeset -H\".\n");

enum options
{
  OPTS (OPT_ENUM)
};

static struct opt_desc opts[] =
{
  OPTS (OPT_DESC)
    {-1, 0, 0, 0, 0}
};



int
arch_cmd_apply_changeset (t_uchar * program_name, int argc, char * argv[])
{
  int o;
  struct opt_parsed * option;
  int reverse = 0;
  int forward = 0;
  int escape_classes = arch_escape_classes;
  char *target_dir = 0;

  safe_buffer_fd (1, 0, O_WRONLY, 0);

  option = 0;

  while (1)
    {
      o = opt_standard (lim_use_must_malloc, &option, opts, &argc, argv, program_name, usage, version_string, arch_cmd_apply_changeset_help, opt_help_msg, opt_long_help, opt_version);
      if (o == opt_none)
        break;
      switch (o)
        {
        default:
          safe_printfmt (2, "unhandled option `%s'\n", option->opt_string);
          panic ("internal error parsing arguments");

        usage_error:
          opt_usage (2, argv[0], program_name, usage, 1);
          exit (1);

          /* bogus_arg: */
          safe_printfmt (2, "ill-formed argument for `%s' (`%s')\n", option->opt_string, option->arg_string);
          goto usage_error;

        case opt_reverse:
          {
            reverse = 1;
            break;
          }

        case opt_forward:
          {
            forward = 1;
            break;
          }

	case opt_unescaped:
	  {
	    escape_classes = 0;
	    break;
	  }
        }
    }

  if (argc == 3)
    target_dir = argv[2];
  else if (argc == 2)
    target_dir = ".";
  else
    goto usage_error;

  {
    struct arch_apply_changeset_report report;

    mem_set0 ((void *)&report, sizeof (report));

    report.callback = changeset_callback;

    arch_apply_changeset (&report, argv[1], target_dir, arch_unspecified_id_tagging, arch_inventory_unrecognized, reverse, forward, 0, 0, 0, escape_classes);
    arch_check_directory (argv[1], 0);
    arch_check_directory (argv[2], 1);
    if (arch_conflicts_occured (&report))
      {
        safe_printfmt (2, "%s: conflicts occured\n", argv[0]);
        exit (1);
      }
  }

  return 0;
}


static void
changeset_callback (void * ign, const char * fmt, va_list ap)
{
  safe_printfmt_va_list (1, fmt, ap);
}





/* tag: Tom Lord Fri May 16 09:59:03 2003 (do-changeset.c)
 */
