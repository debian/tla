/* sync-tree.h:
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */

#ifndef INCLUDE__LIBARCH__SYNC_TREE_H
#define INCLUDE__LIBARCH__SYNC_TREE_H


#include "tla/libarch/archive.h"


/* automatically generated __STDC__ prototypes */
extern void arch_sync_tree (int chatter_fd, t_uchar * tree_root, struct arch_archive * arch, t_uchar * archive, t_uchar * revision);
#endif  /* INCLUDE__LIBARCH__SYNC_TREE_H */


/* tag: Tom Lord Sat Jan  5 15:26:10 2002 (sync-tree.h)
 */
