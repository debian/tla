/* archive-cache.h:
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */

#ifndef INCLUDE__LIBARCH__ARCHIVE_CACHE_H
#define INCLUDE__LIBARCH__ARCHIVE_CACHE_H


#include "tla/libarch/archive.h"


/* automatically generated __STDC__ prototypes */
extern void arch_archive_cache (int chatter_fd,
                                struct arch_archive * arch,
                                const t_uchar * archive,
                                const t_uchar * revision,
                                const t_uchar * cache_dir);
#endif  /* INCLUDE__LIBARCH__ARCHIVE_CACHE_H */


/* tag: Tom Lord Sat Jan  5 15:26:10 2002 (archive-cache.h)
 */
