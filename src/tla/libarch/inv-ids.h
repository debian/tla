/* inv-ids.h: file inventory ids
 *
 ****************************************************************
 * Copyright (C) 2002, 2003 Tom Lord
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */

#ifndef INCLUDE__LIBARCH__INV_IDS_H
#define INCLUDE__LIBARCH__INV_IDS_H


#include "hackerlab/machine/types.h"
#include "hackerlab/os/sys/stat.h"
#include "tla/libawk/associative.h"
#include "tla/libawk/relational.h"



enum arch_id_tagging_method
{
  arch_names_id_tagging,
  arch_implicit_id_tagging,
  arch_tagline_id_tagging,
  arch_explicit_id_tagging,

  arch_unspecified_id_tagging,     /* used in make/apply_changeset */
};


enum arch_inventory_category
{
  arch_inventory_source = 1,
  arch_inventory_precious = 2,
  arch_inventory_backup = 4,
  arch_inventory_junk = 8,
  arch_inventory_tree = 16,
  arch_inventory_unrecognized = 32,
  arch_inventory_excludes = 64
};




/* automatically generated __STDC__ prototypes */
extern t_uchar * arch_log_file_id (const t_uchar * archive,
                                   const t_uchar * revision);
extern assoc_table arch_filenames_ids (rel_table * file_list,
                                       const t_uchar * tree_root);
extern t_uchar * arch_inventory_id (enum arch_id_tagging_method method,
                                    int untagged_is_source,
                                    const t_uchar * path,
                                    assoc_table id_tagging_shortcut,
                                    struct stat * known_lstat,
                                    assoc_table * explicit_skips);
extern t_uchar * arch_id_tagging_method_name (enum arch_id_tagging_method m);
extern enum arch_id_tagging_method arch_id_tagging_method_from_name (const t_uchar * name);
extern t_uchar * arch_default_id_tagging_method_contents (enum arch_id_tagging_method method);
extern t_uchar * arch_tree_id_tagging_method_file (const t_uchar * tree_root);
extern enum arch_id_tagging_method arch_tree_id_tagging_method (enum arch_inventory_category * cat_var,
                                                                const t_uchar * tree_root,
                                                                int strict);
extern void arch_set_tree_id_tagging_method (const t_uchar * tree_root,
                                             enum arch_id_tagging_method method);
extern t_uchar * arch_explicit_id_file_for (const t_uchar * path);
extern t_uchar * arch_generate_id (void);
extern int arch_add_explicit_id (const t_uchar * path,
                                  const t_uchar * id);
extern void arch_delete_explicit_id (const t_uchar * path);
extern void arch_move_explicit_id (const t_uchar * from,
                                   const t_uchar * to);
extern t_uchar * arch_strong_explicit_dflt_file (const t_uchar * dir);
extern t_uchar * arch_weak_explicit_dflt_file (const t_uchar * dir);
extern t_uchar * arch_dont_care_explicit_dflt_file (const t_uchar * dir);
extern int arch_is_dont_care_explicit_dflt_dir (const t_uchar * dir);
extern void arch_delete_strong_explicit_default (const t_uchar * dir);
extern void arch_delete_weak_explicit_default (const t_uchar * dir);
extern void arch_delete_dont_care_explicit_default (const t_uchar * dir);
extern void arch_set_strong_explicit_default (const t_uchar * dir,
                                              const t_uchar * id);
extern void arch_set_weak_explicit_default (const t_uchar * dir,
                                            const t_uchar * id);
extern void arch_set_dont_care_explicit_default (const t_uchar * dir);
extern t_uchar * arch_id_from_explicit_file (int *errn,
                                             const t_uchar * path);
extern int str_cmp_suffix_n(t_uchar * a_string, t_uchar * b_string, int length);

#endif  /* INCLUDE__LIBARCH__INV_IDS_H */


/* tag: Tom Lord Sat Jan  5 15:26:10 2002 (inv-tags.h)
 */
