/* editor.h:
 *
 ****************************************************************
 * Copyright (C) 2003 Colin Walters
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */

#ifndef INCLUDE__LIBARCH__EDITOR_H
#define INCLUDE__LIBARCH__EDITOR_H


#include "hackerlab/machine/types.h"


/* automatically generated __STDC__ prototypes */
extern int arch_run_editor (t_uchar * name);
#endif  /* INCLUDE__LIBARCH__EDITOR_H */


/* tag: Colin Walters Wed, 19 Nov 2003 22:21:39 -0500 (editor.h)
 */
