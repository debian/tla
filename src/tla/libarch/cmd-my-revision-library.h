/* cmd-my-revision-library.h:
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord, Stig Brautaset
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */

#ifndef INCLUDE__LIBARCH__CMD_MY_REVISION_LIBRARY_H
#define INCLUDE__LIBARCH__CMD_MY_REVISION_LIBRARY_H


#include "hackerlab/machine/types.h"



extern t_uchar arch_cmd_my_revision_library_help[];


/* automatically generated __STDC__ prototypes */
extern int arch_cmd_my_revision_library (t_uchar * program_name, int argc, char * argv[]);
#endif  /* INCLUDE__LIBARCH__CMD_MY_REVISION_LIBRARY_H */


/* tag: Stig Brautaset Sat Jun  7 16:43:26 BST 2003 (cmd-my-revision-library.h)
 */
