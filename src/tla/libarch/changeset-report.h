/* changeset-report.h:
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */

#ifndef INCLUDE__LIBARCH__CHANGESET_REPORT_H
#define INCLUDE__LIBARCH__CHANGESET_REPORT_H


#include "tla/libawk/relational.h"



struct arch_changeset_report
{
  rel_table orig_files_index;   /* [0] loc [1] id      (sort by 1) */
  rel_table orig_dirs_index;    /* [0] loc [1] id      (sort by 1) */
  rel_table mod_files_index;    /* [0] loc [1] id      (sort by 1) */
  rel_table mod_dirs_index;     /* [0] loc [1] id      (sort by 1) */

  rel_table removed_dirs;       /* [0] loc [1] id [2] perms            (sort by 0) */
  rel_table added_dirs;         /* [0] loc [1] id [2] perms            (sort by 0) */

  rel_table removed_files;      /* [0] loc [1] id [2] archive-path     (sort by 0)*/
  rel_table added_files;        /* [0] loc [1] id [2] archive-path     (sort by 0) */

  rel_table removed_symlinks;   /* [0] loc [1] id [2] archive-path     (sort by 0) */
  rel_table added_symlinks;     /* [0] loc [1] id [2] archive-path     (sort by 0) */

  rel_table renamed_files;      /* [0] orig-loc [1] mod-loc [2] id     (sort by 0) */
  rel_table renamed_dirs;       /* [0] orig-loc [1] mod-loc [2] id     (sort by 0) */

  rel_table patched_regular_files; /* [0] mod-loc [1] id [2] patch-path (sort by 0) */
  rel_table patched_symlinks;   /* [0] mod-loc [1] id [2] patch-path-sans-suffix (sort by 0) */
  rel_table patched_binaries;   /* [0] mod-loc [1] id [2] patch-path-sans-suffix (sort by 0) */
  rel_table file_metadata_changed; /* [0] mod-loc [1] id [2] patch-path-sans-suffix (sort by 0) */
  rel_table dir_metadata_changed; /*  [0] mod-loc [1] id [2] patch-path-sans-suffix (sort by 0) */
  rel_table symlink_to_file;    /* [0] mod-loc [1] id [2] patch-path-sans-suffix (sort by 0) */
  rel_table file_to_symlink;    /* [0] mod-loc [1] id [2] patch-path-sans-suffix (sort by 0) */
};


/* automatically generated __STDC__ prototypes */
extern int arch_any_changes (struct arch_changeset_report * report);
extern void arch_evaluate_changeset (struct arch_changeset_report * report, t_uchar * path);
extern void arch_reverse_changeset (struct arch_changeset_report * report);
extern void arch_print_changeset_diffs (int out_fd, struct arch_changeset_report * report, int escape_classes);
extern void arch_print_changeset_custom_diffs (int out_fd, struct arch_changeset_report * report, t_uchar * orig_dir, t_uchar * mod_dir, t_uchar ** opts, int escape_classes);
extern void arch_print_changeset  (int out_fd, struct arch_changeset_report * report, int diffs, int escape_classes);
extern void arch_free_changeset_report_data (struct arch_changeset_report * r);
#endif  /* INCLUDE__LIBARCH__CHANGESET_REPORT_H */


/* tag: Tom Lord Sat Jan  5 15:26:10 2002 (changeset-report.h)
 */
