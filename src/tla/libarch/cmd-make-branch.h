/* cmd-make-branch.h:
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord, Stig Brautaset
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */

#ifndef INCLUDE__LIBARCH__CMD_MAKE_BRANCH_H
#define INCLUDE__LIBARCH__CMD_MAKE_BRANCH_H


#include "hackerlab/machine/types.h"



extern t_uchar arch_cmd_make_branch_help[];


/* automatically generated __STDC__ prototypes */
extern int arch_cmd_make_branch (t_uchar * program_name, int argc, char * argv[]);
#endif  /* INCLUDE__LIBARCH__CMD_MAKE_BRANCH_H */


/* tag: Stig Brautaset Sat Jun  7 15:55:48 BST 2003 (cmd-make-branch.h)
 */
