/* cmd-make-log.c
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */


#include "config-options.h"
#include "hackerlab/cmd/main.h"
#include "tla/libarch/namespace.h"
#include "tla/libarch/project-tree.h"
#include "tla/libarch/patch-logs.h"
#include "tla/libarch/cmd-make-log.h"



static t_uchar * usage = "[options] [version]";
static t_uchar * version_string = (cfg__std__package " from regexps.com\n"
                                   "\n"
                                   "Copyright 2003 Tom Lord\n"
                                   "\n"
                                   "This is free software; see the source for copying conditions.\n"
                                   "There is NO warranty; not even for MERCHANTABILITY or FITNESS FOR A\n"
                                   "PARTICULAR PURPOSE.\n"
                                   "\n"
                                   "Report bugs to " cfg__tla_bug_mail ".\n"
                                   "\n"
                                   cfg__std__release_id_string
                                   "\n");

#define OPTS(OP) \
  OP (opt_help_msg, "h", "help", 0, \
      "Display a help message and exit.") \
  OP (opt_long_help, "H", 0, 0, \
      "Display a verbose help message and exit.") \
  OP (opt_version, "V", "version", 0, \
      "Display a release identifier string\n" \
      "and exit.") \
  OP (opt_archive, "A", "archive", 1, \
      "Override `my-default-archive'") \
  OP (opt_dir, "d", "dir DIR", 1, \
      "cd to DIR first")

t_uchar arch_cmd_make_log_help[] = ("initialize a new log file entry\n"
                                    "Create (if needed) a log entry file in the root of the project\n"
                                    "tree containing DIR, for the indicated VERSION.\n"
                                    "\n"
                                    "The name of the new log file is printed to standard output.\n");

enum options
{
  OPTS (OPT_ENUM)
};

static struct opt_desc opts[] =
{
  OPTS (OPT_DESC)
    {-1, 0, 0, 0, 0}
};



int
arch_cmd_make_log (t_uchar * program_name, int argc, char * argv[])
{
  int o;
  struct opt_parsed * option;
  t_uchar * default_archive = 0;
  char * dir = 0;

  dir = str_save (0, ".");

  safe_buffer_fd (1, 0, O_WRONLY, 0);

  option = 0;

  while (1)
    {
      o = opt_standard (lim_use_must_malloc, &option, opts, &argc, argv, program_name, usage, version_string, arch_cmd_make_log_help, opt_help_msg, opt_long_help, opt_version);
      if (o == opt_none)
        break;
      switch (o)
        {
        default:
          safe_printfmt (2, "unhandled option `%s'\n", option->opt_string);
          panic ("internal error parsing arguments");

        usage_error:
          opt_usage (2, argv[0], program_name, usage, 1);
          exit (1);

          /* bogus_arg: */
          safe_printfmt (2, "ill-formed argument for `%s' (`%s')\n", option->opt_string, option->arg_string);
          goto usage_error;

        case opt_archive:
          {
            lim_free (0, default_archive);
            default_archive = str_save (0, option->arg_string);
            break;
          }

        case opt_dir:
          {
            lim_free (0, dir);
            dir = str_save (0, option->arg_string);
            break;
          }
        }
    }

  if (argc > 2)
    goto usage_error;

  {
    t_uchar * tree_root = 0;
    t_uchar * vsnspec = 0;
    t_uchar * archive = 0;
    t_uchar * version = 0;
    t_uchar * log_file = 0;

    if (default_archive && !arch_valid_archive_name (default_archive))
      {
        safe_printfmt (2, "%s: invalid archive name: %s\n",
                       argv[0], default_archive);
        exit (2);
      }


    tree_root = arch_tree_root (0, dir, 0);

    if (!tree_root)
      {
        safe_printfmt (2, "%s: not in a project tree\n  dir: %s\n",
                       argv[0], dir);
        exit (2);
      }

    if (argc == 2)
      vsnspec = str_save (0, argv[1]);
    else
      vsnspec = arch_tree_version (tree_root);

    if (!vsnspec)
      {
        safe_printfmt (2, "%s: no tree-version set\n  tree: %s\n",
                       argv[0], tree_root);
        exit (2);
      }

    if (!arch_valid_package_name (vsnspec, arch_maybe_archive, arch_req_version, 0))
      {
        safe_printfmt (2, "%s: illegal version name: %s\n",
                       argv[0], vsnspec);
        exit (2);
      }

    archive = arch_parse_package_name (arch_ret_archive, default_archive, vsnspec);
    version = arch_parse_package_name (arch_ret_non_archive, 0, vsnspec);

    log_file = arch_make_log_file (tree_root, archive, version);
    safe_printfmt (1, "%s\n", log_file);

    if (safe_access (log_file, F_OK) == -1)
      {
        lim_free (0, log_file);
        log_file = arch_make_log (tree_root, archive, version);
      }
    else
      {
        safe_printfmt (2, "Warning: not overwriting existing log file\n");
      }

    lim_free (0, vsnspec);
    lim_free (0, tree_root);
    lim_free (0, archive);
    lim_free (0, version);
    lim_free (0, log_file);
  }

  lim_free (0, default_archive);
  lim_free (0, dir);
  return 0;
}




/* tag: Tom Lord Fri May 23 13:48:00 2003 (make-log.c)
 */
