/* cmd-merges.c
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */


#include "config-options.h"
#include "hackerlab/cmd/main.h"
#include "tla/libfsutils/file-contents.h"
#include "tla/libarch/namespace.h"
#include "tla/libarch/project-tree.h"
#include "tla/libarch/archive.h"
#include "tla/libarch/merge-points.h"
#include "tla/libarch/cmd-merges.h"



static t_uchar * usage = "[options] INTO [FROM]";
static t_uchar * version_string = (cfg__std__package " from regexps.com\n"
                                   "\n"
                                   "Copyright 2003 Tom Lord\n"
                                   "\n"
                                   "This is free software; see the source for copying conditions.\n"
                                   "There is NO warranty; not even for MERCHANTABILITY or FITNESS FOR A\n"
                                   "PARTICULAR PURPOSE.\n"
                                   "\n"
                                   "Report bugs to " cfg__tla_bug_mail ".\n"
                                   "\n"
                                   cfg__std__release_id_string
                                   "\n");

#define OPTS(OP) \
  OP (opt_help_msg, "h", "help", 0, \
      "Display a help message and exit.") \
  OP (opt_long_help, "H", 0, 0, \
      "Display a verbose help message and exit.") \
  OP (opt_version, "V", "version", 0, \
      "Display a release identifier string\n" \
      "and exit.") \
  OP (opt_archive, "A", "archive", 1, \
      "Override `my-default-archive'") \
  OP (opt_dir, "d", "dir DIR", 1, \
      "cd to DIR first") \
  OP (opt_reverse, "r", "reverse", 0, \
      "sort from newest to oldest") \
  OP (opt_full, "f", "full", 0, \
      "print full patch level names")


t_uchar arch_cmd_merges_help[] = ("report where two branches have been merged\n"
                                  "Print a list of pairs of patch level names:\n"
                                  "\n"
                                  "        %s\t%s   INTO-RVN  FROM-RVN\n"
                                  "\n"
                                  "where each pair indicates that at patch level INTO-RVN of\n"
                                  "INTO, the patch log entry FROM-RVN was added.\n"
                                  "\n"
                                  "FROM may be a branch name, version name, or revision name.\n"
                                  "If a branch or version name, all merges from that branch or version\n"
                                  "are reported.  If a revision name, only the merge points for that\n"
                                  "specific revision are reported.\n"
                                  "\n"
                                  "INTO may be a version name or revision name.  If a version name,\n"
                                  "all merge points within that version are printed.  If a revision\n"
                                  "name, all merge points at that revision or earlier are printed.\n"
                                  "\n"
                                  "Output is sorted using patch-level ordering of the first column.\n"
                                  "\n"
                                  "Included patches are listed by full name unless FROM is a revision\n"
                                  "name.  If FROM is a revision, --full causes its full name to be\n"
                                  "printed.\n");

enum options
{
  OPTS (OPT_ENUM)
};

static struct opt_desc opts[] =
{
  OPTS (OPT_DESC)
    {-1, 0, 0, 0, 0}
};



int
arch_cmd_merges (t_uchar * program_name, int argc, char * argv[])
{
  int o;
  struct opt_parsed * option;
  t_uchar * default_archive = 0;
  char * dir = 0;
  int reverse = 0;
  int full = 0;

  default_archive = 0;

  safe_buffer_fd (1, 0, O_WRONLY, 0);

  option = 0;

  while (1)
    {
      o = opt_standard (lim_use_must_malloc, &option, opts, &argc, argv, program_name, usage, version_string, arch_cmd_merges_help, opt_help_msg, opt_long_help, opt_version);
      if (o == opt_none)
        break;
      switch (o)
        {
        default:
          safe_printfmt (2, "unhandled option `%s'\n", option->opt_string);
          panic ("internal error parsing arguments");

        usage_error:
          opt_usage (2, argv[0], program_name, usage, 1);
          exit (1);

          /* bogus_arg: */
          safe_printfmt (2, "ill-formed argument for `%s' (`%s')\n", option->opt_string, option->arg_string);
          goto usage_error;

        case opt_archive:
          {
            default_archive = str_save (0, option->arg_string);
            break;
          }

        case opt_dir:
          {
            dir = str_save (0, option->arg_string);
            break;
          }

        case opt_reverse:
          {
            reverse = 1;
            break;
          }

        case opt_full:
          {
            full = 1;
            break;
          }
        }
    }

  if ((argc < 2) || (argc > 3))
    goto usage_error;



  {
    t_uchar * into = 0;
    t_uchar * from = 0;
    int here_fd;
    t_uchar * into_archive = 0;
    t_uchar * from_archive = 0;
    t_uchar * into_spec = 0;
    t_uchar * from_spec = 0;
    rel_table merge_points = rel_table_nil;

    here_fd = safe_open (".", O_RDONLY, 0);

    into = str_save (0, argv[1]);
    if (argc == 3)
      from = str_save (0, argv[2]);

    if (default_archive && !arch_valid_archive_name (default_archive))
      {
        safe_printfmt (2, "%s: invalid archive name (%s)\n",
                       argv[0], default_archive);
        exit (1);
      }

    if (!arch_valid_package_name (into, arch_maybe_archive, arch_req_version, 1))
      {
        safe_printfmt (2, "%s: invalid version name (%s)\n",
                       argv[0], into);
        exit (1);
      }

    if (from && !arch_valid_package_name (into, arch_maybe_archive, arch_req_package, 1))
      {
        safe_printfmt (2, "%s: invalid package name (%s)\n", argv[0], from);
        exit (1);
      }

    into_archive = arch_parse_package_name (arch_ret_archive, default_archive, into);
    if (from)
      from_archive = arch_parse_package_name (arch_ret_archive, default_archive, from);

    into_spec = arch_parse_package_name (arch_ret_non_archive, 0, into);
    if (from)
      from_spec = arch_parse_package_name (arch_ret_non_archive, 0, from);

    if (dir)
      {
        t_uchar * tree_root = 0;

        tree_root = arch_tree_root (0, dir, 0);

        if (!tree_root)
          {
            safe_printfmt (2, "%s: not in a project tree (%s)\n",
                           argv[0], dir);
            exit (1);
          }

        merge_points = arch_tree_merge_points (tree_root, into_archive, into_spec, from_archive, from_spec);

        lim_free (0, tree_root);
      }
    else
      {
        struct arch_archive * into_arch = 0;

        into_arch = arch_archive_connect (into_archive, 0);

        merge_points = arch_archive_merge_points (into_arch, into_spec, from_archive, from_spec, 0);

        arch_archive_close (into_arch);
      }

    if (reverse)
      rel_reverse_table (merge_points);

    if (full || !from || !arch_valid_package_name (from_spec, arch_no_archive, arch_req_version, 0))
      {
        rel_print_table (1, merge_points);
      }
    else
      {
        int x;

        for (x = 0; x < rel_n_records (merge_points); ++x)
          {
            t_uchar * level = 0;

            level = arch_parse_package_name (arch_ret_patch_level, 0, rel_peek_str (merge_points, x, 1));

            safe_printfmt (1, "%s\t%s\n", rel_peek_str (merge_points, x, 0), level);

            lim_free (0, level);
          }
      }


    safe_close (here_fd);
    lim_free (0, into);
    lim_free (0, from);
    lim_free (0, into_archive);
    lim_free (0, from_archive);
    rel_free_table (merge_points);
  }



  lim_free (0, default_archive);
  lim_free (0, dir);

  return 0;
}




/* tag: Tom Lord Tue May 27 20:58:39 2003 (merges.c)
 */
