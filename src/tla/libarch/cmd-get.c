/* cmd-get.c:
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */




#include "config-options.h"
#include "hackerlab/cmd/main.h"
#include "hackerlab/fs/file-names.h"
#include "hackerlab/fs/cwd.h"
#include "tla/libfsutils/tmp-files.h"
#include "tla/libfsutils/rmrf.h"
#include "tla/libfsutils/link-tree.h"
#include "tla/libarch/namespace.h"
#include "tla/libarch/project-tree.h"
#include "tla/libarch/my.h"
#include "tla/libarch/archive.h"
#include "tla/libarch/pristines.h"
#include "tla/libarch/build-revision.h"
#include "tla/libarch/libraries.h"
#include "tla/libarch/local-cache.h"
#include "tla/libarch/library-txn.h"
#include "tla/libarch/cmd.h"
#include "tla/libarch/cmd-library-add.h"
#include "tla/libarch/cmdutils.h"
#include "tla/libarch/inode-sig.h"


static t_uchar * usage = "[options] revision [dir]";
static t_uchar * version_string = (cfg__std__package " from regexps.com\n"
                                   "\n"
                                   "Copyright 2003 Tom Lord\n"
                                   "\n"
                                   "This is free software; see the source for copying conditions.\n"
                                   "There is NO warranty; not even for MERCHANTABILITY or FITNESS FOR A\n"
                                   "PARTICULAR PURPOSE.\n"
                                   "\n"
                                   "Report bugs to " cfg__tla_bug_mail ".\n"
                                   "\n"
                                   cfg__std__release_id_string
                                   "\n");

#define OPTS(OP) \
  OP (opt_help_msg, "h", "help", 0, \
      "Display a help message and exit.") \
  OP (opt_long_help, "H", 0, 0, \
      "Display a verbose help message and exit.") \
  OP (opt_version, "V", "version", 0, \
      "Display a release identifier string\n" \
      "and exit.") \
  OP (opt_archive, "A", "archive", 1, \
      "Override `my-default-archive'") \
  OP (opt_cache, 0, "cache DIR", 1, \
      "cache root for trees with pristines") \
  OP (opt_no_pristine, 0, "no-pristine", 0, \
      "don't save a pristine copy") \
  OP (opt_hardlinks, 0, "link", 0, \
      "hardlink files to revision library instead of copying") \
  OP (opt_library, 0, "library", 0, \
      "ensure the revision is in a revision library") \
  OP (opt_sparse, 0, "sparse", 0, \
      "add library entries sparsely (--link, --library)") \
  OP (opt_non_sparse, 0, "non-sparse", 0, \
      "add library entries densely (--link, --library)") \
  OP (opt_silent, "s", "silent", 0, \
      "no output") \
  OP (opt_no_greedy_add, 0, "no-greedy-add", 0, \
      "do not allow greedy libraries to add revisions")\
  OP (opt_unescaped, 0, "unescaped", 0, \
      "show filenames in unescaped form")


t_uchar arch_cmd_get_help[] = ("construct a project tree for a revision\n"
			       "Extract REVISION from an archive, creating the new project tree\n"
			       "DIR.   If DIR is not specified, store the working copy in a subdirectory\n"
			       "of the current directory, giving it the name of the revision.\n"
			       "CAUTION: when using the links option be sure to use copy-on-write on your\n"
			       "editor or the revision library will be corrupted.\n"
			       "Either way, the project tree must not already exist.\n");

enum options
{
  OPTS (OPT_ENUM)
};

static struct opt_desc opts[] =
{
  OPTS (OPT_DESC)
    {-1, 0, 0, 0, 0}
};



int
arch_cmd_get (t_uchar * program_name, int argc, char * argv[])
{
  int o;
  struct opt_parsed * option;
  t_uchar * default_archive = 0;
  t_uchar * cache_dir_spec = 0;
  t_uchar * selected_library = 0;
  int no_pristine = 0;
  int hardlinks = 0;
  int sparse = -1;
  int library = 0;
  int silent = 0;
  int permit_greed = 1;
  int escape_classes = arch_escape_classes;

  safe_buffer_fd (1, 0, O_WRONLY, 0);

  option = 0;

  while (1)
    {
      o = opt_standard (lim_use_must_malloc, &option, opts, &argc, argv, program_name, usage, version_string, arch_cmd_get_help, opt_help_msg, opt_long_help, opt_version);
      if (o == opt_none)
        break;
      switch (o)
        {
        default:
          safe_printfmt (2, "unhandled option `%s'\n", option->opt_string);
          panic ("internal error parsing arguments");

        usage_error:
          opt_usage (2, argv[0], program_name, usage, 1);
          exit (1);

          /* bogus_arg: */
          safe_printfmt (2, "ill-formed argument for `%s' (`%s')\n", option->opt_string, option->arg_string);
          goto usage_error;

        case opt_archive:
          {
            lim_free (0, default_archive);
            default_archive = str_save (0, option->arg_string);
            break;
          }

        case opt_cache:
          {
            lim_free (0, cache_dir_spec);
            cache_dir_spec = str_save (0, option->arg_string);
            break;
          }

        case opt_no_pristine:
          {
            no_pristine = 1;
            break;
          }

        case opt_hardlinks:
          {
            hardlinks = 1;
            no_pristine = 1;
            break;
          }

        case opt_library:
          {
            no_pristine = 1;
            library = 1;
            break;
          }

        case opt_sparse:
          {
            sparse = 1;
            break;
          }

        case opt_non_sparse:
          {
            sparse = 0;
            break;
          }

        case opt_silent:
          {
            silent = 1;
            break;
          }

	case opt_unescaped:
	  {
	    escape_classes = 0;
	    break;
	  }

 	case opt_no_greedy_add:
 	  {
 	    permit_greed = 0;
 	    break;
 	  }
        }
    }

  if ((argc < 2) || (argc > 3))
    goto usage_error;

  default_archive = arch_my_default_archive (default_archive);

  {
    int here_fd;
    int mirror_check = 0;
    t_uchar * revision_spec = 0;
    t_uchar * archive = 0;
    t_uchar * version = 0;
    t_uchar * revision = 0;
    t_uchar * output_dir_spec = 0;
    t_uchar * output_dir_dir_spec = 0;
    t_uchar * output_dir_tail = 0;
    t_uchar * tmp_tail = 0;
    t_uchar * output_dir_dir = 0;
    t_uchar * output_dir = 0;
    t_uchar * cache_dir = 0;
    t_uchar * tmp_dir = 0;
    t_uchar *opt_same_dev_path = 0; 
    struct arch_archive * arch = 0;

    here_fd = safe_open (".", O_RDONLY, 0);

    if (library)
      {
        rel_table library_path = rel_table_nil;

        library_path = arch_my_library_path (arch_library_path_search_order);
        if (!rel_n_records (library_path))
          {
            safe_printfmt (2, "%s: no default library (try tla my-revision-library -H)\n", argv[0]);
            exit (2);
          }
        rel_free_table (library_path);
      }
    
    revision_spec = str_save (0, argv[1]);

    if (!arch_valid_package_name (revision_spec, arch_maybe_archive, arch_req_package, 1))
      {
        safe_printfmt (2, "%s: invalid revision spec (%s)\n",
                       argv[0], revision_spec);
        exit (2);
      }

    archive = arch_parse_package_name (arch_ret_archive, default_archive, revision_spec);
    arch = arch_archive_connect (archive, 0);
    mirror_check = arch_check_arch (arch);

    if (arch_valid_package_name (revision_spec, arch_maybe_archive, arch_req_package, 0))
      {
        t_uchar * package = 0;
        rel_table versions = rel_table_nil;

        package = arch_parse_package_name (arch_ret_package, 0, revision_spec);

        arch_check_for (arch, arch_req_package, package);

        versions = arch_archive_versions (arch, package);
        arch_sort_table_by_name_field (1, versions, 0);
        if (!rel_n_records (versions))
          {
            safe_printfmt (2, "%s: package has no versions (%s)\n",
                           argv[0], revision_spec);
            exit (1);
          }

        lim_free (0, revision_spec);
        revision_spec = str_save (0, rel_peek_str (versions, 0, 0));

        lim_free (0, package);
        rel_free_table (versions);
      }


    if (arch_valid_package_name (revision_spec, arch_maybe_archive, arch_req_version, 0))
      {
        rel_table revisions = rel_table_nil;

	arch_check_for (arch, arch_req_version, revision_spec);
        version = arch_parse_package_name (arch_ret_package_version, 0, revision_spec);

        revisions = arch_archive_revisions (arch, version, 1);
        arch_sort_table_by_name_field (1, revisions, 0);
        if (!rel_n_records (revisions))
          {
            safe_printfmt (2, "%s: no revisions in version (%s/%s)\n",
                           argv[0], archive, revision_spec);
            exit (2);
          }

        revision = arch_parse_package_name (arch_ret_non_archive, 0, rel_peek_str (revisions, 0, 0));

        rel_free_table (revisions);
      }
    else
      {
        version = arch_parse_package_name (arch_ret_package_version, 0, revision_spec);
        revision = arch_parse_package_name (arch_ret_non_archive, 0, revision_spec);
	arch_check_for (arch, arch_req_patch_level, revision);
      }


    if (argc == 3)
      output_dir_spec = file_name_from_directory (0, argv[2]);
    else
      output_dir_spec = file_name_from_directory (0, revision);

    output_dir_dir_spec = file_name_directory_file (0, output_dir_spec);
    if (!output_dir_dir_spec)
      output_dir_dir_spec = str_save (0, ".");

    output_dir_tail = file_name_tail (0, output_dir_spec);

    tmp_tail = str_alloc_cat_many (0, ",,get.", output_dir_tail, str_end);

    if (!cache_dir_spec)
      cache_dir_spec = str_save (0, output_dir_dir_spec);

    safe_fchdir (here_fd);
    safe_chdir (output_dir_dir_spec);
    output_dir_dir = safe_current_working_directory ();
    opt_same_dev_path = (!!hardlinks) ? output_dir_dir : 0;

    safe_fchdir (here_fd);
    safe_chdir (cache_dir_spec);
    cache_dir = safe_current_working_directory ();

    output_dir = file_name_in_vicinity (0, output_dir_dir, output_dir_tail);

    if (!safe_access (output_dir, F_OK))
      {
        safe_printfmt (2, "%s: output directory already exists (%s)\n",
                       argv[0], output_dir);
        exit (1);
      }

    tmp_dir = tmp_file_name (output_dir_dir, tmp_tail);

    rmrf_file (tmp_dir);


    if (!library && permit_greed)
      {
        /* Prefer a library entry over making a pristine tree, if there's
         * a greedy library.
         */
        selected_library = arch_library_greedy_add_choice (archive, revision, opt_same_dev_path, 1);
	if (selected_library)
	  library = 1;
      }
    /* If we can't find a greedy for linking to, we add to a non-greedy library.
     * Previous behavior was to *always* auto-add to a library, greedy or not
     */
    if (hardlinks)
      library=1; 

    /* ensure revision is in the library if needed
     */
    if (library)
      {
        safe_printfmt (1, "* ensuring library has %s/%s\n", archive, revision);
        safe_flush (1);

 	arch_library_add (1, 1, arch, revision, selected_library, opt_same_dev_path, sparse, escape_classes);
      }

    /* make hardlinks or build-revision
     */
    if (hardlinks)
      {
        t_uchar * revision_dir = 0;
        rel_table index = rel_table_nil;
        int full_meta;

        index = arch_library_index (archive, revision);
        rel_sort_table_by_field (0, index, 0);

        safe_printfmt (1, "* hard linking to library\n");
        safe_flush (1);

        revision_dir = arch_library_find (rel_table_nil, archive, revision, 1);
        invariant (0 != revision_dir);
        full_meta = arch_tree_has_meta_flag (revision_dir);
        
        build_partial_link_tree (arch_library_find (rel_table_nil, archive, revision, 1), tmp_dir, index, full_meta);
        arch_snap_inode_sig (tmp_dir, archive, revision);

        lim_free (0, revision_dir);
      }
    else
      {
        safe_mkdir (tmp_dir, 0777);
        no_pristine = arch_build_revision ((silent ? -1 : 1), tmp_dir, arch, archive, revision, cache_dir);
      }

   // if we manage a mirror, we have to direct the tree to the original archive, not the mirror
   if( mirror_check ) {
      archive = str_save(0,arch->official_name);
      safe_flush (1);
    }
    arch_archive_close (arch);

    if (!no_pristine)
      {
        if (!silent)
          {
            safe_printfmt (1, "* making pristine copy\n");
            safe_flush (1);
          }
        arch_make_pristine (tmp_dir, archive, revision);
      }

    {
      t_uchar * fqversion = 0;

      fqversion = arch_fully_qualify (archive, version);

      arch_set_tree_version (tmp_dir, archive, version);

      if (!silent)
        {
          safe_printfmt (1, "* tree version set %s\n", fqversion);
          safe_flush (1);
        }

      lim_free (0, fqversion);
    }

    safe_fchdir (here_fd);
    safe_close (here_fd);
    safe_rename (tmp_dir, output_dir);

    lim_free (0, revision_spec);
    lim_free (0, archive);
    lim_free (0, version);
    lim_free (0, revision);
    lim_free (0, output_dir_spec);
    lim_free (0, output_dir_dir_spec);
    lim_free (0, output_dir_tail);
    lim_free (0, tmp_tail);
    lim_free (0, output_dir_dir);
    lim_free (0, output_dir);
    lim_free (0, tmp_dir);
  }

  lim_free (0, default_archive);
  lim_free (0, selected_library);
  lim_free (0, cache_dir_spec);

  return 0;
}




/* tag: Tom Lord Mon Jun  2 16:50:36 2003 (cmd-getrev.c)
 */
