/* cmd-ancestry.h:
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */

#ifndef INCLUDE__LIBARCH__CMD_ANCESTRY_H
#define INCLUDE__LIBARCH__CMD_ANCESTRY_H


#include "hackerlab/machine/types.h"



extern t_uchar arch_cmd_ancestry_help[];


/* automatically generated __STDC__ prototypes */
extern int arch_cmd_ancestry (t_uchar * program_name, int argc, char * argv[]);
#endif  /* INCLUDE__LIBARCH__CMD_ANCESTRY_H */


/* tag: Tom Lord Wed Jul 16 13:58:28 2003 (cmd-ancestry.h)
 */
