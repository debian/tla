/* undo.h:
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */

#ifndef INCLUDE__LIBARCH__UNDO_H
#define INCLUDE__LIBARCH__UNDO_H


#include "hackerlab/machine/types.h"


/* automatically generated __STDC__ prototypes */
extern void arch_undo (int chatter_fd, t_uchar * dest_dir, int delete_changeset, t_uchar * tree_root,
                       t_uchar * baseline_root, rel_table file_list, assoc_table inode_shortcut, int forward,   int escape_classes);
extern t_uchar * arch_latest_undo_changeset (t_ulong * level_n, t_uchar * tree_root);
extern t_uchar * arch_next_undo_changeset (t_ulong * level_n, t_uchar * tree_root);
#endif  /* INCLUDE__LIBARCH__UNDO_H */


/* tag: Tom Lord Sat Jan  5 15:26:10 2002 (undo.h)
 */
