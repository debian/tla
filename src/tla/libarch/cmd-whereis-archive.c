/* cmd-whereis-archive.c
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */


#include "config-options.h"
#include "hackerlab/cmd/main.h"
#include "tla/libfsutils/ensure-dir.h"
#include "tla/libarch/my.h"
#include "tla/libarch/namespace.h"
#include "tla/libarch/archives.h"
#include "tla/libarch/cmd-whereis-archive.h"



static t_uchar * usage = "[options] archive";
static t_uchar * version_string = (cfg__std__package " from regexps.com\n"
                                   "\n"
                                   "Copyright 2003 Tom Lord\n"
                                   "\n"
                                   "This is free software; see the source for copying conditions.\n"
                                   "There is NO warranty; not even for MERCHANTABILITY or FITNESS FOR A\n"
                                   "PARTICULAR PURPOSE.\n"
                                   "\n"
                                   "Report bugs to " cfg__tla_bug_mail ".\n"
                                   "\n"
                                   cfg__std__release_id_string
                                   "\n");

#define OPTS(OP) \
  OP (opt_help_msg, "h", "help", 0, \
      "Display a help message and exit.") \
  OP (opt_long_help, "H", 0, 0, \
      "Display a verbose help message and exit.") \
  OP (opt_version, "V", "version", 0, \
      "Display a release identifier string\n" \
      "and exit.")

t_uchar arch_cmd_whereis_archive_help[] = ("print an archive location registration\n"
                                           "Print the registered location of an archive.\n"
                                           "\n"
                                           "Usually the archive must have been previously registered with\n"
                                           "\"tla register-archive\".\n"
                                           "\n"
                                           "As a special exception, the the archive is not registered, but\n"
                                           "is the name of the archive rooted at the location given with\n"
                                           "the option -R (--root) or in the environment variable ARCHROOT\n"
                                           "then print that root directory.\n");

enum options
{
  OPTS (OPT_ENUM)
};

static struct opt_desc opts[] =
{
  OPTS (OPT_DESC)
    {-1, 0, 0, 0, 0}
};



int
arch_cmd_whereis_archive (t_uchar * program_name, int argc, char * argv[])
{
  int o;
  struct opt_parsed * option;
  t_uchar * which_archive;
  t_uchar * found_archive;

  safe_buffer_fd (1, 0, O_WRONLY, 0);

  option = 0;

  while (1)
    {
      o = opt_standard (lim_use_must_malloc, &option, opts, &argc, argv, program_name, usage, version_string, arch_cmd_whereis_archive_help, opt_help_msg, opt_long_help, opt_version);
      if (o == opt_none)
        break;
      switch (o)
        {
        default:
          safe_printfmt (2, "unhandled option `%s'\n", option->opt_string);
          panic ("internal error parsing arguments");

        usage_error:
          opt_usage (2, argv[0], program_name, usage, 1);
          exit (1);

          /* bogus_arg: */
          safe_printfmt (2, "ill-formed argument for `%s' (`%s')\n", option->opt_string, option->arg_string);
          goto usage_error;
        }
    }

  if (argc != 2)
    goto usage_error;

  which_archive = argv[1];

  if (!arch_valid_archive_name (which_archive))
    {
      safe_printfmt (2, "%s: invalid archive name (%s)\n",
                     argv[0], which_archive);
      exit (2);
    }

  found_archive = arch_archive_location (which_archive, 1);

  if (!found_archive)
      exit (1);
  
  safe_printfmt (1, "%s\n", found_archive);

  return 0;
}




/* tag: Tom Lord Sun May 18 21:37:02 2003 (whereis-archive.c)
 */
