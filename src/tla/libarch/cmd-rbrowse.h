/* cmd-rbrowse.h:
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */

#ifndef INCLUDE__LIBARCH__CMD_RBROWSE_H
#define INCLUDE__LIBARCH__CMD_RBROWSE_H


#include "hackerlab/machine/types.h"



extern t_uchar arch_cmd_rbrowse_help[];


/* automatically generated __STDC__ prototypes */
extern int arch_cmd_rbrowse (t_uchar * program_name,
                             int argc,
                             char * argv[]);
#endif  /* INCLUDE__LIBARCH__CMD_RBROWSE_H */


/* tag: James Blackwell Thu Dec 11 16:48:53 EST 2003 (cmd-rbrowse.h) 
 */
