/* cmd-switch.c
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */


#include "config-options.h"

#include "hackerlab/cmd/main.h"
#include "hackerlab/fs/file-names.h"
#include "tla/libfsutils/rmrf.h"
#include "tla/libarch/archive.h"
#include "tla/libarch/chatter.h"

#include "tla/libarch/inode-sig.h"
#include "tla/libarch/local-cache.h"
#include "tla/libarch/make-changeset.h"
#include "tla/libarch/my.h"
#include "tla/libarch/namespace.h"
#include "tla/libarch/project-tree.h"
#include "tla/libarch/proj-tree-lint.h"
#include "tla/libarch/patch-logs.h"
#include "tla/libarch/changeset-report.h"
#include "tla/libarch/cmdutils.h"
#include "tla/libarch/cmd.h"
#include "tla/libarch/cmd-apply-delta.h"
#include "tla/libarch/cmd-switch.h"


/* gettext support not uet incorporated into tla, reserve the gettext notation for later */
#define _(x) (x)
#define N_(x) (x)






static t_uchar * usage = "[options] [package]";
static t_uchar * version_string = (cfg__std__package " from regexps.com\n"
                                   "\n"
                                   "Copyright 2003 Tom Lord\n"
                                   "\n"
                                   "This is free software; see the source for copying conditions.\n"
                                   "There is NO warranty; not even for MERCHANTABILITY or FITNESS FOR A\n"
                                   "PARTICULAR PURPOSE.\n"
                                   "\n"
                                   "Report bugs to " cfg__tla_bug_mail ".\n"
                                   "\n"
                                   cfg__std__release_id_string
                                   "\n");

#define OPTS(OP) \
  OP (opt_help_msg, "h", "help", 0, \
      "Display a help message and exit.") \
  OP (opt_long_help, "H", 0, 0, \
      "Display a verbose help message and exit.") \
  OP (opt_version, "V", "version", 0, \
      "Display a release identifier string and exit.") \
  OP (opt_archive, "A", "archive", 1, \
      N_("Override `my-default-archive'.")) \
  OP (opt_dir, "d", "dir DIR", 1, \
      "Change to DIR first.") \
  OP (opt_quiet, "q", "quiet", 0, \
      "Suppress progress information")


t_uchar arch_cmd_switch_help[] = (
 "change the working trees version\n"
 
 "change the working trees version to that of package, and make\n"
 "equivalent to revision. Preserves uncommitted changes.\n"
 "\n"
 "Note: tla automatically switches to the _latest_ revision in the\n"
 "supplied version if no revision explicitly specified.\n");

enum options
{
  OPTS (OPT_ENUM)
};

static struct opt_desc opts[] =
{
  OPTS (OPT_DESC)
    {-1, 0, 0, 0, 0}
};



int
arch_cmd_switch (t_uchar * program_name, int argc, char * argv[])
{
  int o;
  struct opt_parsed * option;
  t_uchar * dir = 0;
  t_uchar * default_archive = 0;
  int quiet = 0;

  dir = str_save (0, ".");

  safe_buffer_fd (1, 0, O_WRONLY, 0);

  option = 0;

  while (1)
    {
      if ((argc > 1) && !str_cmp ("--", argv[1]))
        break;

      o = opt_standard (lim_use_must_malloc, &option, opts, &argc, argv, program_name, usage, version_string, arch_cmd_switch_help, opt_help_msg, opt_long_help, opt_version);
      if (o == opt_none)
        break;
      switch (o)
        {
        default:
          safe_printfmt (2, "unhandled option `%s'\n", option->opt_string);
          panic ("internal error parsing arguments");

        usage_error:
          opt_usage (2, argv[0], program_name, usage, 1);
          exit (1);

          /* bogus_arg: */
          safe_printfmt (2, "ill-formed argument for `%s' (`%s')\n", option->opt_string, option->arg_string);
          goto usage_error;


        case opt_archive:
          {
            lim_free (0, default_archive);
            default_archive = str_save (0, option->arg_string);
            break;
          }

        case opt_dir:
          {
            lim_free (0, dir);
            dir = str_save (0, option->arg_string);
            break;
          }

        case opt_quiet:
          {
            quiet = 1;
            break;
          }

        }
    }

  {
 
    t_uchar * tree_root = 0;
    t_uchar * fqvsn = 0;
    t_uchar * rvsnspec = 0;
    t_uchar * archive = 0;
    t_uchar * revision = 0;
    t_uchar * log_file = 0;
    t_uchar * tree_arch = 0;
    t_uchar * latest_log = 0;
    t_uchar * version = 0;
   
    t_uchar * tree_revision = 0;
    t_uchar * current_spec = 0;
    t_uchar * tag_spec = 0;
    
    int commandline_rvsn = 0;
 
    int result;
    
      {

        t_uchar *arch = 0, *tag = 0;
        tag_spec = argv[1];
        arch_separate_arch_name_from_fqrvsn(tag_spec, &arch, &tag);
        if (arch[0])
          {
             if (default_archive)
              {
                safe_printfmt (2, "default archive specified multiple times! Operation aborts\n");
                exit (2);
              }

             lim_free(0, default_archive);
             default_archive = arch;
          }

        lim_free(0, tag);
      }


    if (default_archive && !arch_valid_archive_name (default_archive))
      {
        safe_printfmt (2, "%s: invalid archive name: %s\n",
                       argv[0], default_archive);
        exit (2);
      }

    tree_root = arch_tree_root (0, dir, 0);

    if (!tree_root)
      {
        safe_printfmt (2, "%s: not in a project tree\n  dir: %s\n",
                       argv[0], dir);
        exit (2);
      }

   
    if (argc != 2)
    {
      safe_printfmt (2, "%s: sno target version or revision provided.\n", argv[0]);
    
      exit(2);
    }
    fqvsn =  str_save (0, argv[1]);
    if (!fqvsn)
      {
        safe_printfmt (2, "%s: no tree-version set\n  tree: %s\n", argv[0], tree_root);
        exit (2);
      }

    commandline_rvsn = arch_valid_package_name (fqvsn, arch_maybe_archive, arch_req_patch_level, 0);
    
    tree_arch = arch_parse_package_name (arch_ret_archive, 0, fqvsn);
    tree_revision = arch_parse_package_name (arch_ret_non_archive, 0, fqvsn);

    invariant (!!tree_arch);

    if (!arch_valid_package_name (tree_revision, arch_no_archive, arch_req_version, 0))
      {
	rvsnspec = str_save (0, fqvsn);
      }
    else
      {

        struct arch_archive *arch = arch_archive_connect (tree_arch, 0);
        invariant (!!arch);
        latest_log = arch_archive_latest_revision(arch, tree_revision, 0);    
     
        arch_archive_close (arch);
	if (!latest_log)
	  {
	    safe_printfmt (2, "%s: tree shows no revisions in version\n  tree: %s\n  version: %s\n", argv[0], tree_root, fqvsn);
	    exit (2);
	  }

	rvsnspec = str_alloc_cat_many (0, fqvsn, "--", latest_log, str_end);
      }

    lim_free (0, fqvsn);
    lim_free (0, tree_arch);
    lim_free (0, tree_revision);
    lim_free (0, latest_log);


    if (!arch_valid_package_name (rvsnspec, arch_maybe_archive, arch_req_patch_level, 0))
      {
        safe_printfmt (2, "%s: illegal revision name: %s\n",
                       argv[0], rvsnspec);
        exit (2);
      }
    archive = arch_parse_package_name (arch_ret_archive, default_archive, rvsnspec);
    revision = arch_parse_package_name (arch_ret_non_archive, 0, rvsnspec);
    version = arch_parse_package_name (arch_ret_package_version, 0, rvsnspec);

    if (commandline_rvsn)
      arch_check_revision_local (archive, revision);

    current_spec = arch_get_tree_fqrevision(tree_root);

    tree_arch = arch_parse_package_name (arch_ret_archive, default_archive, current_spec);
    tree_revision = arch_parse_package_name (arch_ret_non_archive, 0, current_spec);

   
    /*arch_tree_ensure_no_conflicts (tree);*/

    /* remember that call_cmd never returns, so we have to set the 
     * tree version now, before we move on to appylying the delta
     */

    arch_set_tree_version (tree_root, archive, version);

    result = arch_call_cmd (arch_cmd_apply_delta, argv[0], 
                          "-A", tree_arch,
                          "-d", tree_root, tree_revision,
                          rvsnspec, NULL);

    lim_free (0, rvsnspec);
    lim_free (0, tree_root);
    lim_free (0, archive);
    lim_free (0, revision);
    lim_free (0, version);
    lim_free (0, tree_arch);
    lim_free (0, tree_revision);
    lim_free (0, current_spec);
    lim_free (0, dir);

    lim_free (0, default_archive);
  
    return result;
  }
  
}

/* tag: Tom Lord Fri May 23 14:06:15 2003 (cmd-switch.c)
 */
