/* cmd-tag.h:
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord, Stig Brautaset
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */

#ifndef INCLUDE__LIBARCH__CMD_TAG_H
#define INCLUDE__LIBARCH__CMD_TAG_H


#include "hackerlab/machine/types.h"



extern t_uchar arch_cmd_tag_help[];


/* automatically generated __STDC__ prototypes */
extern int arch_cmd_tag (t_uchar * program_name, int argc, char * argv[]);
#endif  /* INCLUDE__LIBARCH__CMD_TAG_H */


/* tag: Stig Brautaset Sat Jun  7 17:30:28 BST 2003 (cmd-tagrev.h)
 */
