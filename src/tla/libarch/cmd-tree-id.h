/* cmd-tree-id.h:
 *
 * vim:smartindent ts=8:sts=2:sta:et:ai:shiftwidth=2
 ****************************************************************
 * Copyright (C) 2004 Canonical Limited
 *      Authors: Robert Collins <robert.collins@canonical.com>
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */
#ifndef INCLUDE__LIBARCH__CMD_TREE_ID_H
#define INCLUDE__LIBARCH__CMD_TREE_ID_H

#include "hackerlab/machine/types.h"


extern t_uchar arch_cmd_tree_id_help[];
extern int arch_cmd_tree_id (t_uchar * program_name, int argc, char * argv[]);
#endif  /* INCLUDE__LIBARCH__CMD_TREE_ID_H */

/* tag: Tom Lord Fri May 23 14:06:15 2003 (cmd-tree-id.h)
 */
