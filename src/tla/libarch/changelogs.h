/* changelogs.h:
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */

#ifndef INCLUDE__LIBARCH__CHANGELOGS_H
#define INCLUDE__LIBARCH__CHANGELOGS_H



/* automatically generated __STDC__ prototypes */
extern int arch_id_indicates_changelog (const t_uchar * id);
extern void arch_parse_changelog_id (t_uchar ** archive, t_uchar ** version, const t_uchar * id);
extern void arch_generate_changelog (int out_fd,
                                     const t_uchar * tree_root,
                                     int no_files,
                                     int untagged,
                                     const t_uchar * new_entry_patch_lvl,
                                     const t_uchar * new_entry_file,
                                     const t_uchar * archive,
                                     const t_uchar * version);
#endif  /* INCLUDE__LIBARCH__CHANGELOGS_H */


/* tag: Tom Lord Sat Jan  5 15:26:10 2002 (changelogs.h)
 */
