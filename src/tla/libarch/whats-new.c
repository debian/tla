/* whats-new.c:
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */


#include "hackerlab/bugs/panic.h"
#include "hackerlab/char/str.h"
#include "tla/libarch/namespace.h"
#include "tla/libarch/patch-logs.h"
#include "tla/libarch/missing.h"



rel_table
arch_whats_new (t_uchar * tree_root, struct arch_archive * arch, t_uchar * version, int skip_present)
{
  t_uchar * latest_applied = 0;
  rel_table available = rel_table_nil;
  rel_table answer = rel_table_nil;
  int x;

  invariant (arch_valid_package_name (version, arch_no_archive, arch_req_version, 0));

  latest_applied = arch_highest_patch_level (tree_root, arch->name, version);

  available = arch_archive_revisions (arch, version, 0);

  for (x = 0; x < rel_n_records (available); ++x)
    {
      if (!str_cmp (latest_applied, rel_peek_str (available, x, 0)))
        {
          ++x;
          break;
        }
    }

  while (x < rel_n_records (available))
    {
      rel_add_records (&answer, rel_singleton_record_taking (rel_get_field (available, x, 0)), rel_record_null);
      ++x;
    }

  if (skip_present)
    answer = arch_filter_present_logs (tree_root, arch, version, answer);

  lim_free (0, latest_applied);
  rel_free_table (available);
  return answer;
}




/* tag: Tom Lord Thu Jun  5 00:53:51 2003 (whats-new.c)
 */
