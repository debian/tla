/* chatter.c:
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */


#include "hackerlab/os/stdarg.h"
#include "hackerlab/vu/safe.h"
#include "tla/libarch/chatter.h"



void
arch_chatter (int chatter_fd, char * format, ...)
{
  if (chatter_fd >= 0)
    {
      va_list ap;

      va_start (ap, format);
      safe_printfmt_va_list (chatter_fd, format, ap);
      va_end (ap);
      safe_flush (chatter_fd);
    }
}




/* tag: Tom Lord Tue Jun 10 14:55:23 2003 (chatter.c)
 */
