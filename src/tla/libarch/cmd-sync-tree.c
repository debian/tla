/* cmd-sync-tree.c:
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */




#include "config-options.h"
#include "hackerlab/cmd/main.h"
#include "hackerlab/fs/file-names.h"
#include "hackerlab/fs/cwd.h"
#include "hackerlab/vu/vu-dash.h"
#include "tla/libfsutils/tmp-files.h"
#include "tla/libfsutils/rmrf.h"
#include "tla/libfsutils/copy-file.h"
#include "tla/libfsutils/dir-as-cwd.h"
#include "tla/libarch/namespace.h"
#include "tla/libarch/project-tree.h"
#include "tla/libarch/my.h"
#include "tla/libarch/copy-project-tree.h"
#include "tla/libarch/sync-tree.h"
#include "tla/libarch/chatter.h"
#include "tla/libarch/cmd.h"
#include "tla/libarch/cmdutils.h"
#include "tla/libarch/cmd-sync-tree.h"



static t_uchar * usage = "[options] revision";
static t_uchar * version_string = (cfg__std__package " from regexps.com\n"
                                   "\n"
                                   "Copyright 2003 Tom Lord\n"
                                   "\n"
                                   "This is free software; see the source for copying conditions.\n"
                                   "There is NO warranty; not even for MERCHANTABILITY or FITNESS FOR A\n"
                                   "PARTICULAR PURPOSE.\n"
                                   "\n"
                                   "Report bugs to " cfg__tla_bug_mail ".\n"
                                   "\n"
                                   cfg__std__release_id_string
                                   "\n");

#define OPTS(OP) \
  OP (opt_help_msg, "h", "help", 0, \
      "Display a help message and exit.") \
  OP (opt_long_help, "H", 0, 0, \
      "Display a verbose help message and exit.") \
  OP (opt_version, "V", "version", 0, \
      "Display a release identifier string\n" \
      "and exit.") \
  OP (opt_archive, "A", "archive", 1, \
      "Override `my-default-archive'") \
  OP (opt_dir, "d", "dir DIR", 1, \
      "Operate on project tree in DIR (default `.')") \
  OP (opt_dest, 0, "dest DEST", 1, \
      "Instead of modifying the project tree in-place,\n" \
      "make a copy of it to DEST and apply the result to that") \
  OP (opt_unescaped, 0, "unescaped", 0, \
      "show filenames in unescaped form")


t_uchar arch_cmd_sync_tree_help[] = ("unify a project tree's patch-log with a given revision\n"

                                     "The new project tree is formed by getting the REVISION and adding all\n"
                                     "patch-log entries from REVISION.  No actual merging is performed --\n"
                                     "only the patch-log is changed.\n");

enum options
{
  OPTS (OPT_ENUM)
};

static struct opt_desc opts[] =
{
  OPTS (OPT_DESC)
    {-1, 0, 0, 0, 0}
};



int
arch_cmd_sync_tree (t_uchar * program_name, int argc, char * argv[])
{
  int o;
  struct opt_parsed * option;
  t_uchar * default_archive = 0;
  t_uchar * upon = 0;
  t_uchar * dest = 0;
  int escape_classes = arch_escape_classes;

  safe_buffer_fd (1, 0, O_WRONLY, 0);

  option = 0;

  while (1)
    {
      o = opt_standard (lim_use_must_malloc, &option, opts, &argc, argv, program_name, usage, version_string, arch_cmd_sync_tree_help, opt_help_msg, opt_long_help, opt_version);
      if (o == opt_none)
        break;
      switch (o)
        {
        default:
          safe_printfmt (2, "unhandled option `%s'\n", option->opt_string);
          panic ("internal error parsing arguments");

        usage_error:
          opt_usage (2, argv[0], program_name, usage, 1);
          exit (1);

          /* bogus_arg: */
          safe_printfmt (2, "ill-formed argument for `%s' (`%s')\n", option->opt_string, option->arg_string);
          goto usage_error;

        case opt_archive:
          {
            lim_free (0, default_archive);
            default_archive = str_save (0, option->arg_string);
            break;
          }

        case opt_dir:
          {
            lim_free (0, upon);
            upon = str_save (0, option->arg_string);
            break;
          }

        case opt_dest:
          {
            lim_free (0, dest);
            dest = str_save (0, option->arg_string);
            break;
          }

	case opt_unescaped:
	  {
	    escape_classes = 0;
	    break;
	  }
        }
    }

  if (argc != 2)
    goto usage_error;

  if (default_archive && !arch_valid_archive_name (default_archive))
    {
      safe_printfmt (2, "%s: invalid archive name (%s)\n",
                     argv[0], default_archive);
      exit (1);
    }

  {
    t_uchar * revspec = 0;
    t_uchar * upon_root = 0;
    t_uchar * archive = 0;
    struct arch_archive * arch = 0;
    t_uchar * revision = 0;

    revspec = argv[1];

    upon_root = arch_tree_root (0, upon, 0);

    if (!upon_root)
      {
        safe_printfmt (2, "%s: dir not in a project tree (%s)\n",
                       argv[0], upon);
        exit (1);
      }
    
    revision = arch_determine_revision (&arch, default_archive, revspec, argv[0]);
    archive = str_save (0, arch->name);

    arch_chatter (1, "* making sync tree with %s\n", revision);
    if (dest)
      arch_chatter (1, "** source %s\n** dest %s\n", upon_root, dest);
    else
      arch_chatter (1, "** directory %s\n", upon_root);

    if (dest)
      {
        safe_printfmt (1, "* copying %s to %s\n", upon_root, dest);
        safe_flush (1);
        arch_copy_project_tree (upon_root, dest, 1, 1);
      }
    else
      dest = str_save (0, upon_root);

    arch_sync_tree (1, dest, arch, archive, revision);

    lim_free (0, upon_root);
    lim_free (0, archive);
    arch_archive_close (arch);
    lim_free (0, revision);
  }

  lim_free (0, upon);
  lim_free (0, dest);
  lim_free (0, default_archive);

  return 0;
}




/* tag: Tom Lord Tue Jun 17 16:02:09 2003 (cmd-sync-tree.c)
 */
