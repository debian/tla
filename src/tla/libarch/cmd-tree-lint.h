/* cmd-tree-lint.h:
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord, Stig Brautaset
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */

#ifndef INCLUDE__LIBARCH__CMD_TREE_LINT_H
#define INCLUDE__LIBARCH__CMD_TREE_LINT_H


#include "hackerlab/machine/types.h"



extern t_uchar arch_cmd_tree_lint_help[];


/* automatically generated __STDC__ prototypes */
extern int arch_cmd_tree_lint (t_uchar * program_name, int argc, char * argv[]);
#endif  /* INCLUDE__LIBARCH__CMD_TREE_LINT_H */


/* tag: Stig Brautaset Sat Jun  7 17:34:59 BST 2003 (cmd-tree-lint.h)
 */
