/* cmd-register-archive.c
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */


#include "config-options.h"
#include "hackerlab/cmd/main.h"
#include "hackerlab/mem/alloc-limits.h"
#include "hackerlab/mem/mem.h"
#include "tla/libfsutils/ensure-dir.h"
#include "tla/libarch/my.h"
#include "tla/libarch/namespace.h"
#include "tla/libarch/archive.h"
#include "tla/libarch/archives.h"
#include "tla/libarch/pfs.h"
#include "tla/libarch/cmdutils.h"
#include "tla/libarch/cmd-register-archive.h"



static t_uchar * usage = "[options] [archive] location";
static t_uchar * version_string = (cfg__std__package " from regexps.com\n"
                                   "\n"
                                   "Copyright 2003 Tom Lord\n"
                                   "\n"
                                   "This is free software; see the source for copying conditions.\n"
                                   "There is NO warranty; not even for MERCHANTABILITY or FITNESS FOR A\n"
                                   "PARTICULAR PURPOSE.\n"
                                   "\n"
                                   "Report bugs to " cfg__tla_bug_mail ".\n"
                                   "\n"
                                   cfg__std__release_id_string
                                   "\n");

#define OPTS(OP) \
  OP (opt_help_msg, "h", "help", 0, \
      "Display a help message and exit.") \
  OP (opt_long_help, "H", 0, 0, \
      "Display a verbose help message and exit.") \
  OP (opt_version, "V", "version", 0, \
      "Display a release identifier string\n" \
      "and exit.") \
  OP (opt_force, "f", "force", 0, \
      "overwrite existing location") \
  OP (opt_present_ok, 0, "present-ok", 0, \
      "return 0 even if archive exists") \
  OP (opt_delete, "d", "delete", 0, \
      "delete archive registration")

t_uchar arch_cmd_register_archive_help[] = ("change an archive location registration\n"
                                            "Record the location of ARCHIVE.\n"
                                            "\n"
                                            "With -d, remove the registration of a previously registered\n"
                                            "archive.  When accompanied by -f, override permissions on\n"
                                            "the registration file and don't complain if the archive is\n"
                                            "not registered.\n"
                                            "\n"
                                            "A LOCATION should be either a directory name or a distant URL.\n"
                                            "\n"
                                            "When registering a new archive, if no ARCHIVE's name is passed on the\n"
                                            "command line, then the archive's name will be read automatically from\n"
                                            "the archive's meta data.\n"
                                            "\n"
                                            "Archive locations are stored in ~/.arch-params/=locations.\n"
                                            "\n"
                                            "You must register the location of a remote archive before you\n"
                                            "access it.  It is not strictly necessary to register the locations\n"
                                            "of local archives (you can always specify their location using\n"
                                            "command line arguments and/or environment variables), but registering\n"
                                            "local archive locations is recommend (for simplicity).\n");

enum options
{
  OPTS (OPT_ENUM)
};

static struct opt_desc opts[] =
{
  OPTS (OPT_DESC)
    {-1, 0, 0, 0, 0}
};



int
arch_cmd_register_archive (t_uchar * program_name, int argc, char * argv[])
{
  int o;
  struct opt_parsed * option;
  int delete;
  int force;
  int quietly_fail;

  delete = 0;
  force = 0;
  quietly_fail = 0;

  safe_buffer_fd (1, 0, O_WRONLY, 0);

  option = 0;

  while (1)
    {
      o = opt_standard (lim_use_must_malloc, &option, opts, &argc, argv, program_name, usage, version_string, arch_cmd_register_archive_help, opt_help_msg, opt_long_help, opt_version);
      if (o == opt_none)
        break;
      switch (o)
        {
        default:
          safe_printfmt (2, "unhandled option `%s'\n", option->opt_string);
          panic ("internal error parsing arguments");

        usage_error:
          opt_usage (2, argv[0], program_name, usage, 1);
          exit (1);

          /* bogus_arg: */
          safe_printfmt (2, "ill-formed argument for `%s' (`%s')\n", option->opt_string, option->arg_string);
          goto usage_error;

        case opt_delete:
          {
            delete = 1;
            break;
          }
        case opt_force:
          {
            force = 1;
            break;
          }
        case opt_present_ok:
          {
            quietly_fail = ARCH_REG_FAIL_NOFAIL;
          }
        }
    }

  if (argc != 3 && argc != 2)
    goto usage_error;

  if (delete)
    {
      t_uchar * archive_name;

      archive_name = argv[1];

      if (!arch_valid_archive_name (archive_name))
        {
          safe_printfmt (2, "%s: invalid archive name (%s)\n",
                         argv[0], archive_name);
          exit (1);
        }

      arch_delete_archive_location (archive_name, force);
    }
  else
    {
      t_uchar * archive_name;
      t_uchar * archive_location;

      if (argc == 3)
        {
          archive_name = argv[1];
          archive_location = argv[2];
          arch_check_uri (archive_location);
        }
      else
        {
          struct arch_archive * archive = 0;

          archive_location = argv[1];
          arch_check_uri (archive_location);

          archive = arch_archive_connect_location (0, archive_location, 0);
          archive_name = arch_get_meta_info_trimming (archive, "name");
          arch_archive_close (archive);
          
          safe_printfmt (1, "Registering archive: %s\n", archive_name);

        }

      if (!arch_valid_archive_name (archive_name))
        {
          safe_printfmt (2, "%s: invalid archive name (%s)\n",
                         argv[0], archive_name);
          exit (1);
        }

      arch_set_archive_location (archive_name, archive_location, force, quietly_fail);
    }

  return 0;
}




/* tag: Tom Lord Sun May 18 20:34:28 2003 (register-archive.c)
 */
