/* cmd-pristines.c
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */


#include "config-options.h"
#include "hackerlab/cmd/main.h"
#include "tla/libarch/namespace.h"
#include "tla/libarch/project-tree.h"
#include "tla/libarch/pristines.h"
#include "tla/libarch/cmd-pristines.h"



static t_uchar * usage = "[options] [limit]";
static t_uchar * version_string = (cfg__std__package " from regexps.com\n"
                                   "\n"
                                   "Copyright 2003 Tom Lord\n"
                                   "\n"
                                   "This is free software; see the source for copying conditions.\n"
                                   "There is NO warranty; not even for MERCHANTABILITY or FITNESS FOR A\n"
                                   "PARTICULAR PURPOSE.\n"
                                   "\n"
                                   "Report bugs to " cfg__tla_bug_mail ".\n"
                                   "\n"
                                   cfg__std__release_id_string
                                   "\n");

#define OPTS(OP) \
  OP (opt_help_msg, "h", "help", 0, \
      "Display a help message and exit.") \
  OP (opt_long_help, "H", 0, 0, \
      "Display a verbose help message and exit.") \
  OP (opt_version, "V", "version", 0, \
      "Display a release identifier string\n" \
      "and exit.") \
  OP (opt_dir, "d", "dir DIR", 1, \
      "cd to DIR first") \
  OP (opt_unlocked, "u", "unlocked", 0, \
      "return only unlocked pristines") \
  OP (opt_locked, "l", "locked", 0, \
      "return only locked pristines") \
  OP (opt_reverse, "r", "reverse", 0, \
      "reverse sort order")

t_uchar arch_cmd_pristines_help[] = ("list pristine trees in a project tree\n"
				     "Print the list of pristine revisions cached in project tree\n"
				     "DIR (or the current directory).\n");

enum options
{
  OPTS (OPT_ENUM)
};

static struct opt_desc opts[] =
{
  OPTS (OPT_DESC)
    {-1, 0, 0, 0, 0}
};



int
arch_cmd_pristines (t_uchar * program_name, int argc, char * argv[])
{
  int o;
  struct opt_parsed * option;
  int pristine_types;
  int reverse;
  char * dir;

  pristine_types = arch_any_pristine;
  reverse = 0;
  dir = str_save (0, ".");

  safe_buffer_fd (1, 0, O_WRONLY, 0);

  option = 0;

  while (1)
    {
      o = opt_standard (lim_use_must_malloc, &option, opts, &argc, argv, program_name, usage, version_string, arch_cmd_pristines_help, opt_help_msg, opt_long_help, opt_version);
      if (o == opt_none)
        break;
      switch (o)
        {
        default:
          safe_printfmt (2, "unhandled option `%s'\n", option->opt_string);
          panic ("internal error parsing arguments");

        usage_error:
          opt_usage (2, argv[0], program_name, usage, 1);
          exit (1);

          /* bogus_arg: */
          safe_printfmt (2, "ill-formed argument for `%s' (`%s')\n", option->opt_string, option->arg_string);
          goto usage_error;

        case opt_dir:
          {
            lim_free (0, dir);
            dir = str_save (0, option->arg_string);
            break;
          }

        case opt_unlocked:
          {
            pristine_types = arch_unlocked_pristine;
            break;
          }

        case opt_locked:
          {
            pristine_types = arch_locked_pristine;
            break;
          }

        case opt_reverse:
          {
            reverse = 1;
            break;
          }
        }
    }

  if (argc > 2)
    goto usage_error;

  {
    t_uchar * limitspec;
    t_uchar * tree_root = 0;
    t_uchar * archive = 0;
    t_uchar * limit = 0;
    rel_table answer = rel_table_nil;

    if (argc == 1)
      limitspec = 0;
    else
      limitspec = argv[1];

    if (limitspec && !arch_valid_package_name (limitspec, arch_maybe_archive, arch_req_category, 1))
      {
        safe_printfmt (2, "%s: invalid revision name (%s)\n",
                       argv[0], limitspec);
        exit (2);
      }

    if (limitspec)
      {
        if (arch_valid_package_name (limitspec, arch_req_archive, arch_req_category, 1))
          archive = arch_parse_package_name (arch_ret_archive, 0, limitspec);

        limit = arch_parse_package_name (arch_ret_non_archive, 0, limitspec);
      }

    tree_root = arch_tree_root (0, dir, 0);

    if (!tree_root)
      {
        safe_printfmt (2, "%s: directory is not a project tree (%s)\n",
                       argv[0], dir);
        exit (2);
      }

    answer = arch_pristines (tree_root, archive, limit, pristine_types);

    if (reverse)
      arch_sort_table_by_name_field (1, answer, 0);

    rel_print_table (1, answer);
  }

  return 0;
}




/* tag: Tom Lord Thu May 22 00:19:17 2003 (ls-pristines.c)
 */
