/* library-txn.h:
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */

#ifndef INCLUDE__LIBARCH__LIBRARY_TXN_H
#define INCLUDE__LIBARCH__LIBRARY_TXN_H


#include "tla/libarch/archive.h"


/* automatically generated __STDC__ prototypes */
extern t_uchar * arch_library_add_choice (const t_uchar * archive,
                                          const t_uchar * revision,
                                          const t_uchar * opt_library,
                                          const t_uchar * opt_same_dev_path);
extern t_uchar * arch_library_greedy_add_choice (const t_uchar * archive, 
                                                 const t_uchar * revision, 
                                                 const t_uchar * opt_same_dev_path,
                                                 int require_greedy);
extern rel_table make_search_path (rel_table full_user_path,
                                   const t_uchar * opt_same_dev_path,
                                   int require_greedy);
extern t_uchar * arch_find_best_library(const t_uchar * archive, 
                                        rel_table search_path,
                                        const t_uchar * revision);
extern void arch_library_add (int chatter_fd,
                              int no_patch_noise,
                              struct arch_archive * arch,
                              const t_uchar * revision,
                              const t_uchar * opt_library,
                              const t_uchar * opt_same_dev_path,
                              int sparse,
                              int escape_classes);
#endif  /* INCLUDE__LIBARCH__LIBRARY_TXN_H */


/* tag: Tom Lord Sat Jan  5 15:26:10 2002 (library-txn.h)
 */
