/* cmd-grab.h:
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord, Mark Thomas
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */

#ifndef INCLUDE__LIBARCH__CMD_GRAB_H
#define INCLUDE__LIBARCH__CMD_GRAB_H


#include "hackerlab/machine/types.h"



extern t_uchar arch_cmd_grab_help[];


/* automatically generated __STDC__ prototypes */
extern int arch_cmd_grab (t_uchar * program_name, int argc, char * argv[]);
#endif  /* INCLUDE__LIBARCH__CMD_MY_ID_H */


/* tag: Mark Thomas Tue Jul 29 22:22:36 BST 2003 (cmd-grab.h)
 */
