/* archive-pfs.h:
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */

#ifndef INCLUDE__LIBARCH__ARCHIVE_PFS_H
#define INCLUDE__LIBARCH__ARCHIVE_PFS_H


#include "tla/libarch/archive.h"
#include "tla/libarch/pfs.h"



struct arch_pfs_archive
{
  struct arch_archive arch;
  struct arch_pfs_session * pfs;

  struct arch_archive * from_arch;

  t_uchar * txn_signature_file;
  rel_table deferred_sha1_sums;

  int txn_signature_fd;
};




/* automatically generated __STDC__ prototypes */
extern void arch_pfs_make_archive (const t_uchar * name,
                                   const t_uchar *location,
                                   const t_uchar * version,
                                   const t_uchar * mirror_of,
                                   int dot_listing_lossage,
                                   int signed_archive);
extern void arch_pfs_archive_connect (struct arch_archive ** a);
#endif  /* INCLUDE__LIBARCH__ARCHIVE_PFS_H */


/* tag: Tom Lord Sat Jan  5 15:26:10 2002 (archive-pfs.h)
 */
