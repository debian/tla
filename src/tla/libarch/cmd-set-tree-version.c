/* cmd-set-tree-version.c
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */


#include "config-options.h"
#include "hackerlab/cmd/main.h"
#include "tla/libarch/namespace.h"
#include "tla/libarch/project-tree.h"
#include "tla/libarch/cmd-set-tree-version.h"



static t_uchar * usage = "[options] [archive]/version";
static t_uchar * version_string = (cfg__std__package " from regexps.com\n"
                                   "\n"
                                   "Copyright 2003 Tom Lord\n"
                                   "\n"
                                   "This is free software; see the source for copying conditions.\n"
                                   "There is NO warranty; not even for MERCHANTABILITY or FITNESS FOR A\n"
                                   "PARTICULAR PURPOSE.\n"
                                   "\n"
                                   "Report bugs to " cfg__tla_bug_mail ".\n"
                                   "\n"
                                   cfg__std__release_id_string
                                   "\n");

#define OPTS(OP) \
  OP (opt_help_msg, "h", "help", 0, \
      "Display a help message and exit.") \
  OP (opt_long_help, "H", 0, 0, \
      "Display a verbose help message and exit.") \
  OP (opt_version, "V", "version", 0, \
      "Display a release identifier string\n" \
      "and exit.") \
  OP (opt_archive, "A", "archive", 1, \
      "Override `my-default-archive'") \
  OP (opt_dir, "d", "dir DIR", 1, \
      "cd to DIR first")

t_uchar arch_cmd_set_tree_version_help[] = ("set the default version for a project tree\n"
                                            "This operation sets the default version on which to check-in\n"
                                            "the source tree DIR (default: the current directory) during\n"
                                            "a commit.\n"
                                            "\n"
                                            "You can specify the archive for this version as an ordinary\n"
                                            "command line argument (ARCHIVE/VERSION) or via -R, -A and\n"
                                            "the environment as usual (try \"tla my-default-archive --help\").\n");

enum options
{
  OPTS (OPT_ENUM)
};

static struct opt_desc opts[] =
{
  OPTS (OPT_DESC)
    {-1, 0, 0, 0, 0}
};



int
arch_cmd_set_tree_version (t_uchar * program_name, int argc, char * argv[])
{
  int o;
  struct opt_parsed * option;
  char * default_archive = 0;
  char * dir = 0;

  dir = str_save (0, ".");

  safe_buffer_fd (1, 0, O_WRONLY, 0);

  option = 0;

  while (1)
    {
      o = opt_standard (lim_use_must_malloc, &option, opts, &argc, argv, program_name, usage, version_string, arch_cmd_set_tree_version_help, opt_help_msg, opt_long_help, opt_version);
      if (o == opt_none)
        break;
      switch (o)
        {
        default:
          safe_printfmt (2, "unhandled option `%s'\n", option->opt_string);
          panic ("internal error parsing arguments");

        usage_error:
          opt_usage (2, argv[0], program_name, usage, 1);
          exit (1);

          /* bogus_arg: */
          safe_printfmt (2, "ill-formed argument for `%s' (`%s')\n", option->opt_string, option->arg_string);
          goto usage_error;


        case opt_dir:
          {
            dir = str_save (0, option->arg_string);
            break;
          }

        case opt_archive:
          {
            default_archive = str_save (0, option->arg_string);
            break;
          }
        }
    }

  if (argc != 2)
    goto usage_error;

  {
    char * version_spec;
    char * archive = 0;
    char * version = 0;
    t_uchar * tree_root = 0;

    version_spec = argv[1];

    if (!arch_valid_package_name (version_spec, arch_maybe_archive, arch_req_version, 0))
      {
        safe_printfmt (2, "%s: invalid version id (%s)\n",
                       argv[0], version_spec);
      }

    archive = arch_parse_package_name (arch_ret_archive, default_archive, version_spec);
    version = arch_parse_package_name (arch_ret_package_version, 0, version_spec);

    tree_root = arch_tree_root (0, dir, 0);

    if (!tree_root)
      {
        safe_printfmt (2, "%s: not in project tree (%s)\n", argv[0], dir);
        exit (1);
      }

    arch_set_tree_version (tree_root, archive, version);

    lim_free (0, archive);
    lim_free (0, version);
  }

  lim_free (0, default_archive);
  lim_free (0, dir);

  return 0;
}



/* tag: Tom Lord Mon May 12 14:52:06 2003 (set-tree-version.c)
 */
