/* add-id.c
 *
 ****************************************************************
 * Copyright (C) 2001, 2002, 2003  Tom Lord
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */


#include "config-options.h"
#include "hackerlab/cmd/main.h"
#include "tla/libarch/changelogs.h"
#include "tla/libarch/inv-ids.h"
#include "tla/libarch/cmd.h"
#include "tla/libarch/cmd-add-id.h"



static t_uchar * usage = "[options] file ...";
static t_uchar * version_string = (cfg__std__package " from regexps.com\n"
                                   "\n"
                                   "Copyright 2001, 2002, 2003 Tom Lord\n"
                                   "\n"
                                   "This is free software; see the source for copying conditions.\n"
                                   "There is NO warranty; not even for MERCHANTABILITY or FITNESS FOR A\n"
                                   "PARTICULAR PURPOSE.\n"
                                   "\n"
                                   "Report bugs to " cfg__tla_bug_mail ".\n"
                                   "\n"
                                   cfg__std__release_id_string
                                   "\n");

#define OPTS(OP) \
  OP (opt_help_msg, "h", "help", 0, \
      "Display a help message and exit.") \
  OP (opt_long_help, "H", 0, 0, \
      "Display a verbose help message and exit.") \
  OP (opt_version, "V", "version", 0, \
      "Display a release identifier string\n" \
      "and exit.") \
  OP (opt_stop_on_error, "s", "stop-on-error", 0, \
      "stop adding further on error (default to continue)") \
  OP (opt_id, "i", "id ID", 1, \
      "Specify ID, instead of using auto-generated id.")

t_uchar arch_cmd_add_id_help[] = ("add an explicit inventory id\n"
                                   "Create an explicit inventory id for FILE (which may be a\n"
                                   "regular file, symbolic link, or directory).\n");


enum options
{
  OPTS (OPT_ENUM)
};

static struct opt_desc opts[] =
{
  OPTS (OPT_DESC)
    {-1, 0, 0, 0, 0}
};



int
arch_cmd_add_id (t_uchar *program_name, int argc, char * argv[])
{
  int o;
  struct opt_parsed * option;
  char * id = 0;
  int stop_on_error = 0;
  safe_buffer_fd (1, 0, O_WRONLY, 0);

  option = 0;

  while (1)
    {
      o = opt_standard (lim_use_must_malloc, &option, opts, &argc, argv, program_name, usage, version_string, arch_cmd_add_id_help, opt_help_msg, opt_long_help, opt_version);
      if (o == opt_none)
        break;
      switch (o)
        {
        default:
          safe_printfmt (2, "unhandled option `%s'\n", option->opt_string);
          panic ("internal error parsing arguments");

        usage_error:
          opt_usage (2, argv[0], program_name, usage, 1);
          exit (1);

          /* bogus_arg: */
          safe_printfmt (2, "ill-formed argument for `%s' (`%s')\n", option->opt_string, option->arg_string);
          goto usage_error;

        case opt_id:
           {
                  id = str_save(0, option->arg_string);
                  break;
           }
        case opt_stop_on_error:
           {
               stop_on_error = 1;
               break;
           }   
        }
    }

  if (argc < 2)
    goto usage_error;

  {
    int a, r;

    for (a = 1; a < argc; ++a)
      {
        t_uchar * file;
        t_uchar * tagline_id;
        t_uchar * new_explicit_id;

        file = argv[a];

        if (!safe_access (file, F_OK))
          tagline_id = arch_inventory_id (arch_tagline_id_tagging, 0, file, 0, 0, 0);
        else
          tagline_id = 0;

        if (!tagline_id || !arch_id_indicates_changelog (tagline_id))
          if (!id)
            new_explicit_id = arch_generate_id ();
          else
            new_explicit_id = id;
        else
          new_explicit_id = str_save (0, 2 + tagline_id);

        r = arch_add_explicit_id (file, new_explicit_id);
        if ((r == 0) && stop_on_error)
          {
            safe_printfmt (2, "error detected, stop adding here\n");
            exit(1);
          }
        lim_free (0, new_explicit_id);
        lim_free (0, tagline_id);
      }
  }


  return 0;
}



/* tag: Tom Lord Wed May 14 12:24:43 2003 (add-tag.c)
 */
