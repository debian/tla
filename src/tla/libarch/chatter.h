/* chatter.h:
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */

#ifndef INCLUDE__LIBARCH__CHATTER_H
#define INCLUDE__LIBARCH__CHATTER_H


#if !defined(__GNUC__)
#  undef __attribute__
#  define __attribute__(X)
#endif

extern void arch_chatter (int fd, char * fmt, ...)
     __attribute__((format (printf, 2, 3)));



/* automatically generated __STDC__ prototypes */
extern void arch_chatter (int chatter_fd, char * format, ...);
#endif  /* INCLUDE__LIBARCH__CHATTER_H */


/* tag: Tom Lord Sat Jan  5 15:26:10 2002 (chatter.h)
 */
