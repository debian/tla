/* exec.h:
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */

#ifndef INCLUDE__LIBARCH__EXEC_H
#define INCLUDE__LIBARCH__EXEC_H


#include "hackerlab/machine/types.h"


/* automatically generated __STDC__ prototypes */
extern int arch_util_execvp (t_uchar * prog, t_uchar ** argv);
#endif  /* INCLUDE__LIBARCH__EXEC_H */


/* tag: Tom Lord Sat Jan  5 15:26:10 2002 (exec.h)
 */
