/* cmd-library-add.c
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */


#include "config-options.h"
#include "hackerlab/cmd/main.h"
#include "tla/libarch/my.h"
#include "tla/libarch/libraries.h"
#include "tla/libarch/namespace.h"
#include "tla/libarch/archive.h"
#include "tla/libarch/library-txn.h"
#include "tla/libarch/cmd.h"
#include "tla/libarch/cmdutils.h"
#include "tla/libarch/cmd-library-add.h"



static t_uchar * usage = "[options] revision";
static t_uchar * version_string = (cfg__std__package " from regexps.com\n"
                                   "\n"
                                   "Copyright 2003 Tom Lord\n"
                                   "\n"
                                   "This is free software; see the source for copying conditions.\n"
                                   "There is NO warranty; not even for MERCHANTABILITY or FITNESS FOR A\n"
                                   "PARTICULAR PURPOSE.\n"
                                   "\n"
                                   "Report bugs to " cfg__tla_bug_mail ".\n"
                                   "\n"
                                   cfg__std__release_id_string
                                   "\n");

#define OPTS(OP) \
  OP (opt_help_msg, "h", "help", 0, \
      "Display a help message and exit.") \
  OP (opt_long_help, "H", 0, 0, \
      "Display a verbose help message and exit.") \
  OP (opt_version, "V", "version", 0, \
      "Display a release identifier string\n" \
      "and exit.") \
  OP (opt_archive, "A", "archive", 1, \
      "Override `my-default-archive'") \
  OP (opt_sparse, "s", "sparse", 0, \
      "Don't fill in gaps in the library.") \
  OP (opt_non_sparse, 0, "non-sparse", 0, \
      "Fill in gaps in the library.") \
  OP (opt_library, "L", "library LIB", 1, \
      "specify which library to add to") \
  OP (opt_devpath, 0, "for-links PATH", 1, \
      "require a lib on the same device as PATH") \
  OP (opt_unescaped, 0, "unescaped", 0, \
      "show filenames in unescaped form")

t_uchar arch_cmd_library_add_help[] = ("add a revision to the revision library\n"
                                       "Add REVISION to your revision library.\n"
                                       "\n"
                                       "This command has no effect and exits with status 0 if the\n"
                                       "indicated revision is already in the library.\n");

enum options
{
  OPTS (OPT_ENUM)
};

static struct opt_desc opts[] =
{
  OPTS (OPT_DESC)
    {-1, 0, 0, 0, 0}
};



int
arch_cmd_library_add (t_uchar * program_name, int argc, char * argv[])
{
  int o;
  struct opt_parsed * option;
  t_uchar * default_archive = 0;
  int sparse = -1;
  t_uchar * in_library = 0;
  t_uchar * on_same_device = 0;
  int escape_classes = arch_escape_classes;

  default_archive = 0;

  safe_buffer_fd (1, 0, O_WRONLY, 0);

  option = 0;

  while (1)
    {
      o = opt_standard (lim_use_must_malloc, &option, opts, &argc, argv, program_name, usage, version_string, arch_cmd_library_add_help, opt_help_msg, opt_long_help, opt_version);
      if (o == opt_none)
        break;
      switch (o)
        {
        default:
          safe_printfmt (2, "unhandled option `%s'\n", option->opt_string);
          panic ("internal error parsing arguments");

        usage_error:
          opt_usage (2, argv[0], program_name, usage, 1);
          exit (1);

          /* bogus_arg: */
          safe_printfmt (2, "ill-formed argument for `%s' (`%s')\n", option->opt_string, option->arg_string);
          goto usage_error;

        case opt_archive:
          {
            default_archive = str_save (0, option->arg_string);
            break;
          }

        case opt_sparse:
          {
            sparse = 1;
            break;
          }

        case opt_non_sparse:
          {
            sparse = 0;
            break;
          }

        case opt_library:
          {
            lim_free (0, in_library);
            in_library = str_save (0, option->arg_string);
            break;
          }

        case opt_devpath:
          {
            lim_free (0, on_same_device);
            on_same_device = str_save (0, option->arg_string);
            break;
          }
        
        case opt_unescaped:
          {
            escape_classes = 0;
            break;
          }
        }
    }

  if (argc != 2)
    goto usage_error;

  if (default_archive && !arch_valid_archive_name (default_archive))
    {
      safe_printfmt (2, "%s: invalid archive name (%s)\n",
                     argv[0], default_archive);
      exit (1);
    }

  {
    t_uchar * revspec;
    t_uchar * archive = 0;
    t_uchar * revision = 0;
    struct arch_archive * arch = 0;

    revspec = argv[1];

    if (!arch_valid_package_name (revspec, arch_maybe_archive, arch_req_version, 1))
      {
        safe_printfmt (2, "%s: invalid revision specification (%s)\n",
                       argv[0], revspec);
        exit (2);
      }

    {
      rel_table library_path = rel_table_nil;

      library_path = arch_my_library_path (arch_library_path_add_order);
      if (!rel_n_records (library_path))
        {
          safe_printfmt (2, "%s: no default library (try tla my-revision-library -H)\n", argv[0]);
          exit (2);
        }
      rel_free_table (library_path);
    }

    archive = arch_parse_package_name (arch_ret_archive, default_archive, revspec);
    if (!archive)
      {
        safe_printfmt (2, "%s: no archive specified\n", argv[0]);
        exit (2);
      }

    arch = arch_archive_connect (archive, 0);

    if (arch_valid_package_name (revspec, arch_maybe_archive, arch_req_patch_level, 0))
      {
        arch_check_for (arch, arch_req_patch_level, revspec);
        revision = arch_parse_package_name (arch_ret_non_archive, 0, revspec);
      }
    else
      {
        t_uchar * version = 0;
        rel_table revisions = rel_table_nil;
        
        arch_check_for (arch, arch_req_version, revspec);

        version = arch_parse_package_name (arch_ret_non_archive, 0, revspec);
        revisions = arch_archive_revisions (arch, version, 2);

        if (!rel_n_records (revisions))
          {
            safe_printfmt (2, "%s: version has no revisions (%s/%s)\n",
                           argv[0], archive, version);
            exit (1);
          }

        revision = str_save (0, rel_peek_str (revisions, rel_n_records (revisions) - 1, 0));

        lim_free (0, version);
        rel_free_table (revisions);
      }


    arch_library_add (1, 0, arch, revision, in_library, on_same_device, sparse, escape_classes);

    lim_free (0, archive);
    lim_free (0, revision);
    arch_archive_close (arch);
  }

  return 0;
}




/* tag: Tom Lord Fri May 30 15:02:05 2003 (library-add.c)
 */
