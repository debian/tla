/* archive-setup.h:
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */

#ifndef INCLUDE__LIBARCH__ARCHIVE_SETUP_H
#define INCLUDE__LIBARCH__ARCHIVE_SETUP_H


#include "tla/libawk/relational.h"
#include "tla/libarch/archive.h"



enum arch_archive_setup_tag_op
{
  arch_archive_setup_no_tags,
  arch_archive_setup_make_base0_tag,
};


/* automatically generated __STDC__ prototypes */
extern void arch_setup_archive_simple (int chatter_fd,
                                       const t_uchar * archive_spec,
                                       const t_uchar * revision_spec);
extern void arch_setup_archive_simple_ext (int chatter_fd,
                                           struct arch_archive * arch,
                                           const t_uchar * revision_spec);
extern void arch_setup_archive (int chatter_fd, rel_table wants,
                                enum arch_archive_setup_tag_op tag_op,
                                int archive_cache);
extern void arch_setup_archive_ext (int chatter_fd, rel_table wants,
                                    enum arch_archive_setup_tag_op tag_op,
                                    int archive_cache,
                                    struct arch_archive * initial_arch);
#endif  /* INCLUDE__LIBARCH__ARCHIVE_SETUP_H */


/* tag: Tom Lord Sat Jan  5 15:26:10 2002 (archive-setup.h)
 */
