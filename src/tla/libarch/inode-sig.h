/* inode-sig.h:
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */

#ifndef INCLUDE__LIBARCH__INODE_SIG_H
#define INCLUDE__LIBARCH__INODE_SIG_H


#include "tla/libawk/relational.h"
#include "tla/libawk/associative.h"


/* automatically generated __STDC__ prototypes */
extern t_uchar * arch_statb_inode_sig (struct stat * statb);
extern rel_table arch_tree_inode_sig (const t_uchar * tree_root);
extern void arch_snap_inode_sig (const t_uchar * tree_root,
                                 const t_uchar * archive,
                                 const t_uchar * revision);
extern void arch_snap_inode_sig_files (const t_uchar * tree_root,
                                       const t_uchar * archive,
                                       const t_uchar * prev_revision,
                                       const t_uchar * revision,
                                       rel_table file_list);
extern int arch_valid_inode_sig (const t_uchar * tree_root,
                                 const t_uchar * archive,
                                 const t_uchar * revision);
extern void arch_read_inode_sig (rel_table * as_table,
                                 assoc_table * as_assoc,
                                 const t_uchar * tree_root,
                                 const t_uchar * archive,
                                 const t_uchar * revision);
extern void arch_read_id_shortcut (assoc_table * ids_shortcut,
                                   const t_uchar * tree_root);
#endif  /* INCLUDE__LIBARCH__INODE_SIG_H */


/* tag: Tom Lord Fri Sep 12 10:33:59 2003 (inode-sig.h)
 */
