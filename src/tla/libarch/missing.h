/* missing.h:
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */

#ifndef INCLUDE__LIBARCH__WHATS_MISSING_H
#define INCLUDE__LIBARCH__WHATS_MISSING_H


#include "tla/libawk/relational.h"
#include "tla/libarch/archive.h"


/* automatically generated __STDC__ prototypes */
extern rel_table arch_missing (t_uchar * tree_root, struct arch_archive * arch, t_uchar * version, int skip_present);
extern int arch_revision_has_present_patch (t_uchar * tree_root, struct arch_archive * arch, t_uchar * revision);
extern rel_table arch_filter_present_logs (t_uchar * tree_root, struct arch_archive * arch, t_uchar * version, rel_table logs);
#endif  /* INCLUDE__LIBARCH__WHATS_MISSING_H */


/* tag: Tom Lord Sat Jan  5 15:26:10 2002 (whats-missing.h)
 */
