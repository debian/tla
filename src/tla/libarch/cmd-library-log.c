/* cmd-library-log.c
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */


#include "config-options.h"
#include "hackerlab/cmd/main.h"
#include "tla/libarch/my.h"
#include "tla/libarch/libraries.h"
#include "tla/libarch/namespace.h"
#include "tla/libarch/cmdutils.h"
#include "tla/libarch/cmd-library-log.h"



static t_uchar * usage = "[options] revision";
static t_uchar * version_string = (cfg__std__package " from regexps.com\n"
                                   "\n"
                                   "Copyright 2003 Tom Lord\n"
                                   "\n"
                                   "This is free software; see the source for copying conditions.\n"
                                   "There is NO warranty; not even for MERCHANTABILITY or FITNESS FOR A\n"
                                   "PARTICULAR PURPOSE.\n"
                                   "\n"
                                   "Report bugs to " cfg__tla_bug_mail ".\n"
                                   "\n"
                                   cfg__std__release_id_string
                                   "\n");

#define OPTS(OP) \
  OP (opt_help_msg, "h", "help", 0, \
      "Display a help message and exit.") \
  OP (opt_long_help, "H", 0, 0, \
      "Display a verbose help message and exit.") \
  OP (opt_version, "V", "version", 0, \
      "Display a release identifier string\n" \
      "and exit.") \
  OP (opt_archive, "A", "archive", 1, \
      "Override `my-default-archive'") \
  OP (opt_silent, "s", "silent", 0, \
      "suppress reassuring messages")

t_uchar arch_cmd_library_log_help[] = ("output a log message from the revision library\n"
                                       "Print the log message for REVISION from the library.\n");

enum options
{
  OPTS (OPT_ENUM)
};

static struct opt_desc opts[] =
{
  OPTS (OPT_DESC)
    {-1, 0, 0, 0, 0}
};



int
arch_cmd_library_log (t_uchar * program_name, int argc, char * argv[])
{
  int o;
  struct opt_parsed * option;
  t_uchar * default_archive;
  int silent;

  default_archive = 0;
  silent = 0;

  safe_buffer_fd (1, 0, O_WRONLY, 0);

  option = 0;

  while (1)
    {
      o = opt_standard (lim_use_must_malloc, &option, opts, &argc, argv, program_name, usage, version_string, arch_cmd_library_log_help, opt_help_msg, opt_long_help, opt_version);
      if (o == opt_none)
        break;
      switch (o)
        {
        default:
          safe_printfmt (2, "unhandled option `%s'\n", option->opt_string);
          panic ("internal error parsing arguments");

        usage_error:
          opt_usage (2, argv[0], program_name, usage, 1);
          exit (1);

          /* bogus_arg: */
          safe_printfmt (2, "ill-formed argument for `%s' (`%s')\n", option->opt_string, option->arg_string);
          goto usage_error;


        case opt_archive:
          {
            default_archive = str_save (0, option->arg_string);
            break;
          }

        case opt_silent:
          {
            silent = 1;
            break;
          }
        }
    }

  if (argc != 2)
    goto usage_error;

  {
    t_uchar * revspec;
    t_uchar * archive = 0;
    t_uchar * revision = 0;
    t_uchar * answer;

    revspec = argv[1];

    if (!arch_valid_package_name (revspec, arch_maybe_archive, arch_req_patch_level, 0))
      {
        safe_printfmt (2, "%s: invalid revision name (%s)\n",
                       argv[0], revspec);
        exit (2);
      }

    archive = arch_parse_package_name (arch_ret_archive, default_archive, revspec);
    if (!archive)
      {
        safe_printfmt (2, "%s: no archive specified for library-log\n",
                       argv[0]);
        exit (2);
      }

    revision = arch_parse_package_name (arch_ret_non_archive, 0, revspec);

    arch_check_library_for_revision (archive, revision);

    answer = arch_library_log (archive, revision);

    if (answer)
      safe_printfmt (1, "%s\n", answer);
    else
      {
        if (!silent)
          {
            safe_printfmt (2, "%s: revision not present in library (%s/%s)\n",
                           argv[0], archive, revision);
          }
        exit (1);
      }
  }

  return 0;
}




/* tag: Tom Lord Wed May 21 14:05:20 2003 (library-log.c)
 */
