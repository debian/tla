/* namespace.h:
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */

#ifndef INCLUDE__LIBARCH__NAMESPACE_H
#define INCLUDE__LIBARCH__NAMESPACE_H


#include "hackerlab/machine/types.h"
#include "tla/libawk/relational.h"



#define arch_archive_re "[-a-zA-Z0-9]+[[:(\\.[-a-zA-Z0-9]*):]]*@[-a-zA-Z0-9.]*[[:(--[-a-zA-Z0-9.]*):]]?"
#define arch_base_re "[[:([a-zA-Z][[:([a-zA-Z0-9]*[[:(-[a-zA-Z0-9]+):]]?):]]*):]]"
#define arch_vsn_re "[[:([0-9]+\\.[0-9]+):]]"
#define arch_lvl_re "[[:(base-0|[[:([[:(patch|version|versionfix:)]]-[[:digit:]]+):]]):]]"


enum arch_valid_package_name_archive
{
  arch_maybe_archive,
  arch_req_archive,
  arch_no_archive,
};

enum arch_valid_package_name_types
{
  arch_req_category,
  arch_req_package,
  arch_req_version,
  arch_req_patch_level,
};

enum arch_parse_package_name_type
{
  arch_ret_archive,
  arch_ret_non_archive,
  arch_ret_category,
  arch_ret_branch,
  arch_ret_package,
  arch_ret_version,
  arch_ret_patch_level,
  arch_ret_package_version,
};

enum arch_patch_level_type
{
  arch_is_base0_level,
  arch_is_patch_level,
  arch_is_version_level,
  arch_is_versionfix_level,
};





/* automatically generated __STDC__ prototypes */
extern int arch_valid_id (const t_uchar * id);
extern int arch_valid_archive_name (const t_uchar * name);
extern int arch_valid_patch_level_name (const t_uchar * name);
extern int arch_valid_config_name (const t_uchar * name);
extern int arch_valid_package_name (const t_uchar * name,
                                    enum arch_valid_package_name_archive archive_disposition,
                                    enum arch_valid_package_name_types type,
                                    int tolerant);
extern int arch_is_system_package_name (const t_uchar * name);
extern t_uchar * arch_parse_package_name (enum arch_parse_package_name_type type,
                                          const t_uchar * default_archive,
                                          const t_uchar * name);
extern t_uchar * arch_fully_qualify (const t_uchar * default_archive,
                                     const t_uchar * name);
extern int arch_names_cmp (const t_uchar * a,
                           const t_uchar * b);
extern void arch_sort_table_by_name_field (int reverse_p, rel_table table, int field);
extern void arch_sort_table_by_patch_level_field (int reverse_p, rel_table table, int field);
extern int arch_patch_lvl_cmp (const t_uchar * a,
                               const t_uchar * b);
extern rel_table arch_pick_archives_by_field (rel_table in, int field);
extern rel_table arch_pick_categories_by_field (rel_table in, int field);
extern rel_table arch_pick_branches_by_field (rel_table in, int field);
extern rel_table arch_pick_versions_by_field (rel_table in, int field);
extern rel_table arch_pick_revisions_by_field (rel_table in, int field);
extern rel_table arch_pick_patch_levels_by_field (rel_table in, int field);
extern enum arch_patch_level_type arch_analyze_patch_level (t_ulong * n,
                                                            const t_uchar * patch_level);
extern t_uchar * arch_form_patch_level (enum arch_patch_level_type type, t_ulong n);
#endif  /* INCLUDE__LIBARCH__NAMESPACE_H */


/* tag: Tom Lord Sat Jan  5 15:26:10 2002 (namespace.h)
 */
