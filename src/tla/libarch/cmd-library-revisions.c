/* cmd-library-revisions.c
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */


#include "config-options.h"
#include "hackerlab/cmd/main.h"
#include "tla/libarch/patch-logs.h"
#include "tla/libarch/namespace.h"
#include "tla/libarch/project-tree.h"
#include "tla/libarch/my.h"
#include "tla/libarch/libraries.h"
#include "tla/libarch/cmd-library-revisions.h"



static t_uchar * usage = "[options] [version]";
static t_uchar * version_string = (cfg__std__package " from regexps.com\n"
                                   "\n"
                                   "Copyright 2003 Tom Lord\n"
                                   "\n"
                                   "This is free software; see the source for copying conditions.\n"
                                   "There is NO warranty; not even for MERCHANTABILITY or FITNESS FOR A\n"
                                   "PARTICULAR PURPOSE.\n"
                                   "\n"
                                   "Report bugs to " cfg__tla_bug_mail ".\n"
                                   "\n"
                                   cfg__std__release_id_string
                                   "\n");

#define OPTS(OP) \
  OP (opt_help_msg, "h", "help", 0, \
      "Display a help message and exit.") \
  OP (opt_long_help, "H", 0, 0, \
      "Display a verbose help message and exit.") \
  OP (opt_version, "V", "version", 0, \
      "Display a release identifier string\n" \
      "and exit.") \
  OP (opt_archive, "A", "archive", 1, \
      "Override `my-default-archive'") \
  OP (opt_reverse, "r", "reverse", 0, \
      "sort from newest to oldest") \
  OP (opt_full, "f", "full", 0, \
      "list fully qualified names") \
  OP (opt_summary, "s", "summary", 0, \
      "print a summary of each patch") \
  OP (opt_creator, "c", "creator", 0, \
      "print the creator of each patch") \
  OP (opt_date, "D", "date", 0, \
      "print the date of each patch")


t_uchar arch_cmd_library_revisions_help[] = ("list the revisions in a library version\n"
                                             "List all revisions within a particular archive/version with\n"
                                             "records in the revision library.\n");

enum options
{
  OPTS (OPT_ENUM)
};

static struct opt_desc opts[] =
{
  OPTS (OPT_DESC)
    {-1, 0, 0, 0, 0}
};



int
arch_cmd_library_revisions (t_uchar * program_name, int argc, char * argv[])
{
  int o;
  struct opt_parsed * option;
  t_uchar * default_archive;
  int reverse;
  int full;
  int summary;
  int creator;
  int date;
  int any_headers;


  default_archive = 0;
  reverse = 0;
  full = 0;
  summary = 0;
  creator = 0;
  date = 0;
  any_headers = 0;

  safe_buffer_fd (1, 0, O_WRONLY, 0);

  option = 0;

  while (1)
    {
      o = opt_standard (lim_use_must_malloc, &option, opts, &argc, argv, program_name, usage, version_string, arch_cmd_library_revisions_help, opt_help_msg, opt_long_help, opt_version);
      if (o == opt_none)
        break;
      switch (o)
        {
        default:
          safe_printfmt (2, "unhandled option `%s'\n", option->opt_string);
          panic ("internal error parsing arguments");

        usage_error:
          opt_usage (2, argv[0], program_name, usage, 1);
          exit (1);

          /* bogus_arg: */
          safe_printfmt (2, "ill-formed argument for `%s' (`%s')\n", option->opt_string, option->arg_string);
          goto usage_error;

        case opt_archive:
          {
            default_archive = str_save (0, option->arg_string);
            break;
          }

        case opt_reverse:
          {
            reverse = 1;
            break;
          }

        case opt_full:
          {
            full = 1;
            break;
          }

        case opt_summary:
          {
            summary = 1;
            any_headers = 1;
            break;
          }
        case opt_date:
          {
            date = 1;
            any_headers = 1;
            break;
          }
        case opt_creator:
          {
            creator = 1;
            any_headers = 1;
            break;
          }
        }
    }

  if (argc > 2)
    goto usage_error;

  default_archive = arch_my_default_archive (default_archive);

  {
    t_uchar * version_spec = 0;
    t_uchar * archive = 0;
    t_uchar * version = 0;
    rel_table revisions = rel_table_nil;

    if (argc == 2)
      version_spec = str_save (0, argv[1]);
    else
      version_spec = arch_try_tree_version (program_name);

    if (!arch_valid_package_name (version_spec, arch_maybe_archive, arch_req_version, 0))
      {
        safe_printfmt (2, "%s: invalid version name (%s)\n",
                       argv[0], version_spec);
        exit (2);
      }

    archive = arch_parse_package_name (arch_ret_archive, default_archive, version_spec);
    version = arch_parse_package_name (arch_ret_package_version, 0, version_spec);

    revisions = arch_library_revisions (archive, version, full);

    if (reverse)
      {
        if (full)
          arch_sort_table_by_name_field (1, revisions, 0);
        else
          arch_sort_table_by_patch_level_field (1, revisions, 0);
      }

    if (!any_headers)
      rel_print_table (1, revisions);
    else
      {
        int x;

        for (x = 0; x < rel_n_records (revisions); ++x)
          {
            t_uchar * package_patch_level = 0;
            t_uchar * log;
            assoc_table headers = 0;
            t_uchar * body = 0;

            if (full)
              package_patch_level = arch_parse_package_name (arch_ret_non_archive, 0, rel_peek_str (revisions, x, 0));
            else
              package_patch_level = str_alloc_cat_many (0, version, "--", rel_peek_str (revisions, x, 0), str_end);

            log = arch_library_log (archive, package_patch_level);

            arch_parse_log (0, &headers, (const t_uchar **)&body, log);

            safe_printfmt (1, "%s\n", rel_peek_str (revisions, x, 0));
            if (date)
              {
                const t_uchar * d;
                d = assoc_get_str_taking (headers, rel_make_field_str ("standard-date"));
                safe_printfmt (1, "    %s\n", (d ? d : (t_uchar *)"<no standard-date: header>"));
              }
            if (creator)
              {
                const t_uchar * c;
                c = assoc_get_str_taking (headers, rel_make_field_str ("creator"));
                safe_printfmt (1, "    %s\n", (c ? c : (t_uchar *)"<no creator: header>"));
              }
            if (summary)
              {
                const t_uchar * s;
                s = assoc_get_str_taking (headers, rel_make_field_str ("summary"));
                safe_printfmt (1, "    %s\n", (s ? s : (t_uchar *)"<no summary: header>"));
              }

            lim_free (0, package_patch_level);
            free_assoc_table (headers);
            lim_free (0, body);
          }


      }

    lim_free (0, version_spec);
  }

  return 0;
}




/* tag: Tom Lord Wed May 21 14:54:14 2003 (library-revisions.c)
 */
