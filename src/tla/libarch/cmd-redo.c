/* cmd-redo.c
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */


#include "config-options.h"
#include "hackerlab/cmd/main.h"
#include "hackerlab/fs/file-names.h"
#include "tla/libfsutils/rmrf.h"
#include "tla/libarch/namespace.h"
#include "tla/libarch/project-tree.h"
#include "tla/libarch/patch-logs.h"
#include "tla/libarch/local-cache.h"
#include "tla/libarch/proj-tree-lint.h"
#include "tla/libarch/undo.h"
#include "tla/libarch/apply-changeset.h"
#include "tla/libarch/cmd.h"
#include "tla/libarch/cmd-redo.h"


/* __STDC__ prototypes for static functions */
static void redo_callback (void * ign, const char * fmt, va_list ap);



static t_uchar * usage = "[options] [changeset]";
static t_uchar * version_string = (cfg__std__package " from regexps.com\n"
                                   "\n"
                                   "Copyright 2003 Tom Lord\n"
                                   "\n"
                                   "This is free software; see the source for copying conditions.\n"
                                   "There is NO warranty; not even for MERCHANTABILITY or FITNESS FOR A\n"
                                   "PARTICULAR PURPOSE.\n"
                                   "\n"
                                   "Report bugs to " cfg__tla_bug_mail ".\n"
                                   "\n"
                                   cfg__std__release_id_string
                                   "\n");

#define OPTS(OP) \
  OP (opt_help_msg, "h", "help", 0, \
      "Display a help message and exit.") \
  OP (opt_long_help, "H", 0, 0, \
      "Display a verbose help message and exit.") \
  OP (opt_version, "V", "version", 0, \
      "Display a release identifier string\n" \
      "and exit.") \
  OP (opt_forward, "N", "forward", 0, \
      "pass the --forward option to `patch'") \
  OP (opt_keep, "k", "keep", 0, \
      "do not delete the patch") \
  OP (opt_quiet, "q", "quiet", 0, \
      "no progress reports while computing changeset") \
  OP (opt_dir, "d", "dir DIR", 1, \
      "Operate on project tree in DIR (default `.')") \
  OP (opt_unescaped, 0, "unescaped", 0, \
      "show filenames in unescaped form")

t_uchar arch_cmd_redo_help[] = ("redo changes in project tree\n"

				"Apply CHANGESET to the project tree and then delete CHANGESET.\n"
				"\n"
				"If CHANGESET is not specified, the highest numbered ,,undo-N directory\n"
				"in the project tree root is used.\n"
				"\n"
				"If --keep is given, the changeset directory is not deleted.\n"
				"\n"
				"See also \"tla undo --help\" and \"tla apply-changeset --help\".\n");

enum options
{
  OPTS (OPT_ENUM)
};

static struct opt_desc opts[] =
{
  OPTS (OPT_DESC)
    {-1, 0, 0, 0, 0}
};



int
arch_cmd_redo (t_uchar * program_name, int argc, char * argv[])
{
  int o;
  struct opt_parsed * option;
  t_uchar * dir = 0;
  t_uchar * patch = 0;
  int quiet = 0;
  int keep = 0;
  int forward = 0;
  int escape_classes = arch_escape_classes;

  dir = str_save (0, ".");

  safe_buffer_fd (1, 0, O_WRONLY, 0);

  option = 0;

  while (1)
    {
      o = opt_standard (lim_use_must_malloc, &option, opts, &argc, argv, program_name, usage, version_string, arch_cmd_redo_help, opt_help_msg, opt_long_help, opt_version);
      if (o == opt_none)
        break;
      switch (o)
        {
        default:
          safe_printfmt (2, "unhandled option `%s'\n", option->opt_string);
          panic ("internal error parsing arguments");

        usage_error:
          opt_usage (2, argv[0], program_name, usage, 1);
          exit (1);

          /* bogus_arg: */
          safe_printfmt (2, "ill-formed argument for `%s' (`%s')\n", option->opt_string, option->arg_string);
          goto usage_error;

        case opt_dir:
          {
            lim_free (0, dir);
            dir = str_save (0, option->arg_string);
            break;
          }

        case opt_forward:
          {
            forward = 1;
            break;
          }

        case opt_keep:
          {
            keep = 1;
            break;
          }

        case opt_quiet:
          {
            quiet = 1;
            break;
          }

	case opt_unescaped:
	  {
	    escape_classes = 0;
	    break;
	  }
        }
    }

  if (argc != 1 && argc != 2)
    goto usage_error;

  if (argc == 2)
    patch = str_save (0, argv[1]);

  {
    t_uchar * tree_root = 0;

    tree_root = arch_tree_root (0, dir, 0);

    if (!tree_root)
      {
        safe_printfmt (2, "%s: not in a project tree\n  dir: %s\n",
                       argv[0], dir);
        exit (2);
      }

    if (!patch)
      {
        patch = arch_latest_undo_changeset (0, tree_root);
        if (!patch)
          {
            safe_printfmt (2, "%s: no changeset from undo found\n  dir: %s\n",
                           argv[0], tree_root);
            exit (2);
          }
      }


    {
      struct arch_tree_lint_result * lint = 0;

      if (!quiet)
        safe_printfmt (1, "* linting the source tree\n");
      lint = arch_tree_lint (tree_root);
      if ((0 > arch_print_tree_lint_report ((quiet ? 2 : 1), lint, escape_classes)))
        {
          exit (1);
        }
    }

    {
      struct arch_apply_changeset_report report = {0, };

      if (!quiet)
        {
          report.callback = redo_callback;
          report.thunk = (void *)1;
        }

      arch_apply_changeset (&report, patch, tree_root, arch_unspecified_id_tagging, arch_inventory_unrecognized, 0, forward, 0, 0, 0, escape_classes);

      if (!keep)
        rmrf_file (patch);

      arch_free_apply_changeset_report_data (&report);
    }

    lim_free (0, tree_root);
  }


  lim_free (0, dir);
  lim_free (0, patch);

  return 0;
}



static void
redo_callback (void * ign, const char * fmt, va_list ap)
{
  safe_printfmt_va_list (1, fmt, ap);
  safe_flush (1);
}



/* tag: Tom Lord Thu Jun  5 00:34:33 2003 (cmd-redo-changes.c)
 */
