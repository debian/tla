/* archive-version.c:
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */


#include "hackerlab/bugs/panic.h"
#include "hackerlab/char/str.h"
#include "tla/libarch/archive-version.h"



/* These must agree about the format version number.
 */
static const int arch_archive_format_vsn = 2;
static const char arch_archive_format_vsn_id[] = "2";
const char arch_tree_format_1_str[] = "Hackerlab arch archive directory, format version 1.";
const char arch_tree_format_2_str[] = "Hackerlab arch archive directory, format version 2.";
static const char arch_tree_format_str[] = "Bazaar archive format 1 0";



t_uchar *
arch_archive_version_for_new_archive (int tla_archive)
{
  if (!tla_archive)
    return str_save (0, (t_uchar *)arch_tree_format_str);
  else
    return str_save (0, (t_uchar *)arch_tree_format_2_str);
}


enum arch_archive_access
arch_archive_access (t_uchar * version)
{
  if (!str_cmp (version, arch_tree_format_str))
    return arch_archive_writable;
  else if (!str_cmp (version, arch_tree_format_2_str))
    return arch_archive_writable;
  else if (!str_cmp (version, arch_tree_format_1_str))
    return arch_archive_readable;
  else
    return arch_archive_incompatible;
}

enum arch_archive_type 
arch_archive_type (t_uchar * version)
{
  if (!str_cmp (version, arch_tree_format_str))
    return arch_archive_baz;
  else if (!str_cmp (version, arch_tree_format_2_str))
    return arch_archive_tla;
  else if (!str_cmp (version, arch_tree_format_1_str))
    return arch_archive_tla;
  else
    {
      panic ("unknown archive type");
      return (enum arch_archive_type)-1; /* not reached */
    }
}




/* tag: Tom Lord Fri May 23 22:17:13 2003 (archive-version.c)
 */
