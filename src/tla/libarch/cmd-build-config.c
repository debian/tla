/* build-config.c
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */


#include "config-options.h"
#include "hackerlab/cmd/main.h"
#include "tla/libarch/namespace.h"
#include "tla/libarch/project-tree.h"
#include "tla/libarch/configs.h"
#include "tla/libarch/cmd.h"
#include "tla/libarch/cmd-build-config.h"



static t_uchar * usage = "[options] config";
static t_uchar * version_string = (cfg__std__package " from regexps.com\n"
                                   "\n"
                                   "Copyright 2003 Tom Lord\n"
                                   "\n"
                                   "This is free software; see the source for copying conditions.\n"
                                   "There is NO warranty; not even for MERCHANTABILITY or FITNESS FOR A\n"
                                   "PARTICULAR PURPOSE.\n"
                                   "\n"
                                   "Report bugs to " cfg__tla_bug_mail ".\n"
                                   "\n"
                                   cfg__std__release_id_string
                                   "\n");

#define OPTS(OP) \
  OP (opt_help_msg, "h", "help", 0, \
      "Display a help message and exit.") \
  OP (opt_long_help, "H", 0, 0, \
      "Display a verbose help message and exit.") \
  OP (opt_version, "V", "version", 0, \
      "Display a release identifier string\n" \
      "and exit.") \
  OP (opt_dir, "d", "dir DIR", 1, \
      "cd to DIR first") \
  OP (opt_no_pristines, 0, "no-pristines", 0, \
      "don't create pristine copies") \
  OP (opt_hardlinks, 0, "link", 0, \
      "hardlink files to revision library instead of copying") \
  OP (opt_library, 0, "library", 0, \
      "ensure revisions are in the revision library") \
  OP (opt_sparse, 0, "sparse", 0, \
      "add library entries sparsely (--link, --library)") \
  OP (opt_release_id, "r", "release-id", 0, \
      "overwrite ./=RELEASE-ID for this config")

t_uchar arch_cmd_build_config_help[] = ("instantiate a multi-project config\n"
					"Build the named configuration.  See also"
					" \"tla cat-config -H\".\n");

enum options
{
  OPTS (OPT_ENUM)
};

static struct opt_desc opts[] =
{
  OPTS (OPT_DESC)
    {-1, 0, 0, 0, 0}
};



int
arch_cmd_build_config (t_uchar * program_name, int argc, char * argv[])
{
  int o;
  struct opt_parsed * option;
  t_uchar * dir = 0;
  struct arch_build_config_params params = {0, };

  dir = str_save (0, ".");

  safe_buffer_fd (1, 0, O_WRONLY, 0);

  option = 0;

  while (1)
    {
      o = opt_standard (lim_use_must_malloc, &option, opts, &argc, argv, program_name, usage, version_string, arch_cmd_build_config_help, opt_help_msg, opt_long_help, opt_version);
      if (o == opt_none)
        break;
      switch (o)
        {
        default:
          safe_printfmt (2, "unhandled option `%s'\n", option->opt_string);
          panic ("internal error parsing arguments");

        usage_error:
          opt_usage (2, argv[0], program_name, usage, 1);
          exit (1);

          /* bogus_arg: */
          safe_printfmt (2, "ill-formed argument for `%s' (`%s')\n", option->opt_string, option->arg_string);
          goto usage_error;

        case opt_dir:
          {
            lim_free (0, dir);
            dir = str_save (0, option->arg_string);
            break;
          }

        case opt_no_pristines:
          {
            params.no_pristines = 1;
            break;
          }

        case opt_hardlinks:
          {
            params.hardlinks = 1;
            break;
          }

        case opt_library:
          {
            params.library = 1;
            break;
          }

        case opt_sparse:
          {
            params.sparse = 1;
            break;
          }

        case opt_release_id:
          {
            params.release_id = 1;
            break;
          }
        }
    }

  if (argc != 2)
    goto usage_error;

  {
    t_uchar * config;
    t_uchar * tree_root = 0;
    t_uchar * def_archive = 0;

    config = argv[1];

    if (!arch_valid_config_name (config))
      {
        safe_printfmt (2, "%s: invalid config name (%s)\n",
                       argv[0], config);
        exit (2);
      }

    tree_root = arch_tree_root (0, dir, 0);

    if (!tree_root)
      {
        safe_printfmt (2, "%s: not in project tree (%s)\n",
                       argv[0], dir);
        exit (1);
      }

    { /* possibly get the default archive from the project tree */
      t_uchar * tree_version = arch_tree_version (tree_root);

      if (tree_version) {
        def_archive = arch_parse_package_name (arch_ret_archive, 0, tree_version);
        lim_free (0, tree_version);
      }
    }

    arch_build_config (tree_root, config, &params, def_archive);

    lim_free (0, def_archive);
    lim_free (0, tree_root);

  }

  lim_free (0, dir);
  return 0;
}



/* tag: Tom Lord Mon Jun  2 21:41:48 2003 (buildcfg.c)
 */
