/* cmd-make-archive.c
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */


#include "config-options.h"
#include "hackerlab/cmd/main.h"
#include "tla/libfsutils/ensure-dir.h"
#include "tla/libarch/archive.h"
#include "tla/libarch/archives.h"
#include "tla/libarch/namespace.h"
#include "tla/libarch/cmdutils.h"
#include "tla/libarch/cmd-make-archive.h"



static t_uchar * usage = "[options] name location";
static t_uchar * usage2 = "[options] [name] location";
static t_uchar * version_string = (cfg__std__package " from regexps.com\n"
                                   "\n"
                                   "Copyright 2003 Tom Lord\n"
                                   "\n"
                                   "This is free software; see the source for copying conditions.\n"
                                   "There is NO warranty; not even for MERCHANTABILITY or FITNESS FOR A\n"
                                   "PARTICULAR PURPOSE.\n"
                                   "\n"
                                   "Report bugs to " cfg__tla_bug_mail ".\n"
                                   "\n"
                                   cfg__std__release_id_string
                                   "\n");

#define OPTS(OP) \
  OP (opt_help_msg, "h", "help", 0, \
      "Display a help message and exit.") \
  OP (opt_long_help, "H", 0, 0, \
      "Display a verbose help message and exit.") \
  OP (opt_version, "V", "version", 0, \
      "Display a release identifier string\n" \
      "and exit.") \
  OP (opt_mirror, "m", "mirror MASTER", 1, \
      "create mirror of specified archive") \
  OP (opt_mirror_from, "M", "mirror-from MASTER-SOURCE", 1, \
      "create pull-based mirror of specified archive") \
  OP (opt_dot_listing, "l", "listing", 0, \
      "Keep .listing files up-to-date in this archive.") \
  OP (opt_tla, "t", "tla", 0, \
      "Create a tla format archive.") \
  OP (opt_baz, "b", "baz", 0, \
      "Create a baz format archive (the default).") \
  OP (opt_signed, "s", "signed", 0, \
      "GPG sign the contents of this archive.")


t_uchar arch_cmd_make_archive_help[] = ("create a new archive directory\n"
                                        "NAME is the global name for the archive.  It must be an\n"
                                        "email address with a fully qualified domain name, optionally\n"
                                        "followed by \"--\" and a string of letters, digits, periods\n"
                                        "and dashes.\n"
                                        "\n"
                                        "With --mirror, label the new archive as a mirror of MASTER.\n"
                                        "Ordinary commits can not be made to a mirror archive, however\n"
                                        "the command \"tla archive-mirror\" can write to a mirror.\n"
                                        "\n"
                                        "Two special forms of this command are available:\n"
                                        "\n"
                                        "   tla make-archive --mirror MASTER LOCATION\n"
                                        "\n"
                                        "is equivalent to:\n"
                                        "\n"
                                        "   tla make-archive --mirror MASTER MASTER-MIRROR LOCATION\n"
                                        "\n"
                                        "and thus \"tla archive-mirror MASTER\" will push changes\n"
                                        "from MASTER to the newly created mirror.\n"
                                        "\n"
                                        "   tla make-archive --mirror-from MASTER-SOURCE LOCATION\n"
                                        "\n"
                                        "is equivalent to:\n"
                                        "\n"
                                        "   tla make-archive --mirror MASTER-SOURCE MASTER LOCATION\n"
                                        "\n"
                                        "and thus \"tla archive-mirror MASTER\" will pull changes\n"
                                        "from MASTER-SOURCE to the newly created mirror.\n"
                                        "\n"
                                        "The first of these two special forms is especially handy\n"
                                        "when creating a remote mirror in which to publish a local\n"
                                        "repository.\n"
                                        "\n"
                                        "The second special form is handy when creating a local mirror\n"
                                        "of a remote archive, locally bound to the remote archive's name.\n"
                                        "\n"
                                        "The --tla option creates a tla format archive, the default is \n"
                                        "to create a Bazaar archive.\n"
                                        "\n"
                                        );
enum options
{
  OPTS (OPT_ENUM)
};

static struct opt_desc opts[] =
{
  OPTS (OPT_DESC)
    {-1, 0, 0, 0, 0}
};



enum operation
{
  op_print,
  op_set,
};

int
arch_cmd_make_archive (t_uchar * program_name, int argc, char * argv[])
{
  int o;
  struct opt_parsed * option;
  t_uchar * mirror_of = 0;
  t_uchar * mirror_from = 0;
  int dot_listing_lossage = 0;
  int signed_archive = 0;
  int tla_archive = 0;

  safe_buffer_fd (1, 0, O_WRONLY, 0);

  option = 0;

  while (1)
    {
      o = opt_standard (lim_use_must_malloc, &option, opts, &argc, argv, program_name, usage, version_string, arch_cmd_make_archive_help, opt_help_msg, opt_long_help, opt_version);
      if (o == opt_none)
        break;
      switch (o)
        {
        default:
          safe_printfmt (2, "unhandled option `%s'\n", option->opt_string);
          panic ("internal error parsing arguments");

        usage_error:
          opt_usage (2, argv[0], program_name, usage, 1);
          safe_printfmt(2, "or if the option --mirror or --mirror-from is used \n");
          opt_usage (2, argv[0], program_name, usage2, 1);
          
          exit (1);

          /* bogus_arg: */
          safe_printfmt (2, "ill-formed argument for `%s' (`%s')\n", option->opt_string, option->arg_string);
          goto usage_error;

        case opt_mirror:
          {
            lim_free (0, mirror_from);
            mirror_from = 0;
            lim_free (0, mirror_of);
            mirror_of = str_save (0, option->arg_string);
            break;
          }

        case opt_mirror_from:
          {
            lim_free (0, mirror_of);
            mirror_of = 0;
            lim_free (0, mirror_from);
            mirror_from = str_save (0, option->arg_string);
            break;
          }

        case opt_dot_listing:
          dot_listing_lossage = 1;
          break;
	  
        case opt_signed:
          signed_archive = 1;
          break;

        case opt_tla:
          tla_archive = 1;
          break;

        case opt_baz:
          tla_archive = 0;
          break;
        }
    }

  if (!(mirror_of || mirror_from) ? (argc != 3) : ((argc != 2) && (argc != 3)))
    goto usage_error;

  {
    t_uchar * name = 0;
    t_uchar * location = 0;

    if (mirror_of && !arch_valid_archive_name (mirror_of))
      {
        safe_printfmt (2, "%s: invalid archive name supplied to --mirror (%s)\n",
                       argv[0], mirror_of);
        exit (2);
      }

    if (mirror_from && !arch_valid_archive_name (mirror_from))
      {
        safe_printfmt (2, "%s: invalid archive name supplied to --mirror-from (%s)\n",
                       argv[0], mirror_from);
        exit (2);
      }

    if (!(mirror_of || mirror_from) || (argc == 3))
      {
        name = str_save (0, argv[1]);
        location = str_save (0, argv[2]);
      }
    else if (mirror_of)
      {
        invariant (argc == 2);

        name = arch_mirrored_at_name (mirror_of);
        location = str_save (0, argv[1]);
      }
    else
      {
        size_t len;
        invariant (mirror_from && (argc == 2));

        len = str_length (mirror_from);
        if ((len < (sizeof ("-SOURCE") - 1)) || str_cmp ("-SOURCE", mirror_from + len - (sizeof ("-SOURCE") - 1)))
          {
            safe_printfmt (2, "%s: --mirror-from requires an argument ending with -SOURCE\n  got: %s\n",
                           argv[0], mirror_from);
            exit (1);
          }

        name = str_save_n (0, mirror_from, len - (sizeof ("-SOURCE") - 1));
        location = str_save (0, argv[1]);
        mirror_of = str_save (0, name);
      }

    if (!arch_valid_archive_name (name))
      {
        safe_printfmt (1, "make-archive: invalid archive name (%s)\n", argv[1]);
        exit (1);
      }

    arch_check_uri (location);
    arch_make_archive (name, location, mirror_of, dot_listing_lossage, signed_archive, tla_archive);

    lim_free (0, name);
    lim_free (0, location);
  }

  lim_free (0, mirror_of);
  return 0;
}




/* tag: Tom Lord Mon May 19 19:23:42 2003 (make-archive.c)
 */
