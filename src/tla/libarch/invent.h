/* inv.h: file inventories
 *
 ****************************************************************
 * Copyright (C) 2002, 2003 Tom Lord
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */

#ifndef INCLUDE__FILE_UTILS__INV_H
#define INCLUDE__FILE_UTILS__INV_H

#include "hackerlab/os/sys/stat.h"
#include "hackerlab/machine/types.h"
#include "hackerlab/rx-posix/regex.h"
#include "tla/libawk/relational.h"
#include "tla/libarch/inv-ids.h"



struct arch_inventory_regexps
{
  regex_t excludes_pattern;
  regex_t junk_pattern;
  regex_t backup_pattern;
  regex_t precious_pattern;
  regex_t unrecognized_pattern;
  regex_t source_pattern;
};

struct arch_inventory_options
{
  t_uint categories;                    /* set of enum inv_category */

  int want_ids;                        /* go to the expense of computing file ids? */
  int treat_unrecognized_source_as_source;                 /* report source-named files that are missing ids? */
  enum arch_id_tagging_method method;      /* id tagging method */
  enum arch_inventory_category untagged_source_category; /* what are untagged files matching source? */

  int nested;                           /* search in nested trees? */
  int include_excluded;                 /* ignore the `excludes_pattern'? */
  int override_method;	              /* override tree's id tagging methods? */

  struct arch_inventory_regexps regexps;
};

typedef void (*inv_callback) (const t_uchar * path,
                              struct stat * stat_buf,
                              enum arch_inventory_category category,
                              const t_uchar * id,
                              int has_source_name,
                              void * closure,
                              int escape_classes);



/* automatically generated __STDC__ prototypes */
extern rel_table arch_source_inventory (const t_uchar * tree_root,
                                        int include_ctl,
                                        int include_precious,
                                        int include_nested);
extern rel_table arch_source_files_inventory (const t_uchar * tree_root,
                                              int include_ctl,
                                              int include_precious);
extern rel_table arch_source_files_file_list(const t_uchar * tree_root,
                                              int include_ctl,
                                              int include_precious);
extern t_uchar * arch_default_naming_conventions_regexp (enum arch_inventory_category cat);
extern t_uchar * arch_ancient_default_naming_conventions_regexp (enum arch_inventory_category cat);
extern t_uchar * arch_null_default_naming_conventions_regexp (enum arch_inventory_category cat);
extern void arch_get_inventory_naming_conventions (struct arch_inventory_options * options,
                                                   const char * tree_root);
extern void arch_free_inventory_naming_conventions (struct arch_inventory_options * options);
extern void arch_inventory_traversal (struct arch_inventory_options * options,
                                      const t_uchar * root,
                                      inv_callback callback,
                                      void * closure,
                                      int escape_classes);
extern int arch_is_control_file (const char * rel_file, const char * filename);

extern t_uchar * normal_from_path (t_uchar const *path);
extern t_uchar * prefix_from_path (t_uchar const *path);

#endif  /* INCLUDE__FILE_UTILS__INV_H */


/* tag: Tom Lord Wed May 14 11:10:28 2003 (invent.h)
 */
