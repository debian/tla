/* cmd-lock-revision.c
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */


#include "config-options.h"
#include "hackerlab/cmd/main.h"
#include "tla/libarch/namespace.h"
#include "tla/libarch/project-tree.h"
#include "tla/libarch/my.h"
#include "tla/libarch/archive.h"
#include "tla/libarch/cmd-lock-revision.h"



static t_uchar * usage = "[options] revision";
static t_uchar * version_string = (cfg__std__package " from regexps.com\n"
                                   "\n"
                                   "Copyright 2003 Tom Lord\n"
                                   "\n"
                                   "This is free software; see the source for copying conditions.\n"
                                   "There is NO warranty; not even for MERCHANTABILITY or FITNESS FOR A\n"
                                   "PARTICULAR PURPOSE.\n"
                                   "\n"
                                   "Report bugs to " cfg__tla_bug_mail ".\n"
                                   "\n"
                                   cfg__std__release_id_string
                                   "\n");

#define OPTS(OP) \
  OP (opt_help_msg, "h", "help", 0, \
      "Display a help message and exit.") \
  OP (opt_long_help, "H", 0, 0, \
      "Display a verbose help message and exit.") \
  OP (opt_version, "V", "version", 0, \
      "Display a release identifier string\n" \
      "and exit.") \
  OP (opt_archive, "A", "archive", 1, \
      "Override `my-default-archive'") \
  OP (opt_unlock, "u", "unlock", 0, \
      "release a lock owned by you") \
  OP (opt_break, "b", "break", 0, \
      "break any existing lock")


t_uchar arch_cmd_lock_revision_help[] = ("lock (or unlock) an archive revision\n"
                                         "Acquire the lock needed to create REVISION.\n"
                                         "\n"
                                         "Revision must be the next patch-level in sequence, however,\n"
                                         "if no patch level is specified, the lock is taken unconditionally.\n"
                                         "If no version is specified, the latest version is locked.\n");

enum options
{
  OPTS (OPT_ENUM)
};

static struct opt_desc opts[] =
{
  OPTS (OPT_DESC)
    {-1, 0, 0, 0, 0}
};



enum op
{
  lock,
  unlock,
  breaklock
};

int
arch_cmd_lock_revision (t_uchar * program_name, int argc, char * argv[])
{
  int o;
  struct opt_parsed * option;
  t_uchar * default_archive = 0;
  enum op op;
  int status;



  default_archive = 0;
  op = lock;

  safe_buffer_fd (1, 0, O_WRONLY, 0);

  option = 0;

  while (1)
    {
      o = opt_standard (lim_use_must_malloc, &option, opts, &argc, argv, program_name, usage, version_string, arch_cmd_lock_revision_help, opt_help_msg, opt_long_help, opt_version);
      if (o == opt_none)
        break;
      switch (o)
        {
        default:
          safe_printfmt (2, "unhandled option `%s'\n", option->opt_string);
          panic ("internal error parsing arguments");

        usage_error:
          opt_usage (2, argv[0], program_name, usage, 1);
          exit (1);

          /* bogus_arg: */
          safe_printfmt (2, "ill-formed argument for `%s' (`%s')\n", option->opt_string, option->arg_string);
          goto usage_error;

        case opt_archive:
          {
            default_archive = str_save (0, option->arg_string);
            break;
          }

        case opt_unlock:
          {
            op = unlock;
            break;
          }

        case opt_break:
          {
            op = breaklock;
            break;
          }
        }
    }

  if (argc != 2)
    goto usage_error;

  default_archive = arch_my_default_archive (default_archive);

  {
    t_uchar * revision_spec;
    t_uchar * archive = 0;
    t_uchar * revision = 0;
    t_uchar * version = 0;
    t_uchar * uid = 0;
    struct arch_archive * arch = 0;
    t_uchar * prev_revision = 0;
    t_uchar * prev_level = 0;
    t_uchar * errstr = 0;

    revision_spec = argv[1];

    if (!arch_valid_package_name (revision_spec, arch_maybe_archive, arch_req_patch_level, 0))
      {
        safe_printfmt (2, "%s: invalid revision name (%s)\n",
                       argv[0], revision_spec);
        exit (2);
      }

    archive = arch_parse_package_name (arch_ret_archive, default_archive, revision_spec);
    revision = arch_parse_package_name (arch_ret_non_archive, 0, revision_spec);
    version = arch_parse_package_name (arch_ret_package_version, 0, revision);

    uid = arch_my_id_uid ();

    arch = arch_archive_connect (archive, 0);

    prev_revision = arch_previous_revision (arch, revision);
    if (!prev_revision)
      prev_level = 0;
    else
      prev_level = arch_parse_package_name (arch_ret_patch_level, 0, prev_revision);

    switch (op)
      {
      case lock:
        {
          if (arch_archive_lock_revision (&errstr, arch, version, prev_level, uid, 0, 0))
            {
              safe_printfmt (2, "%s: error locking revision %s/%s -- %s\n",
                             argv[0], archive, revision, errstr);
              status = 1;
            }
          else
            status = 0;
          break;
        }

      case unlock:
        {
          if (arch_archive_break_revision_lock (&errstr, arch, version, prev_level, uid, 0))
            {
              safe_printfmt (2, "%s: error unlocking revision %s/%s -- %s\n",
                             argv[0], archive, revision, errstr);
              status = 1;
            }
          else
            status = 0;
          break;
        }

      case breaklock:
        {
          t_uchar * lock_prev_level = 0;
          t_uchar * locker_uid = 0;
          t_uchar * txn_id = 0;
          enum arch_revision_lock_state state;

          state = arch_archive_revision_lock_state (&lock_prev_level, &locker_uid, &txn_id, arch, version);

          switch (state)
            {
            default:
              {
                safe_printfmt (2, "%s: unrecognized lock state for %s/%s\n",
                               argv[0], archive, revision);
                exit (2);
                break;
              }
            case arch_revision_illegal_lock_state:
              {
                safe_printfmt (2, "%s: illegal lock state for %s/%s\n",
                               argv[0], archive, revision);
                safe_printfmt (2, "  consult an admin or arch expert (this shouldn't happen)\n");
                exit (2);
                break;
              }
            case arch_revision_unknown_lock_state:
              {
                safe_printfmt (2, "%s: unkown lock state for %s/%s\n",
                               argv[0], archive, revision);
                safe_printfmt (2, "   (lock was in transition -- consider retrying)\n");
                exit (1);
                break;
              }
            case arch_revision_unlocked:
              {
              break_from_unlocked:
                safe_printfmt (2, "%s: revision not locked -- %s/%s\n",
                               argv[0], archive, revision);
                exit (0);
                break;
              }
            case arch_revision_user_locked:
            case arch_revision_txn_locked:
              {
                if (str_cmp (lock_prev_level, prev_level))
                  goto break_from_unlocked;

                if (arch_archive_break_revision_lock (&errstr, arch, version, lock_prev_level, locker_uid, txn_id))
                  {
                    safe_printfmt (2, "%s: error unlocking revision %s/%s -- %s\n",
                                   argv[0], archive, revision, errstr);
                    status = 1;
                  }
                else
                  status = 0;
              }
            }

          lim_free (0, lock_prev_level);
          lim_free (0, locker_uid);
          lim_free (0, txn_id);
        }
        break;
      }

    lim_free (0, archive);
    lim_free (0, revision);
    lim_free (0, version);
    lim_free (0, uid);
    lim_free (0, prev_revision);
    lim_free (0, prev_level);
  }

  lim_free (0, default_archive);
  exit (status);
  return 0;
}




/* tag: Tom Lord Sat May 24 16:18:02 2003 (lock-revision.c)
 */
