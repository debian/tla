/* libraries.h:
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */

#ifndef INCLUDE__LIBARCH__LIBRARIES_H
#define INCLUDE__LIBARCH__LIBRARIES_H


#include "hackerlab/machine/types.h"
#include "tla/libawk/relational.h"



/* automatically generated __STDC__ prototypes */
extern rel_table arch_library_archive_dirs (rel_table opt_lib_path,
                                            const t_uchar * archive,
                                            int for_add);
extern rel_table arch_library_category_dirs (rel_table opt_lib_path,
                                             const t_uchar * archive,
                                             const t_uchar * category,
                                             int for_add);
extern rel_table arch_library_branch_dirs (rel_table opt_lib_path,
                                           const t_uchar * archive,
                                           const t_uchar * branch,
                                           int for_add);
extern rel_table arch_library_version_dirs (rel_table opt_lib_path,
                                            const t_uchar * archive,
                                            const t_uchar * version,
                                            int for_add);
extern rel_table arch_library_revision_dirs (rel_table opt_lib_path,
                                             const t_uchar * archive,
                                             const t_uchar * revision,
                                             int for_add);
extern t_uchar * arch_library_find (rel_table opt_lib_path,
                                    const t_uchar * archive,
                                    const t_uchar * revision,
                                    int check_inode_sigs);
extern t_uchar * arch_library_find_file (const t_uchar * archive,
                                         const t_uchar * revision,
                                         const t_uchar * loc);
extern t_uchar * arch_library_find_file_by_id (const t_uchar * archive,
                                               const t_uchar * revision,
                                               const t_uchar * id);
extern rel_table arch_library_archives (void);
extern rel_table arch_library_categories (const t_uchar * archive);
extern rel_table arch_library_branches (const t_uchar * archive,
                                        const t_uchar * category);
extern rel_table arch_library_versions (const t_uchar * archive,
                                        const t_uchar * branch);
extern rel_table arch_library_revisions (const t_uchar * archive,
                                         const t_uchar * version,
                                         int full);
extern t_uchar * arch_library_log (const t_uchar * archive,
                                   const t_uchar * revision);
extern t_uchar * arch_library_index_file (const t_uchar * archive,
                                          const t_uchar * revision);
extern rel_table arch_library_index (const t_uchar * archive,
                                     const t_uchar * revision);
extern int arch_library_has_archive (const t_uchar * lib,
                                     const t_uchar * archive);
extern int arch_library_has_category (const t_uchar * lib,
                                      const t_uchar * archive,
                                      const t_uchar * category);
extern int arch_library_has_branch (const t_uchar * lib,
                                    const t_uchar * archive,
                                    const t_uchar * branch);
extern int arch_library_has_version (const t_uchar * lib,
                                     const t_uchar * archive,
                                     const t_uchar * version);
extern t_uchar * arch_library_revision_dir_in_lib (const t_uchar * lib,
                                                   const t_uchar * archive,
                                                   const t_uchar * revision);
extern int arch_library_is_greedy (const t_uchar * lib);
extern void arch_set_library_greediness (const t_uchar * lib,
                                         int setting);
extern int arch_library_is_sparse (const t_uchar * lib);
extern void arch_set_library_sparseness (const t_uchar * lib,
                                         int setting);
extern void arch_verify_is_library (const t_uchar * lib);
#endif  /* INCLUDE__LIBARCH__LIBRARIES_H */


/* tag: Tom Lord Sat Jan  5 15:26:10 2002 (libraries.h)
 */
