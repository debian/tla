/* cmd-join-branch.c:
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */




#include "config-options.h"
#include "hackerlab/cmd/main.h"
#include "hackerlab/fs/file-names.h"
#include "hackerlab/fs/cwd.h"
#include "tla/libfsutils/tmp-files.h"
#include "tla/libfsutils/rmrf.h"
#include "tla/libarch/namespace.h"
#include "tla/libarch/project-tree.h"
#include "tla/libarch/my.h"
#include "tla/libarch/archive.h"
#include "tla/libarch/pristines.h"
#include "tla/libarch/build-revision.h"
#include "tla/libarch/patch-logs.h"
#include "tla/libarch/cmd.h"
#include "tla/libarch/cmd-replay.h"
#include "tla/libarch/copy-project-tree.h"
#include "tla/libarch/cmd-join-branch.h"



static t_uchar * usage = "[options] version";
static t_uchar * version_string = (cfg__std__package " from regexps.com\n"
                                   "\n"
                                   "Copyright 2003 Tom Lord\n"
                                   "\n"
                                   "This is free software; see the source for copying conditions.\n"
                                   "There is NO warranty; not even for MERCHANTABILITY or FITNESS FOR A\n"
                                   "PARTICULAR PURPOSE.\n"
                                   "\n"
                                   "Report bugs to " cfg__tla_bug_mail ".\n"
                                   "\n"
                                   cfg__std__release_id_string
                                   "\n");

#define OPTS(OP) \
  OP (opt_help_msg, "h", "help", 0, \
      "Display a help message and exit.") \
  OP (opt_long_help, "H", 0, 0, \
      "Display a verbose help message and exit.") \
  OP (opt_version, "V", "version", 0, \
      "Display a release identifier string\n" \
      "and exit.") \
  OP (opt_archive, "A", "archive", 1, \
      "Override `my-default-archive'") \
  OP (opt_dir, "d", "dir DIR", 1, \
      "Operate on project tree in DIR (default `.')") \
  OP (opt_dest, 0, "dest DEST", 1, \
      "Instead of modifying the project tree in-place,\n" \
      "make a copy of it to DEST and apply the result to that") \
  OP (opt_unescaped, 0, "unescaped", 0, \
      "show filenames in unescaped form")


t_uchar arch_cmd_join_branch_help[] = ("add a version as an ancestor of a project tree\n"

                                       "VERSION--base-0 must be a continuation (e.g. tag). The command replays the\n"
                                       "changeset for VERSION--base-0 in the project tree, which has the effect of\n"
                                       "adding the log for the branch (making the tag an ancestor of the resulting\n"
                                       "tree).\n");

enum options
{
  OPTS (OPT_ENUM)
};

static struct opt_desc opts[] =
{
  OPTS (OPT_DESC)
    {-1, 0, 0, 0, 0}
};



int
arch_cmd_join_branch (t_uchar * program_name, int argc, char * argv[])
{
  int o;
  struct opt_parsed * option;
  t_uchar * default_archive = 0;
  t_uchar * upon = 0;
  t_uchar * dest = 0;
  int escape_classes = arch_escape_classes;

  safe_buffer_fd (1, 0, O_WRONLY, 0);

  option = 0;

  while (1)
    {
      o = opt_standard (lim_use_must_malloc, &option, opts, &argc, argv, program_name, usage, version_string, arch_cmd_join_branch_help, opt_help_msg, opt_long_help, opt_version);
      if (o == opt_none)
        break;
      switch (o)
        {
        default:
          safe_printfmt (2, "unhandled option `%s'\n", option->opt_string);
          panic ("internal error parsing arguments");

        usage_error:
          opt_usage (2, argv[0], program_name, usage, 1);
          exit (1);

          /* bogus_arg: */
          safe_printfmt (2, "ill-formed argument for `%s' (`%s')\n", option->opt_string, option->arg_string);
          goto usage_error;

        case opt_archive:
          {
            lim_free (0, default_archive);
            default_archive = str_save (0, option->arg_string);
            break;
          }

        case opt_dir:
          {
            lim_free (0, upon);
            upon = str_save (0, option->arg_string);
            break;
          }

        case opt_dest:
          {
            lim_free (0, dest);
            dest = str_save (0, option->arg_string);
            break;
          }

	case opt_unescaped:
	  {
	    escape_classes = 0;
	    break;
	  }
        }
    }

  if (argc != 2)
    goto usage_error;

  if (default_archive && !arch_valid_archive_name (default_archive))
    {
      safe_printfmt (2, "%s: invalid archive name (%s)\n",
                     argv[0], default_archive);
      exit (1);
    }

  {
    t_uchar * vsn_spec;
    t_uchar * archive = 0;
    t_uchar * version = 0;
    t_uchar * base0_revision = 0;
    t_uchar * fqbase0 = 0;
    t_uchar * upon_root = 0;
    struct arch_archive * arch = 0;

    if (! upon)
      upon = str_save (0, ".");

    vsn_spec = argv[1];

    if (!arch_valid_package_name (vsn_spec, arch_maybe_archive, arch_req_version, 0))
      {
        safe_printfmt (2, "%s: invalid version name (%s)\n",
                       argv[0], vsn_spec);
        exit (1);
      }

    archive = arch_parse_package_name (arch_ret_archive, default_archive, vsn_spec);
    version = arch_parse_package_name (arch_ret_non_archive, 0, vsn_spec);
    base0_revision = str_alloc_cat (0, version, "--base-0");
    fqbase0 = arch_fully_qualify (archive, base0_revision);

    upon_root = arch_tree_root (0, upon, 0);
    if (!upon_root)
      {
        safe_printfmt (2, "%s: not in a project tree -- %s\n",
                       argv[0], upon);
        exit (1);
      }

    if (arch_tree_has_log (upon_root, archive, base0_revision))
      {
        safe_printfmt (2, "%s: tree already has log for %s/%s\n",
                       argv[0], archive, base0_revision);
        exit (1);
      }

    arch = arch_archive_connect (archive, 0);

    {
      enum arch_revision_type type;

      arch_revision_type (&type, 0, arch, base0_revision);

      if (type != arch_continuation_revision)
        {
          safe_printfmt (2, "%s: not a tag revision (%s/%s)\n",
                         argv[0], archive, base0_revision);
          exit (1);
        }
    }


    if (dest)
      {
        safe_printfmt (1, "* copying %s to %s\n", upon, dest);
        safe_flush (1);
        arch_copy_project_tree (upon, dest, 1, 1);
      }
    else
      dest = str_save (0, upon);

    arch_call_cmd (arch_cmd_replay, argv[0], "--dir", dest, fqbase0, ((escape_classes == 0) ? "--unescaped" : 0), (char*)0);


    lim_free (0, archive);
    lim_free (0, version);
    lim_free (0, base0_revision);
    lim_free (0, fqbase0);

    arch_archive_close (arch);
  }

  lim_free (0, upon);
  lim_free (0, dest);
  lim_free (0, default_archive);

  return 0;
}




/* tag: Tom Lord Wed Jun  4 18:23:19 2003 (cmd-join-branch.c)
 */
