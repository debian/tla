/* my.h:
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */

#ifndef INCLUDE__LIBARCH__MY_H
#define INCLUDE__LIBARCH__MY_H


#include "tla/libawk/relational.h"



enum arch_library_path_type
{
  arch_library_path_search_only = 1,
  arch_library_path_add_only = 2,
  arch_library_path_search_order = 3,
  arch_library_path_add_order = 4,
};


enum arch_library_edit_op
{
  arch_library_first,
  arch_library_last,
  arch_library_remove
};


/* automatically generated __STDC__ prototypes */
extern t_uchar * arch_my_arch_params (void);
extern void arch_ensure_my_arch_params (void);
extern t_uchar * arch_my_hook_script (void);
extern t_uchar * arch_my_id_file (void);
extern void arch_set_my_id (const t_uchar * id);
extern t_uchar * arch_my_id (void);
extern t_uchar * arch_my_id_uid (void);
extern t_uchar * arch_my_default_archive (const t_uchar * default_archive);
extern void arch_set_my_default_archive (const t_uchar * archive);
extern void arch_delete_my_default_archive (void);
extern t_uchar * arch_my_archive_locations_dir (void);
extern rel_table arch_my_library_path (enum arch_library_path_type path_type);
extern void arch_set_my_library_path (enum arch_library_path_type path_type,
                                      enum arch_library_edit_op op,
                                      const t_uchar * dir_spec);
extern t_uchar * arch_my_util_path_file (void);
extern t_uchar ** arch_my_util_path (void);
extern t_uchar * arch_my_metadata_rules_file (void);
extern int arch_my_allow_full_meta (void);
#endif  /* INCLUDE__LIBARCH__MY_H */


/* tag: Tom Lord Sat Jan  5 15:26:10 2002 (my.h)
 */
