/* cmd-tree-id.c
 *
 * vim:smartindent ts=8:sts=2:sta:et:ai:shiftwidth=2
 ****************************************************************
 * Copyright (C) 2004 Canonical Limited
 *       Authors: Robert Collins <robert.collins@canonical.com>
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */


#include "config-options.h"


#include "hackerlab/cmd/main.h"
#include "tla/libarch/namespace.h"
#include "tla/libarch/project-tree.h"
#include "tla/libarch/cmd-tree-id.h"


/* gettext support not uet incorporated into tla, reserve the gettext notation for later */
#define _(x) (x)
#define N_(x) (x)




static t_uchar * usage = N_("[options] [dir]");
static t_uchar * version_string = (cfg__std__package " from regexps.com\n"
                                   "\n"
                                   "Copyright 2003 Tom Lord\n"
                                   "\n"
                                   "This is free software; see the source for copying conditions.\n"
                                   "There is NO warranty; not even for MERCHANTABILITY or FITNESS FOR A\n"
                                   "PARTICULAR PURPOSE.\n"
                                   "\n"
                                   "Report bugs to " cfg__tla_bug_mail ".\n"
                                   "\n"
                                   cfg__std__release_id_string
                                   "\n");

#define OPTS(OP) \
  OP (opt_help_msg, "h", "help", 0, \
      N_("Display a help message and exit.")) \
  OP (opt_long_help, "H", 0, 0, \
      N_("Display a verbose help message and exit.")) \
  OP (opt_version, "V", "version", 0, \
      N_("Display a release identifier string\n" \
      "and exit."))

t_uchar arch_cmd_tree_id_help[] = N_("Print the tree identifier for a project tree\nPrint the patchset identifier that represents the\ncurrent tree-> I.e. foo@example.com/demo--1.0--patch-4.\nDefaults to the tree the current directory is in.\n");

enum options
{
  OPTS (OPT_ENUM)
};

static struct opt_desc opts[] =
{
  OPTS (OPT_DESC)
    {-1, 0, 0, 0, 0}
};



int
arch_cmd_tree_id (t_uchar * program_name, int argc, char * argv[])
{
  int o;
  struct opt_parsed * option;
  char * dir;
  t_uchar * tree_root = 0;
  t_uchar * revision_spec = 0;
  dir = ".";

  safe_buffer_fd (1, 0, O_WRONLY, 0);

  option = 0;

  while (1)
    {
      o = opt_standard (lim_use_must_malloc, &option, opts, &argc, argv, program_name, usage, version_string, arch_cmd_tree_id_help, opt_help_msg, opt_long_help, opt_version);
      if (o == opt_none)
        break;
      switch (o)
        {
        default:
          safe_printfmt (2, "unhandled option `%s'\n", option->opt_string);
          panic ("internal error parsing arguments");

        usage_error:
          opt_usage (2, argv[0], program_name, usage, 1);
          exit (1);

          /* bogus_arg: */
          safe_printfmt (2, "ill-formed argument for `%s' (`%s')\n", option->opt_string, option->arg_string);
          goto usage_error;
        }
    }

  if (argc > 2)
    goto usage_error;

  if (argc == 2)
    dir = argv[1];

  tree_root = arch_tree_root(0, dir, 0);
  


  if (!tree_root)
    {
      safe_printfmt (2, "%s: directory (%s) is not in a project tree\n", argv[0], dir);
      exit (1);
    }
    
  revision_spec = arch_get_tree_fqrevision(tree_root);
 
  if (!revision_spec)
    {
      safe_printfmt (2, "%s: unable to determine project tree identifier.\n  tree: %s\n", argv[0], tree_root);
      exit (2);
    }


  safe_printfmt (1, "%s\n", revision_spec);

  lim_free (0, revision_spec);
  lim_free (0, tree_root);

  return 0;
}


/* tag: Tom Lord Fri May 23 14:06:15 2003 (cmd-tree-id.c)
 */
