/* cmd-archives.c
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */


#include "config-options.h"
#include "hackerlab/cmd/main.h"
#include "tla/libfsutils/ensure-dir.h"
#include "tla/libarch/my.h"
#include "tla/libarch/namespace.h"
#include "tla/libarch/archives.h"
#include "tla/libarch/cmd-archives.h"



static t_uchar * usage = "[options] [search regular expression]";
static t_uchar * version_string = (cfg__std__package " from regexps.com\n"
                                   "\n"
                                   "Copyright 2003 Tom Lord\n"
                                   "\n"
                                   "This is free software; see the source for copying conditions.\n"
                                   "There is NO warranty; not even for MERCHANTABILITY or FITNESS FOR A\n"
                                   "PARTICULAR PURPOSE.\n"
                                   "\n"
                                   "Report bugs to " cfg__tla_bug_mail ".\n"
                                   "\n"
                                   cfg__std__release_id_string
                                   "\n");

#define OPTS(OP) \
  OP (opt_help_msg, "h", "help", 0, \
      "Display a help message and exit.") \
  OP (opt_long_help, "H", 0, 0, \
      "Display a verbose help message and exit.") \
  OP (opt_version, "V", "version", 0, \
      "Display a release identifier string\n" \
      "and exit.") \
  OP (opt_names, "n", "names", 0, \
      "print archive names only") \
  OP (opt_noremote, "R", "exclude-remote", 0, \
      "Exclude MIRROR and SOURCE archives.") 

t_uchar arch_cmd_archives_help[] = ("report registered archives and their locations\n"
				    "Print a list of registered archives and their locations\n"
				    "\n"
				    "If [search regex] is given then only archives with names\n"
				    "that match [search regex] will be shown\n");

enum options
{
  OPTS (OPT_ENUM)
};

static struct opt_desc opts[] =
{
  OPTS (OPT_DESC)
    {-1, 0, 0, 0, 0}
};



int
arch_cmd_archives (t_uchar * program_name, int argc, char * argv[])
{
  int o;
  struct opt_parsed * option;
  int nolist;
  int names_only;
  int exclude_remote;

  nolist = 0;
  names_only = 0;
  exclude_remote = 0;

  safe_buffer_fd (1, 0, O_WRONLY, 0);

  option = 0;

  while (1)
    {
      o = opt_standard (lim_use_must_malloc, &option, opts, &argc, argv, program_name, usage, version_string, arch_cmd_archives_help, opt_help_msg, opt_long_help, opt_version);
      if (o == opt_none)
        break;
      switch (o)
        {
        default:
          safe_printfmt (2, "unhandled option `%s'\n", option->opt_string);
          panic ("internal error parsing arguments");

        usage_error:
          opt_usage (2, argv[0], program_name, usage, 1);
          exit (1);

          /* bogus_arg: */
          safe_printfmt (2, "ill-formed argument for `%s' (`%s')\n", option->opt_string, option->arg_string);
          goto usage_error;

        case opt_names:
          {
            names_only = 1;
            break;
          }

        case opt_noremote:
          {
            exclude_remote = 1;
            break;
          }

        case opt_double_dash:
          {
            goto no_more_args;
          }
        }
    }

 no_more_args:

  if (argc > 2)
    goto usage_error;

  {
    rel_table list = rel_table_nil;
    int x;
    int re_error;
    regex_t archive_needle;
    regex_t mirror_needle;
    regex_t source_needle;

    list = arch_registered_archives ();
    rel_sort_table_by_field (0, list, 0);

    if (argc == 2)
      re_error = regcomp(&archive_needle, argv[1], REG_EXTENDED);
    else
      re_error = regcomp(&archive_needle, ".*", REG_EXTENDED);

    if (re_error)
      panic("Invalid regular expression given for archive name restriction.");


    re_error = regcomp(&mirror_needle, "-MIRROR", REG_EXTENDED);

    if (re_error)
      panic("Unable to compile regular expression");

    re_error = regcomp(&source_needle, "-SOURCE", REG_EXTENDED);

    if (re_error)
      panic("Unable to compile regular expresion.");


    for (x = 0; x < rel_n_records (list); ++x)
      {

        nolist = 0;
        nolist =  nolist || regexec (&archive_needle, rel_peek_str (list, x, 0), 0, 0, 0);

        if (exclude_remote)
          {
            nolist = nolist || (! regexec (&mirror_needle, rel_peek_str (list, x, 0), 0, 0, 0));
            nolist = nolist || (! regexec (&source_needle, rel_peek_str (list, x, 0), 0, 0, 0));
          }

        if (!nolist)
          {
            safe_printfmt (1, "%s\n", rel_peek_str (list, x, 0));
            if (!names_only)
              safe_printfmt (1, "    %s\n", rel_peek_str (list, x, 1));
          }
      }
    regfree(&archive_needle);
    regfree(&mirror_needle);
    regfree(&source_needle);
  }

  return 0;
}




/* tag: Tom Lord Sun May 18 22:09:05 2003 (ls-archives.c)
 */
