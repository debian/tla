/* cmd-add-pristine.c
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */


#include "config-options.h"
#include "hackerlab/cmd/main.h"
#include "tla/libarch/namespace.h"
#include "tla/libarch/project-tree.h"
#include "tla/libarch/pristines.h"
#include "tla/libarch/cmd.h"
#include "tla/libarch/cmd-add-pristine.h"



static t_uchar * usage = "[options] revision";
static t_uchar * version_string = (cfg__std__package " from regexps.com\n"
                                   "\n"
                                   "Copyright 2003 Tom Lord\n"
                                   "\n"
                                   "This is free software; see the source for copying conditions.\n"
                                   "There is NO warranty; not even for MERCHANTABILITY or FITNESS FOR A\n"
                                   "PARTICULAR PURPOSE.\n"
                                   "\n"
                                   "Report bugs to " cfg__tla_bug_mail ".\n"
                                   "\n"
                                   cfg__std__release_id_string
                                   "\n");

#define OPTS(OP) \
  OP (opt_help_msg, "h", "help", 0, \
      "Display a help message and exit.") \
  OP (opt_long_help, "H", 0, 0, \
      "Display a verbose help message and exit.") \
  OP (opt_version, "V", "version", 0, \
      "Display a release identifier string\n" \
      "and exit.") \
  OP (opt_archive, "A", "archive", 1, \
      "Override `my-default-archive'") \
  OP (opt_dir, "d", "dir DIR", 1, \
      "cd to DIR first")

t_uchar arch_cmd_add_pristine_help[] = ("ensure that a project tree has a particular pristine revision\n"
                                        "Extract REVISION from an archive, creating a pristine copy.\n"
                                        "Store that pristine copy in the current directory's project tree\n");

enum options
{
  OPTS (OPT_ENUM)
};

static struct opt_desc opts[] =
{
  OPTS (OPT_DESC)
    {-1, 0, 0, 0, 0}
};



int
arch_cmd_add_pristine (t_uchar * program_name, int argc, char * argv[])
{
  int o;
  struct opt_parsed * option;
  char * dir = 0;
  t_uchar * default_archive = 0;

  dir = str_save (0, ".");

  safe_buffer_fd (1, 0, O_WRONLY, 0);

  option = 0;

  while (1)
    {
      o = opt_standard (lim_use_must_malloc, &option, opts, &argc, argv, program_name, usage, version_string, arch_cmd_add_pristine_help, opt_help_msg, opt_long_help, opt_version);
      if (o == opt_none)
        break;
      switch (o)
        {
        default:
          safe_printfmt (2, "unhandled option `%s'\n", option->opt_string);
          panic ("internal error parsing arguments");

        usage_error:
          opt_usage (2, argv[0], program_name, usage, 1);
          exit (1);

          /* bogus_arg: */
          safe_printfmt (2, "ill-formed argument for `%s' (`%s')\n", option->opt_string, option->arg_string);
          goto usage_error;


        case opt_archive:
          {
            lim_free (0, default_archive);
            default_archive = str_save (0, option->arg_string);
            break;
          }

        case opt_dir:
          {
            lim_free (0, dir);
            dir = str_save (0, option->arg_string);
            break;
          }
        }
    }

  if (argc != 2)
    goto usage_error;

  {
    t_uchar * revspec;
    t_uchar * tree_root = 0;
    t_uchar * archive = 0;
    t_uchar * revision = 0;
    t_uchar * pristine_path = 0;


    if (default_archive && !arch_valid_archive_name (default_archive))
      {
        safe_printfmt (2, "%s: invalid archive name (%s)\n",
                       argv[0], default_archive);
        exit (2);
      }

    revspec = argv[1];

    if (!arch_valid_package_name (revspec, arch_maybe_archive, arch_req_patch_level, 0))
      {
        safe_printfmt (2, "%s: invalid revision name (%s)\n",
                       argv[0], revspec);
        exit (2);
      }

    archive = arch_parse_package_name (arch_ret_archive, default_archive, revspec);
    revision = arch_parse_package_name (arch_ret_non_archive, 0, revspec);

    tree_root = arch_tree_root (0, dir, 0);

    if (!tree_root)
      {
        safe_printfmt (2, "%s: directory is not a project tree (%s)\n",
                       argv[0], dir);
        exit (2);
      }

    pristine_path = arch_find_pristine (0, tree_root, archive, revision, arch_unlocked_pristine, arch_tree_pristine_search);

    if (!pristine_path)
      {
        arch_add_pristine (1, tree_root, 0, archive, revision);
      }
  }

  lim_free (0, dir);
  lim_free (0, default_archive);

  return 0;
}




/* tag: Tom Lord Tue Jun 10 20:38:56 2003 (cmd-add-pristine.c)
 */
