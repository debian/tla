/* copy-project-tree.h:
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */

#ifndef INCLUDE__LIBARCH__COPY_PROJECT_TREE_H
#define INCLUDE__LIBARCH__COPY_PROJECT_TREE_H


#include "hackerlab/machine/types.h"


/* automatically generated __STDC__ prototypes */
extern void arch_copy_project_tree (t_uchar * from, t_uchar * to, int precious, int nested);
#endif  /* INCLUDE__LIBARCH__COPY_PROJECT_TREE_H */


/* tag: Tom Lord Sat Jan  5 15:26:10 2002 (copy-project-tree.h)
 */
