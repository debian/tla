/* cat-archive-log.c
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */


#include "config-options.h"
#include "hackerlab/cmd/main.h"
#include "tla/libarch/namespace.h"
#include "tla/libarch/project-tree.h"
#include "tla/libarch/my.h"
#include "tla/libarch/archive.h"
#include "tla/libarch/cmdutils.h"
#include "tla/libarch/cmd.h"
#include "tla/libarch/cmd-cat-archive-log.h"



static t_uchar * usage = "[options] revision";
static t_uchar * version_string = (cfg__std__package " from regexps.com\n"
                                   "\n"
                                   "Copyright 2003 Tom Lord\n"
                                   "\n"
                                   "This is free software; see the source for copying conditions.\n"
                                   "There is NO warranty; not even for MERCHANTABILITY or FITNESS FOR A\n"
                                   "PARTICULAR PURPOSE.\n"
                                   "\n"
                                   "Report bugs to " cfg__tla_bug_mail ".\n"
                                   "\n"
                                   cfg__std__release_id_string
                                   "\n");

#define OPTS(OP) \
  OP (opt_help_msg, "h", "help", 0, \
      "Display a help message and exit.") \
  OP (opt_long_help, "H", 0, 0, \
      "Display a verbose help message and exit.") \
  OP (opt_version, "V", "version", 0, \
      "Display a release identifier string\n" \
      "and exit.") \
  OP (opt_archive, "A", "archive", 1, \
      "Override `my-default-archive'") \
  OP (opt_headers, 0, "headers", 0, \
      "show only log headers")


t_uchar arch_cmd_cat_archive_log_help[] = ("print the contents of an archived log entry\n"
                                           "Retrieve and print the log message for the indicated\n"
                                           "revision from its archive.\n");

enum options
{
  OPTS (OPT_ENUM)
};

static struct opt_desc opts[] =
{
  OPTS (OPT_DESC)
    {-1, 0, 0, 0, 0}
};



int
arch_cmd_cat_archive_log (t_uchar * program_name, int argc, char * argv[])
{
  int o;
  struct opt_parsed * option;
  t_uchar * default_archive;
  int headers;


  default_archive = 0;
  headers = 0;

  safe_buffer_fd (1, 0, O_WRONLY, 0);

  option = 0;

  while (1)
    {
      o = opt_standard (lim_use_must_malloc, &option, opts, &argc, argv, program_name, usage, version_string, arch_cmd_cat_archive_log_help, opt_help_msg, opt_long_help, opt_version);
      if (o == opt_none)
        break;
      switch (o)
        {
        default:
          safe_printfmt (2, "unhandled option `%s'\n", option->opt_string);
          panic ("internal error parsing arguments");

        usage_error:
          opt_usage (2, argv[0], program_name, usage, 1);
          exit (1);

          /* bogus_arg: */
          safe_printfmt (2, "ill-formed argument for `%s' (`%s')\n", option->opt_string, option->arg_string);
          goto usage_error;

        case opt_archive:
          {
            default_archive = str_save (0, option->arg_string);
            break;
          }

        case opt_headers:
          {
            headers = 1;
            break;
          }
        }
    }

  if (argc != 2)
    goto usage_error;

  default_archive = arch_my_default_archive (default_archive);

  {
    t_uchar * revision_spec;
    t_uchar * archive = 0;
    t_uchar * revision = 0;
    struct arch_archive * arch = 0;
    t_uchar * log = 0;

    revision_spec = argv[1];

    if (!arch_valid_package_name (revision_spec, arch_maybe_archive, arch_req_patch_level, 0))
      {
        safe_printfmt (2, "%s: invalid revision name (%s)\n",
                       argv[0], revision_spec);
        exit (2);
      }

    archive = arch_parse_package_name (arch_ret_archive, default_archive, revision_spec);
    revision = arch_parse_package_name (arch_ret_non_archive, 0, revision_spec);

    arch = arch_archive_connect (archive, 0);
    arch_check_for (arch, arch_req_patch_level, revision);
    log = arch_archive_log (arch, revision);

    if (!headers)
      safe_write_retry (1, log, str_length (log));
    else
      {
        t_uchar * headers_end;

        headers_end = log;
        while (1)
          {
            headers_end = str_chr_index (headers_end, '\n');
            if (!headers_end)
              {
                safe_write_retry (1, log, str_length (log));
                exit (0);
              }
            if (headers_end[1] == '\n')
              {
                safe_write_retry (1, log, headers_end + 1 - log);
                exit (0);
              }
            ++headers_end;
          }
      }
  }

  return 0;
}




/* tag: Tom Lord Tue May 20 16:05:52 2003 (cat-archive-log.c)
 */
