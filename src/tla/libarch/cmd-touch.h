/* cmd-touch.h:
 *
 ****************************************************************
 * Copyright (C) 2005 Tom Lord
 * 
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */

#ifndef INCLUDE__LIBARCH__CMD_TOUCH_H
#define INCLUDE__LIBARCH__CMD_TOUCH_H


#include "hackerlab/machine/types.h"



extern t_uchar arch_cmd_touch_help[];


/* automatically generated __STDC__ prototypes */
extern int arch_cmd_touch (t_uchar *program_name, int argc, char * argv[]);
#endif  /* INCLUDE__LIBARCH__CMD_TOUCH_H */


/* tag: Tom Lord Fri Mar 18 16:49:01 2005 (cmd-touch.h)
 */
