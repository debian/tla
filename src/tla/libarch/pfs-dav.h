/* pfs-dav.h:
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */

#ifndef INCLUDE__LIBARCH__PFS_DAV_H
#define INCLUDE__LIBARCH__PFS_DAV_H


#include "tla/libarch/pfs.h"



/* automatically generated __STDC__ prototypes */
extern int arch_pfs_dav_supported_protocol (const t_uchar * uri);
extern struct arch_pfs_session * arch_pfs_dav_connect (const t_uchar * uri);
#endif  /* INCLUDE__LIBARCH__PFS_DAV_H */


/* tag: Tom Lord Sat Jan  5 15:26:10 2002 (pfs-dav.h)
 */
