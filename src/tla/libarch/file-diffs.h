/* file-diffs.h:
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */

#ifndef INCLUDE__LIBARCH__FILE_DIFFS_H
#define INCLUDE__LIBARCH__FILE_DIFFS_H



/* automatically generated __STDC__ prototypes */
extern int arch_file_get_or_diff (int out_fd, t_uchar * tree_root, t_uchar * mod_loc, t_uchar * archive, t_uchar * revision, int diff, int new_is_null, int escape_classes);
#endif  /* INCLUDE__LIBARCH__FILE_DIFFS_H */


/* tag: Tom Lord Sat Jan  5 15:26:10 2002 (file-diffs.h)
 */
