/* cmd-get-changeset.c
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */


#include "config-options.h"
#include "hackerlab/cmd/main.h"
#include "tla/libarch/namespace.h"
#include "tla/libarch/project-tree.h"
#include "tla/libarch/my.h"
#include "tla/libarch/archive.h"
#include "tla/libarch/cmd.h"
#include "tla/libarch/cmdutils.h"
#include "tla/libarch/cmd-get-changeset.h"



static t_uchar * usage = "[options] revision [dir]";
static t_uchar * version_string = (cfg__std__package " from regexps.com\n"
                                   "\n"
                                   "Copyright 2003 Tom Lord\n"
                                   "\n"
                                   "This is free software; see the source for copying conditions.\n"
                                   "There is NO warranty; not even for MERCHANTABILITY or FITNESS FOR A\n"
                                   "PARTICULAR PURPOSE.\n"
                                   "\n"
                                   "Report bugs to " cfg__tla_bug_mail ".\n"
                                   "\n"
                                   cfg__std__release_id_string
                                   "\n");

#define OPTS(OP) \
  OP (opt_help_msg, "h", "help", 0, \
      "Display a help message and exit.") \
  OP (opt_long_help, "H", 0, 0, \
      "Display a verbose help message and exit.") \
  OP (opt_version, "V", "version", 0, \
      "Display a release identifier string\n" \
      "and exit.") \
  OP (opt_archive, "A", "archive", 1, \
      "Override `my-default-archive'")


t_uchar arch_cmd_get_changeset_help[] = ("retrieve a changeset from an archive\n"
					 "Retrieve the changeset for REVISION and store it in the directory DIR\n"
					 "(or a directory named REVISION.patches).  DIR must not already exist.\n");

enum options
{
  OPTS (OPT_ENUM)
};

static struct opt_desc opts[] =
{
  OPTS (OPT_DESC)
    {-1, 0, 0, 0, 0}
};



int
arch_cmd_get_changeset (t_uchar * program_name, int argc, char * argv[])
{
  int o;
  struct opt_parsed * option;
  t_uchar * default_archive;


  default_archive = 0;

  safe_buffer_fd (1, 0, O_WRONLY, 0);

  option = 0;

  while (1)
    {
      o = opt_standard (lim_use_must_malloc, &option, opts, &argc, argv, program_name, usage, version_string, arch_cmd_get_changeset_help, opt_help_msg, opt_long_help, opt_version);
      if (o == opt_none)
        break;
      switch (o)
        {
        default:
          safe_printfmt (2, "unhandled option `%s'\n", option->opt_string);
          panic ("internal error parsing arguments");

        usage_error:
          opt_usage (2, argv[0], program_name, usage, 1);
          exit (1);

          /* bogus_arg: */
          safe_printfmt (2, "ill-formed argument for `%s' (`%s')\n", option->opt_string, option->arg_string);
          goto usage_error;

        case opt_archive:
          {
            default_archive = str_save (0, option->arg_string);
            break;
          }
        }
    }

  if ((argc < 2) || (argc > 3))
    goto usage_error;

  default_archive = arch_my_default_archive (default_archive);

  {
    t_uchar * revision = 0;
    t_uchar * output_dir = 0;
    struct arch_archive * arch = 0;

    revision = arch_determine_revision (&arch, default_archive, argv[1], argv[0]);

    if (argc == 3)
      output_dir = argv[2];
    else
      output_dir = str_alloc_cat (0, revision, ".patches");

    arch_check_for (arch, arch_req_patch_level, revision);

    arch_get_patch (arch, revision, output_dir);
  }

  return 0;
}




/* tag: Tom Lord Tue May 20 17:32:40 2003 (get-patch.c)
 */
