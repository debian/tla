/* local-cache.h:
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */

#ifndef INCLUDE__LIBARCH__LOCAL_CACHE_H
#define INCLUDE__LIBARCH__LOCAL_CACHE_H


#include "tla/libarch/archive.h"




/* automatically generated __STDC__ prototypes */
extern int arch_greedy_library_wants_revision (t_uchar * archive, t_uchar * revision);
extern t_uchar * arch_find_local_copy (int chatter_fd,
                                       t_uchar * tree_root,
                                       t_uchar * cache_dir,
                                       t_uchar * archive,
                                       t_uchar * revision,
                                       t_uchar * hook);
extern t_uchar * arch_find_or_make_local_copy (int chatter_fd,
                                               t_uchar * tree_root,
                                               t_uchar * cache_dir,
                                               struct arch_archive * arch,
                                               t_uchar * archive,
                                               t_uchar * revision);
extern t_uchar * arch_find_or_make_tmp_local_copy  (int chatter_fd,
                                                    t_uchar * tmp_dir,
                                                    t_uchar * tree_root,
                                                    t_uchar * cache_dir,
                                                    struct arch_archive * arch,
                                                    t_uchar * archive,
                                                    t_uchar * revision);
#endif  /* INCLUDE__LIBARCH__LOCAL_CACHE_H */


/* tag: Tom Lord Sat Jan  5 15:26:10 2002 (local-cache.h)
 */
