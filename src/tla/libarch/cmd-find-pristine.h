/* cmd-find-pristine.h:
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord, Stig Brautaset
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */

#ifndef INCLUDE__LIBARCH__CMD_FIND_PRISTINE_H
#define INCLUDE__LIBARCH__CMD_FIND_PRISTINE_H


#include "hackerlab/machine/types.h"



extern t_uchar arch_cmd_find_pristine_help[];


/* automatically generated __STDC__ prototypes */
extern int arch_cmd_find_pristine (t_uchar * program_name, int argc, char * argv[]);
#endif  /* INCLUDE__LIBARCH__CMD_FIND_PRISTINE_H */


/* tag: Stig Brautaset Sat Jun  7 14:12:48 BST 2003 (cmd-find-pristine.h)
 */
