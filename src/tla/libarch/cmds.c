/* cmds.c:
 *
 ****************************************************************
 * Copyright (C) 2003, 2004 Tom Lord
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */


#include "tla/libarch/cmd-abrowse.h"
#include "tla/libarch/cmd-add-log-version.h"
#include "tla/libarch/cmd-add-pristine.h"
#include "tla/libarch/cmd-add-id.h"
#include "tla/libarch/cmd-ancestry.h"
#include "tla/libarch/cmd-ancestry-graph.h"
#include "tla/libarch/cmd-archive-fixup.h"
#include "tla/libarch/cmd-archive-meta-info.h"
#include "tla/libarch/cmd-archive-mirror.h"
#include "tla/libarch/cmd-archive-setup.h"
#include "tla/libarch/cmd-archive-snapshot.h"
#include "tla/libarch/cmd-archive-version.h"
#include "tla/libarch/cmd-branch.h"
#include "tla/libarch/cmd-branches.h"
#include "tla/libarch/cmd-build-config.h"
#include "tla/libarch/cmd-cachedrevs.h"
#include "tla/libarch/cmd-cacherev.h"
#include "tla/libarch/cmd-cat-archive-log.h"
#include "tla/libarch/cmd-cat-log.h"
#include "tla/libarch/cmd-categories.h"
#include "tla/libarch/cmd-cat-config.h"
#include "tla/libarch/cmd-changelog.h"
#include "tla/libarch/cmd-changeset.h"
#include "tla/libarch/cmd-commit.h"
#include "tla/libarch/cmd-default-id.h"
#include "tla/libarch/cmd-delete-id.h"
#include "tla/libarch/cmd-diff.h"
#include "tla/libarch/cmd-rm.h"
#include "tla/libarch/cmd-apply-delta.h"
#include "tla/libarch/cmd-apply-changeset.h"
#include "tla/libarch/cmd-export.h"
#include "tla/libarch/cmd-file-diff.h"
#include "tla/libarch/cmd-file-find.h"
#include "tla/libarch/cmd-find-pristine.h"
#include "tla/libarch/cmd-get-changeset.h"
#include "tla/libarch/cmd-get.h"
#include "tla/libarch/cmd-grab.h"
#include "tla/libarch/cmd-help.h"
#include "tla/libarch/cmd-import.h"
#include "tla/libarch/cmd-init-tree.h"
#include "tla/libarch/cmd-id.h"
#include "tla/libarch/cmd-join-branch.h"
#include "tla/libarch/cmd-library-add.h"
#include "tla/libarch/cmd-library-archives.h"
#include "tla/libarch/cmd-library-branches.h"
#include "tla/libarch/cmd-library-categories.h"
#include "tla/libarch/cmd-library-config.h"
#include "tla/libarch/cmd-library-file.h"
#include "tla/libarch/cmd-library-find.h"
#include "tla/libarch/cmd-library-log.h"
#include "tla/libarch/cmd-library-remove.h"
#include "tla/libarch/cmd-library-revisions.h"
#include "tla/libarch/cmd-library-versions.h"
#include "tla/libarch/cmd-lock-pristine.h"
#include "tla/libarch/cmd-lock-revision.h"
#include "tla/libarch/cmd-log-for-merge.h"
#include "tla/libarch/cmd-logs.h"
#include "tla/libarch/cmd-log-versions.h"
#include "tla/libarch/cmd-archives.h"
#include "tla/libarch/cmd-pristines.h"
#include "tla/libarch/cmd-make-archive.h"
#include "tla/libarch/cmd-make-branch.h"
#include "tla/libarch/cmd-make-category.h"
#include "tla/libarch/cmd-make-log.h"
#include "tla/libarch/cmd-make-version.h"
#include "tla/libarch/cmd-merges.h"
#include "tla/libarch/cmd-move-id.h"
#include "tla/libarch/cmd-mv.h"
#include "tla/libarch/cmd-my-default-archive.h"
#include "tla/libarch/cmd-my-id.h"
#include "tla/libarch/cmd-my-revision-library.h"
#include "tla/libarch/cmd-new-merges.h"
#include "tla/libarch/cmd-parse-package-name.h"
#include "tla/libarch/cmd-rbrowse.h"
#include "tla/libarch/cmd-redo.h"
#include "tla/libarch/cmd-register-archive.h"
#include "tla/libarch/cmd-remove-log-version.h"
#include "tla/libarch/cmd-replay.h"
#include "tla/libarch/cmd-delta.h"
#include "tla/libarch/cmd-revisions.h"
#include "tla/libarch/cmd-set-tree-version.h"
#include "tla/libarch/cmd-show-changeset.h"
#include "tla/libarch/cmd-inventory.h"
#include "tla/libarch/cmd-star-merge.h"
#include "tla/libarch/cmd-switch.h"
#include "tla/libarch/cmd-sync-tree.h"
#include "tla/libarch/cmd-id-tagging-defaults.h"
#include "tla/libarch/cmd-id-tagging-method.h"
#include "tla/libarch/cmd-tag.h"
#include "tla/libarch/cmd-touch.h"
#include "tla/libarch/cmd-tree-id.h"
#include "tla/libarch/cmd-tree-lint.h"
#include "tla/libarch/cmd-tree-root.h"
#include "tla/libarch/cmd-tree-version.h"
#include "tla/libarch/cmd-uncacherev.h"
#include "tla/libarch/cmd-undo.h"
#include "tla/libarch/cmd-update.h"
#include "tla/libarch/cmd-valid-package-name.h"
#include "tla/libarch/cmd-versions.h"
#include "tla/libarch/cmd-changes.h"
#include "tla/libarch/cmd-missing.h"
#include "tla/libarch/cmd-whereis-archive.h"
#include "tla/libarch/cmd-escape.h"
#include "tla/libarch/cmds.h"



struct arch_command arch_commands[] =
{
  {"help", 0, 0, 0, arch_level_basic},

  {"help", arch_cmd_help, arch_cmd_help_help, 0, arch_level_basic},


  {"User Commands", 0, 0, 0, arch_level_basic},

  {"my-id", arch_cmd_my_id, arch_cmd_my_id_help, 0, arch_level_basic},
  {"", 0},
  {"my-default-archive", arch_cmd_my_default_archive, arch_cmd_my_default_archive_help, 0, arch_level_basic},
  {"register-archive", arch_cmd_register_archive, arch_cmd_register_archive_help, 0, arch_level_basic},
  {"archive-register", arch_cmd_register_archive, arch_cmd_register_archive_help, "register-archive", arch_level_basic},
  {"whereis-archive", arch_cmd_whereis_archive, arch_cmd_whereis_archive_help, 0, arch_level_advanced},
  {"archives", arch_cmd_archives, arch_cmd_archives_help, 0, arch_level_basic},
  {"ls-archives", arch_cmd_archives, arch_cmd_archives_help, "archives", arch_level_deprecated},


  {"Project Tree Commands", 0, 0, 0, arch_level_basic},

  {"init-tree", arch_cmd_init_tree, arch_cmd_init_tree_help, 0, arch_level_basic},
  {"tree-root", arch_cmd_tree_root, arch_cmd_tree_root_help, 0, arch_level_advanced},
  {"", 0},
  {"tree-version", arch_cmd_tree_version, arch_cmd_tree_version_help, 0, arch_level_advanced},
  {"tree-id", arch_cmd_tree_id, arch_cmd_tree_id_help, 0, arch_level_basic},
  {"tree-revision", arch_cmd_tree_id, arch_cmd_tree_id_help, 0, arch_level_basic},
  {"set-tree-version", arch_cmd_set_tree_version, arch_cmd_set_tree_version_help, 0, arch_level_advanced},
  {"", 0},
  {"undo", arch_cmd_undo, arch_cmd_undo_help, 0, arch_level_basic},
  {"undo-changes", arch_cmd_undo, arch_cmd_undo_help, "undo", arch_level_deprecated},
  {"redo", arch_cmd_redo, arch_cmd_redo_help, 0, arch_level_basic},
  {"redo-changes", arch_cmd_redo, arch_cmd_redo_help, "redo", arch_level_deprecated},
  {"", 0},
  {"changes", arch_cmd_changes, arch_cmd_changes_help, 0, arch_level_advanced},
  {"what-changed", arch_cmd_changes, arch_cmd_changes_help, "changes", arch_level_deprecated},
  {"file-diff", arch_cmd_file_diff, arch_cmd_file_diff_help, 0, arch_level_advanced},
  {"file-diffs", arch_cmd_file_diff, arch_cmd_file_diff_help, "file-diff", arch_level_deprecated},
  {"diff", arch_cmd_diff, arch_cmd_diff_help, 0, arch_level_basic},
  {"export", arch_cmd_export, arch_cmd_export_help, 0, arch_level_advanced},

  {"Project Tree Inventory Commands", 0, 0, 0, arch_level_basic},

  {"inventory", arch_cmd_inventory, arch_cmd_inventory_help, 0, arch_level_basic},
  {"srcfind", arch_cmd_inventory, arch_cmd_inventory_help, "inventory", arch_level_deprecated},
  {"tree-lint", arch_cmd_tree_lint, arch_cmd_tree_lint_help, "lint", arch_level_advanced},
  {"lint", arch_cmd_tree_lint, arch_cmd_tree_lint_help, 0, arch_level_basic},
  {"id", arch_cmd_id, arch_cmd_id_help, 0, arch_level_advanced},
  {"", 0},
  {"id-tagging-method", arch_cmd_id_tagging_method, arch_cmd_id_tagging_method_help, 0, arch_level_advanced},
  {"", 0},
  {"add", arch_cmd_add_id, arch_cmd_add_id_help, "add-id", arch_level_basic},
  {"add-id", arch_cmd_add_id, arch_cmd_add_id_help, 0, arch_level_advanced},
  {"delete-id", arch_cmd_delete_id, arch_cmd_delete_id_help, 0, arch_level_advanced},
  {"move-id", arch_cmd_move_id, arch_cmd_move_id_help, 0, arch_level_advanced},
  {"touch", arch_cmd_touch, arch_cmd_touch_help, 0, arch_level_advanced},
  {"rm", arch_cmd_rm, arch_cmd_rm_help, 0, arch_level_basic},
  {"mv", arch_cmd_mv, arch_cmd_mv_help, 0, arch_level_basic},
  {"explicit-default", arch_cmd_default_id, arch_cmd_default_id_help, 0, arch_level_advanced},
  {"default-id", arch_cmd_default_id, arch_cmd_default_id_help, "explicit-default", arch_level_advanced},
  {"", 0},
  {"id-tagging-defaults", arch_cmd_id_tagging_defaults, arch_cmd_id_tagging_defaults_help, 0, arch_level_advanced},


  {"Patch Set Commands", 0, 0, 0, arch_level_advanced},

  {"changeset", arch_cmd_changeset, arch_cmd_changeset_help, 0, arch_level_advanced},
  {"mkpatch", arch_cmd_changeset, arch_cmd_changeset_help, "changeset", arch_level_advanced},
  {"apply-changeset", arch_cmd_apply_changeset, arch_cmd_apply_changeset_help, 0, arch_level_advanced},
  {"dopatch", arch_cmd_apply_changeset, arch_cmd_apply_changeset_help, "apply-changeset", arch_level_advanced},
  {"do-changeset", arch_cmd_apply_changeset, arch_cmd_apply_changeset_help, "apply-changeset", arch_level_deprecated},
  {"show-changeset", arch_cmd_show_changeset, arch_cmd_show_changeset_help, 0, arch_level_advanced},


  {"Archive Transaction Commands", 0, 0, 0, arch_level_basic},

  {"make-archive", arch_cmd_make_archive, arch_cmd_make_archive_help, 0, arch_level_basic},
  {"archive-create", arch_cmd_make_archive, arch_cmd_make_archive_help, "make-archive", arch_level_basic},
  {"archive-setup",  arch_cmd_archive_setup,  arch_cmd_archive_setup_help, 0, arch_level_basic},
  {"", 0},
  {"make-category", arch_cmd_make_category, arch_cmd_make_category_help, 0, arch_level_advanced},
  {"make-branch", arch_cmd_make_branch, arch_cmd_make_branch_help, 0, arch_level_advanced},
  {"make-version", arch_cmd_make_version, arch_cmd_make_version_help, 0, arch_level_advanced},
  {"", 0},
  {"import", arch_cmd_import, arch_cmd_import_help, 0, arch_level_advanced},
  {"imprev", arch_cmd_import, arch_cmd_import_help, "import", arch_level_deprecated},
  {"commit", arch_cmd_commit, arch_cmd_commit_help, 0, arch_level_basic},
  {"cmtrev", arch_cmd_commit, arch_cmd_commit_help, "commit", arch_level_deprecated},
  {"", 0},
  {"get", arch_cmd_get, arch_cmd_get_help, 0, arch_level_basic},
  {"getrev", arch_cmd_get, arch_cmd_get_help, "get", arch_level_deprecated},
  {"get-changeset", arch_cmd_get_changeset, arch_cmd_get_changeset_help, 0, arch_level_advanced},
  {"get-patch", arch_cmd_get_changeset, arch_cmd_get_changeset_help, "get-changeset", arch_level_deprecated},
  {"", 0},
  {"lock-revision", arch_cmd_lock_revision, arch_cmd_lock_revision_help, 0, arch_level_advanced},
  {"archive-mirror", arch_cmd_archive_mirror, arch_cmd_archive_mirror_help, 0, arch_level_advanced},
  {"push-mirror", arch_cmd_archive_mirror, arch_cmd_archive_mirror_help, "archive-mirror", arch_level_deprecated},


  {"Archive Commands", 0, 0, 0, arch_level_basic},

  {"abrowse", arch_cmd_abrowse, arch_cmd_abrowse_help, 0, arch_level_advanced},
  {"rbrowse", arch_cmd_rbrowse, arch_cmd_rbrowse_help, 0, arch_level_basic},
  {"categories", arch_cmd_categories, arch_cmd_categories_help, 0, arch_level_advanced},
  {"branches", arch_cmd_branches, arch_cmd_branches_help, 0, arch_level_advanced},
  {"versions", arch_cmd_versions, arch_cmd_versions_help, 0, arch_level_advanced},
  {"revisions", arch_cmd_revisions, arch_cmd_revisions_help, 0, arch_level_advanced},
  {"ancestry", arch_cmd_ancestry, arch_cmd_ancestry_help, 0, arch_level_advanced},
  {"ancestry-graph", arch_cmd_ancestry_graph, arch_cmd_ancestry_graph_help, 0, arch_level_advanced},

  {"", 0},
  {"cat-archive-log", arch_cmd_cat_archive_log, arch_cmd_cat_archive_log_help, 0, arch_level_advanced},
  {"", 0},
  {"cacherev", arch_cmd_cacherev, arch_cmd_cacherev_help, 0, arch_level_advanced},
  {"cachedrevs", arch_cmd_cachedrevs, arch_cmd_cachedrevs_help, 0, arch_level_advanced},
  {"uncacherev", arch_cmd_uncacherev, arch_cmd_uncacherev_help, 0, arch_level_advanced},
  {"", 0},
  {"archive-meta-info", arch_cmd_archive_meta_info, arch_cmd_archive_meta_info_help, 0, arch_level_advanced},
  {"archive-snapshot", arch_cmd_archive_snapshot, arch_cmd_archive_snapshot_help, 0, arch_level_advanced},
  {"archive-version", arch_cmd_archive_version, arch_cmd_archive_version_help, 0, arch_level_advanced},
  {"", 0},
  {"archive-fixup", arch_cmd_archive_fixup, arch_cmd_archive_fixup_help, 0, arch_level_advanced},


  {"Patch Log Commands", 0, 0, 0, arch_level_basic},

  {"make-log", arch_cmd_make_log, arch_cmd_make_log_help, 0, arch_level_basic},
  {"log-versions", arch_cmd_log_versions, arch_cmd_log_versions_help, 0, arch_level_advanced},
  {"add-log-version", arch_cmd_add_log_version, arch_cmd_add_log_version_help, 0, arch_level_advanced},
  {"remove-log-version", arch_cmd_remove_log_version, arch_cmd_remove_log_version_help, 0, arch_level_advanced},
  {"logs", arch_cmd_logs, arch_cmd_logs_help, 0, arch_level_basic},
  {"cat-log", arch_cmd_cat_log, arch_cmd_cat_log_help, 0, arch_level_advanced},
  {"", 0},
  {"changelog", arch_cmd_changelog, arch_cmd_changelog_help, 0, arch_level_basic},
  {"", 0},
  {"log-for-merge", arch_cmd_log_for_merge, arch_cmd_log_for_merge_help, 0, arch_level_basic},
  {"merges", arch_cmd_merges, arch_cmd_merges_help, 0, arch_level_advanced},
  {"new-merges", arch_cmd_new_merges, arch_cmd_new_merges_help, 0, arch_level_advanced},


  {"Multi-project Configuration Commands", 0, 0, 0, arch_level_advanced},

  {"build-config", arch_cmd_build_config, arch_cmd_build_config_help, 0, arch_level_advanced},
  {"buildcfg", arch_cmd_build_config, arch_cmd_build_config_help, "build-config", arch_level_deprecated},
  {"cat-config", arch_cmd_cat_config, arch_cmd_cat_config_help, 0, arch_level_advanced},
  {"catcfg", arch_cmd_cat_config, arch_cmd_cat_config_help, "cat-config", arch_level_deprecated},
  {"cfgcat", arch_cmd_cat_config, arch_cmd_cat_config_help, "cat-config", arch_level_deprecated},


  {"Commands for Branching and Merging", 0, 0, 0, arch_level_basic},

  {"tag", arch_cmd_tag, arch_cmd_tag_help, 0, arch_level_basic},
  {"branch", arch_cmd_branch, arch_cmd_branch_help, 0, arch_level_basic},
  {"switch", arch_cmd_switch, arch_cmd_switch_help, 0, arch_level_basic},
  {"tagrev", arch_cmd_tag, arch_cmd_tag_help, "tag", arch_level_deprecated},
  {"", 0},
  {"update", arch_cmd_update, arch_cmd_update_help, 0, arch_level_advanced},
  {"replay", arch_cmd_replay, arch_cmd_replay_help, 0, arch_level_advanced},
  {"star-merge", arch_cmd_star_merge, arch_cmd_star_merge_help, 0, arch_level_basic},
  {"apply-delta", arch_cmd_apply_delta, arch_cmd_apply_delta_help, 0, arch_level_advanced},
  {"delta-patch", arch_cmd_apply_delta, arch_cmd_apply_delta_help, "apply-delta", arch_level_deprecated},
  {"deltapatch", arch_cmd_apply_delta, arch_cmd_apply_delta_help, "apply-delta", arch_level_deprecated},
  {"missing", arch_cmd_missing, arch_cmd_missing_help, 0, arch_level_basic},
  {"whats-missing", arch_cmd_missing, arch_cmd_missing_help, "missing", arch_level_deprecated},
  {"", 0},
  {"join-branch", arch_cmd_join_branch, arch_cmd_join_branch_help, 0, arch_level_advanced},
  {"sync-tree", arch_cmd_sync_tree, arch_cmd_sync_tree_help, 0, arch_level_advanced},
  {"make-sync-tree", arch_cmd_sync_tree, arch_cmd_sync_tree_help, "sync-tree", arch_level_deprecated},
  {"", 0},
  {"delta", arch_cmd_delta, arch_cmd_delta_help, 0, arch_level_advanced},
  {"revdelta", arch_cmd_delta, arch_cmd_delta_help, "delta", arch_level_deprecated},


  {"Local Cache Commands", 0, 0, 0, arch_level_advanced},

  {"changes", arch_cmd_changes, arch_cmd_changes_help, 0, arch_level_advanced},
  {"what-changed", arch_cmd_changes, arch_cmd_changes_help, "changes", arch_level_deprecated},
  {"file-diff", arch_cmd_file_diff, arch_cmd_file_diff_help, 0, arch_level_advanced},
  {"file-find", arch_cmd_file_find, arch_cmd_file_find_help, 0, arch_level_advanced},
  {"", 0},
  {"pristines", arch_cmd_pristines, arch_cmd_pristines_help, 0, arch_level_advanced},
  {"ls-pristines", arch_cmd_pristines, arch_cmd_pristines_help, "pristines", arch_level_deprecated},
  {"lock-pristine", arch_cmd_lock_pristine, arch_cmd_lock_pristine_help, 0, arch_level_advanced},
  {"add-pristine", arch_cmd_add_pristine, arch_cmd_add_pristine_help, 0, arch_level_advanced},
  {"find-pristine", arch_cmd_find_pristine, arch_cmd_find_pristine_help, 0, arch_level_advanced},


  {"Revision Library Commands", 0, 0, 0, arch_level_advanced},

  {"my-revision-library", arch_cmd_my_revision_library, arch_cmd_my_revision_library_help, 0, arch_level_advanced},
  {"library-dir", arch_cmd_my_revision_library, arch_cmd_my_revision_library_help, "my-revision-library", arch_level_advanced},
  {"library-config", arch_cmd_library_config, arch_cmd_library_config_help, 0, arch_level_advanced},
  {"library-find", arch_cmd_library_find, arch_cmd_library_find_help, 0, arch_level_advanced},
  {"library-add", arch_cmd_library_add, arch_cmd_library_add_help, 0, arch_level_advanced},
  {"library-remove", arch_cmd_library_remove, arch_cmd_library_remove_help, 0, arch_level_advanced},
  {"library-archives", arch_cmd_library_archives, arch_cmd_library_archives_help, 0, arch_level_advanced},
  {"library-categories", arch_cmd_library_categories, arch_cmd_library_categories_help, 0, arch_level_advanced},
  {"library-branches", arch_cmd_library_branches, arch_cmd_library_branches_help, 0, arch_level_advanced},
  {"library-versions", arch_cmd_library_versions, arch_cmd_library_versions_help, 0, arch_level_advanced},
  {"library-revisions", arch_cmd_library_revisions, arch_cmd_library_revisions_help, 0, arch_level_advanced},
  {"library-log", arch_cmd_library_log, arch_cmd_library_log_help, 0, arch_level_advanced},
  {"library-file", arch_cmd_library_file, arch_cmd_library_file_help, 0, arch_level_advanced},


  {"Published Revisions Commands", 0, 0, 0, arch_level_advanced},

  {"grab", arch_cmd_grab, arch_cmd_grab_help, 0, arch_level_advanced},



  {"Miscellaneous Scripting Support", 0, 0, 0, arch_level_advanced},

  {"parse-package-name", arch_cmd_parse_package_name, arch_cmd_parse_package_name_help, 0, arch_level_advanced},
  {"valid-package-name", arch_cmd_valid_package_name, arch_cmd_valid_package_name_help, 0, arch_level_advanced},
  {"escape", arch_cmd_escape, arch_cmd_escape_help, 0, arch_level_advanced},


  {0, },
};




/* tag: Tom Lord Tue Jun 10 17:15:53 2003 (cmds.c)
 */
