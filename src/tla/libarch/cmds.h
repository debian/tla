/* cmds.h:
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */

#ifndef INCLUDE__LIBARCH__CMDS_H
#define INCLUDE__LIBARCH__CMDS_H


#include "hackerlab/machine/types.h"



enum arch_command_level
{
  arch_level_basic = 1,
  arch_level_advanced,
  arch_level_guru,
  arch_level_deprecated,
};

struct arch_command
{
  t_uchar * name;
  int (*fn) (t_uchar * name, int argc, char * argv[]);
  t_uchar * long_help;
  t_uchar * alias_of;
  enum arch_command_level level;
};

extern struct arch_command arch_commands[];



/* automatically generated __STDC__ prototypes */
#endif  /* INCLUDE__LIBARCH__CMDS_H */


/* tag: Tom Lord Sat Jan  5 15:26:10 2002 (cmds.h)
 */
