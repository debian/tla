/* sync-tree.c:
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */


#include "hackerlab/fs/file-names.h"
#include "tla/libfsutils/rmrf.h"
#include "tla/libfsutils/tmp-files.h"
#include "tla/libawk/relational.h"
#include "tla/libarch/local-cache.h"
#include "tla/libarch/patch-logs.h"
#include "tla/libarch/namespace.h"
#include "tla/libarch/sync-tree.h"



void
arch_sync_tree (int chatter_fd, t_uchar * tree_root, struct arch_archive * arch, t_uchar * archive, t_uchar * revision)
{
  t_uchar * tmp_dir = 0;
  t_uchar * revision_instance = 0;
  rel_table has_logs = rel_table_nil;
  rel_table needs_logs = rel_table_nil;
  rel_table missing_logs = rel_table_nil;
  int x;

  tmp_dir = tmp_file_name (tree_root, ",,sync-tree-tmp");

  revision_instance = arch_find_or_make_tmp_local_copy (chatter_fd, tmp_dir, tree_root, 0, arch, archive, revision);

  has_logs = arch_all_logs (tree_root);
  needs_logs = arch_all_logs (revision_instance);
  rel_sort_table_by_field (0, has_logs, 0);
  rel_sort_table_by_field (0, needs_logs, 0);
  missing_logs = rel_join (2, rel_join_output (2,0, 2,1, -1), 0, 0, has_logs, needs_logs);

  for (x = 0; x < rel_n_records (missing_logs); ++x)
    {
      t_uchar * archive = 0;
      t_uchar * revision = 0;

      archive = arch_parse_package_name (arch_ret_archive, 0, rel_peek_str (missing_logs, x, 0));
      revision = arch_parse_package_name (arch_ret_non_archive, 0, rel_peek_str (missing_logs, x, 0));

      arch_copy_to_patch_log (tree_root, archive, revision, rel_peek_str (missing_logs, x, 1));

      lim_free (0, archive);
      lim_free (0, revision);
    }

  rmrf_file (tmp_dir);
  lim_free (0, tmp_dir);
  lim_free (0, revision_instance);
}




/* tag: Tom Lord Tue Jun 17 15:51:03 2003 (sync-tree.c)
 */
