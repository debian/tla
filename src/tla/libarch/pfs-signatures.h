/* pfs-signatures.h:
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord
 * 
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */

#ifndef INCLUDE__LIBARCH__PFS_SIGNATURES_H
#define INCLUDE__LIBARCH__PFS_SIGNATURES_H


#include "tla/libarch/archive-pfs.h"


/* automatically generated __STDC__ prototypes */
extern int arch_pfs_has_signing_rule (t_uchar * archive);
extern int arch_pfs_sign_for_archive (const t_uchar * archive,
                                      const t_uchar * official_archive,
                                      const t_uchar * revision,
                                      const t_uchar * sigfile,
                                      int in_fd,
                                      int out_fd);
extern int arch_pfs_check_signature_for_archive (const t_uchar * archive,
                                                 const t_uchar * signed_message);
extern int arch_pfs_ensure_checksum_data (struct arch_pfs_archive * arch,
                                          const t_uchar * revision);
extern int arch_pfs_checksum_anticipates_file (const t_uchar * archive,
                                               const t_uchar * revision,
                                               const t_uchar * file);
extern int arch_pfs_checksum_governs (const t_uchar * archive,
                                      const t_uchar * revision);
extern int arch_pfs_checksum_governs_strictly (struct arch_pfs_archive * arch);
extern t_uchar * arch_pfs_checked_file_contents (struct arch_pfs_archive * arch,
                                                 const t_uchar * revision,
                                                 const t_uchar * path);
extern void arch_pfs_checked_get_file (struct arch_pfs_archive * arch,
                                       const t_uchar * revision,
                                       int out_fd,
                                       const t_uchar * path);
#endif  /* INCLUDE__LIBARCH__PFS_SIGNATURES_H */


/* tag: Tom Lord Wed Dec 24 21:07:03 2003 (pfs-signatures.h)
 */
