/* exec.c:
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */


#include "hackerlab/os/unistd.h"
#include "hackerlab/mem/alloc-limits.h"
#include "hackerlab/fs/file-names.h"
#include "hackerlab/vu/safe.h"
#include "tla/libarch/my.h"
#include "tla/libarch/exec.h"



int
arch_util_execvp (t_uchar * prog, t_uchar ** argv)
{
  t_uchar ** path = 0;
  int answer;

  path = arch_my_util_path ();

  if (!path)
    answer = execvp (prog, (char **)argv);
  else
    {
      t_uchar * executable = 0;

      executable = path_find_executable (0, (const t_uchar **)path, prog);
      if (!executable)
        {
          safe_printfmt (2, "arch: unable to locate %s on the path in ~/.arch-params/path\n", prog);
          exit (2);
        }

      answer = execv (executable, (char **)argv);

      lim_free (0, executable);
    }

  free_path (0, path);
  return answer;
}




/* tag: Tom Lord Fri Jun 27 21:13:41 2003 (exec.c)
 */
