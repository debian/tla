/* hooks.h:
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */

#ifndef INCLUDE__LIBARCH__HOOKS_H
#define INCLUDE__LIBARCH__HOOKS_H


#include "hackerlab/machine/types.h"



/* automatically generated __STDC__ prototypes */
extern int arch_run_hook (t_uchar * name, ...);
  /* must terminate list with (t_uchar*)0, NOT 0 or NULL ! */
#endif  /* INCLUDE__LIBARCH__HOOKS_H */


/* tag: Tom Lord Sat Jan  5 15:26:10 2002 (hooks.h)
 */
