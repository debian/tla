/* pfs.h:
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */

#ifndef INCLUDE__LIBARCH__PFS_H
#define INCLUDE__LIBARCH__PFS_H


#include "hackerlab/machine/types.h"
#include "hackerlab/vu/vu.h"
#include "tla/libfsutils/dir-listing.h"
#include "tla/libawk/relational.h"



struct arch_pfs_session;

struct arch_pfs_vtable
{
  int (*file_exists) (struct arch_pfs_session * p, const t_uchar * path);
  int (*is_dir) (struct arch_pfs_session * p, const t_uchar * path);

  t_uchar * (*file_contents) (struct arch_pfs_session * p, const t_uchar * path, int soft_errors);
  int (*get_file) (struct arch_pfs_session * p, int out_fd, const t_uchar * path, int soft_errors);
  rel_table (*directory_files) (struct arch_pfs_session * p, const t_uchar * path, int soft_errors);

  int (*put_file) (struct arch_pfs_session * p, const t_uchar * path, mode_t perms, int in_fd, int soft_errors);

  int (*mkdir) (struct arch_pfs_session * p, const t_uchar * path, mode_t mkdir, int soft_errors);
  int (*rename) (struct arch_pfs_session * p, t_uchar ** errstr, const t_uchar * from, const t_uchar * to, int soft_errors);

  int (*rmdir) (struct arch_pfs_session * p, const t_uchar * path, int soft_errors);
  int (*rm) (struct arch_pfs_session * p, const t_uchar * path, int soft_errors);
};

struct arch_pfs_session
{
  struct arch_pfs_vtable * vtable;
};



/* automatically generated __STDC__ prototypes */
extern void arch_pfs_rmrf_file (struct arch_pfs_session * pfs,
                                const t_uchar * path);
extern void arch_pfs_pfs_make_archive (const t_uchar * name,
                                       const t_uchar * uri,
                                       const t_uchar * version,
                                       const t_uchar *mirror_of,
                                       int dot_listing_lossage,
                                       int signed_archive);
extern int arch_valid_uri (const t_uchar * uri);
extern struct arch_pfs_session * arch_pfs_connect (const t_uchar * uri);
extern t_uchar * arch_pfs_file_contents (struct arch_pfs_session * pfs,
                                         const t_uchar * path,
                                         int soft_errors);
extern rel_table arch_pfs_directory_files (struct arch_pfs_session * pfs,
                                           const t_uchar * path,
                                           int soft_errors);
extern int arch_pfs_file_exists (struct arch_pfs_session * pfs,
                                 const t_uchar * path);
extern int arch_pfs_get_file (struct arch_pfs_session * pfs,
                              int out_fd,
                              const t_uchar * path,
                              int soft_errors);
extern int arch_pfs_put_file (struct arch_pfs_session * pfs,
                              const t_uchar * path,
                              mode_t perms,
                              int in_fd,
                              int soft_errors);
extern int arch_pfs_put_atomic (struct arch_pfs_session * pfs,
                                t_uchar ** errstr,
                                const t_uchar * path,
                                mode_t perms,
                                int in_fd,
                                int replace,
                                int soft_errors);
extern int arch_pfs_mkdir (struct arch_pfs_session * pfs,
                           const t_uchar * path,
                           mode_t perms,
                           int soft_errors);
extern int arch_pfs_rename (struct arch_pfs_session * pfs,
                            t_uchar ** errstr,
                            const t_uchar * from,
                            const t_uchar * to,
                            int soft_errors);
extern int arch_pfs_is_dir (struct arch_pfs_session * pfs,
                            const t_uchar * path);
extern int arch_pfs_rmdir (struct arch_pfs_session * pfs,
                           const t_uchar * path,
                           int soft_errors);
extern int arch_pfs_rm (struct arch_pfs_session * pfs,
                        const t_uchar * path,
                        int soft_errors);
extern void arch_pfs_update_listing_file (struct arch_pfs_session * session,
                                          const t_uchar * dir);
#endif  /* INCLUDE__LIBARCH__PFS_H */


/* tag: Tom Lord Sat Jan  5 15:26:10 2002 (pfs.h)
 */
