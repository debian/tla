/* archives.h:
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */

#ifndef INCLUDE__LIBARCH__ARCHIVES_H
#define INCLUDE__LIBARCH__ARCHIVES_H


#include "tla/libawk/relational.h"

#define ARCH_REG_FAIL_NORM             0
#define ARCH_REG_FAIL_QUIET            1
#define ARCH_REG_FAIL_NOFAIL           2


/* automatically generated __STDC__ prototypes */
extern t_uchar * arch_archive_location_file (const t_uchar * archive_name);
extern void arch_set_archive_location (const t_uchar * archive_name,
                                       const t_uchar * location,
                                       int force_p,
                                       int quietly_fail);
extern t_uchar * arch_archive_location (const t_uchar * archive_name,
                                        int soft);
extern void arch_delete_archive_location (const t_uchar * archive_name,
                                          int force_p);
extern rel_table arch_registered_archives (void);
extern t_uchar * arch_mirrored_at_name (const t_uchar * archive);
extern t_uchar * arch_mirrored_from_name (const t_uchar * archive);
extern t_uchar * arch_mirrored_at (const t_uchar * archive);
extern t_uchar * arch_mirrored_from (const t_uchar * archive);
extern t_uchar * arch_fs_archive_archive_version_path (const t_uchar * archive_path);
extern t_uchar * arch_fs_archive_meta_info_path (const t_uchar * archive_path);
extern t_uchar * arch_fs_archive_meta_info_item_path (const t_uchar * archive_path,
                                                      const t_uchar * meta_info_name);
#endif  /* INCLUDE__LIBARCH__ARCHIVES_H */


/* tag: Tom Lord Sat Jan  5 15:26:10 2002 (archives.h)
 */
