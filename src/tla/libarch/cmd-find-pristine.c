/* cmd-find-pristine.c
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */


#include "config-options.h"
#include "hackerlab/cmd/main.h"
#include "tla/libarch/namespace.h"
#include "tla/libarch/project-tree.h"
#include "tla/libarch/pristines.h"
#include "tla/libarch/cmd.h"
#include "tla/libarch/cmd-find-pristine.h"



static t_uchar * usage = "[options] revision";
static t_uchar * version_string = (cfg__std__package " from regexps.com\n"
                                   "\n"
                                   "Copyright 2003 Tom Lord\n"
                                   "\n"
                                   "This is free software; see the source for copying conditions.\n"
                                   "There is NO warranty; not even for MERCHANTABILITY or FITNESS FOR A\n"
                                   "PARTICULAR PURPOSE.\n"
                                   "\n"
                                   "Report bugs to " cfg__tla_bug_mail ".\n"
                                   "\n"
                                   cfg__std__release_id_string
                                   "\n");

#define OPTS(OP) \
  OP (opt_help_msg, "h", "help", 0, \
      "Display a help message and exit.") \
  OP (opt_long_help, "H", 0, 0, \
      "Display a verbose help message and exit.") \
  OP (opt_version, "V", "version", 0, \
      "Display a release identifier string\n" \
      "and exit.") \
  OP (opt_archive, "A", "archive", 1, \
      "Override `my-default-archive'") \
  OP (opt_dir, "d", "dir DIR", 1, \
      "cd to DIR first") \
  OP (opt_unlocked, "u", "unlocked", 0, \
      "return only an unlocked pristine") \
  OP (opt_locked, "l", "locked", 0, \
      "return only a locked pristine") \
  OP (opt_no_siblings, "t", "tree-only", 0, \
      "search this tree only, not siblings") \
  OP (opt_silent, "s", "silent", 0, \
      "exit status only")

t_uchar arch_cmd_find_pristine_help[] = ("find and print the path to a pristine revision\n"
                                         "Print the location of a pristine copy of the indicated revision\n");

enum options
{
  OPTS (OPT_ENUM)
};

static struct opt_desc opts[] =
{
  OPTS (OPT_DESC)
    {-1, 0, 0, 0, 0}
};



int
arch_cmd_find_pristine (t_uchar * program_name, int argc, char * argv[])
{
  int o;
  struct opt_parsed * option;
  int silent;
  int pristine_type;
  enum arch_pristine_search_scope scope;
  char * dir;
  t_uchar * default_archive;

  silent = 0;
  pristine_type = arch_any_pristine;
  scope = arch_tree_and_sibling_pristine_search;
  dir = str_save (0, ".");
  default_archive = 0;

  safe_buffer_fd (1, 0, O_WRONLY, 0);

  option = 0;

  while (1)
    {
      o = opt_standard (lim_use_must_malloc, &option, opts, &argc, argv, program_name, usage, version_string, arch_cmd_find_pristine_help, opt_help_msg, opt_long_help, opt_version);
      if (o == opt_none)
        break;
      switch (o)
        {
        default:
          safe_printfmt (2, "unhandled option `%s'\n", option->opt_string);
          panic ("internal error parsing arguments");

        usage_error:
          opt_usage (2, argv[0], program_name, usage, 1);
          exit (1);

          /* bogus_arg: */
          safe_printfmt (2, "ill-formed argument for `%s' (`%s')\n", option->opt_string, option->arg_string);
          goto usage_error;


        case opt_archive:
          {
            lim_free (0, default_archive);
            default_archive = str_save (0, option->arg_string);
            break;
          }

        case opt_dir:
          {
            lim_free (0, dir);
            dir = str_save (0, option->arg_string);
            break;
          }

        case opt_silent:
          {
            silent = 1;
            break;
          }

        case opt_unlocked:
          {
            pristine_type = arch_unlocked_pristine;
            break;
          }

        case opt_locked:
          {
            pristine_type = arch_locked_pristine;
            break;
          }

        case opt_no_siblings:
          {
            scope = arch_tree_pristine_search;
            break;
          }
        }
    }

  if (argc != 2)
    goto usage_error;

  {
    t_uchar * revspec;
    t_uchar * tree_root = 0;
    t_uchar * archive = 0;
    t_uchar * revision = 0;
    t_uchar * pristine_path = 0;
    int is_locked = 0;


    if (default_archive && !arch_valid_archive_name (default_archive))
      {
        safe_printfmt (2, "%s: invalid archive name (%s)\n",
                       argv[0], default_archive);
        exit (2);
      }

    revspec = argv[1];

    if (!arch_valid_package_name (revspec, arch_maybe_archive, arch_req_patch_level, 0))
      {
        safe_printfmt (2, "%s: invalid revision name (%s)\n",
                       argv[0], revspec);
        exit (2);
      }

    archive = arch_parse_package_name (arch_ret_archive, default_archive, revspec);
    revision = arch_parse_package_name (arch_ret_non_archive, 0, revspec);

    tree_root = arch_tree_root (0, dir, 0);

    if (!tree_root)
      {
        safe_printfmt (2, "%s: directory is not a project tree (%s)\n",
                       argv[0], dir);
        exit (2);
      }

    pristine_path = arch_find_pristine (&is_locked, tree_root, archive, revision, pristine_type, scope);

    if (pristine_path)
      safe_printfmt (1, "%s\n", pristine_path);
    else
      {
        if (!silent)
          {
            safe_printfmt (2, "%s: unable to find pristine for %s/%s\n",
                           argv[0], archive, revision);
          }
        exit (1);
      }
  }

  return 0;
}




/* tag: Tom Lord Wed May 21 19:30:43 2003 (find-pristine.c)
 */
