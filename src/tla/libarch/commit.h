/* commit.h:
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */

#ifndef INCLUDE__LIBARCH__COMMIT_H
#define INCLUDE__LIBARCH__COMMIT_H


#include "tla/libarch/archive.h"


/* automatically generated __STDC__ prototypes */
extern void arch_commit (int chatter_fd,
                         struct arch_archive * arch,
                         t_uchar * revision,
                         t_uchar * tree_root,
                         t_uchar * raw_log,
                         rel_table file_list,
                         int escape_classes);
#endif  /* INCLUDE__LIBARCH__COMMIT_H */


/* tag: Tom Lord Sat Jan  5 15:26:10 2002 (commit.h)
 */
