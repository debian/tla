/* import.h:
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */

#ifndef INCLUDE__LIBARCH__IMPORT_H
#define INCLUDE__LIBARCH__IMPORT_H


#include "tla/libarch/archive.h"


/* automatically generated __STDC__ prototypes */
extern void arch_import (struct arch_archive * arch,
                         t_uchar * version,
                         t_uchar * tree_root,
                         t_uchar * raw_log);
#endif  /* INCLUDE__LIBARCH__IMPORT_H */


/* tag: Tom Lord Sat Jan  5 15:26:10 2002 (import.h)
 */
