/* cmd-library-file.c
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */


#include "config-options.h"
#include "hackerlab/cmd/main.h"
#include "tla/libarch/my.h"
#include "tla/libarch/libraries.h"
#include "tla/libarch/inv-ids.h"
#include "tla/libarch/namespace.h"
#include "tla/libarch/cmd-library-file.h"



static t_uchar * usage = "[options] file revision";
static t_uchar * version_string = (cfg__std__package " from regexps.com\n"
                                   "\n"
                                   "Copyright 2003 Tom Lord\n"
                                   "\n"
                                   "This is free software; see the source for copying conditions.\n"
                                   "There is NO warranty; not even for MERCHANTABILITY or FITNESS FOR A\n"
                                   "PARTICULAR PURPOSE.\n"
                                   "\n"
                                   "Report bugs to " cfg__tla_bug_mail ".\n"
                                   "\n"
                                   cfg__std__release_id_string
                                   "\n");

#define OPTS(OP) \
  OP (opt_help_msg, "h", "help", 0, \
      "Display a help message and exit.") \
  OP (opt_long_help, "H", 0, 0, \
      "Display a verbose help message and exit.") \
  OP (opt_version, "V", "version", 0, \
      "Display a release identifier string\n" \
      "and exit.") \
  OP (opt_archive, "A", "archive", 1, \
      "Override `my-default-archive'") \
  OP (opt_id, 0, "id", 0, \
      "interpret FILE as an inventory id") \
  OP (opt_this, 0, "this", 0, \
      "interpret FILE as a file within a project tree")

t_uchar arch_cmd_library_file_help[] = ("find a file in a revision library\n"
                                        "Find FILE in REVISION in your revision library and print\n"
                                        "its path. If the revision or file is not present, print\n"
                                        "an error message and exit with non-0 status.\n");

enum options
{
  OPTS (OPT_ENUM)
};

static struct opt_desc opts[] =
{
  OPTS (OPT_DESC)
    {-1, 0, 0, 0, 0}
};



int
arch_cmd_library_file (t_uchar * program_name, int argc, char * argv[])
{
  int o;
  struct opt_parsed * option;
  t_uchar * default_archive;
  int id = 0;
  int this = 0;

  default_archive = 0;

  safe_buffer_fd (1, 0, O_WRONLY, 0);

  option = 0;

  while (1)
    {
      o = opt_standard (lim_use_must_malloc, &option, opts, &argc, argv, program_name, usage, version_string, arch_cmd_library_file_help, opt_help_msg, opt_long_help, opt_version);
      if (o == opt_none)
        break;
      switch (o)
        {
        default:
          safe_printfmt (2, "unhandled option `%s'\n", option->opt_string);
          panic ("internal error parsing arguments");

        usage_error:
          opt_usage (2, argv[0], program_name, usage, 1);
          exit (1);

          /* bogus_arg: */
          safe_printfmt (2, "ill-formed argument for `%s' (`%s')\n", option->opt_string, option->arg_string);
          goto usage_error;

        case opt_archive:
          {
            default_archive = str_save (0, option->arg_string);
            break;
          }

        case opt_id:
          {
            id = 1;
            break;
          }

        case opt_this:
          {
            this = 1;
            break;
          }
        }
    }

  if (argc != 3)
    goto usage_error;

  if (default_archive && !arch_valid_archive_name (default_archive))
    {
      safe_printfmt (2, "%s: invalid archive name (%s)\n",
                     argv[0], default_archive);
      exit (1);
    }

  {
    t_uchar * filespec;
    t_uchar * revspec;
    t_uchar * archive = 0;
    t_uchar * revision = 0;
    t_uchar * answer;

    filespec = argv[1];
    revspec = argv[2];

    if (!arch_valid_package_name (revspec, arch_maybe_archive, arch_req_patch_level, 0))
      {
        safe_printfmt (2, "%s: invalid revision name (%s)\n",
                       argv[0], revspec);
        exit (2);
      }

    archive = arch_parse_package_name (arch_ret_archive, default_archive, revspec);
    if (!archive)
      {
        safe_printfmt (2, "%s: no archive specified for library-file\n",
                       argv[0]);
        exit (2);
      }

    revision = arch_parse_package_name (arch_ret_non_archive, 0, revspec);

    if (!this && !id)
      {
        answer = arch_library_find_file (archive, revision, filespec);
      }
    else
      {
        t_uchar * the_id = 0;

        if (!this)
          {
            the_id = str_save (0, filespec);
          }
        else
          {
            the_id = arch_inventory_id (arch_unspecified_id_tagging, 0, filespec, 0, 0, 0);
          }

        answer = arch_library_find_file_by_id (archive, revision, the_id);

        lim_free (0, the_id);
      }

    if (!answer)
      {
        if (id && !this)
          safe_printfmt (2, "%s: file not found in library (%s/%s*%s)\n",
                         argv[0], archive, revision, filespec);
        else
          safe_printfmt (2, "%s: file not found in library (%s/%s:%s)\n",
                         argv[0], archive, revision, filespec);
        exit (1);
      }
    else
      safe_printfmt (1, "%s\n", answer);


    lim_free (0, archive);
    lim_free (0, revision);
    lim_free (0, answer);
  }

  return 0;
}




/* tag: Tom Lord Wed May 28 18:24:40 2003 (library-file.c)
 */
