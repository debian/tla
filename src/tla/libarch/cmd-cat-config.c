/* cat-config.c
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */


#include "config-options.h"
#include "hackerlab/cmd/main.h"
#include "tla/libarch/namespace.h"
#include "tla/libarch/project-tree.h"
#include "tla/libarch/configs.h"
#include "tla/libarch/cmd.h"
#include "tla/libarch/cmd-cat-config.h"



static t_uchar * usage = "[options] config";
static t_uchar * version_string = (cfg__std__package " from regexps.com\n"
                                   "\n"
                                   "Copyright 2003 Tom Lord\n"
                                   "\n"
                                   "This is free software; see the source for copying conditions.\n"
                                   "There is NO warranty; not even for MERCHANTABILITY or FITNESS FOR A\n"
                                   "PARTICULAR PURPOSE.\n"
                                   "\n"
                                   "Report bugs to " cfg__tla_bug_mail ".\n"
                                   "\n"
                                   cfg__std__release_id_string
                                   "\n");

#define OPTS(OP) \
  OP (opt_help_msg, "h", "help", 0, \
      "Display a help message and exit.") \
  OP (opt_long_help, "H", 0, 0, \
      "Display a verbose help message and exit.") \
  OP (opt_version, "V", "version", 0, \
      "Display a release identifier string\n" \
      "and exit.") \
  OP (opt_dir, "d", "dir DIR", 1, \
      "cd to DIR first") \
  OP (opt_output, "o", "output CFG", 1, \
      "write the output as config CFG") \
  OP (opt_force, "f", "force", 0, \
      "overwrite an exiting config (with --output)") \
  OP (opt_snap, "s", "snap", 0, \
      "Show current patch levels of subtree packages.") \
  OP (opt_unescaped, 0, "unescaped", 0, \
      "show filenames in unescaped form")

t_uchar arch_cmd_cat_config_help[] = ("output information about a multi-project config\n"
                                  "Parse and print the indicate config file from a project tree\n"
                                  "\n"
                                  "A config file contains blank lines, comment lines starting with\n"
                                  "\"#\", and config specification lines.   The config file called\n"
                                  "$NAME is stored in a project tree as ./configs/$NAME or as\n"
                                  "./$NAME.\n"
                                  "\n"
                                  "A config specification line contains a relative path within\n"
                                  "a project tree, and a specification of the project or revision\n"
                                  "to store in that location.\n"
                                  "\n"
                                  "For example, the line:\n"
                                  "\n"
                                  "   ./src/arch   lord@emf.net--2003b/arch--devo--1.0\n"
                                  "\n"
                                  "means that, when building the configuration, the latest\n"
                                  "revision of arch--devo--1.0 should be created within the\n"
                                  "tree as ./src/arch.\n"
                                  "\n"
                                  "The project specification can be a branch name, version name\n"
                                  "or the name of a specific revision.\n"
                                  "\n"
                                  "The option --snap says to examine the project tree to find\n"
                                  "out which revisions of configured project are printed, and\n"
                                  "generate a new config specification that references those specific\n"
                                  "revisions.   For example, the output for the line shown above\n"
                                  "might be:\n"
                                  "\n"
                                  "    ./src/arch  lord@emf.net--2003b/arch--devo--1.0--patch-21\n"
                                  "\n"
                                  "The option --output causes the output from this command to be\n"
                                  "recorded as a new configuration file (or to replace an existing\n"
                                  "file if --force is provided).\n");

enum options
{
  OPTS (OPT_ENUM)
};

static struct opt_desc opts[] =
{
  OPTS (OPT_DESC)
    {-1, 0, 0, 0, 0}
};



int
arch_cmd_cat_config (t_uchar * program_name, int argc, char * argv[])
{
  int o;
  struct opt_parsed * option;
  t_uchar * dir = 0;
  int snap = 0;
  int force = 0;
  int escape_classes = arch_escape_classes;
  t_uchar * output = 0;

  dir = str_save (0, ".");

  safe_buffer_fd (1, 0, O_WRONLY, 0);

  option = 0;

  while (1)
    {
      o = opt_standard (lim_use_must_malloc, &option, opts, &argc, argv, program_name, usage, version_string, arch_cmd_cat_config_help, opt_help_msg, opt_long_help, opt_version);
      if (o == opt_none)
        break;
      switch (o)
        {
        default:
          safe_printfmt (2, "unhandled option `%s'\n", option->opt_string);
          panic ("internal error parsing arguments");

        usage_error:
          opt_usage (2, argv[0], program_name, usage, 1);
          exit (1);

          /* bogus_arg: */
          safe_printfmt (2, "ill-formed argument for `%s' (`%s')\n", option->opt_string, option->arg_string);
          goto usage_error;

        case opt_dir:
          {
            lim_free (0, dir);
            dir = str_save (0, option->arg_string);
            break;
          }

        case opt_snap:
          {
            snap = 1;
            break;
          }

        case opt_force:
          {
            force = 1;
            break;
          }

        case opt_output:
          {
            output = str_save (0, option->arg_string);
            break;
          }

	case opt_unescaped:
	  {
	    escape_classes = 0;
	    break;
	  }
        }
    }

  if (argc != 2)
    goto usage_error;

  {
    t_uchar * config;
    t_uchar * tree_root = 0;
    rel_table config_list = rel_table_nil;

    config = argv[1];

    if (!arch_valid_config_name (config))
      {
        safe_printfmt (2, "%s: invalid config name (%s)\n", argv[0], config);
        exit (2);
      }

    if (output && !arch_valid_config_name (config))
      {
        safe_printfmt (2, "%s: invalid config name (%s)\n", argv[0], output);
        exit (2);
      }

    tree_root = arch_tree_root (0, dir, 0);

    if (!tree_root)
      {
        safe_printfmt (2, "%s: not in project tree (%s)\n", argv[0], dir);
        exit (1);
      }

    config_list = arch_read_config (tree_root, config);


    {
      int out_fd;

      if (output)
        out_fd = arch_begin_new_config (tree_root, output, force);
      else
        out_fd = 1;

      if (!snap)
        rel_print_pika_escape_iso8859_1_table (out_fd, escape_classes,
                                                  config_list);
      else
        {
          rel_table snap = rel_table_nil;

          snap = arch_config_from_tree (tree_root, config_list);
          rel_print_pika_escape_iso8859_1_table (out_fd, escape_classes,
                                                    snap);

          rel_free_table (snap);
        }

      if (output)
        {
          arch_finish_new_config (out_fd, tree_root, output, force);
          safe_printfmt (1, "* wrote config %s\n", output);
        }

      rel_free_table (config_list);
      lim_free (0, tree_root);
    }
  }

  lim_free (0, dir);
  lim_free (0, output);
  return 0;
}



/* tag: Tom Lord Fri May 30 02:12:37 2003 (cfgcat.c)
 */
