/* star-merge.h:
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */

#ifndef INCLUDE__LIBARCH__STAR_MERGE_H
#define INCLUDE__LIBARCH__STAR_MERGE_H


#include "hackerlab/machine/types.h"
#include "tla/libarch/archive.h"
#include "tla/libawk/associative.h"


/* automatically generated __STDC__ prototypes */
extern t_uchar * arch_nearest_star_ancester (t_uchar * from_tree_root, t_uchar * from_archive, t_uchar * from_revision,
                                             t_uchar * to_tree_root, t_uchar * to_archive, t_uchar * to_version);
extern int arch_star_merge (int chatter_fd,
                            struct arch_archive * from_arch, t_uchar * from_archive, t_uchar * from_revision,
                            t_uchar * to_tree_root, struct arch_archive * to_arch, t_uchar * to_archive, t_uchar * to_version,
                            t_uchar * cache_dir, t_uchar * changeset_output, int use_diff3, int forward,
                            int escape_classes);
extern void 
arch_merge3(int chatter_fd, t_uchar * mine_tree_root, t_uchar * base_tree_root, t_uchar * other_tree_root, int escape_classes);


#endif  /* INCLUDE__LIBARCH__STAR_MERGE_H */


/* tag: Tom Lord Sat Jan  5 15:26:10 2002 (star-merge.h)
 */
