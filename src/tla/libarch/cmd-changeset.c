/* cmd-changeset.c
 *
 ****************************************************************
 * Copyright (C) 2001, 2002, 2003  Tom Lord
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */


#include "config-options.h"
#include "hackerlab/cmd/main.h"
#include "hackerlab/mem/mem.h"
#include "tla/libarch/make-changeset.h"
#include "tla/libarch/make-changeset-files.h"
#include "tla/libarch/cmd.h"
#include "tla/libarch/cmd-changeset.h"


/* __STDC__ prototypes for static functions */
static void changeset_callback (void * ign, const char * fmt, va_list ap);



static t_uchar * usage = "[options] ORIG MOD DEST [files]";
static t_uchar * version_string = (cfg__std__package " from regexps.com\n"
                                   "\n"
                                   "Copyright 2001, 2002, 2003 Tom Lord\n"
                                   "\n"
                                   "This is free software; see the source for copying conditions.\n"
                                   "There is NO warranty; not even for MERCHANTABILITY or FITNESS FOR A\n"
                                   "PARTICULAR PURPOSE.\n"
                                   "\n"
                                   "Report bugs to " cfg__tla_bug_mail ".\n"
                                   "\n"
                                   cfg__std__release_id_string
                                   "\n");

#define OPTS(OP) \
  OP (opt_help_msg, "h", "help", 0, \
      "Display a help message and exit.") \
  OP (opt_long_help, "H", 0, 0, \
      "Display a verbose help message and exit.") \
  OP (opt_version, "V", "version", 0, \
      "Display a release identifier string\n" \
      "and exit.") \
  OP (opt_file_list, 0, "file-list FILES", 1, \
      "record only diffs of selected files") \
  OP (opt_unescaped, 0, "unescaped", 0, \
      "show filenames in unescaped form")

t_uchar arch_cmd_changeset_help[] = ("compute a whole-tree changeset\n"
                                     "Create the output directory DESTINATION (it must not already\n"
                                     "exist).\n"
                                     "\n"
                                     "Compare source trees ORIGINAL and MODIFIED.  Create a changeset\n"
                                     "tree in DESTINATION\n"
                                     "\n"
                                     "See also \"tla apply-changeset -H\".\n");

enum options
{
  OPTS (OPT_ENUM)
};

static struct opt_desc opts[] =
{
  OPTS (OPT_DESC)
    {-1, 0, 0, 0, 0}
};



int
arch_cmd_changeset (t_uchar * program_name, int argc, char * argv[])
{
  int o;
  struct opt_parsed * option;
  t_uchar * file_list_file = 0;
  int escape_classes = arch_escape_classes;

  safe_buffer_fd (1, 0, O_WRONLY, 0);

  option = 0;

  while (1)
    {
      o = opt_standard (lim_use_must_malloc, &option, opts, &argc, argv, program_name, usage, version_string, arch_cmd_changeset_help, opt_help_msg, opt_long_help, opt_version);
      if (o == opt_none)
        break;
      switch (o)
        {
        default:
          safe_printfmt (2, "unhandled option `%s'\n", option->opt_string);
          panic ("internal error parsing arguments");

        case opt_double_dash:
        usage_error:
          opt_usage (2, argv[0], program_name, usage, 1);
          exit (1);

          /* bogus_arg: */
          safe_printfmt (2, "ill-formed argument for `%s' (`%s')\n", option->opt_string, option->arg_string);
          goto usage_error;

        case opt_file_list:
          {
            lim_free (0, file_list_file);
            file_list_file = str_save (0, option->arg_string);
            break;
          }

	case opt_unescaped:
	  {
	    escape_classes = 0;
	    break;
	  }
        }
    }

  if (argc < 4
      || str_cmp (argv[2], "--") == 0 || str_cmp (argv[3], "--") == 0)
    goto usage_error;

  {
    struct arch_make_changeset_report report;

    mem_set0 ((void *)&report, sizeof (report));
    report.callback = changeset_callback;

    if (argc == 4 && !file_list_file)
      {
        arch_make_changeset (&report, argv[1], argv[2], argv[3], arch_unspecified_id_tagging, arch_inventory_unrecognized, rel_table_nil, 0, 0, escape_classes);
      }
    else
      {
        rel_table file_list = rel_table_nil;

        if (argc > 4)
          {
            int x = 4;

            if (str_cmp (argv[x], "--") == 0)
              x++;

            while (x < argc)
              rel_add_records (&file_list, rel_singleton_record_taking (rel_make_field_str (argv[x++])), rel_record_null);
          }

        if (file_list_file)
          {
            int in_fd = safe_open (file_list_file, O_RDONLY, 0);
            rel_read_table (in_fd, 1, argv[0], file_list_file);
            safe_close (in_fd);
          }

        arch_make_files_changeset (&report, argv[3], file_list, argv[1], argv[2], arch_unspecified_id_tagging, arch_inventory_unrecognized, escape_classes);

        rel_free_table (file_list);
      }

    arch_free_make_changeset_report_data (&report);
  }

  lim_free (0, file_list_file);

  return 0;
}


static void
changeset_callback (void * ign, const char * fmt, va_list ap)
{
  safe_printfmt_va_list (1, fmt, ap);
  safe_flush (1);
}





/* tag: Tom Lord Thu May 15 02:25:10 2003 (changeset.c)
 */
