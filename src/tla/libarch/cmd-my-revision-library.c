/* cmd-my-revision-library.c
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */


#include "config-options.h"
#include "hackerlab/cmd/main.h"
#include "tla/libfsutils/dir-as-cwd.h"
#include "tla/libarch/my.h"
#include "tla/libarch/cmd-my-revision-library.h"



static t_uchar * usage = "[options] [dir]";
static t_uchar * version_string = (cfg__std__package " from regexps.com\n"
                                   "\n"
                                   "Copyright 2003 Tom Lord\n"
                                   "\n"
                                   "This is free software; see the source for copying conditions.\n"
                                   "There is NO warranty; not even for MERCHANTABILITY or FITNESS FOR A\n"
                                   "PARTICULAR PURPOSE.\n"
                                   "\n"
                                   "Report bugs to " cfg__tla_bug_mail ".\n"
                                   "\n"
                                   cfg__std__release_id_string
                                   "\n");

#define OPTS(OP) \
  OP (opt_help_msg, "h", "help", 0, \
      "Display a help message and exit.") \
  OP (opt_long_help, "H", 0, 0, \
      "Display a verbose help message and exit.") \
  OP (opt_version, "V", "version", 0, \
      "Display a release identifier string\n" \
      "and exit.") \
  OP (opt_errname, "e", "errname", 1, \
      "specify program name for errors") \
  OP (opt_delete, "d", "delete", 0, \
      "unspecify your revision library") \
  OP (opt_silent, "s", "silent", 0, \
      "suppress reassuring messages") \
  OP (opt_search, 0, "search", 0, \
      "use the full search path") \
  OP (opt_add, 0, "add", 0, \
      "use the full add path") \
  OP (opt_search_only, 0, "search-only", 0, \
      "use the search-only path") \
  OP (opt_add_only, 0, "add-only", 0, \
      "use the add-only path") \
  OP (opt_first, 0, "first", 0, \
      "prepend to the path if setting (default appends)")

t_uchar arch_cmd_my_revision_library_help[] = ("print or change your revision library path\n"
                                               "With no argument, and without -d, print the path to your revision\n"
                                               "library.\n"
                                               "\n"
                                               "With an argument, record DIR as the path to your revision library\n"
                                               "in ~/.arch-params/=revision-library\n"
                                               "\n"
                                               "With the option -d (--delete) and no argument, ensure that\n"
                                               "you do not have a revision library path set in ~/.arch-params.\n"
                                               "\n"
                                               "If no revision library is set, the program exits with status 1,\n"
                                               "printing an error message unless the -s (--silent) option is given.\n");

enum options
{
  OPTS (OPT_ENUM)
};

static struct opt_desc opts[] =
{
  OPTS (OPT_DESC)
    {-1, 0, 0, 0, 0}
};



int
arch_cmd_my_revision_library (t_uchar * program_name, int argc, char * argv[])
{
  int o;
  struct opt_parsed * option;
  char * dir = 0;
  char * errname;
  enum arch_library_edit_op op = arch_library_last;
  int print_only = 1;
  int silent;
  enum arch_library_path_type path_type = arch_library_path_search_order;

  errname = argv[0];
  silent = 0;

  safe_buffer_fd (1, 0, O_WRONLY, 0);

  option = 0;

  while (1)
    {
      o = opt_standard (lim_use_must_malloc, &option, opts, &argc, argv, program_name, usage, version_string, arch_cmd_my_revision_library_help, opt_help_msg, opt_long_help, opt_version);
      if (o == opt_none)
        break;
      switch (o)
        {
        default:
          safe_printfmt (2, "unhandled option `%s'\n", option->opt_string);
          panic ("internal error parsing arguments");

        usage_error:
          opt_usage (2, argv[0], program_name, usage, 1);
          exit (1);

          /* bogus_arg: */
          safe_printfmt (2, "ill-formed argument for `%s' (`%s')\n", option->opt_string, option->arg_string);
          goto usage_error;

        case opt_errname:
          {
            if (!option->arg_string[0])
              errname = 0;
            else
              errname = str_save (0, option->arg_string);
            break;
          }

        case opt_delete:
          {
            op = arch_library_remove;
            print_only = 0;
            break;
          }

        case opt_first:
          {
            op = arch_library_first;
            print_only = 0;
            break;
          }

        case opt_silent:
          {
            silent = 1;
            break;
          }

        case opt_search:
          {
            path_type = arch_library_path_search_order;
            break;
          }

        case opt_add:
          {
            path_type = arch_library_path_add_order;
            break;
          }

        case opt_search_only:
          {
            path_type = arch_library_path_search_only;
            break;
          }

        case opt_add_only:
          {
            path_type = arch_library_path_add_only;
            break;
          }
        }
    }


  if (argc > 2)
    goto usage_error;

  if (print_only && (argc == 2))
    {
      print_only = 0;
      op = arch_library_last;
    }

  if (!print_only && (argc < 2))
    goto usage_error;

  if (argc == 2)
    {
      dir = str_save (0, argv[1]);
    }

  /****************************************************************
   * Do It
   */

  if (print_only)
    {
      rel_table lib_path = rel_table_nil;

      lib_path = arch_my_library_path (path_type);
      if (rel_n_records (lib_path))
        {
          rel_print_table (1, lib_path);
        }
      else
        {
          if (errname)
            safe_printfmt (2, "%s: no revision library path set\n", errname);
          exit (1);
        }
    }
  else
    {
      arch_set_my_library_path (path_type, op, dir);
    }
  lim_free(0, dir);
  return 0;
}



/* tag: Tom Lord Wed May 21 13:36:58 2003 (my-revision-library.c)
 */
