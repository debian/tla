/* diffs.h:
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */

#ifndef INCLUDE__LIBFSUTILS__DIFFS_H
#define INCLUDE__LIBFSUTILS__DIFFS_H


#include "hackerlab/machine/types.h"
#include "tla/libawk/associative.h"


/* automatically generated __STDC__ prototypes */
extern int arch_binary_files_differ (const t_uchar * a, const t_uchar * b, const t_uchar * id, assoc_table inode_sig_shortcuts_of_b);
extern int arch_file_stats_differ (struct stat *a_stat, struct stat *b_stat);
extern int arch_inode_sigs_differ (struct stat *b_stat, const t_uchar * id, assoc_table inode_sig_shortcuts_of_b);
extern int arch_filename_contents_differ (const t_uchar * a, const t_uchar * b);
extern int arch_file_contents_differ (int a_fd, int b_fd);
extern int arch_invoke_diff (int output_fd,
                             const char * orig_path, const char * orig_loc,
                             const char * mod_path, const char * mod_loc,
                             t_uchar * id,
                             assoc_table inode_sig_shortcuts_of_mod);
extern int arch_really_invoke_diff (int output_fd,
                                    const char * orig_path, const char * orig_loc,
                                    const char * mod_path, const char * mod_loc, const char **extraopts);
#endif  /* INCLUDE__LIBFSUTILS__DIFFS_H */


/* tag: Tom Lord Sat Jan  5 15:26:10 2002 (diffs.h)
 */
