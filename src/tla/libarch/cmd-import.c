/* cmd-import.c
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */


#include "config-options.h"
#include "hackerlab/cmd/main.h"
#include "tla/libfsutils/file-contents.h"
#include "tla/libarch/namespace.h"
#include "tla/libarch/project-tree.h"
#include "tla/libarch/patch-logs.h"
#include "tla/libarch/archive.h"
#include "tla/libarch/proj-tree-lint.h"
#include "tla/libarch/archive-setup.h"
#include "tla/libarch/import.h"
#include "tla/libarch/cmd.h"
#include "tla/libarch/cmdutils.h"
#include "tla/libarch/cmd-import.h"



static t_uchar * usage = "[options] [[archive]/version]";
static t_uchar * version_string = (cfg__std__package " from regexps.com\n"
                                   "\n"
                                   "Copyright 2003 Tom Lord\n"
                                   "\n"
                                   "This is free software; see the source for copying conditions.\n"
                                   "There is NO warranty; not even for MERCHANTABILITY or FITNESS FOR A\n"
                                   "PARTICULAR PURPOSE.\n"
                                   "\n"
                                   "Report bugs to " cfg__tla_bug_mail ".\n"
                                   "\n"
                                   cfg__std__release_id_string
                                   "\n");

#define OPTS(OP) \
  OP (opt_help_msg, "h", "help", 0, \
      "Display a help message and exit.") \
  OP (opt_long_help, "H", 0, 0, \
      "Display a verbose help message and exit.") \
  OP (opt_version, "V", "version", 0, \
      "Display a release identifier string\n" \
      "and exit.") \
  OP (opt_archive, "A", "archive", 1, \
      "Override `my-default-archive'.") \
  OP (opt_dir, "d", "dir DIR", 1, \
      "Change to DIR first.") \
  OP (opt_log, "l", "log FILE", 1, \
      "Commit with log file FILE.") \
  OP (opt_summary, "s", "summary TEXT", 1, \
      "log with summary TEXT plus log-for-merge output" ) \
  OP (opt_log_msg, "L", "log-message TEXT", 1, \
      "log with TEXT") \
  OP (opt_setup, "S", "setup", 0, \
      "Use `archive-setup' if necessary; implied by default.") \
  OP (opt_no_setup, 0, "no-setup", 0, \
      "Do not use `archive-setup' even if necessary.") \
  OP (opt_unescaped, 0, "unescaped", 0, \
      "show filenames in unescaped form")

t_uchar arch_cmd_import_help[] = ("archive a full-source base-0 revision\n"
                                  "Archive a from-scratch base revision of the project tree\n"
                                  "containing DIR (or the current directory).  Use this command\n"
                                  "to create the first revision of a new project.\n"
				  "\n"
				  "If --log-message is specified without --summary, then TEXT is used both\n"
				  "as the summary and the first line of the log body.\n");

enum options
{
  OPTS (OPT_ENUM)
};

static struct opt_desc opts[] =
{
  OPTS (OPT_DESC)
    {-1, 0, 0, 0, 0}
};



int
arch_cmd_import (t_uchar * program_name, int argc, char * argv[])
{
  int o;
  struct opt_parsed * option;
  t_uchar * default_archive = 0;
  t_uchar * dir = 0;
  t_uchar * log_file = 0;
  t_uchar * log_text = 0;
  t_uchar * summary = 0;
  int setup = 1;
  int escape_classes = arch_escape_classes;

  dir = str_save (0, ".");

  safe_buffer_fd (1, 0, O_WRONLY, 0);

  option = 0;

  while (1)
    {
      o = opt_standard (lim_use_must_malloc, &option, opts, &argc, argv, program_name, usage, version_string, arch_cmd_import_help, opt_help_msg, opt_long_help, opt_version);
      if (o == opt_none)
        break;
      switch (o)
        {
        default:
          safe_printfmt (2, "unhandled option `%s'\n", option->opt_string);
          panic ("internal error parsing arguments");

        usage_error:
          opt_usage (2, argv[0], program_name, usage, 1);
          exit (1);

          /* bogus_arg: */
          safe_printfmt (2, "ill-formed argument for `%s' (`%s')\n", option->opt_string, option->arg_string);
          goto usage_error;

        case opt_archive:
          {
            default_archive = str_save (0, option->arg_string);
            break;
          }

        case opt_dir:
          {
            dir = str_save (0, option->arg_string);
            break;
          }

        case opt_log:
          {
            log_file = str_save (0, option->arg_string);
            break;
          }

        case opt_log_msg:
          {
            lim_free (0, log_text);
            log_text = str_save (0, option->arg_string);
            lim_free (0, log_file);
            log_file = 0;
            break;
          }

        case opt_summary:
          {
            lim_free (0, summary);
            summary = str_save (0, option->arg_string);
            lim_free (0, log_file);
            log_file = 0;
            break;
          }

	    case opt_setup:
	      {
	        setup = 1;
	        break;
	      }

        case opt_no_setup:
          {
            setup = 0;
            break;
          }

	case opt_unescaped:
	  {
	    escape_classes = 0;
	    break;
	  }
        }
    }

  if (argc > 2)
    goto usage_error;

  {
    t_uchar * log = 0;
    t_uchar * vsnspec;
    t_uchar * tree_root = 0;
    t_uchar * archive = 0;
    t_uchar * version = 0;
    struct arch_archive * arch = 0;

    if (default_archive)
      {
        if (!arch_valid_archive_name (default_archive))
          {
            safe_printfmt (2, "%s: invalid archive name (%s)\n",
                           argv[0], default_archive);
            exit (1);
          }
      }

    tree_root = arch_tree_root (0, dir, 1);

    if (!tree_root)
      {
        safe_printfmt (2, "%s: not in project tree (%s)\n",
                       argv[0], dir);
        exit (1);
      }

    if (argc == 2)
      vsnspec = argv[1];
    else
      {
        vsnspec = arch_tree_version (tree_root);
        if (!vsnspec)
          {
            safe_printfmt (2, "%s: project tree has no default version\n  tree: %s\n",
                           argv[0], tree_root);
            exit (1);
          }
      }

    if (!arch_valid_package_name (vsnspec, arch_maybe_archive, arch_req_version, 0))
      {
        safe_printfmt (2, "%s: invalid version name -- %s\n",
                       argv[0], vsnspec);
        exit (1);
      }

    archive = arch_parse_package_name (arch_ret_archive, default_archive, vsnspec);
    version = arch_parse_package_name (arch_ret_non_archive, 0, vsnspec);

    if (arch_is_system_package_name (version))
      {
        safe_printfmt (2, "%s: user's can not import into system versions\n  version: %s\n", argv[0], version);
        exit (2);
      }

    if (log_text || summary)
      {
        if (! summary)
          summary = log_text;
        log = arch_auto_log_message (tree_root, archive, version,
                                     summary, log_text);
      }
    else
       {
         if (!log_file)
           {
             log_file = arch_make_log_file (tree_root, archive, version);
           }
         else
           {
             if (safe_access (log_file, F_OK))
               {
                 safe_printfmt (2, "%s: specified log file not found (%s)\n", argv[0], log_file);
                 exit (1);
               }
           }

         if (!safe_access (log_file, F_OK))
           {
             log = file_contents (log_file);
             if (!arch_valid_log_file (log))
               {
                 safe_printfmt (2, "%s: invalid log file (%s)\n", argv[0], log_file);
                 exit (1);
               }
           }
         else
           {
             lim_free (0, log_file);
             log_file = 0;
           }
       }

    if (!arch_has_patch_log (tree_root, archive, version))
      {
        safe_printfmt (2, "%s: tree has no patch log for version\n    tree: %s\n    version: %s/%s\n",
                       argv[0], tree_root, archive, version);
        exit (1);
      }

    {
      rel_table log_entries = rel_table_nil;

      log_entries = arch_logs (tree_root, archive, version, 0);

      if (rel_n_records (log_entries))
        {
          safe_printfmt (2, "%s: tree already has patch log entries for version\n    tree: %s\n    version: %s/%s\n",
                         argv[0], tree_root, archive, version);
          exit (1);
        }
    }


    {
      struct arch_tree_lint_result * lint = 0;
      int status;

      lint = arch_tree_lint (tree_root);
      status = arch_print_tree_lint_report (2, lint, escape_classes);

      if (status < 0)
        {
          safe_printfmt (2, "%s: import aborted\n", argv[0]);
          exit (1);
        }
    }

    if (setup)
      arch_setup_archive_simple (1, archive, version);

    arch = arch_archive_connect (archive, 0);
    arch_check_for (arch, arch_req_version, version);
    arch_import (arch, version, tree_root,  log);

    safe_printfmt (1, "* imported %s/%s\n", archive, version);

    if (log_file)
      safe_unlink (log_file);

    lim_free(0, log_file);
    lim_free (0, log);
    lim_free (0, tree_root);
    lim_free (0, archive);
    lim_free (0, version);
  }

  lim_free (0, default_archive);
  lim_free (0, dir);

  return 0;
}




/* tag: Tom Lord Sun May 25 13:15:21 2003 (revimport.c)
 */
