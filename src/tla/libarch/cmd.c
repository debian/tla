/* cmd.c:
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */


#include "hackerlab/os/stdarg.h"
#include "hackerlab/arrays/ar.h"
#include "tla/libarch/cmd.h"



int
arch_call_cmd (arch_cmd_fn fn, t_uchar * prog_name, ...)
{
  char ** argv = 0;
  int status = 1;
  va_list ap;

  *(char **)ar_push ((void **)&argv, 0, sizeof (char *)) = prog_name;

  va_start (ap, prog_name);
  while (1)
    {
      char * arg;

      arg = va_arg (ap, char *);
      if (!arg)
        break;

      *(char **)ar_push ((void **)&argv, 0, sizeof (char *)) = arg;
    }
  va_end (ap);

  *(char **)ar_push ((void **)&argv, 0, sizeof (char *)) = 0;

  status = fn (prog_name, (ar_size ((void *)argv, 0, sizeof (char *)) - 1), argv);

  ar_free ((void **)&argv, 0);

  return status;
}




/* tag: Tom Lord Mon Jun  2 17:26:01 2003 (cmd.c)
 */
