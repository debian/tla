/* cmd-archive-meta-info.h:
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */

#ifndef INCLUDE__LIBARCH__CMD_ARCHIVE_META_INFO_H
#define INCLUDE__LIBARCH__CMD_ARCHIVE_META_INFO_H


#include "hackerlab/machine/types.h"



extern t_uchar arch_cmd_archive_meta_info_help[];


/* automatically generated __STDC__ prototypes */
extern int arch_cmd_archive_meta_info (t_uchar *program_name, int argc, char * argv[]);
#endif  /* INCLUDE__LIBARCH__CMD_ARCHIVE_META_INFO_H */


/* tag: Tom Lord Sat Jan  5 15:26:10 2002 (cmd-archive-meta-info.h)
 */
