/* changeset-utils.h:
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */

#ifndef INCLUDE__LIBARCH__CHANGESET_UTILS_H
#define INCLUDE__LIBARCH__CHANGESET_UTILS_H


#include "tla/libawk/relational.h"
#include "tla/libarch/inv-ids.h"



struct arch_changeset_inventory
{
  /* [0] == loc;  [1] == id   (sort by 1)
   */
  rel_table dirs;
  rel_table files;

  enum arch_id_tagging_method method;
};



/* automatically generated __STDC__ prototypes */
extern void arch_changeset_inventory (struct arch_changeset_inventory * inv_out,
                                      const t_uchar * tree_root,
                                      const t_uchar * path,
                                      enum arch_id_tagging_method method,
                                      enum arch_inventory_category untagged_source_category,
                                      int escape_classes);
extern void arch_free_changeset_inventory_data (struct arch_changeset_inventory * i);
extern rel_table arch_read_changeset_index (const t_uchar * path);
extern rel_table arch_read_changeset_dir_metadata (const t_uchar * path);
extern mode_t arch_read_permissions_patch (long * uid, long * gid, const t_uchar * file);
extern mode_t arch_parse_permissions_params (long * uid, long * gid, const t_uchar * const line);
#endif  /* INCLUDE__LIBARCH__CHANGESET_UTILS_H */


/* tag: Tom Lord Sat Jan  5 15:26:10 2002 (changeset-utils.h)
 */
