/* merge-points.h:
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */

#ifndef INCLUDE__LIBARCH__MERGE_POINTS_H
#define INCLUDE__LIBARCH__MERGE_POINTS_H


#include "tla/libawk/relational.h"
#include "tla/libarch/archive.h"


/* automatically generated __STDC__ prototypes */
extern rel_table arch_tree_merge_points (const t_uchar * tree_root,
                                         const t_uchar * into_archive,
                                         const t_uchar * into_spec,
                                         const t_uchar * from_archive,
                                         const t_uchar * from_spec);
extern rel_table arch_archive_merge_points (struct arch_archive * into_arch,
                                            const t_uchar * into_spec,
                                            const t_uchar * from_archive,
                                            const t_uchar * from_spec,
                                            int upto);
extern rel_table arch_new_in_version (const t_uchar * tree_root,
                                      const t_uchar * archive,
                                      const t_uchar * version);
#endif  /* INCLUDE__LIBARCH__MERGE_POINTS_H */


/* tag: Tom Lord Sat Jan  5 15:26:10 2002 (merge-points.h)
 */
