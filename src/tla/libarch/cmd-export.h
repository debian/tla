/* export.h:
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord
 *               2005 Canonical Ltd
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */

#ifndef INCLUDE__LIBARCH__CMD_EXPORT_H
#define INCLUDE__LIBARCH__CMD_EXPORT_H


#include "hackerlab/machine/types.h"
#include "tla/libawk/relational.h"



extern t_uchar arch_cmd_export_help[];


/* automatically generated __STDC__ prototypes */
extern int arch_cmd_export (t_uchar * program_name, int argc, char * argv[]);
extern int arch_is_in_control_dir(t_uchar * path);
extern rel_table arch_filter_control_files (rel_table index);
rel_table arch_filter_to_subdir (rel_table index, t_uchar * prefix);
#endif  /* INCLUDE__LIBARCH__CMD_EXPORT_H */
