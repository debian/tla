/* tag.h:
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */

#ifndef INCLUDE__LIBARCH__TAG_H
#define INCLUDE__LIBARCH__TAG_H



/* automatically generated __STDC__ prototypes */
extern void arch_tag (int chatter_fd,
                      struct arch_archive * arch,
                      t_uchar * revision,
                      struct arch_archive * from_arch,
                      t_uchar * from_revision,
                      t_uchar * raw_log);
#endif  /* INCLUDE__LIBARCH__TAG_H */


/* tag: Tom Lord Sat Jan  5 15:26:10 2002 (tag.h)
 */
