/* replay.h:
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */

#ifndef INCLUDE__LIBARCH__REPLAY_H
#define INCLUDE__LIBARCH__REPLAY_H


#include "tla/libarch/archive.h"


/* automatically generated __STDC__ prototypes */
extern int arch_replay_exact (int chatter_fd,
                              int no_patch_noise,
                              const t_uchar * tree_root,
                              struct arch_archive * arch,
                              const t_uchar * revision,
                              int reverse,
                              int forward,
                              int escape_classes);
extern int arch_replay_list (int chatter_fd,
                             int no_patch_noise,
                             const t_uchar * tree_root,
                             struct arch_archive * arch,
                             rel_table revisions,
                             int reverse,
                             int forward,
                             int escape_classes);
extern int arch_replay_fqlist (int chatter_fd,
                               int no_patch_noise,
                               const t_uchar * tree_root,
                               const t_uchar * default_archive,
                               rel_table revisions,
                               int reverse,
                               int forward,
                               int escape_classes);
#endif  /* INCLUDE__LIBARCH__REPLAY_H */


/* tag: Tom Lord Sat Jan  5 15:26:10 2002 (replay.h)
 */
