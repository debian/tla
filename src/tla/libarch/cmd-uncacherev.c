/* cmd-uncacherev.c
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */


#include "config-options.h"
#include "hackerlab/cmd/main.h"
#include "hackerlab/fs/file-names.h"
#include "hackerlab/fs/cwd.h"
#include "tla/libfsutils/tmp-files.h"
#include "tla/libfsutils/rmrf.h"
#include "tla/libarch/namespace.h"
#include "tla/libarch/project-tree.h"
#include "tla/libarch/my.h"
#include "tla/libarch/archive.h"
#include "tla/libarch/pristines.h"
#include "tla/libarch/build-revision.h"
#include "tla/libarch/cmd-uncacherev.h"



static t_uchar * usage = "[options] revision [dir]";
static t_uchar * version_string = (cfg__std__package " from regexps.com\n"
                                   "\n"
                                   "Copyright 2003 Tom Lord\n"
                                   "\n"
                                   "This is free software; see the source for copying conditions.\n"
                                   "There is NO warranty; not even for MERCHANTABILITY or FITNESS FOR A\n"
                                   "PARTICULAR PURPOSE.\n"
                                   "\n"
                                   "Report bugs to " cfg__tla_bug_mail ".\n"
                                   "\n"
                                   cfg__std__release_id_string
                                   "\n");

#define OPTS(OP) \
  OP (opt_help_msg, "h", "help", 0, \
      "Display a help message and exit.") \
  OP (opt_long_help, "H", 0, 0, \
      "Display a verbose help message and exit.") \
  OP (opt_version, "V", "version", 0, \
      "Display a release identifier string\n" \
      "and exit.") \
  OP (opt_archive, "A", "archive", 1, \
      "Override `my-default-archive'")


t_uchar arch_cmd_uncacherev_help[] = ("remove a cached full source tree from an archive\n"
                                      "Remove the cached form of REVISION from its archive.\n"
                                      "\n"
                                      "If REVISION is not specified, but the command is run from within\n"
                                      "a project tree, uncache the latest revision in the default\n"
                                      "version of that tree.\n"
                                      "\n"
                                      "Also see \"tla cacherev -H\".\n");

enum options
{
  OPTS (OPT_ENUM)
};

static struct opt_desc opts[] =
{
  OPTS (OPT_DESC)
    {-1, 0, 0, 0, 0}
};



int
arch_cmd_uncacherev (t_uchar * program_name, int argc, char * argv[])
{
  int o;
  struct opt_parsed * option;
  t_uchar * default_archive = 0;

  safe_buffer_fd (1, 0, O_WRONLY, 0);

  option = 0;

  while (1)
    {
      o = opt_standard (lim_use_must_malloc, &option, opts, &argc, argv, program_name, usage, version_string, arch_cmd_uncacherev_help, opt_help_msg, opt_long_help, opt_version);
      if (o == opt_none)
        break;
      switch (o)
        {
        default:
          safe_printfmt (2, "unhandled option `%s'\n", option->opt_string);
          panic ("internal error parsing arguments");

        usage_error:
          opt_usage (2, argv[0], program_name, usage, 1);
          exit (1);

          /* bogus_arg: */
          safe_printfmt (2, "ill-formed argument for `%s' (`%s')\n", option->opt_string, option->arg_string);
          goto usage_error;

        case opt_archive:
          {
            lim_free (0, default_archive);
            default_archive = str_save (0, option->arg_string);
            break;
          }
        }
    }

  if (argc != 2)
    goto usage_error;

  default_archive = arch_my_default_archive (default_archive);

  {
    t_uchar * revision_spec;
    t_uchar * archive = 0;
    t_uchar * revision = 0;
    struct arch_archive * arch = 0;

    revision_spec = argv[1];

    if (!arch_valid_package_name (revision_spec, arch_maybe_archive, arch_req_patch_level, 1))
      {
        safe_printfmt (2, "%s: invalid revision spec (%s)\n",
                       argv[0], revision_spec);
        exit (2);
      }

    archive = arch_parse_package_name (arch_ret_archive, default_archive, revision_spec);
    revision = arch_parse_package_name (arch_ret_non_archive, 0, revision_spec);
    arch = arch_archive_connect (archive, 0);

    {
      t_uchar * errstr;

      if (arch_archive_delete_cached (&errstr, arch, revision))
        {
          safe_printfmt (2, "%s: unable to delete cached revision of %s/%s (%s)\n",
                         argv[0], archive, revision, errstr);
          exit (1);
        }
    }

    arch_archive_close (arch);

    lim_free (0, archive);
    lim_free (0, revision);
  }

  lim_free (0, default_archive);
  return 0;
}




/* tag: Tom Lord Thu May 29 23:16:23 2003 (uncacherev.c)
 */
