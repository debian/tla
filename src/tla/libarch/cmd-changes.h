/* cmd-changes.h:
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord, Stig Brautaset
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */

#ifndef INCLUDE__LIBARCH__CMD_WHAT_CHANGED_H
#define INCLUDE__LIBARCH__CMD_WHAT_CHANGED_H


#include "hackerlab/machine/types.h"



extern t_uchar arch_cmd_changes_help[];


/* automatically generated __STDC__ prototypes */
extern int arch_cmd_changes (t_uchar * program_name, int argc, char * argv[]);
#endif  /* INCLUDE__LIBARCH__CMD_WHAT_CHANGED_H */


/* tag: Stig Brautaset Sat Jun  7 17:54:58 BST 2003 (cmd-what-changed.h)
 */
