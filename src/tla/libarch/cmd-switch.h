/* cmd-switch.h:
 *
 * vim:smartindent ts=8:sts=2:sta:et:ai:shiftwidth=2
 ****************************************************************
 * Copyright (C) 2004 Canonical Ltd
 *               Author: Robert Collins
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */

#ifndef INCLUDE__LIBARCH__CMD_SWITCH_H
#define INCLUDE__LIBARCH__CMD_SWITCH_H


#include "hackerlab/machine/types.h"



extern t_uchar arch_cmd_switch_help[];


extern int arch_cmd_switch (t_uchar * program_name, int argc, char * argv[]);
#endif  /* INCLUDE__LIBARCH__CMD_SWITCH_H */

/* tag: Tom Lord Fri May 23 14:06:15 2003 (cmd-switch.h)
 */
