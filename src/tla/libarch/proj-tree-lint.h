/* proj-tree-lint.h:
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */

#ifndef INCLUDE__LIBARCH__TREE_LINT_H
#define INCLUDE__LIBARCH__TREE_LINT_H


#include "tla/libawk/relational.h"
#include "tla/libawk/associative.h"
#include "tla/libarch/inv-ids.h"



enum arch_lint_category
{
  symlinks_sans_targets = 1,
  unrecognized_files = 2,
  untagged_files = 4,
  ids_sans_files = 8,
  duplicate_id_groups = 16,
};

struct arch_tree_lint_result
{
  enum arch_id_tagging_method id_tagging_method;
  rel_table unrecognized_files;
  rel_table symlinks_sans_targets;
  rel_table untagged_files;
  rel_table ids_sans_files;
  rel_table * duplicate_id_groups;

  rel_table warning_files;      /* things which aren't source but have source-like names */
};



/* automatically generated __STDC__ prototypes */
extern int arch_print_tree_lint_report (int fd, struct arch_tree_lint_result * lint, int escape_classes);
extern int arch_print_filtered_tree_lint_report (int fd,
                                                 struct arch_tree_lint_result * lint,
                                                 t_uint categories,
                                                 int escape_classes);
extern struct arch_tree_lint_result * arch_tree_lint (t_uchar * dirspec);
extern void arch_free_lint_result (struct arch_tree_lint_result * result);
#endif  /* INCLUDE__LIBARCH__TREE_LINT_H */


/* tag: Tom Lord Sat Jan  5 15:26:10 2002 (proj-tree-lint.h)
 */
