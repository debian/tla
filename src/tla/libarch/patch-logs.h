/* patch-logs.h:
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */

#ifndef INCLUDE__LIBARCH__PATCH_LOGS_H
#define INCLUDE__LIBARCH__PATCH_LOGS_H


#include "tla/libawk/relational.h"
#include "tla/libawk/associative.h"




enum arch_log_headers_summary
{
  arch_include_date = 1,
  arch_include_creator = 2,
  arch_include_summary = 4,
  arch_include_foreign_merges = 8,
  arch_include_local_merges = 16,
};



/* automatically generated __STDC__ prototypes */
extern t_uchar * arch_make_log_file (const t_uchar * tree_root,
                                     const t_uchar * archive,
                                     const t_uchar * version);
extern t_uchar * arch_make_log (const t_uchar * tree_root,
                                const t_uchar * archive,
                                const t_uchar * version);
extern t_uchar * arch_auto_log_message (const t_uchar * tree_root,
                                        const t_uchar * archive,
                                        const t_uchar * version,
                                        const t_uchar * summary,
                                        const t_uchar * body);
extern t_uchar * arch_log_for_merge (const t_uchar * tree_root,
                                     const t_uchar * archive,
                                     const t_uchar * version,
                                     int reverse);
extern t_uchar * arch_log_dir (const t_uchar * tree_root,
                               const t_uchar * archive,
                               const t_uchar * version);
extern t_uchar * arch_log_file (const t_uchar * tree_root,
                                const t_uchar * archive,
                                const t_uchar * revision);
extern t_uchar * arch_log_contents (const t_uchar * tree_root,
                                    const t_uchar * archive,
                                    const t_uchar * revision);
extern int arch_tree_has_log (const t_uchar * tree_root,
                              const t_uchar * archive,
                              const t_uchar * revision);
extern void arch_add_log_version (const t_uchar * tree_root,
                                  const t_uchar * archive,
                                  const t_uchar * version);
extern void arch_remove_log_version (const t_uchar * tree_root,
                                     const t_uchar * archive,
                                     const t_uchar * version);
extern int arch_has_patch_log (const t_uchar * tree_root,
                               const t_uchar * archive,
                               const t_uchar * version);
extern rel_table arch_log_versions (const t_uchar * tree_root,
                                    const t_uchar * only_archive,
                                    const t_uchar * only_category_param,
                                    const t_uchar * only_branch_param,
                                    const t_uchar * only_version_param);
extern t_uchar * arch_highest_patch_level (const t_uchar * tree_root,
                                           const t_uchar * archive,
                                           const t_uchar * version);
extern t_uchar * arch_latest_logged_version (const t_uchar * tree_root,
                                             const t_uchar * archive,
                                             const t_uchar * package);
extern rel_table arch_logs (const t_uchar * tree_root,
                            const t_uchar * archive,
                            const t_uchar * version,
                            int full);
extern int arch_valid_log_file (const t_uchar * log);
extern void arch_parse_log (rel_table * headers_list,
                            assoc_table * headers,
                            const t_uchar ** body,
                            const t_uchar * log);
extern void arch_print_headers_summary (int out_fd,
                                        int indent_level,
                                        assoc_table headers,
                                        int summarized_headers);
extern void arch_print_log_list_header (int out_fd,
                                        const t_uchar * header,
                                        rel_table list,
                                        int field);
extern void arch_print_log_pairs_header (int out_fd,
                                         const t_uchar * header,
                                         rel_table list,
                                         int field_a,
                                         int field_b);
extern void arch_copy_to_patch_log (const t_uchar * tree_root,
                                    const t_uchar * archive,
                                    const t_uchar * revision,
                                    const t_uchar * log_file);
extern void arch_rename_to_patch_log (const t_uchar * tree_root,
                                      const t_uchar * archive,
                                      const t_uchar * revision,
                                      const t_uchar * log_file);
extern rel_table arch_all_logs (const t_uchar * tree_root);

extern t_uchar * 
arch_tree_latest_revision (t_uchar * base_tree_root);


#endif  /* INCLUDE__LIBARCH__PATCH_LOGS_H */


/* tag: Tom Lord Sat Jan  5 15:26:10 2002 (patch-logs.h)
 */
