/* cmd-escape.h:
 *
 ****************************************************************
 * Copyright (C) 2004 Christian Thaeter
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */

#ifndef INCLUDE__LIBARCH__CMD_ESCAPE_H
#define INCLUDE__LIBARCH__CMD_ESCAPE_H


#include "hackerlab/char/pika-escaping-utils.h"


extern t_uchar arch_cmd_escape_help[];


/* automatically generated __STDC__ prototypes */
extern int arch_cmd_escape (t_uchar * program_name, int argc, char * argv[]);
#endif  /* INCLUDE__LIBARCH__CMD_ID_H */


/* tag: Christian Thaeter Sun Mar  7 04:45:11 CET 2004 (cmd-escape.h)
 */
