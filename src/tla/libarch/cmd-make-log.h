/* cmd-make-log.h:
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord, Stig Brautaset
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */

#ifndef INCLUDE__LIBARCH__CMD_MAKE_LOG_H
#define INCLUDE__LIBARCH__CMD_MAKE_LOG_H


#include "hackerlab/machine/types.h"



extern t_uchar arch_cmd_make_log_help[];


/* automatically generated __STDC__ prototypes */
extern int arch_cmd_make_log (t_uchar * program_name, int argc, char * argv[]);
#endif  /* INCLUDE__LIBARCH__CMD_MAKE_LOG_H */


/* tag: Stig Brautaset Sat Jun  7 15:59:28 BST 2003 (cmd-make-log.h)
 */
