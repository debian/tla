/* cmd-log-versions.c
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */


#include "config-options.h"
#include "hackerlab/cmd/main.h"
#include "tla/libarch/namespace.h"
#include "tla/libarch/project-tree.h"
#include "tla/libarch/patch-logs.h"
#include "tla/libarch/cmd-log-versions.h"



static t_uchar * usage = "[options]";
static t_uchar * version_string = (cfg__std__package " from regexps.com\n"
                                   "\n"
                                   "Copyright 2003 Tom Lord\n"
                                   "\n"
                                   "This is free software; see the source for copying conditions.\n"
                                   "There is NO warranty; not even for MERCHANTABILITY or FITNESS FOR A\n"
                                   "PARTICULAR PURPOSE.\n"
                                   "\n"
                                   "Report bugs to " cfg__tla_bug_mail ".\n"
                                   "\n"
                                   cfg__std__release_id_string
                                   "\n");

#define OPTS(OP) \
  OP (opt_help_msg, "h", "help", 0, \
      "Display a help message and exit.") \
  OP (opt_long_help, "H", 0, 0, \
      "Display a verbose help message and exit.") \
  OP (opt_version, "V", "version", 0, \
      "Display a release identifier string\n" \
      "and exit.") \
  OP (opt_dir, "d", "dir DIR", 1, \
      "cd to DIR first") \
  OP (opt_only_archive, "a", "archive ARCHIVE", 1, \
      "list only logs for ARCHIVE") \
  OP (opt_only_category, "c", "category CATEGORY", 1, \
      "list only logs for CATEGORY") \
  OP (opt_only_branch, "b", "branch BRANCH", 1, \
      "list only logs for BRANCH") \
  OP (opt_only_version, "v", "vsn VERSION", 1, \
      "list only logs for VERSION") \
  OP (opt_reverse, "r", "reverse", 0, \
      "reverse the sort order")

t_uchar arch_cmd_log_versions_help[] = ("list patch log versions in a project tree\n"
                                "Print the list of versions for which there are patch logs in\n"
                                "the project tree containing DIR (or the current directory).\n");

enum options
{
  OPTS (OPT_ENUM)
};

static struct opt_desc opts[] =
{
  OPTS (OPT_DESC)
    {-1, 0, 0, 0, 0}
};



static int cut_field_0[] = {0, -1};

int
arch_cmd_log_versions (t_uchar * program_name, int argc, char * argv[])
{
  int o;
  struct opt_parsed * option;
  int reverse;
  char * dir;
  t_uchar * tree_root;
  char * only_archive;
  char * only_category;
  char * only_branch;
  char * only_version;
  rel_table log_versions;

  reverse = 0;
  dir = ".";
  only_archive = 0;
  only_category = 0;
  only_branch = 0;
  only_version = 0;

  safe_buffer_fd (1, 0, O_WRONLY, 0);

  option = 0;

  while (1)
    {
      o = opt_standard (lim_use_must_malloc, &option, opts, &argc, argv, program_name, usage, version_string, arch_cmd_log_versions_help, opt_help_msg, opt_long_help, opt_version);
      if (o == opt_none)
        break;
      switch (o)
        {
        default:
          safe_printfmt (2, "unhandled option `%s'\n", option->opt_string);
          panic ("internal error parsing arguments");

        usage_error:
          opt_usage (2, argv[0], program_name, usage, 1);
          exit (1);

          /* bogus_arg: */
          safe_printfmt (2, "ill-formed argument for `%s' (`%s')\n", option->opt_string, option->arg_string);
          goto usage_error;


        case opt_reverse:
          {
            reverse = 1;
            break;
          }

        case opt_dir:
          {
            dir = str_save (0, option->arg_string);
            break;
          }

        case opt_only_archive:
          {
            only_archive = str_save (0, option->arg_string);
            break;
          }
        case opt_only_category:
          {
            only_category = str_save (0, option->arg_string);
            break;
          }
        case opt_only_branch:
          {
            only_branch = str_save (0, option->arg_string);
            break;
          }
        case opt_only_version:
          {
            only_version = str_save (0, option->arg_string);
            break;
          }
        }
    }

  if (argc != 1)
    goto usage_error;

  tree_root = arch_tree_root (0, dir, 0);

  if (!tree_root)
    {
      safe_printfmt (2, "%s: directory not in project tree (%s)\n",
                     argv[0], dir);
      exit (1);
    }

  log_versions = arch_log_versions (tree_root, only_archive, only_category, only_branch, only_version);
  log_versions = rel_cut (cut_field_0, log_versions);
  arch_sort_table_by_name_field (reverse, log_versions, 0);
  rel_print_table (1, log_versions);

  return 0;
}




/* tag: Tom Lord Tue May 13 09:59:25 2003 (logs.c)
 */
