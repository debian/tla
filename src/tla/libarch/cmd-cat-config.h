/* cmd-cat-config.h:
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord, Stig Brautaset
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */

#ifndef INCLUDE__LIBARCH__CMD_CAT_CONFIG_H
#define INCLUDE__LIBARCH__CMD_CAT_CONFIG_H


#include "hackerlab/machine/types.h"



extern t_uchar arch_cmd_cat_config_help[];


/* automatically generated __STDC__ prototypes */
extern int arch_cmd_cat_config (t_uchar * program_name, int argc, char * argv[]);
#endif  /* INCLUDE__LIBARCH__CMD_CAT_CONFIG_H */


/* tag: Stig Brautaset Sat Jun  7 13:38:07 BST 2003 (cmd-cfgcat.h)
 */
