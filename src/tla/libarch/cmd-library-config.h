/* cmd-library-config.h:
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord
 * 
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */

#ifndef INCLUDE__LIBARCH__CMD_LIBRARY_CONFIG_H
#define INCLUDE__LIBARCH__CMD_LIBRARY_CONFIG_H


extern t_uchar arch_cmd_library_config_help[];


/* automatically generated __STDC__ prototypes */
extern int arch_cmd_library_config (t_uchar * program_name, int argc, char * argv[]);
#endif  /* INCLUDE__LIBARCH__CMD_LIBRARY_CONFIG_H */


/* tag: Tom Lord Fri Dec  5 16:35:19 2003 (cmd-library-config.h)
 */
