/* cmd-id-tagging-method.c
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */


#include "config-options.h"
#include "hackerlab/cmd/main.h"
#include "tla/libarch/inv-ids.h"
#include "tla/libarch/project-tree.h"
#include "tla/libarch/cmd-id-tagging-method.h"



static t_uchar * usage = "[options] [method]";
static t_uchar * version_string = (cfg__std__package " from regexps.com\n"
                                   "\n"
                                   "Copyright 2003 Tom Lord\n"
                                   "\n"
                                   "This is free software; see the source for copying conditions.\n"
                                   "There is NO warranty; not even for MERCHANTABILITY or FITNESS FOR A\n"
                                   "PARTICULAR PURPOSE.\n"
                                   "\n"
                                   "Report bugs to " cfg__tla_bug_mail ".\n"
                                   "\n"
                                   cfg__std__release_id_string
                                   "\n");

#define OPTS(OP) \
  OP (opt_help_msg, "h", "help", 0, \
      "Display a help message and exit.") \
  OP (opt_long_help, "H", 0, 0, \
      "Display a verbose help message and exit.") \
  OP (opt_version, "V", "version", 0, \
      "Display a release identifier string\n" \
      "and exit.") \
  OP (opt_dir, "d", "dir DIR", 1, \
      "cd to DIR first") \
  OP (opt_strict, 0, "strict", 0, \
      "exit with error if method not set")

t_uchar arch_cmd_id_tagging_method_help[] = ("print or change a project tree id tagging method\n"
                                          "Print or change the method by which source files are\n"
                                          "identified in DIR (or the current directory).\n"
                                          "\n"
                                          "When setting, METHOD must be one of:\n"
                                          "\n"
                                          "        names           -- use naming conventions only\n"
                                          "        implicit        -- use naming conventions but permit\n"
                                          "                           for inventory tags\n"
                                          "        explicit        -- require explicit designation of source\n"
                                          "        tagline         -- mix names, explicit and arch" "-tag: methods\n"
                                          "\n"
                                          "When printing, if --strict is provided but no id tagging\n"
                                          "method is explicitly set, print an error.\n");

enum options
{
  OPTS (OPT_ENUM)
};

static struct opt_desc opts[] =
{
  OPTS (OPT_DESC)
    {-1, 0, 0, 0, 0}
};



int
arch_cmd_id_tagging_method (t_uchar * program_name, int argc, char * argv[])
{
  int o;
  struct opt_parsed * option;
  char * dir;
  t_uchar * tree_root;
  int strict;

  dir = ".";
  tree_root = 0;
  strict = 0;

  safe_buffer_fd (1, 0, O_WRONLY, 0);

  option = 0;

  while (1)
    {
      o = opt_standard (lim_use_must_malloc, &option, opts, &argc, argv, program_name, usage, version_string, arch_cmd_id_tagging_method_help, opt_help_msg, opt_long_help, opt_version);
      if (o == opt_none)
        break;
      switch (o)
        {
        default:
          safe_printfmt (2, "unhandled option `%s'\n", option->opt_string);
          panic ("internal error parsing arguments");

        usage_error:
          opt_usage (2, argv[0], program_name, usage, 1);
          exit (1);

          /* bogus_arg: */
          safe_printfmt (2, "ill-formed argument for `%s' (`%s')\n", option->opt_string, option->arg_string);
          goto usage_error;

        case opt_dir:
          {
            dir = str_save (0, option->arg_string);
            break;
          }

        case opt_strict:
          {
            strict = 1;
            break;
          }
        }
    }

  if (argc > 2)
    goto usage_error;

  tree_root = arch_tree_root (0, dir, 0);

  if (!tree_root)
    {
      safe_printfmt (2, "%s: not in project tree (%s)\n", argv[0], dir);
      exit (1);
    }

  if (argc == 1)
    {
      enum arch_id_tagging_method method;
      t_uchar * method_name;

      method = arch_tree_id_tagging_method (0, tree_root, strict);
      method_name = arch_id_tagging_method_name (method);
      safe_printfmt (1, "%s\n", method_name);
      exit (0);
    }
  else
    {
      t_uchar * new_method_name;
      enum arch_id_tagging_method new_method;

      new_method_name = argv[1];
      new_method = arch_id_tagging_method_from_name (new_method_name);
      arch_set_tree_id_tagging_method (tree_root, new_method);
      safe_printfmt (1, "method set: %s\n", new_method_name);
      exit (0);
    }

  return 0;
}




/* tag: Tom Lord Wed May 14 09:16:36 2003 (tagging-method.c)
 */
