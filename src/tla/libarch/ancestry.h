/* ancestry.h:
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */

#ifndef INCLUDE__LIBARCH__ANCESTRY_H
#define INCLUDE__LIBARCH__ANCESTRY_H


#include "tla/libawk/relational.h"
#include "tla/libarch/archive.h"



/* automatically generated __STDC__ prototypes */
extern rel_table arch_trace_ancestry (struct arch_archive * arch, t_uchar * archive, t_uchar * revision, int merges);
#endif  /* INCLUDE__LIBARCH__ANCESTRY_H */


/* tag: Tom Lord Sat Jan  5 15:26:10 2002 (ancestry.h)
 */
