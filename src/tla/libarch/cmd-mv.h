/* cmd-mv.h:
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord, Rob Weir
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */

#ifndef INCLUDE__LIBARCH__CMD_MV_H
#define INCLUDE__LIBARCH__CMD_MV_H


#include "hackerlab/machine/types.h"



extern t_uchar arch_cmd_mv_help[];


/* automatically generated __STDC__ prototypes */
extern int arch_cmd_mv (t_uchar * program_name, int argc, char * argv[]);
#endif  /* INCLUDE__LIBARCH__CMD_MV_H */


/* tag: Rob Weir Sun Sep 21 15:22:29 EST 2003 (cmd-mv.h)
 */
