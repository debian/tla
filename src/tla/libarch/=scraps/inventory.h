/* inventory.h:
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */

#ifndef INCLUDE__LIBARCH__INVENTORY_H
#define INCLUDE__LIBARCH__INVENTORY_H


#include "hackerlab/os/sys/stat.h"
#include "hackerlab/machine/types.h"
#include "hackerlab/rx-posix/regex.h"



struct arch_traversal_file
{
  t_uchar * path;
  t_uchar * name;
  struct stat stat;
};


typedef int (*arch_traversal_callback) (struct arch_traversal_file *, void *);



enum arch_inventory_category
{
  arch_inventory_source = 1,
  arch_inventory_precious = 2,
  arch_inventory_backup = 4,
  arch_inventory_junk = 8,
  arch_inventory_tree = 16,
  arch_inventory_unrecognized = 32,
  arch_inventory_excludes = 64
};


struct arch_id_program
{
  int check_external_tags;
  int check_embedded_tags;
  int check_basename_embedded_tags;
  enum arch_inventory_category untagged_source_category;

  regex_t * regexp_list;
};

typedef struct arch_id_program arch_id_program;



/* automatically generated __STDC__ prototypes */
extern void arch_tree_traversal (t_uchar * root,
                                 arch_traversal_callback callback,
                                 void * closure);
extern void arch_id_program_compile_external (arch_id_program * prog);
extern void arch_id_program_compile_untagged_disposition (arch_id_program * prog,
                                                          enum arch_inventory_category category);
extern void arch_id_program_compile_embedded (arch_id_program * prog, t_uchar * regexp_source);
extern void arch_free_id_program_data (arch_id_program * prog);
#endif  /* INCLUDE__LIBARCH__INVENTORY_H */


/* tag: Tom Lord Sat Jan  5 15:26:10 2002 (inventory.h)
 */
