/* configs.h:
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */

#ifndef INCLUDE__LIBARCH__CONFIGS_H
#define INCLUDE__LIBARCH__CONFIGS_H


#include "tla/libawk/relational.h"



struct arch_build_config_params
{
  int no_pristines;
  int hardlinks;
  int library;
  int sparse;
  int release_id;
};


/* automatically generated __STDC__ prototypes */
extern t_uchar * arch_config_path (t_uchar * tree_root, t_uchar * config_name);
extern rel_table arch_read_config (t_uchar * tree_root, t_uchar * config_name);
extern rel_table arch_config_from_tree (t_uchar * tree_root, rel_table config_in);
extern int arch_begin_new_config (t_uchar * tree_root, t_uchar * name, int force);
extern void arch_finish_new_config (int fd, t_uchar * tree_root, t_uchar * name, int force);
extern void arch_build_config (t_uchar * tree_root,
                               t_uchar * config_name,
                               struct arch_build_config_params * params,
                               t_uchar * default_archive);
#endif  /* INCLUDE__LIBARCH__CONFIGS_H */


/* tag: Tom Lord Sat Jan  5 15:26:10 2002 (configs.h)
 */
