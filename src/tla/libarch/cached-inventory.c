/* cached-inventory.c:
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */


#include "hackerlab/fs/file-names.h"
#include "hackerlab/vu/safe.h"
#include "tla/libfsutils/rmrf.h"
#include "tla/libfsutils/tmp-files.h"
#include "tla/libarch/invent.h"
#include "tla/libarch/changeset-utils.h"
#include "tla/libarch/cached-inventory.h"



rel_table
arch_cached_index (t_uchar * tree_root)
{
  t_uchar * inventory_path = 0;
  rel_table answer = rel_table_nil;

  inventory_path = file_name_in_vicinity (0, tree_root, ",,index");

  if (!safe_access (inventory_path, F_OK))
    {
      answer = arch_read_changeset_index (inventory_path);
      rel_sort_table_by_field (0, answer, 1);
    }
  else
    {
      t_uchar * tmp = 0;
      int out_fd = -1;

      answer = arch_source_inventory (tree_root, 1, 0, 0);
      rel_sort_table_by_field (0, answer, 1);

      tmp = tmp_file_name (tree_root, ",,index");
      rmrf_file (tmp);

      out_fd = safe_open (tmp, O_WRONLY | O_CREAT | O_EXCL, 0444);
      rel_print_table (out_fd, answer);
      safe_close (out_fd);

      safe_rename (tmp, inventory_path);

      lim_free (0, tmp);
    }


  lim_free (0, inventory_path);
  return answer;
}



/* tag: Tom Lord Fri May 30 20:13:03 2003 (cached-inventory.c)
 */
