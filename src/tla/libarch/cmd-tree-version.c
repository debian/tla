/* cmd-tree-version.c
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */


#include "config-options.h"
#include "hackerlab/cmd/main.h"
#include "tla/libarch/namespace.h"
#include "tla/libarch/project-tree.h"
#include "tla/libarch/cmd-tree-version.h"



static t_uchar * usage = "[options] [dir]";
static t_uchar * version_string = (cfg__std__package " from regexps.com\n"
                                   "\n"
                                   "Copyright 2003 Tom Lord\n"
                                   "\n"
                                   "This is free software; see the source for copying conditions.\n"
                                   "There is NO warranty; not even for MERCHANTABILITY or FITNESS FOR A\n"
                                   "PARTICULAR PURPOSE.\n"
                                   "\n"
                                   "Report bugs to " cfg__tla_bug_mail ".\n"
                                   "\n"
                                   cfg__std__release_id_string
                                   "\n");

#define OPTS(OP) \
  OP (opt_help_msg, "h", "help", 0, \
      "Display a help message and exit.") \
  OP (opt_long_help, "H", 0, 0, \
      "Display a verbose help message and exit.") \
  OP (opt_version, "V", "version", 0, \
      "Display a release identifier string\n" \
      "and exit.")

t_uchar arch_cmd_tree_version_help[] = ("print the default version for a project tree\n"
                                        "Print the default version of project tree DIR (or\n"
                                        "the current directory).\n");

enum options
{
  OPTS (OPT_ENUM)
};

static struct opt_desc opts[] =
{
  OPTS (OPT_DESC)
    {-1, 0, 0, 0, 0}
};



int
arch_cmd_tree_version (t_uchar * program_name, int argc, char * argv[])
{
  int o;
  struct opt_parsed * option;
  char * dir;
  t_uchar * tree_root;
  t_uchar * answer;

  dir = ".";
  tree_root = 0;

  safe_buffer_fd (1, 0, O_WRONLY, 0);

  option = 0;

  while (1)
    {
      o = opt_standard (lim_use_must_malloc, &option, opts, &argc, argv, program_name, usage, version_string, arch_cmd_tree_version_help, opt_help_msg, opt_long_help, opt_version);
      if (o == opt_none)
        break;
      switch (o)
        {
        default:
          safe_printfmt (2, "unhandled option `%s'\n", option->opt_string);
          panic ("internal error parsing arguments");

        usage_error:
          opt_usage (2, argv[0], program_name, usage, 1);
          exit (1);

          /* bogus_arg: */
          safe_printfmt (2, "ill-formed argument for `%s' (`%s')\n", option->opt_string, option->arg_string);
          goto usage_error;
        }
    }

  if (argc > 2)
    goto usage_error;

  if (argc == 2)
    dir = argv[1];

  tree_root = arch_tree_root (0, dir, 0);

  if (!tree_root)
    {
      safe_printfmt (2, "%s: not in project tree (%s)\n", argv[0], dir);
      exit (1);
    }

  answer = arch_tree_version (tree_root);

  if (!answer)
    {
      safe_printfmt (2, "%s: no default version set\n  tree: %s\n",
                     argv[0], tree_root);
      exit (1);
    }

  invariant (arch_valid_package_name (answer, arch_req_archive, arch_req_version, 0));

  safe_printfmt (1, "%s\n", answer);

  return 0;
}



/* tag: Tom Lord Mon May 12 18:48:04 2003 (tree-version.c)
 */
