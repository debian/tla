/* archive.h:
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */

#ifndef INCLUDE__LIBARCH__ARCHIVE_H
#define INCLUDE__LIBARCH__ARCHIVE_H


#include "tla/libawk/relational.h"
#include "tla/libarch/hooks.h"
#include "tla/libarch/archive-version.h"




struct arch_archive_vtable;

enum arch_revision_type
{
  arch_import_revision,
  arch_simple_revision,
  arch_continuation_revision
};

enum arch_revision_lock_state
{
  arch_revision_unlocked,
  arch_revision_user_locked,
  arch_revision_txn_locked,
  arch_revision_unknown_lock_state,
  arch_revision_illegal_lock_state,
};


struct arch_archive
{
  struct arch_archive_vtable * vtable;

  int refs;
  struct arch_archive * next;
  
  t_uchar * name;
  t_uchar * official_name;
  t_uchar * location;
  t_uchar * version;
  enum arch_archive_access access;
  enum arch_archive_type type;
  t_uchar * mirror_of;
  t_uchar * client_anticipates_mirror;
  int http_blows;
  int signed_archive;
};

struct arch_archive_vtable
{
  t_uchar * type;

  void (*close) (struct arch_archive * a);

  t_uchar * (*archive_version) (struct arch_archive * a);

  rel_table (*categories) (struct arch_archive * a);
  rel_table (*branches) (struct arch_archive * a, const t_uchar * category);
  rel_table (*versions) (struct arch_archive * a, const t_uchar * package);
  rel_table (*revisions) (struct arch_archive * a, const t_uchar * version);

  t_uchar * (*archive_log) (struct arch_archive *, const t_uchar * revision);
  int (*revision_type) (enum arch_revision_type * type, int * is_cached,
                        struct arch_archive * a, const t_uchar * revision);
  void (*get_patch) (int out_fd, struct arch_archive * a, const t_uchar * revision);
  void (*get_cached) (int out_fd, struct arch_archive * a, const t_uchar * revision);
  void (*get_import) (int out_fd, struct arch_archive * a, const t_uchar * revision);
  t_uchar * (*get_continuation) (struct arch_archive * a, const t_uchar * revision);
  t_uchar * (*get_meta_info)  (struct arch_archive * a, const t_uchar * meta_info_name);

  int (*make_category) (t_uchar ** errstr, struct arch_archive * a, const t_uchar * category);
  int (*make_branch) (t_uchar ** errstr, struct arch_archive * a, const t_uchar * branch);
  int (*make_version) (t_uchar ** errstr, struct arch_archive * a, const t_uchar * version);

  int (*lock_revision) (t_uchar ** errstr, struct arch_archive * a,
                        const t_uchar * version,
                        const t_uchar * prev_level,
                        const t_uchar * uid,
                        const t_uchar * txn_id,
                        const t_uchar * new_level);


  int (*revision_ready) (t_uchar ** errstr, struct arch_archive *a,
                         const t_uchar * version,
                         const t_uchar * prev_level,
                         const t_uchar * uid, 
                         const t_uchar * txn_id,
                         const t_uchar * new_level);

  int (*finish_revision) (t_uchar ** errstr, struct arch_archive * a,
                          const t_uchar * version,
                          const t_uchar * prev_level,
                          const t_uchar * uid,
                          const t_uchar * txn_id,
                          const t_uchar * new_level);

  int (*break_revision_lock) (t_uchar ** errstr, struct arch_archive * a,
                              const t_uchar * version,
                              const t_uchar * prev_level,
                              const t_uchar * uid,
                              const t_uchar * txn_id);

  enum arch_revision_lock_state (*lock_state) (t_uchar ** prev_level_ret,
                                               t_uchar ** uid_ret,
                                               t_uchar ** txn_id_ret,
                                               struct arch_archive * a,
                                               const t_uchar * version);

  int (*put_log) (t_uchar ** errstr, struct arch_archive * a,
                  const t_uchar * version,
                  const t_uchar * prev_level,
                  const t_uchar * uid,
                  const t_uchar * txn_id,
                  const t_uchar * log_text);
  int (*put_continuation) (t_uchar ** errstr, struct arch_archive * a,
                           const t_uchar * version,
                           const t_uchar * prev_level,
                           const t_uchar * uid,
                           const t_uchar * txn_id,
                           const t_uchar * continuation);
  int (*put_changeset) (t_uchar ** errstr, struct arch_archive * a,
                        const t_uchar * version,
                        const t_uchar * prev_level,
                        const t_uchar * uid,
                        const t_uchar * txn_id,
                        const t_uchar * level,
                        int in_fd);
  int (*put_import) (t_uchar ** errstr, struct arch_archive * a,
                     const t_uchar * version,
                     const t_uchar * prev_level,
                     const t_uchar * uid,
                     const t_uchar * txn_id,
                     const t_uchar * level,
                     int in_fd);

  int (*put_cached) (t_uchar ** errstr, struct arch_archive * a,
                     const t_uchar * revision,
                     int in_fd);
  int (*delete_cached) (t_uchar ** errstr, struct arch_archive * a, const t_uchar * revision);

  void (*repair_non_txnal) (int chatter_fd, struct arch_archive * a);
};



/* automatically generated __STDC__ prototypes */
extern void arch_make_archive (const t_uchar * name,
                               const t_uchar * location,
                               const t_uchar * mirror_of,
                               int dot_listing_lossage,
                               int signed_archive,
                               int tla_archive);
extern struct arch_archive * arch_archive_connect_location (const t_uchar * name,
                                                            const t_uchar * location,
                                                            const t_uchar * want_mirror_of);
extern struct arch_archive * arch_archive_connect (const t_uchar * name,
                                                   const t_uchar * want_mirror_of);

extern void arch_archive_close (struct arch_archive * arch);
extern t_uchar * arch_archive_version (struct arch_archive * arch);
extern rel_table arch_archive_categories (struct arch_archive * arch);
extern rel_table arch_archive_branches (struct arch_archive * arch,
                                        const t_uchar * category);
extern rel_table arch_archive_versions (struct arch_archive * arch,
                                        const t_uchar * package);
extern t_uchar * arch_archive_latest_revision (struct arch_archive * arch,
                                               const t_uchar * version,
                                               int full);
extern rel_table arch_archive_revisions (struct arch_archive * arch,
                                         const t_uchar * version,
                                         int full);
extern t_uchar * arch_archive_log (struct arch_archive * arch,
                                   const t_uchar * revision);
extern void arch_revision_type (enum arch_revision_type * type,
                                int * is_cached,
                                struct arch_archive * arch,
                                const t_uchar * revision);
extern int arch_revision_exists (struct arch_archive * arch,
                                 const t_uchar * revision);
extern void arch_get_patch_targz (int out_fd,
                                  struct arch_archive * arch,
                                  const t_uchar * revision);
extern void arch_get_patch (struct arch_archive * arch,
                            const t_uchar * revision,
                            const t_uchar * dest_dir);
extern void arch_get_cached_revision_targz (int out_fd,
                                            struct arch_archive * arch,
                                            const t_uchar * revision);
extern void arch_get_cached_revision (struct arch_archive * arch,
                                      const t_uchar * revision,
                                      const t_uchar * dest_dir);
extern void arch_get_import_targz (int out_fd,
                                   struct arch_archive * arch,
                                   const t_uchar * revision);
extern void arch_get_import_revision (struct arch_archive * arch,
                                      const t_uchar * revision,
                                      const t_uchar * dest_dir);
extern t_uchar * arch_get_continuation (struct arch_archive * arch,
                                        const t_uchar * revision);
extern t_uchar * arch_get_meta_info (struct arch_archive * arch,
                                     const t_uchar * meta_info_name);
extern t_uchar * arch_get_meta_info_trimming (struct arch_archive * arch,
                                              const t_uchar * meta_info_name);
extern int arch_make_category (t_uchar ** errstr,
                               struct arch_archive * arch,
                               const t_uchar * category);
extern int arch_make_branch (t_uchar ** errstr,
                             struct arch_archive * arch,
                             const t_uchar * branch);
extern int arch_make_version (t_uchar ** errstr,
                              struct arch_archive * arch,
                              const t_uchar * version);
extern int arch_archive_lock_revision (t_uchar ** errstr,
                                       struct arch_archive * a,
                                       const t_uchar * version,
                                       const t_uchar * prev_level,
                                       const t_uchar * uid,
                                       const t_uchar * txn_id,
                                       const t_uchar * new_level);
extern int arch_revision_ready (t_uchar ** errstr,
                                struct arch_archive *a,
                                const t_uchar * version,
                                const t_uchar * prev_level,
                                const t_uchar * uid,
                                const t_uchar * txn_id,
                                const t_uchar * new_level);
extern int arch_archive_finish_revision (t_uchar ** errstr,
                                         struct arch_archive * a,
                                         const t_uchar * version,
                                         const t_uchar * prev_level,
                                         const t_uchar * uid,
                                         const t_uchar * txn_id,
                                         const t_uchar * new_level);
extern enum arch_revision_lock_state arch_archive_revision_lock_state (t_uchar ** prev_level_ret,
                                                                       t_uchar ** uid_ret,
                                                                       t_uchar ** txn_id_ret,
                                                                       struct arch_archive * a,
                                                                       const t_uchar * version);
extern int arch_archive_break_revision_lock (t_uchar ** errstr,
                                             struct arch_archive * a,
                                             const t_uchar * version,
                                             const t_uchar * prev_level,
                                             const t_uchar * uid,
                                             const t_uchar * txn_id);
extern int arch_archive_put_log (t_uchar ** errstr,
                                 struct arch_archive * a,
                                 const t_uchar * version,
                                 const t_uchar * prev_level,
                                 const t_uchar * uid,
                                 const t_uchar * txn_id,
                                 const t_uchar * log_text);
extern int arch_archive_put_continuation (t_uchar ** errstr,
                                          struct arch_archive * a,
                                          const t_uchar * version,
                                          const t_uchar * prev_level,
                                          const t_uchar * uid,
                                          const t_uchar * txn_id,
                                          const t_uchar * continuation);
extern int arch_archive_put_changeset_targz (t_uchar ** errstr,
                                             struct arch_archive * a,
                                             const t_uchar * version,
                                             const t_uchar * prev_level,
                                             const t_uchar * uid,
                                             const t_uchar * txn_id,
                                             const t_uchar * level,
                                             int in_fd);
extern int arch_archive_put_changeset (t_uchar ** errstr,
                                       struct arch_archive * a,
                                       const t_uchar * version,
                                       const t_uchar * prev_level,
                                       const t_uchar * uid,
                                       const t_uchar * txn_id,
                                       const t_uchar * level,
                                       const t_uchar * dir);
extern int arch_archive_put_import_targz (t_uchar ** errstr,
                                          struct arch_archive * a,
                                          const t_uchar * version,
                                          const t_uchar * prev_level,
                                          const t_uchar * uid,
                                          const t_uchar * txn_id,
                                          const t_uchar * level,
                                          int in_fd);
extern int arch_archive_put_import (t_uchar ** errstr,
                                    struct arch_archive * a,
                                    const t_uchar * version,
                                    const t_uchar * prev_level,
                                    const t_uchar * uid,
                                    const t_uchar * txn_id,
                                    const t_uchar * level,
                                    const t_uchar * dir);
extern int arch_archive_put_cached_targz (t_uchar ** errstr,
                                          struct arch_archive * a,
                                          const t_uchar * revision,
                                          int in_fd);
extern int arch_archive_put_cached (t_uchar ** errstr,
                                    struct arch_archive * a,
                                    const t_uchar * revision,
                                    const t_uchar * dir);
extern int arch_archive_delete_cached (t_uchar ** errstr,
                                       struct arch_archive * a,
                                       const t_uchar * revision);
extern void arch_archive_repair_non_txnal (int chatter_fd, struct arch_archive * a);
extern t_uchar * arch_generate_txn_id (void);
extern t_uchar * arch_previous_revision (struct arch_archive * arch,
                                         const t_uchar * revision);
extern t_uchar * arch_ancestor_revision (struct arch_archive * arch,
                                         const t_uchar * revision);
extern t_uchar * archive_tmp_file_name (const t_uchar * dir,
                                        const t_uchar * basename);
extern t_uchar * make_tmp_tar_archive (const t_uchar * dir);
extern int arch_get_meta_int_info(struct arch_archive * arch,
                                  const t_uchar * key);
extern t_uchar * arch_fs_archive_category_path (const struct arch_archive * arch,
                                                const t_uchar * archive_path,
                                                const t_uchar * category);
extern t_uchar * arch_fs_archive_branch_path (const struct arch_archive * arch,
                                              const t_uchar * archive_path,
                                              const t_uchar * package);
extern t_uchar * arch_fs_archive_version_path (const struct arch_archive * arch,
                                               const t_uchar * archive_path,
                                               const t_uchar * version);
extern t_uchar * arch_fs_archive_revision_path (const struct arch_archive * arch,
                                                const t_uchar * archive_path,
                                                const t_uchar * revision);
extern t_uchar * arch_fs_archive_revision_log_path (const struct arch_archive * arch,
                                                    const t_uchar * archive_path,
                                                    const t_uchar * revision);
extern t_uchar * arch_fs_archive_changeset_path (const struct arch_archive * arch,
                                                 const t_uchar * archive_path,
                                                 const t_uchar * revision);
extern t_uchar * arch_fs_archive_import_path (const struct arch_archive * arch,
                                              const t_uchar * archive_path,
                                              const t_uchar * revision);
extern t_uchar * arch_fs_archive_cached_path (const struct arch_archive * arch,
                                              const t_uchar * archive_path,
                                              const t_uchar * revision);
extern t_uchar * arch_fs_archive_cached_checksum_path (const struct arch_archive * arch,
                                                       const t_uchar * archive_path,
                                                       const t_uchar * revision);
extern t_uchar * arch_fs_archive_continuation_path (const struct arch_archive * arch,
                                                    const t_uchar * archive_path,
                                                    const t_uchar * revision);
extern t_uchar * arch_fs_archive_revision_lock_unlocked_path (const struct arch_archive * arch,
                                                              const t_uchar * archive_path,
                                                              const t_uchar * version,
                                                              const t_uchar * prev_level);
extern t_uchar * arch_fs_archive_revision_lock_locked_path (const struct arch_archive * arch,
                                                            const t_uchar * archive_path,
                                                            const t_uchar * version,
                                                            const t_uchar * prev_level,
                                                            const t_uchar * arch_user_id,
                                                            const t_uchar * txn_id);
extern t_uchar * arch_fs_archive_revision_lock_locked_contents_path (const struct arch_archive * arch,
                                                                     const t_uchar * archive_path,
                                                                     const t_uchar * version,
                                                                     const t_uchar * prev_level,
                                                                     const t_uchar * arch_user_id,
                                                                     const t_uchar * txn_id);
extern t_uchar * arch_fs_archive_revision_lock_broken_path (const struct arch_archive * arch,
                                                            const t_uchar * archive_path,
                                                            const t_uchar * version,
                                                            const t_uchar * prev_level);
#endif  /* INCLUDE__LIBARCH__ARCHIVE_H */


/* tag: Tom Lord Sat Jan  5 15:26:10 2002 (archive.h)
 */
