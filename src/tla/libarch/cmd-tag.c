/* cmd-tag.c
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */


#include "config-options.h"
#include "hackerlab/cmd/main.h"
#include "hackerlab/fs/file-names.h"
#include "hackerlab/fs/cwd.h"
#include "tla/libfsutils/tmp-files.h"
#include "tla/libfsutils/file-contents.h"
#include "tla/libfsutils/rmrf.h"
#include "tla/libarch/namespace.h"
#include "tla/libarch/project-tree.h"
#include "tla/libarch/patch-logs.h"
#include "tla/libarch/archive.h"
#include "tla/libarch/build-revision.h"
#include "tla/libarch/proj-tree-lint.h"
#include "tla/libarch/tag.h"
#include "tla/libarch/cmd-tag.h"
#include "tla/libarch/cmdutils.h"
#include "tla/libarch/archive-setup.h"



static t_uchar * usage = "[options] SOURCE-REVISION TAG-VERSION";
static t_uchar * version_string = (cfg__std__package " from regexps.com\n"
                                   "\n"
                                   "Copyright 2003 Tom Lord\n"
                                   "\n"
                                   "This is free software; see the source for copying conditions.\n"
                                   "There is NO warranty; not even for MERCHANTABILITY or FITNESS FOR A\n"
                                   "PARTICULAR PURPOSE.\n"
                                   "\n"
                                   "Report bugs to " cfg__tla_bug_mail ".\n"
                                   "\n"
                                   cfg__std__release_id_string
                                   "\n");

#define OPTS(OP) \
  OP (opt_help_msg, "h", "help", 0, \
      "Display a help message and exit.") \
  OP (opt_long_help, "H", 0, 0, \
      "Display a verbose help message and exit.") \
  OP (opt_version, "V", "version", 0, \
      "Display a release identifier string\n" \
      "and exit.") \
  OP (opt_archive, "A", "archive", 1, \
      "Override `my-default-archive'") \
  OP (opt_log, "l", "log FILE", 1, \
      "commit with log file FILE") \
  OP (opt_no_cacherev, 0, "no-cacherev", 0, \
      "Do not cacherev tag even if different archive") \
  OP (opt_seal, 0, "seal", 0, \
      "create a version-0 revision") \
  OP (opt_fix, 0, "fix", 0, \
      "create a versionfix revision") \
  OP (opt_setup, "S", "setup", 0, \
      "Use `archive-setup' if necessary; implied by default.") \
  OP (opt_no_setup, 0, "no-setup", 0, \
      "Do not use `archive-setup' even if necessary.")

t_uchar arch_cmd_tag_help[] = ("create a continuation revision \n"
			       "Create the continuation revision TAG-VERSION (branch point or tag)\n"
			       "which is equivalent to SOURCE-REVISION (plus a log entry).\n"
			       "\n"
			       "If no log entry is provided, a trivial log entry will be created.\n");

enum options
{
  OPTS (OPT_ENUM)
};

static struct opt_desc opts[] =
{
  OPTS (OPT_DESC)
    {-1, 0, 0, 0, 0}
};



int
arch_cmd_tag (t_uchar * program_name, int argc, char * argv[])
{
  int o;
  struct opt_parsed * option;
  t_uchar * default_archive = 0;
  t_uchar * log_file = 0;
  int do_cacherev = 1;
  int seal = 0;
  int fix = 0;
  int setup = 1;

  safe_buffer_fd (1, 0, O_WRONLY, 0);

  option = 0;

  while (1)
    {
      o = opt_standard (lim_use_must_malloc, &option, opts, &argc, argv, program_name, usage, version_string, arch_cmd_tag_help, opt_help_msg, opt_long_help, opt_version);
      if (o == opt_none)
        break;
      switch (o)
        {
        default:
          safe_printfmt (2, "unhandled option `%s'\n", option->opt_string);
          panic ("internal error parsing arguments");

        usage_error:
          opt_usage (2, argv[0], program_name, usage, 1);
          exit (1);

          /* bogus_arg: */
          safe_printfmt (2, "ill-formed argument for `%s' (`%s')\n", option->opt_string, option->arg_string);
          goto usage_error;

        case opt_archive:
          {
            default_archive = str_save (0, option->arg_string);
            break;
          }

        case opt_log:
          {
            log_file = str_save (0, option->arg_string);
            break;
          }
        
        case opt_no_cacherev:
          {
            do_cacherev = 0;
            break;
          }

        case opt_seal:
          {
            seal = 1;
            break;
          }

        case opt_fix:
          {
            fix = 1;
            break;
          }

	    case opt_setup:
	      {
	        setup = 1;
	        break;
	      }

	    case opt_no_setup:
	      {
	        setup = 0;
	        break;
	      }
        }
    }

  if (argc != 3)
    goto usage_error;

  {
    t_uchar * source_spec;
    t_uchar * tag_spec;
    t_uchar * source_revision = 0;
    t_uchar * tag_archive = 0;
    t_uchar * tag_version = 0;
    t_uchar * log = 0;
    struct arch_archive * source_arch = 0;
    struct arch_archive * tag_arch = 0;
    rel_table tag_version_revisions = rel_table_nil;
    t_uchar * last_level = 0;
    enum arch_patch_level_type last_level_type;
    enum arch_patch_level_type desired_level_type;
    t_ulong last_n;
    t_ulong desired_n;
    t_uchar * desired_level = 0;
    t_uchar * tag_revision = 0;

    if (default_archive)
      {
        if (!arch_valid_archive_name (default_archive))
           {
            safe_printfmt (2, "%s: invalid archive name (%s)\n",
                           argv[0], default_archive);
            exit (1);
          }
      }

    source_spec = argv[1];
    tag_spec = argv[2];

    if (log_file)
      {
        log = file_contents (log_file);
        if (!arch_valid_log_file (log))
          {
            safe_printfmt (2, "%s: invalid log file (%s)\n",
                           argv[0], log_file);
            exit (1);
          }
      }

    if (!arch_valid_package_name (tag_spec, arch_maybe_archive, arch_req_version, 0))
      {
        safe_printfmt (2, "%s: invalid version name -- %s\n",
                       argv[0], tag_spec);
        exit (1);
      }

    tag_archive = arch_parse_package_name (arch_ret_archive, default_archive, tag_spec);
    tag_version = arch_parse_package_name (arch_ret_non_archive, default_archive, tag_spec);


    if (arch_is_system_package_name (tag_version))
      {
        safe_printfmt (2, "%s: user's can not tag in system versions\n  version: %s\n", argv[0], tag_version);
        exit (2);
      }

    source_revision = arch_determine_revision (&source_arch, default_archive, source_spec, argv[0]);

    if (setup)
      arch_setup_archive_simple (1, tag_archive, tag_version);

    if (!str_cmp (source_arch->name, tag_archive))
      tag_arch = source_arch;
    else
      tag_arch = arch_archive_connect (tag_archive, 0);

    if (!setup)
      arch_check_for (tag_arch, arch_req_version, tag_version);
    tag_version_revisions = arch_archive_revisions (tag_arch, tag_version, 0);

    if (!rel_n_records (tag_version_revisions))
      {
        desired_level_type = arch_is_base0_level;
        desired_n = 0;
      }
    else
      {
        last_level = str_save (0, rel_peek_str (tag_version_revisions, rel_n_records (tag_version_revisions) - 1, 0));
        last_level_type = arch_analyze_patch_level (&last_n, last_level);

        switch (last_level_type)
          {
          default:
            panic ("NOT IMPLEMENTED YET");
            panic ("internal error");
            break;

          case arch_is_base0_level:
            {
              if (seal)
                {
                  desired_level_type = arch_is_version_level;
                  desired_n = 0;
                }
              else if (fix)
                {
                  safe_printfmt (2, "%s: can not --fix before --seal",
                                 argv[0]);
                  exit (2);
                }
              else
                {
                  desired_level_type = arch_is_patch_level;
                  desired_n = 1;
                }
              break;
            }

          case arch_is_patch_level:
            {
              if (seal)
                {
                  desired_level_type = arch_is_version_level;
                  desired_n = 0;
                }
              else if (fix)
                {
                  safe_printfmt (2, "%s: can not --fix before --seal",
                                 argv[0]);
                  exit (2);
                }
              else
                {
                  desired_level_type = arch_is_patch_level;
                  desired_n = last_n + 1;
                }
              break;
            }

          case arch_is_version_level:
            {
              if (seal)
                {
                  safe_printfmt (2, "%s: version already sealed", argv[0]);
                  exit (2);
                }
              else if (fix)
                {
                  desired_level_type = arch_is_versionfix_level;
                  desired_n = 1;
                }
              else
                {
                  safe_printfmt (2, "%s: cannot commit to sealed version with --fix",
                                 argv[0]);
                  exit (2);
                }
              break;
            }

          case arch_is_versionfix_level:
            {
              if (seal)
                {
                  safe_printfmt (2, "%s: version already sealed", argv[0]);
                  exit (2);
                }
              else if (fix)
                {
                  desired_level_type = arch_is_versionfix_level;
                  desired_n = last_n + 1;
                }
              else
                {
                  safe_printfmt (2, "%s: cannot commit to sealed version with --fix",
                                 argv[0]);
                  exit (2);
                }
              break;
            }
          }
      }

    desired_level = arch_form_patch_level (desired_level_type, desired_n);
    tag_revision = str_alloc_cat_many (0, tag_version, "--", desired_level, str_end);

    arch_tag (1, tag_arch, tag_revision, source_arch, source_revision, log);

    if ( do_cacherev && str_cmp(tag_archive, source_arch->official_name) != 0)
      {
        t_uchar * tmp_dir;
        t_uchar * pristine_dir;
        
        safe_printfmt (1, "* Archive caching revision\n");
        safe_flush (1);

        tmp_dir = tmp_file_name (".", ",,archive-cache-revision");
        pristine_dir = file_name_in_vicinity (0, tmp_dir, tag_revision);
        safe_mkdir (tmp_dir, 0777);
        safe_mkdir (pristine_dir, 0777);

        arch_build_revision (1, pristine_dir, tag_arch, tag_archive, tag_revision, ".");
          
          {
            t_uchar * errstr;
            
            if (arch_archive_put_cached (&errstr, tag_arch, tag_revision, pristine_dir))
              {
                safe_printfmt (2, "Warning: %s: was unable to cache revision %s/%s (%s)\n",
                               argv[0], tag_archive, tag_revision, errstr);
              }
            else
              {
                safe_printfmt (1, "* Made cached revision of  %s/%s \n", tag_archive, tag_revision);
              }
          }
        rmrf_file (tmp_dir);
        rmrf_file (tmp_dir);
        lim_free (0, pristine_dir);
        lim_free (0, tmp_dir);
      }

    if (log_file)
      safe_unlink (log_file);

    lim_free (0, source_revision);
    lim_free (0, tag_archive);
    lim_free (0, tag_version);
    lim_free (0, log);
    if (source_arch != tag_arch)
      arch_archive_close (source_arch);
    arch_archive_close (tag_arch);
    rel_free_table (tag_version_revisions);
    lim_free (0, last_level);
    lim_free (0, desired_level);
    lim_free (0, tag_revision);
  }

  lim_free (0, default_archive);

  return 0;
}



/* tag: Tom Lord Tue May 27 23:03:10 2003 (tagrev.c)
 */
