/* cmd-commit.c
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */


#include "config-options.h"
#include "hackerlab/cmd/main.h"
#include "tla/libfsutils/file-contents.h"
#include "tla/libarch/namespace.h"
#include "tla/libarch/project-tree.h"
#include "tla/libarch/patch-logs.h"
#include "tla/libarch/archive.h"
#include "tla/libarch/proj-tree-lint.h"
#include "tla/libarch/commit.h"
#include "tla/libarch/cmd.h"
#include "tla/libarch/editor.h"
#include "tla/libarch/cmd-commit.h"
#include "tla/libarch/invent.h"


/* __STDC__ prototypes for static functions */
static void arch_make_edit_log (t_uchar *tree_root, t_uchar *archive, t_uchar *version);



static t_uchar * usage = "[options] [[archive]/version] [-- file ...]";
static t_uchar * version_string = (cfg__std__package " from regexps.com\n"
                                   "\n"
                                   "Copyright 2003 Tom Lord\n"
                                   "\n"
                                   "This is free software; see the source for copying conditions.\n"
                                   "There is NO warranty; not even for MERCHANTABILITY or FITNESS FOR A\n"
                                   "PARTICULAR PURPOSE.\n"
                                   "\n"
                                   "Report bugs to " cfg__tla_bug_mail ".\n"
                                   "\n"
                                   cfg__std__release_id_string
                                   "\n");

#define OPTS(OP) \
  OP (opt_help_msg, "h", "help", 0, \
      "Display a help message and exit.") \
  OP (opt_long_help, "H", 0, 0, \
      "Display a verbose help message and exit.") \
  OP (opt_version, "V", "version", 0, \
      "Display a release identifier string\n" \
      "and exit.") \
  OP (opt_archive, "A", "archive", 1, \
      "Override `my-default-archive'") \
  OP (opt_dir, "d", "dir DIR", 1, \
      "cd to DIR first") \
  OP (opt_log, "l", "log FILE", 1, \
      "commit with log file FILE") \
  OP (opt_summary, "s", "summary TEXT", 1, \
      "log with summary TEXT plus log-for-merge output" ) \
  OP (opt_log_msg, "L", "log-message TEXT", 1, \
      "log with TEXT plus log-for-merge output") \
  OP (opt_exclude, "x", "exclude FILE", 1, \
      "exclude FILE from the files to commit") \
  OP (opt_strict, 0, "strict", 0, \
      "strict tree-lint") \
  OP (opt_seal, 0, "seal", 0, \
      "create a version-0 revision") \
  OP (opt_fix, 0, "fix", 0, \
      "create a versionfix revision") \
  OP (opt_out_of_date, 0, "out-of-date-ok", 0, \
      "commit even if out of date") \
  OP (opt_file_list, 0, "file-list FILE", 1, \
      "commit only changes to files listed in FILE") \
  OP (opt_write_revision, 0, "write-revision REVISION_FILE_SPEC", 1, \
      "write the new tree id to REVISION_FILE_SPEC before committing") \
  OP (opt_unescaped, 0, "unescaped", 0, \
      "show filenames in unescaped form")


t_uchar arch_cmd_commit_help[] =
  ("archive a changeset-based revision\n"

   "Archive a revision of the project tree containing DIR (or the current\n"
   "directory)\n"
   "\n"
   "If there are any automated ChangeLog files, update them before\n"
   "computing the patch set.\n"
   "\n"
   "If --log-message is specified without --summary, then TEXT is used both\n"
   "as the summary and the first line of the log body.\n");

enum options
{
  OPTS (OPT_ENUM)
};

static struct opt_desc opts[] =
{
  OPTS (OPT_DESC)
    {-1, 0, 0, 0, 0}
};



static int 
write_revision_file_spec(const t_uchar *revision_file_spec, const t_uchar *revision)
{

  t_uchar *file_name = 0;
  t_uchar *content_spec = 0;
  t_uchar *s = str_chr_index(revision_file_spec, ':');
  int fd ;

  if (!s)
    {
      safe_printfmt (2, "revision file spec %s does not contain file name separator ':'! \n",  
                     revision_file_spec);
      return 0;
    } 
  file_name = str_save_n(0, revision_file_spec, s - revision_file_spec);


  fd = safe_open(file_name, O_WRONLY | O_CREAT | O_TRUNC, 0666);
  if (!fd)
    {
      safe_printfmt (2, "cannot open file %s for writing revision! \n",  
                     file_name);
      lim_free(0, file_name);
      return 0;
    }
  content_spec = str_save(0, s + 1); 
  safe_printfmt(fd, content_spec, revision);
  safe_printfmt(fd, "\n");
  safe_close(fd);
  lim_free(0, file_name);
  lim_free(0, content_spec);

  return 1;
}

int
arch_cmd_commit (t_uchar * program_name, int argc, char * argv[])
{
  int o;
  struct opt_parsed * option;
  t_uchar * default_archive = 0;
  t_uchar * dir = 0;
  t_uchar * log_file = 0;
  t_uchar * log_text = 0;
  t_uchar * summary = 0;
  t_uchar *revision_file_spec = 0;
  int strict = 0;
  int seal = 0;
  int fix = 0;
  int out_of_date_ok = 0;
  t_uchar * file_list_file = 0;
  int opt_end_with_double_dash = 0;
  int escape_classes = arch_escape_classes;
  rel_table exclude_list = rel_table_nil;

  dir = str_save (0, ".");

  safe_buffer_fd (1, 0, O_WRONLY, 0);

  option = 0;

  while (1)
    {
      o = opt_standard (lim_use_must_malloc, &option, opts, &argc, argv, program_name, usage, version_string, arch_cmd_commit_help, opt_help_msg, opt_long_help, opt_version);
      if (o == opt_none)
        break;

      if (o == opt_double_dash)
        {
          opt_end_with_double_dash = 1;
          break;
        }

      switch (o)
        {
        default:
          safe_printfmt (2, "unhandled option `%s'\n", option->opt_string);
          panic ("internal error parsing arguments");

        usage_error:
          opt_usage (2, argv[0], program_name, usage, 1);
          exit (1);

          /* bogus_arg: */
          safe_printfmt (2, "ill-formed argument for `%s' (`%s')\n", option->opt_string, option->arg_string);
          goto usage_error;

        case opt_archive:
          {
            default_archive = str_save (0, option->arg_string);
            break;
          }

        case opt_dir:
          {
            dir = str_save (0, option->arg_string);
            break;
          }

        case opt_log:
          {
            lim_free (0, log_file);
            log_file = str_save (0, option->arg_string);
            lim_free (0, log_text);
            log_text = 0;
            break;
          }

        case opt_log_msg:
          {
            lim_free (0, log_text);
            log_text = str_save (0, option->arg_string);
            lim_free (0, log_file);
            log_file = 0;
            break;
          }

        case opt_summary:
          {
            lim_free (0, summary);
            summary = str_save (0, option->arg_string);
            lim_free (0, log_file);
            log_file = 0;
            break;
          }

        case opt_strict:
          {
            strict = 1;
            break;
          }

        case opt_seal:
          {
            seal = 1;
            break;
          }

        case opt_fix:
          {
            fix = 1;
            break;
          }

        case opt_out_of_date:
          {
            out_of_date_ok = 1;
            break;
          }

        case opt_file_list:
          {
            lim_free (0, file_list_file);
            file_list_file = str_save (0, option->arg_string);
            break;
          }
        case opt_exclude:
          {
            t_uchar * normalized_path = normal_from_path(option->arg_string);
            rel_add_records (&exclude_list, rel_singleton_record_taking (rel_make_field_str (normalized_path)), rel_record_null);
            
            lim_free(0, normalized_path);
            normalized_path = 0;
            break;
          
          }  
	    case opt_unescaped:
	      {
	        escape_classes = 0;
	        break;
	      }
      
        case opt_write_revision:
          {
             lim_free(0, revision_file_spec);
             revision_file_spec = str_save(0, option->arg_string);
             break;
          }
        }
    }

  {
    t_uchar * log = 0;
    t_uchar * vsnspec = 0;
    rel_table file_list = rel_table_nil;
    rel_table file_list2 = rel_table_nil;
    t_uchar * tree_root = 0;
    t_uchar * archive = 0;
    t_uchar * version = 0;
    rel_table revisions = rel_table_nil;
    t_uchar * last_level = 0;
    enum arch_patch_level_type last_level_type;
    enum arch_patch_level_type desired_level_type;
    t_ulong last_n;
    t_ulong desired_n;
    t_uchar * desired_level = 0;
    t_uchar * revision = 0;
    struct arch_archive * arch = 0;

    if (default_archive)
      {
        if (!arch_valid_archive_name (default_archive))
          {
            safe_printfmt (2, "%s: invalid archive name (%s)\n",
                           argv[0], default_archive);
            exit (1);
          }
      }

    tree_root = arch_tree_root (0, dir, 1);

    if (!tree_root)
      {
        safe_printfmt (2, "%s: not in project tree (%s)\n", argv[0], dir);
        exit (1);
      }

    if (argc == 1 || opt_end_with_double_dash)
      /* No args, or file arguments but no version spec.  */
      {
        vsnspec = arch_tree_version (tree_root);
        if (!vsnspec)
          {
            safe_printfmt (2, "%s: project tree has no default version\n  tree: %s\n",
                           argv[0], tree_root);
            exit (1);
          }
      }
    else
      /* Version spec specified.  */
      vsnspec = str_save (0, argv[1]);

    if (file_list_file)
      {
        int in_fd = safe_open (file_list_file, O_RDONLY, 0);
        file_list = rel_read_table (in_fd, 1, argv[0], file_list_file);
        safe_close (in_fd);
      }

    if (argc > 2 || opt_end_with_double_dash)
      /* File arguments.  */
      {
        int argx = opt_end_with_double_dash ? 1 : 2;
        t_uchar *normalized_path = 0;

        if (!opt_end_with_double_dash && str_cmp (argv[argx], "--") == 0)
          argx++;

        if (argx == argc)
          /* --, but no files specified; should this be an error?  */
          goto usage_error;

        while (argx < argc)
          {
            normalized_path = normal_from_path(argv[argx++]);
            rel_add_records (&file_list, rel_singleton_record_taking (rel_make_field_str (normalized_path)), rel_record_null);
            lim_free(0, normalized_path);
            normalized_path = 0;
          }
      }

    if (!arch_valid_package_name (vsnspec, arch_maybe_archive, arch_req_version, 0))
      {
        safe_printfmt (2, "%s: invalid version name -- %s\n",
                       argv[0], vsnspec);
        exit (1);
      }

    archive = arch_parse_package_name (arch_ret_archive, default_archive, vsnspec);
    version = arch_parse_package_name (arch_ret_non_archive, 0, vsnspec);

    if (arch_is_system_package_name (version))
      {
        safe_printfmt (2, "%s: user's can not commit to system versions\n  version: %s\n", argv[0], version);
        exit (2);
      }

    if (log_text || summary)
      {
        if (! summary)
          summary = log_text;
        log = arch_auto_log_message (tree_root, archive, version,
                                     summary, log_text);
      }
    else
      {
        if (!log_file)
          {
            log_file = arch_make_log_file (tree_root, archive, version);
            if (safe_access (log_file, F_OK))
	      arch_make_edit_log (tree_root, archive, version);
          }

        log = file_contents (log_file);
      }

    if (!arch_valid_log_file (log))
      {
        safe_printfmt (2, "%s: invalid log file (%s)\n",
                       argv[0], (log_file ? log_file : log_text));
        exit (1);
      }

    if (!arch_has_patch_log (tree_root, archive, version))
      {
        safe_printfmt (2, "%s: tree has no patch log for version\n    tree: %s\n    version: %s/%s\n",
                       argv[0], tree_root, archive, version);
        exit (1);
      }

    {
      rel_table log_entries = rel_table_nil;

      log_entries = arch_logs (tree_root, archive, version, 0);

      if (!rel_n_records (log_entries))
        {
          safe_printfmt (2, "%s: tree has no patch log entries for version\n    tree: %s\n    version: %s/%s",
                         argv[0], tree_root, archive, version);
          exit (1);
        }
    }

    
    {
      struct arch_tree_lint_result * lint = 0;
      int status;

      lint = arch_tree_lint (tree_root);
      status = arch_print_tree_lint_report (2, lint, escape_classes);

      if ((status < 0) || (strict && status))
        {
          safe_printfmt (2, "%s: commit aborted\n", argv[0]);
          exit (1);
        }
    }

    arch = arch_archive_connect (archive, 0);

    revisions = arch_archive_revisions (arch, version, 0);

    if (!rel_n_records (revisions))
      {
        safe_printfmt (2, "%s: version has no revisions -- use revimport instead\n",
                       argv[0]);
        exit (1);
      }

    last_level = str_save (0, rel_peek_str (revisions, rel_n_records (revisions) - 1, 0));

    last_level_type = arch_analyze_patch_level (&last_n, last_level);

    switch (last_level_type)
      {
      default:
        panic ("NOT IMPLEMENTED YET");
        panic ("internal error");
        break;

      case arch_is_base0_level:
        {
          if (seal)
            {
              desired_level_type = arch_is_version_level;
              desired_n = 0;
            }
          else if (fix)
            {
              safe_printfmt (2, "%s: can not --fix before --seal\n", argv[0]);
              exit (2);
            }
          else
            {
              desired_level_type = arch_is_patch_level;
              desired_n = 1;
            }
        break;
        }

      case arch_is_patch_level:
        {
          if (seal)
            {
              desired_level_type = arch_is_version_level;
              desired_n = 0;
            }
          else if (fix)
            {
              safe_printfmt (2, "%s: can not --fix before --seal\n", argv[0]);
              exit (2);
            }
          else
            {
              desired_level_type = arch_is_patch_level;
              desired_n = last_n + 1;
            }
          break;
        }

      case arch_is_version_level:
        {
          if (seal)
            {
              safe_printfmt (2, "%s: version already sealed\n", argv[0]);
              exit (2);
            }
          else if (fix)
            {
              desired_level_type = arch_is_versionfix_level;
              desired_n = 1;
            }
          else
            {
              safe_printfmt (2, "%s: cannot commit to sealed version without --fix\n",
                             argv[0]);
              exit (2);
            }
          break;
        }

      case arch_is_versionfix_level:
        {
          if (seal)
            {
              safe_printfmt (2, "%s: version already sealed\n", argv[0]);
              exit (2);
            }
          else if (fix)
            {
              desired_level_type = arch_is_versionfix_level;
              desired_n = last_n + 1;
            }
          else
            {
              safe_printfmt (2, "%s: cannot commit to sealed version without --fix\n",
                             argv[0]);
              exit (2);
            }
          break;
        }
      }

    if (!out_of_date_ok)
      {
        t_uchar * prev_revision = 0;
        t_uchar * prev_log_path = 0;

        prev_revision = str_alloc_cat_many (0, version, "--", last_level, str_end);
        prev_log_path = arch_log_file (tree_root, archive, prev_revision);

        if (safe_access (prev_log_path, F_OK))
          {
            safe_printfmt (2, "%s: tree is not up-to-date (missing latest revision is %s/%s)\n",
                           argv[0], archive, prev_revision);
            exit (1);
          }

        lim_free (0, prev_revision);
        lim_free (0, prev_log_path);
      }

    desired_level = arch_form_patch_level (desired_level_type, desired_n);
    revision = str_alloc_cat_many (0, version, "--", desired_level, str_end);

    if (revision_file_spec) /* if revision file spec is specified, write the new revision to the file before committing */
      {
        if (0 == write_revision_file_spec(revision_file_spec, revision))
            exit(1);
      }

    if ((rel_n_records (exclude_list) > 0) && (rel_n_records (file_list) == 0))
      {
          file_list = arch_source_files_file_list(tree_root, 0, 0);
      } 
        
    file_list2 = rel_table_substract(file_list, exclude_list); 
    
    arch_commit (1, arch, revision, tree_root, log, file_list2, escape_classes);

    safe_printfmt (1, "* committed %s/%s\n", archive, revision);

    if (log_file)
      safe_unlink (log_file);

    lim_free (0, vsnspec);
    rel_free_table (file_list);
    rel_free_table (file_list2);
    lim_free (0, log);
    lim_free (0, tree_root);
    lim_free (0, archive);
    lim_free (0, version);
    rel_free_table (revisions);
    lim_free (0, last_level);
    lim_free (0, desired_level);
    lim_free (0, revision);

    lim_free (0, revision_file_spec);
  }

  lim_free (0, default_archive);
  lim_free (0, dir);

  return 0;
}



static void
arch_make_edit_log (t_uchar *tree_root, t_uchar *archive, t_uchar *version)
{
  int out_fd;
  int ecode;
  t_uchar *log_path = arch_make_log_file (tree_root, archive, version);
  t_uchar *text = arch_auto_log_message (tree_root, archive, version, "", "");
  t_uchar *new_text = 0;

  safe_printfmt (1, "* no log found, creating one automatically\n");
  safe_printfmt (1, "* (Use \"tla make-log\" to create a log file.)\n");
  safe_flush (1);

  out_fd = safe_open (log_path, O_WRONLY | O_CREAT | O_EXCL, 0666);

  safe_printfmt (out_fd, "%s", text);
  safe_close (out_fd);

  ecode = arch_run_editor (log_path);
  if (ecode != 0)
    {
      safe_printfmt (2, "arch_make_edit_log: editor exited with error code %d\n", ecode);
      exit (2);
    }

  new_text = file_contents (log_path);
  if (!str_cmp (text, new_text))
    {
      safe_printfmt (2, "arch_make_edit_log: log file is unmodified, aborting\n");
      safe_unlink (log_path);
      exit (2);
    }
}





/* tag: Tom Lord Mon May 26 21:33:18 2003 (revcmt.c)
 */
