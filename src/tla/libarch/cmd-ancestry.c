/* cmd-ancestry.c:
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */




#include "config-options.h"
#include "hackerlab/cmd/main.h"
#include "hackerlab/fs/file-names.h"
#include "hackerlab/fs/cwd.h"
#include "tla/libfsutils/tmp-files.h"
#include "tla/libfsutils/rmrf.h"
#include "tla/libarch/namespace.h"
#include "tla/libarch/project-tree.h"
#include "tla/libarch/my.h"
#include "tla/libarch/archive.h"
#include "tla/libarch/archives.h"
#include "tla/libarch/patch-logs.h"
#include "tla/libarch/cmdutils.h"
#include "tla/libarch/cmd-ancestry.h"



static t_uchar * usage = "[options] [revision]";
static t_uchar * version_string = (cfg__std__package " from regexps.com\n"
                                   "\n"
                                   "Copyright 2003 Tom Lord\n"
                                   "\n"
                                   "This is free software; see the source for copying conditions.\n"
                                   "There is NO warranty; not even for MERCHANTABILITY or FITNESS FOR A\n"
                                   "PARTICULAR PURPOSE.\n"
                                   "\n"
                                   "Report bugs to " cfg__tla_bug_mail ".\n"
                                   "\n"
                                   cfg__std__release_id_string
                                   "\n");

#define OPTS(OP) \
  OP (opt_help_msg, "h", "help", 0, \
      "Display a help message and exit.") \
  OP (opt_long_help, "H", 0, 0, \
      "Display a verbose help message and exit.") \
  OP (opt_version, "V", "version", 0, \
      "Display a release identifier string\n" \
      "and exit.") \
  OP (opt_archive, "A", "archive", 1, \
      "Override `my-default-archive'") \
  OP (opt_dir, "d", "dir DIR", 1, \
      "cd to DIR first") \
  OP (opt_merges, "m", "merges", 0, \
      "show merges into this development line") \
  OP (opt_reverse, "r", "reverse", 0, \
      "list oldest to newest") \
  OP (opt_summary, "s", "summary", 0, \
      "print a summary of each patch") \
  OP (opt_creator, "c", "creator", 0, \
      "print the creator of each patch") \
  OP (opt_date, "D", "date", 0, \
      "print the date of each patch")




t_uchar arch_cmd_ancestry_help[] = ("display the ancestory of a revision\n"
                                    "Print the ancestry of a revision.\n");

enum options
{
  OPTS (OPT_ENUM)
};

static struct opt_desc opts[] =
{
  OPTS (OPT_DESC)
    {-1, 0, 0, 0, 0}
};



int
arch_cmd_ancestry (t_uchar * program_name, int argc, char * argv[])
{
  int o;
  struct opt_parsed * option;
  t_uchar * default_archive = 0;
  t_uchar * dir = 0;
  int merges = 0;
  int reverse = 0;
  int wanted_headers = 0;

  safe_buffer_fd (1, 0, O_WRONLY, 0);

  dir = str_save (0, ".");

  option = 0;

  while (1)
    {
      o = opt_standard (lim_use_must_malloc, &option, opts, &argc, argv, program_name, usage, version_string, arch_cmd_ancestry_help, opt_help_msg, opt_long_help, opt_version);
      if (o == opt_none)
        break;
      switch (o)
        {
        default:
          safe_printfmt (2, "unhandled option `%s'\n", option->opt_string);
          panic ("internal error parsing arguments");

        usage_error:
          opt_usage (2, argv[0], program_name, usage, 1);
          exit (1);

          /* bogus_arg: */
          safe_printfmt (2, "ill-formed argument for `%s' (`%s')\n", option->opt_string, option->arg_string);
          goto usage_error;

        case opt_archive:
          {
            lim_free (0, default_archive);
            default_archive = str_save (0, option->arg_string);
            break;
          }

        case opt_dir:
          {
            lim_free (0, dir);
            dir = str_save (0, option->arg_string);
            break;
          }

        case opt_merges:
          {
            merges = 1;
            break;
          }

        case opt_reverse:
          {
            reverse = 1;
            break;
          }

        case opt_summary:
          {
            wanted_headers |= arch_include_summary;
            break;
          }

        case opt_creator:
          {
            wanted_headers |= arch_include_creator;
            break;
          }

        case opt_date:
          {
            wanted_headers |= arch_include_date;
            break;
          }
        }
    }

  if (argc > 2)
    goto usage_error;

  default_archive = arch_my_default_archive (default_archive);

  {
    t_uchar * revision_spec = 0;
    t_uchar * archive = 0;
    t_uchar * version = 0;
    t_uchar * revision = 0;
    struct arch_archive * arch = 0;
    t_uchar * this_archive = 0;
    t_uchar * this_version = 0;


    if (argc == 2)
      revision_spec = str_save (0, argv[1]);
    else
      revision_spec = arch_try_tree_version (program_name);

    if (!arch_valid_package_name (revision_spec, arch_maybe_archive, arch_req_package, 1))
      {
        safe_printfmt (2, "%s: invalid revision spec (%s)\n",
                       argv[0], revision_spec);
        exit (2);
      }

    archive = arch_parse_package_name (arch_ret_archive, default_archive, revision_spec);
    arch = arch_archive_connect (archive, 0);

    if (arch_valid_package_name (revision_spec, arch_maybe_archive, arch_req_package, 0))
      {
        t_uchar * package = 0;
        rel_table versions = rel_table_nil;
        arch_check_for (arch, arch_req_package, revision_spec);
        package = arch_parse_package_name (arch_ret_package, 0, revision_spec);
        versions = arch_archive_versions (arch, package);
        arch_sort_table_by_name_field (1, versions, 0);
        if (!rel_n_records (versions))
          {
            safe_printfmt (2, "%s: package has no versions (%s)\n",
                           argv[0], revision_spec);
            exit (1);
          }

        lim_free (0, revision_spec);
        revision_spec = str_save (0, rel_peek_str (versions, 0, 0));

        lim_free (0, package);
        rel_free_table (versions);
      }


    if (arch_valid_package_name (revision_spec, arch_maybe_archive, arch_req_version, 0))
      {
        rel_table revisions = rel_table_nil;
        
        arch_check_for (arch, arch_req_version, revision_spec);
        version = arch_parse_package_name (arch_ret_package_version, 0, revision_spec);

        revisions = arch_archive_revisions (arch, version, 1);
        arch_sort_table_by_name_field (1, revisions, 0);
        if (!rel_n_records (revisions))
          {
            safe_printfmt (2, "%s: no revisions in version (%s/%s)\n",
                           argv[0], archive, revision_spec);
            exit (2);
          }

        revision = arch_parse_package_name (arch_ret_non_archive, 0, rel_peek_str (revisions, 0, 0));

        rel_free_table (revisions);
      }
    else
      {
        arch_check_for (arch, arch_req_patch_level, revision_spec);
        version = arch_parse_package_name (arch_ret_package_version, 0, revision_spec);
        revision = arch_parse_package_name (arch_ret_non_archive, 0, revision_spec);
      }

    while (1)
      {
        t_uchar * version_wanted = 0;
        t_uchar * level = 0;

        /* pre-condition:
         *
         * archive and revision tell us the next revision to print.
         *
         * if arch is 0, we can not reach that archive (or find any further information).
         * otherwise, arch is connected to archive.
         *
         * this_archive is 0 or the name of the last archive name printed
         * this_version is 0 or the name of the last package-version printed
         */

        if (str_cmp (this_archive, archive))
          {
            safe_printfmt (1, "%s\n", archive);
            lim_free (0, this_archive);
            this_archive = str_save (0, archive);
            lim_free (0, this_version);
            this_version = 0;
          }

        version_wanted = arch_parse_package_name (arch_ret_package_version, 0, revision);

        if (str_cmp (this_version, version_wanted))
          {
            safe_printfmt (1, "  %s\n", version_wanted);
            lim_free (0, this_version);
            this_version = str_save (0, version_wanted);
          }

        level = arch_parse_package_name (arch_ret_patch_level, 0, revision);

        safe_printfmt (1, "    %s", level);

        if (!arch)
          {
            safe_printfmt (1, " (\?\?\?)\n(\?\?\?)\n");
            break;
          }
        else
          {
            t_uchar * fq_next_revision = 0;
            t_uchar * next_archive = 0;
            t_uchar * next_version = 0;
            t_uchar * next_revision = 0;

            fq_next_revision = arch_ancestor_revision (arch, revision);
            if (fq_next_revision)
              {
                next_archive = arch_parse_package_name (arch_ret_archive, 0, fq_next_revision);
                next_version = arch_parse_package_name (arch_ret_package_version, 0, fq_next_revision);
                next_revision = arch_parse_package_name (arch_ret_non_archive, 0, fq_next_revision);
              }

            if (wanted_headers)
              {
                t_uchar * log = 0;
                assoc_table headers = 0;

                log = arch_archive_log (arch, revision);
                arch_parse_log (0, &headers, 0, log);

                safe_printfmt (1, "\n");
                arch_print_headers_summary (1, 6, headers, wanted_headers);

                lim_free (0, log);
                free_assoc_table (headers);
              }
            else if (fq_next_revision)
              {
                /* scan backwards in the same archive version and print " ... level"
                 */

                while (fq_next_revision && !str_cmp (next_archive, archive) && !str_cmp (next_version, version))
                  {
                    lim_free (0, revision);
                    revision = str_save (0, next_revision);

                    lim_free (0, level);
                    level = arch_parse_package_name (arch_ret_patch_level, 0, revision);

                    lim_free (0, fq_next_revision);
                    lim_free (0, next_archive);
                    lim_free (0, next_version);
                    lim_free (0, next_revision);

                    fq_next_revision = arch_ancestor_revision (arch, revision);
                    if (fq_next_revision)
                      {
                        next_archive = arch_parse_package_name (arch_ret_archive, 0, fq_next_revision);
                        next_version = arch_parse_package_name (arch_ret_package_version, 0, fq_next_revision);
                        next_revision = arch_parse_package_name (arch_ret_non_archive, 0, fq_next_revision);
                      }
                  }

                safe_printfmt (1, " ... %s\n", level);
              }
            else
              safe_printfmt (1, "\n");

            safe_flush (1);

            if (!fq_next_revision)
              {
                break;
              }
            else
              {
                /* update the invariant variables */

                lim_free (0, version);
                version = next_version;
                next_version = 0;

                revision = next_revision;
                next_revision = 0;

                lim_free (0, fq_next_revision);
                fq_next_revision = 0;

                if (!str_cmp (next_archive, archive))
                  {
                    lim_free (0, next_archive);
                    next_archive = 0;
                  }
                else
                  {
                    t_uchar * location;

                    lim_free (0, archive);
                    archive = next_archive;
                    next_archive = 0;

                    arch_archive_close (arch);
                    arch = 0;
                    location = arch_archive_location (archive, 1);

                    if (location)
                      arch = arch_archive_connect (archive, 0);

                    lim_free (0, location);
                  }
              }
          }

        lim_free (0, version_wanted);
        lim_free (0, level);
      }

    arch_archive_close (arch);
    lim_free (0, revision_spec);
    lim_free (0, archive);
    lim_free (0, version);
    lim_free (0, revision);
    lim_free (0, this_archive);
    lim_free (0, this_version);
  }

  lim_free (0, default_archive);

  return 0;
}



/* tag: Tom Lord Wed Jul 16 13:58:59 2003 (cmd-ancestry.c)
 */
