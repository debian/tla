/* cmd-diff.h:
 *
 ****************************************************************
 * Copyright (C) 2004 Canonical Ltd
 *               Author: Robert Collins
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */

#ifndef INCLUDE__LIBARCH__CMD_DIFF_H
#define INCLUDE__LIBARCH__CMD_DIFF_H


#include "hackerlab/machine/types.h"



extern t_uchar arch_cmd_diff_help[];


extern int arch_cmd_diff (t_uchar * program_name, int argc, char * argv[]);
extern t_uchar * arch_diff_default_output_dir(void * context, t_uchar const *tree_root, t_uchar * dirname);
#endif  /* INCLUDE__LIBARCH__CMD_DIFF_H */

/* tag: Tom Lord Tue Jun 10 17:15:53 2003 (cmd-diff.h)
 */

