/* copy-project-tree.c:
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */


#include "hackerlab/vu/safe.h"
#include "tla/libfsutils/copy-file.h"
#include "tla/libarch/project-tree.h"
#include "tla/libarch/invent.h"
#include "tla/libarch/copy-project-tree.h"



void
arch_copy_project_tree (t_uchar * from, t_uchar * to, int precious, int nested)
{
  const int full_meta = arch_tree_has_meta_flag (from);
  rel_table inventory = rel_table_nil;

  inventory = arch_source_inventory (from, 1, precious, nested);

  safe_mkdir (to, 0777);

  copy_file_list (to, from, inventory, full_meta);

  rel_free_table (inventory);
}



/* tag: Tom Lord Wed Jun  4 16:32:04 2003 (copy-project-tree.c)
 */
