/* cmd-my-id.c
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */


#include "config-options.h"
#include "hackerlab/cmd/main.h"
#include "tla/libfsutils/ensure-dir.h"
#include "tla/libarch/my.h"
#include "tla/libarch/namespace.h"
#include "tla/libarch/cmd-my-id.h"



static t_uchar * usage = "[options] [id]";
static t_uchar * version_string = (cfg__std__package " from regexps.com\n"
                                   "\n"
                                   "Copyright 2003 Tom Lord\n"
                                   "\n"
                                   "This is free software; see the source for copying conditions.\n"
                                   "There is NO warranty; not even for MERCHANTABILITY or FITNESS FOR A\n"
                                   "PARTICULAR PURPOSE.\n"
                                   "\n"
                                   "Report bugs to " cfg__tla_bug_mail ".\n"
                                   "\n"
                                   cfg__std__release_id_string
                                   "\n");

#define OPTS(OP) \
  OP (opt_help_msg, "h", "help", 0, \
      "Display a help message and exit.") \
  OP (opt_long_help, "H", 0, 0, \
      "Display a verbose help message and exit.") \
  OP (opt_version, "V", "version", 0, \
      "Display a release identifier string\n" \
      "and exit.") \
  OP (opt_errname, "e", "errname", 1, \
      "specify program name for errors") \
  OP (opt_uid, "u", "uid", 0, \
      "print only the UID portion of the ID")

t_uchar arch_cmd_my_id_help[] = ("print or change your id\n"
                                 "With no argument print your arch id.\n"
                                 "\n"
                                 "With an argument, record ID-STRING as your id\n"
                                 "in ~/.arch-params/=id\n"
                                 "\n"
                                 "Your id is recorded in various archives and log messages\n"
                                 "as you use arch.  It must consist entirely of printable\n"
                                 "characters and fit on one line.  By convention, it should\n"
                                 "have the form of an email address, as in this example:\n"
                                 "\n"
                                 "        Jane Hacker <jane.hacker@gnu.org>\n"
                                 "\n"
                                 "The portion of an id string between < and > is called your\n"
                                 "uid.  arch sometimes uses your uid as a fragment when generating\n"
                                 "unique file names.\n"
                                 "\n"
                                 "The option -u (--uid) causes only the uid part of your id string\n"
                                 "to be printed.\n");

enum options
{
  OPTS (OPT_ENUM)
};

static struct opt_desc opts[] =
{
  OPTS (OPT_DESC)
    {-1, 0, 0, 0, 0}
};



enum operation
{
  op_print,
  op_set,
};

int
arch_cmd_my_id (t_uchar * program_name, int argc, char * argv[])
{
  int o;
  struct opt_parsed * option;
  char * errname;
  enum operation op;
  int uid;
  t_uchar * proposed_id;

  errname = argv[0];
  uid = 0;

  safe_buffer_fd (1, 0, O_WRONLY, 0);

  option = 0;

  while (1)
    {
      o = opt_standard (lim_use_must_malloc, &option, opts, &argc, argv, program_name, usage, version_string, arch_cmd_my_id_help, opt_help_msg, opt_long_help, opt_version);
      if (o == opt_none)
        break;
      switch (o)
        {
        default:
          safe_printfmt (2, "unhandled option `%s'\n", option->opt_string);
          panic ("internal error parsing arguments");

        usage_error:
          opt_usage (2, argv[0], program_name, usage, 1);
          exit (1);

          /* bogus_arg: */
          safe_printfmt (2, "ill-formed argument for `%s' (`%s')\n", option->opt_string, option->arg_string);
          goto usage_error;

        case opt_errname:
          {
            if (!option->arg_string[0])
              errname = 0;
            else
              errname = str_save (0, option->arg_string);
            break;
          }

        case opt_uid:
          {
            uid = 1;
            break;
          }
        }
    }

  if (argc > 2)
    goto usage_error;

  if (argc == 2)
    {
      op = op_set;
      proposed_id = argv[1];
    }
  else
    {
      op = op_print;
      proposed_id = 0;
    }


  switch (op)
    {
    case op_set:
      {
        if (!arch_valid_id (proposed_id))
          {
            safe_printfmt (2, "%s: invalid id (%s)\n", errname, proposed_id);
            exit (1);
          }
        arch_set_my_id (proposed_id);
        break;
      }

    case op_print:
      {
        t_uchar * raw_id;

        raw_id = arch_my_id ();
        invariant (arch_valid_id (raw_id));

        if (!uid)
          safe_printfmt (1, "%s\n", raw_id);
        else
          {
            t_uchar * start;
            t_uchar * end;

            start = str_chr_index (raw_id, '<') + 1;
            end = str_chr_index (start, '>');

            safe_printfmt (1, "%.*s\n", (int)(end - start), start);
          }
        break;
      }
    }

  return 0;
}




/* tag: Tom Lord Mon May 12 15:14:03 2003 (my-id.c)
 */
