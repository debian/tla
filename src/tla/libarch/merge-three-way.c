/* apply-changeset.c:
 *
 * vim:smartindent ts=8:sts=2:sta:et:ai:shiftwidth=2
 ****************************************************************
 * Copyright (C) 2004 Aaron Bentley
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */

#include "hackerlab/fs/file-names.h"
#include "tla/libfsutils/tmp-files.h"

#include "tla/libarch/invent.h"
#include "tla/libarch/chatter.h"
#include "tla/libarch/make-changeset.h"
#include "tla/libarch/apply-changeset.h"
#include "tla/libarch/inode-sig.h"
#include "tla/libarch/project-tree.h"
#include "tla/libarch/patch-logs.h"
#include "tla/libarch/star-merge.h"
#include "tla/libarch/merge-three-way.h"
