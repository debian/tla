/* cmd-archive-snapshot.c
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */


#include "config-options.h"
#include "hackerlab/cmd/main.h"
#include "tla/libarch/namespace.h"
#include "tla/libarch/my.h"
#include "tla/libarch/archive-snapshot.h"
#include "tla/libarch/cmd.h"
#include "tla/libarch/cmd-archive-snapshot.h"



static t_uchar * usage = "[options] dir [limit]";
static t_uchar * version_string = (cfg__std__package " from regexps.com\n"
                                   "\n"
                                   "Copyright 2003 Tom Lord\n"
                                   "\n"
                                   "This is free software; see the source for copying conditions.\n"
                                   "There is NO warranty; not even for MERCHANTABILITY or FITNESS FOR A\n"
                                   "PARTICULAR PURPOSE.\n"
                                   "\n"
                                   "Report bugs to " cfg__tla_bug_mail ".\n"
                                   "\n"
                                   cfg__std__release_id_string
                                   "\n");

#define OPTS(OP) \
  OP (opt_help_msg, "h", "help", 0, \
      "Display a help message and exit.") \
  OP (opt_long_help, "H", 0, 0, \
      "Display a verbose help message and exit.") \
  OP (opt_version, "V", "version", 0, \
      "Display a release identifier string\n" \
      "and exit.") \
  OP (opt_archive, "A", "archive", 1, \
      "Override `my-default-archive'")


t_uchar arch_cmd_archive_snapshot_help[] = ("update an archive snapshot\n"
                                            "Update the directory DIR with a \"snapshot\" of\n"
                                            "of an archive (or the part of the archive indicated\n"
                                            "by LIMIT\n"
                                            "\n"
                                            "For each archive snapshotted, DIR will contain\n"
                                            "a file and subdirectory (where $ARCH is the name\n"
                                            "of the archive):\n"
                                            "\n"
                                            "    ./$ARCH.added\n"
                                            "    ./$ARCH/\n"
                                            "\n"
                                            "Similarly, for each category snapshotted, DIR will\n"
                                            "contain:\n"
                                            "\n"
                                            "    ./$ARCH/$CAT.added\n"
                                            "    ./$ARCH/$CAT/\n"
                                            "\n"
                                            "and so on, recursively, for branches and versions.\n"
                                            "\n"
                                            "For each revision, the snapshot contains:\n"
                                            "\n"
                                            "    ./$ARCH/$CAT/$BRANCH/$VERSION/$REVISION.added\n"
                                            "\n"
                                            "and that file contains a copy of the patch log entry\n"
                                            "for that revision.\n"
                                            "\n"
                                            "Snapshots can be used in combination with other tools\n"
                                            "(\'make\' is suggested) to trigger one-time events\n"
                                            "in response to new additions to an archive.\n");

enum options
{
  OPTS (OPT_ENUM)
};

static struct opt_desc opts[] =
{
  OPTS (OPT_DESC)
    {-1, 0, 0, 0, 0}
};



int
arch_cmd_archive_snapshot (t_uchar * program_name, int argc, char * argv[])
{
  int o;
  struct opt_parsed * option;
  t_uchar * default_archive = 0;

  safe_buffer_fd (1, 0, O_WRONLY, 0);

  option = 0;

  while (1)
    {
      o = opt_standard (lim_use_must_malloc, &option, opts, &argc, argv, program_name, usage, version_string, arch_cmd_archive_snapshot_help, opt_help_msg, opt_long_help, opt_version);
      if (o == opt_none)
        break;
      switch (o)
        {
        default:
          safe_printfmt (2, "unhandled option `%s'\n", option->opt_string);
          panic ("internal error parsing arguments");

        usage_error:
          opt_usage (2, argv[0], program_name, usage, 1);
          exit (1);

          /* bogus_arg: */
          safe_printfmt (2, "ill-formed argument for `%s' (`%s')\n", option->opt_string, option->arg_string);
          goto usage_error;

        case opt_archive:
          {
            default_archive = str_save (0, option->arg_string);
            break;
          }
        }
    }

  if ((argc < 2) || (argc > 3))
    goto usage_error;

  if (default_archive)
    {
      if (!arch_valid_archive_name (default_archive))
        {
          safe_printfmt (2, "%s: invalid archive name (%s)\n",
                         argv[0], default_archive);
          exit (1);
        }
    }

  {
    t_uchar * dir;
    t_uchar * limit_spec;
    t_uchar * archive = 0;
    t_uchar * limit = 0;
    struct arch_archive * arch = 0;

    dir = argv[1];

    if (argc == 2)
      {
        archive = arch_my_default_archive (default_archive);
      }
    else
      {
        limit_spec = argv[1];

        if (!arch_valid_package_name (limit_spec, arch_maybe_archive, arch_req_category, 1))
          {
            safe_printfmt (2, "%s: invalid limit spec (%s)\n",
                           argv[0], limit_spec);
            exit (1);
          }

        archive = arch_parse_package_name (arch_ret_archive, default_archive, limit_spec);
        limit = arch_parse_package_name (arch_ret_non_archive, 0, limit_spec);
      }

    arch = arch_archive_connect (archive, 0);
    archive_snapshot (arch, limit, dir);

    arch_archive_close (arch);

    lim_free (0, archive);
    lim_free (0, limit);
  }

  lim_free (0, default_archive);
  return 0;
}




/* tag: Tom Lord Mon Jun  9 01:39:26 2003 (cmd-archive-snapshot.c)
 */
