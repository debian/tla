/* archive-cache.c:
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */


#include "hackerlab/fs/file-names.h"
#include "hackerlab/vu/safe.h"
#include "tla/libfsutils/tmp-files.h"
#include "tla/libfsutils/rmrf.h"
#include "tla/libarch/build-revision.h"
#include "tla/libarch/archive-cache.h"



void
arch_archive_cache (int chatter_fd,
                    struct arch_archive * arch,
                    const t_uchar * archive,
                    const t_uchar * revision,
                    const t_uchar * cache_dir)
{
  t_uchar * tmp_dir = 0;
  t_uchar * pristine_dir = 0;

  tmp_dir = tmp_file_name (".", ",,archive-cache-revision");
  pristine_dir = file_name_in_vicinity (0, tmp_dir, revision);

  safe_mkdir (tmp_dir, 0777);
  safe_mkdir (pristine_dir, 0777);

  arch_build_revision (chatter_fd, pristine_dir, arch, archive, revision, cache_dir);

  {
    t_uchar * errstr;

    if (arch_archive_put_cached (&errstr, arch, revision, pristine_dir))
      {
        safe_printfmt (2, "cacherev: unable to store cached revision of %s/%s (%s)\n", archive, revision, errstr);
        exit (1);
      }
  }

  rmrf_file (tmp_dir);

  lim_free (0, tmp_dir);
  lim_free (0, pristine_dir);
}




/* tag: Tom Lord Wed Jun 11 14:15:15 2003 (archive-cache.c)
 */
