/* cmdutils.h:
 *
 ****************************************************************
 * Copyright (C) 2004 Tom Lord
 * 
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */

#ifndef INCLUDE__LIBARCH__CMDUTILS_H
#define INCLUDE__LIBARCH__CMDUTILS_H

#include "tla/libarch/namespace.h"
#include "tla/libarch/archive.h"

/* automatically generated __STDC__ prototypes */
extern t_uchar * arch_fqrvsn_from_tree_and_input (t_uchar *cmd,
                                                  t_uchar * string,
                                                  t_uchar * dir);
extern void arch_check_for (struct arch_archive * arch,
                            enum arch_valid_package_name_types type,
                            t_uchar * package);
extern void arch_print_missing (struct arch_archive * arch,
                                enum arch_valid_package_name_types type,
                                enum arch_valid_package_name_types supplied_type,
                                t_uchar * spec);
extern void arch_check_library_for_revision (t_uchar * archive,
                                             t_uchar * revision);
extern t_uchar * arch_determine_revision (struct arch_archive ** arch,
                                          t_uchar * default_archive,
                                          t_uchar * revision_spec,
                                          t_uchar * cmd_name);
extern void arch_check_directory (t_uchar *path, int check_write);
extern void arch_check_uri (t_uchar * uri);
extern void arch_check_revision_local (t_uchar * archive, t_uchar * revision);
extern int arch_check_arch (struct arch_archive * arch);

extern t_uchar * str_replace (t_uchar *current, t_uchar *replacement);

extern int arch_separate_arch_name_from_fqrvsn(const t_uchar *name, t_uchar **archive, t_uchar **fqrvsn);

extern t_uchar *
arch_interpret_delta_path (t_uchar ** arch, t_uchar ** rev, t_uchar * scratch_dir, t_uchar *
    default_archive, t_uchar * spec, t_uchar * cache_dir);

#endif  /* INCLUDE__LIBARCH__CMDUTILS_H */


/* tag: Tom Lord Fri Jan  9 20:27:20 2004 (cmdutils.h)
 */
