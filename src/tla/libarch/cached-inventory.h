/* cached-inventory.h:
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */

#ifndef INCLUDE__LIBARCH__CACHED_INVENTORY_H
#define INCLUDE__LIBARCH__CACHED_INVENTORY_H


#include "hackerlab/machine/types.h"
#include "tla/libawk/relational.h"


/* automatically generated __STDC__ prototypes */
extern rel_table arch_cached_index (t_uchar * tree_root);
#endif  /* INCLUDE__LIBARCH__CACHED_INVENTORY_H */


/* tag: Tom Lord Sat Jan  5 15:26:10 2002 (cached-inventory.h)
 */
