/* arch.h:
 *
 ****************************************************************
 * Copyright (C) 2004 Tom Lord
 * 
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */

#ifndef INCLUDE__LIBARCH__ARCH_H
#define INCLUDE__LIBARCH__ARCH_H


#include "hackerlab/char/char-class.h"



static const int arch_escape_classes = char_class_control | char_class_space | char_class_non_ascii;



/* automatically generated __STDC__ prototypes */
#endif  /* INCLUDE__LIBARCH__ARCH_H */


/* tag: Tom Lord Wed May 12 11:42:33 2004 (arch.h)
 */
