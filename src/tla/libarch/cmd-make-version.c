/* cmd-make-version.c
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */


#include "config-options.h"
#include "hackerlab/cmd/main.h"
#include "tla/libarch/namespace.h"
#include "tla/libarch/project-tree.h"
#include "tla/libarch/my.h"
#include "tla/libarch/archive.h"
#include "tla/libarch/cmd-make-version.h"



static t_uchar * usage = "[options] version";
static t_uchar * version_string = (cfg__std__package " from regexps.com\n"
                                   "\n"
                                   "Copyright 2003 Tom Lord\n"
                                   "\n"
                                   "This is free software; see the source for copying conditions.\n"
                                   "There is NO warranty; not even for MERCHANTABILITY or FITNESS FOR A\n"
                                   "PARTICULAR PURPOSE.\n"
                                   "\n"
                                   "Report bugs to " cfg__tla_bug_mail ".\n"
                                   "\n"
                                   cfg__std__release_id_string
                                   "\n");

#define OPTS(OP) \
  OP (opt_help_msg, "h", "help", 0, \
      "Display a help message and exit.") \
  OP (opt_long_help, "H", 0, 0, \
      "Display a verbose help message and exit.") \
  OP (opt_version, "V", "version", 0, \
      "Display a release identifier string\n" \
      "and exit.") \
  OP (opt_archive, "A", "archive", 1, \
      "Override `my-default-archive'")


t_uchar arch_cmd_make_version_help[] = ("create a new archive version\n"
                                        "Create VERSION as a version in the indicated archive.\n"
                                        "The branch for this version must not already exist.\n");

enum options
{
  OPTS (OPT_ENUM)
};

static struct opt_desc opts[] =
{
  OPTS (OPT_DESC)
    {-1, 0, 0, 0, 0}
};



int
arch_cmd_make_version (t_uchar * program_name, int argc, char * argv[])
{
  int o;
  struct opt_parsed * option;
  t_uchar * default_archive;


  default_archive = 0;

  safe_buffer_fd (1, 0, O_WRONLY, 0);

  option = 0;

  while (1)
    {
      o = opt_standard (lim_use_must_malloc, &option, opts, &argc, argv, program_name, usage, version_string, arch_cmd_make_version_help, opt_help_msg, opt_long_help, opt_version);
      if (o == opt_none)
        break;
      switch (o)
        {
        default:
          safe_printfmt (2, "unhandled option `%s'\n", option->opt_string);
          panic ("internal error parsing arguments");

        usage_error:
          opt_usage (2, argv[0], program_name, usage, 1);
          exit (1);

          /* bogus_arg: */
          safe_printfmt (2, "ill-formed argument for `%s' (`%s')\n", option->opt_string, option->arg_string);
          goto usage_error;

        case opt_archive:
          {
            default_archive = str_save (0, option->arg_string);
            break;
          }
        }
    }

  if (argc != 2)
    goto usage_error;

  default_archive = arch_my_default_archive (default_archive);

  {
    t_uchar * version_spec;
    t_uchar * archive = 0;
    t_uchar * version = 0;
    struct arch_archive * arch = 0;
    t_uchar * errstr = 0;

    version_spec = argv[1];

    if (!arch_valid_package_name (version_spec, arch_maybe_archive, arch_req_version, 0))
      {
        safe_printfmt (2, "%s: invalid version name (%s)\n",
                       argv[0], version_spec);
        exit (2);
      }

    if (arch_is_system_package_name (version_spec))
      {
        safe_printfmt (2, "%s: user's can not create system versions\n  branch: %s\n", argv[0], version_spec);
        exit (2);
      }

    archive = arch_parse_package_name (arch_ret_archive, default_archive, version_spec);
    version = arch_parse_package_name (arch_ret_package_version, 0, version_spec);

    arch = arch_archive_connect (archive, 0);
    if (arch_make_version (&errstr, arch, version))
      {
        safe_printfmt (2, "%s: unable to create version %s (%s)\n  archive: %s\n",
                       argv[0], version, errstr, archive);
        exit (1);
      }
  }

  return 0;
}




/* tag: Tom Lord Wed May 21 09:17:22 2003 (make-version.c)
 */
