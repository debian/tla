/* cmd-missing.c
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */


#include "config-options.h"
#include "hackerlab/cmd/main.h"
#include "hackerlab/fs/file-names.h"
#include "tla/libfsutils/rmrf.h"
#include "tla/libarch/namespace.h"
#include "tla/libarch/project-tree.h"
#include "tla/libarch/patch-logs.h"
#include "tla/libarch/local-cache.h"
#include "tla/libarch/make-changeset.h"
#include "tla/libarch/archive.h"
#include "tla/libarch/missing.h"
#include "tla/libarch/changeset-report.h"
#include "tla/libarch/cmdutils.h"
#include "tla/libarch/cmd-missing.h"



static t_uchar * usage = "[options] [version]";
static t_uchar * version_string = (cfg__std__package " from regexps.com\n"
                                   "\n"
                                   "Copyright 2003 Tom Lord\n"
                                   "\n"
                                   "This is free software; see the source for copying conditions.\n"
                                   "There is NO warranty; not even for MERCHANTABILITY or FITNESS FOR A\n"
                                   "PARTICULAR PURPOSE.\n"
                                   "\n"
                                   "Report bugs to " cfg__tla_bug_mail ".\n"
                                   "\n"
                                   cfg__std__release_id_string
                                   "\n");

#define OPTS(OP) \
  OP (opt_help_msg, "h", "help", 0, \
      "Display a help message and exit.") \
  OP (opt_long_help, "H", 0, 0, \
      "Display a verbose help message and exit.") \
  OP (opt_version, "V", "version", 0, \
      "Display a release identifier string\n" \
      "and exit.") \
  OP (opt_archive, "A", "archive", 1, \
      "Override `my-default-archive'") \
  OP (opt_quiet, "q", "quiet", 0, \
      "produce no ordinary output") \
  OP (opt_exit_status, "x", "exit-status", 0, \
      "exit non-0 if patches are missing") \
  OP (opt_dir, "d", "dir DIR", 1, \
      "cd to DIR first") \
  OP (opt_reverse, "r", "reverse", 0, \
      "sort from newest to oldest") \
  OP (opt_summary, "s", "summary", 0, \
      "display a summary of each missing patch") \
  OP (opt_creator, "c", "creator", 0, \
      "display the creator of each missing patch") \
  OP (opt_date, "D", "date", 0, \
      "display the date of each missing patch") \
  OP (opt_full, "f", "full", 0, \
      "print full revision names") \
  OP (opt_merges, 0, "merges", 0, \
      "print a merge list for each missing patch") \
  OP (opt_skip_present, 0, "skip-present", 0, \
      "skip patches that contain 1 or more patch logs already in this tree")

t_uchar arch_cmd_missing_help[] = ("print patches missing from a project tree\n"
                                   "Print a list of patches missing in the project tree containing\n"
                                   "DIR (or the current directory) for VERSION (or the default version.\n"
                                   "of the project tree).\n"
                                   "\n"
                                   "The flag --merges means, for each patch, to print the list of patches\n"
                                   "included in the patch in two columns.  For example:\n"
                                   "\n"
                                   "        PATCH-A        PATCH-A\n"
                                   "        PATCH-A        PATCH-B\n"
                                   "        PATCH-A        PATCH-C\n"
                                   "\n"
                                   "means that PATCH-A includes the changes from PATCH-B and PATCH-C.\n"
                                   "(Every patch includes at least itself.)\n"
                                   "\n"
                                   "With -x, if there are missing patches, the command exits with\n"
                                   "status 1, otherwise with status 0.\n"
                                   "\n"
                                   "With -q, produce no output.\n"
                                   "\n"
                                   "WARNING: At this time, some error conditions *also* exit with\n"
                                   "error status 1, however, in situations where the caller is\n"
                                   "not concerned with errors, the exit status is still useful.\n");

enum options
{
  OPTS (OPT_ENUM)
};

static struct opt_desc opts[] =
{
  OPTS (OPT_DESC)
    {-1, 0, 0, 0, 0}
};



int
arch_cmd_missing (t_uchar * program_name, int argc, char * argv[])
{
  int o;
  struct opt_parsed * option;
  t_uchar * default_archive = 0;
  t_uchar * dir = 0;
  int exit_status = 0;
  int exit_status_significant = 0;
  int quiet = 0;
  int reverse = 0;
  int summarized_headers = 0;
  int full = 0;
  int merges = 0;
  int skip_present = 0;

  dir = str_save (0, ".");

  safe_buffer_fd (1, 0, O_WRONLY, 0);

  option = 0;

  while (1)
    {
      o = opt_standard (lim_use_must_malloc, &option, opts, &argc, argv, program_name, usage, version_string, arch_cmd_missing_help, opt_help_msg, opt_long_help, opt_version);
      if (o == opt_none)
        break;
      switch (o)
        {
        default:
          safe_printfmt (2, "unhandled option `%s'\n", option->opt_string);
          panic ("internal error parsing arguments");

        usage_error:
          opt_usage (2, argv[0], program_name, usage, 1);
          exit (1);

          /* bogus_arg: */
          safe_printfmt (2, "ill-formed argument for `%s' (`%s')\n", option->opt_string, option->arg_string);
          goto usage_error;


        case opt_archive:
          {
            lim_free (0, default_archive);
            default_archive = str_save (0, option->arg_string);
            break;
          }

        case opt_dir:
          {
            lim_free (0, dir);
            dir = str_save (0, option->arg_string);
            break;
          }

        case opt_quiet:
          {
            quiet = 1;
            break;
          }

        case opt_exit_status:
          {
            exit_status_significant = 1;
            break;
          }

        case opt_full:
          {
            full = 1;
            break;
          }

        case opt_reverse:
          {
            reverse = 1;
            break;
          }
        case opt_summary:
          {
            summarized_headers |= arch_include_summary;
            break;
          }
        case opt_creator:
          {
            summarized_headers |= arch_include_creator;
            break;
          }
        case opt_date:
          {
            summarized_headers |= arch_include_date;
            break;
          }
        case opt_merges:
          {
            merges = 1;
            break;
          }
        case opt_skip_present:
          {
            skip_present = 1;
            break;
          }
        }
    }

  if (argc > 2)
    goto usage_error;

  {
    t_uchar * tree_root = 0;
    t_uchar * vsnspec = 0;
    t_uchar * archive = 0;
    t_uchar * version= 0;
    struct arch_archive * arch = 0;
    rel_table whats_missing = rel_table_nil;

    if (default_archive && !arch_valid_archive_name (default_archive))
      {
        safe_printfmt (2, "%s: invalid archive name: %s\n",
                       argv[0], default_archive);
        exit (1);
      }

    tree_root = arch_tree_root (0, dir, 0);

    if (!tree_root)
      {
        safe_printfmt (2, "%s: not in a project tree\n  dir: %s\n",
                       argv[0], dir);
        exit (1);
      }

    if (argc == 2)
      vsnspec = str_save (0, argv[1]);
    else
      {
        vsnspec = arch_tree_version (tree_root);
        if (!vsnspec)
          {
            safe_printfmt (2, "%s: tree has no default version\n  tree: %s\n",
                           argv[0], tree_root);
            exit (1);
          }
      }

    if (!arch_valid_package_name (vsnspec, arch_maybe_archive, arch_req_version, 0))
      {
        safe_printfmt (2, "%s: illegal version name: %s\n", argv[0], vsnspec);
        exit (2);
      }

    archive = arch_parse_package_name (arch_ret_archive, default_archive, vsnspec);
    version = arch_parse_package_name (arch_ret_non_archive, 0, vsnspec);


    arch = arch_archive_connect (archive, 0);
    arch_check_for (arch, arch_req_version, version);
    
    whats_missing = arch_missing (tree_root, arch, version, skip_present);

    if (rel_n_records (whats_missing))
      {
        exit_status = 1;
      }

    if (!quiet)
      {
        if (reverse)
          arch_sort_table_by_patch_level_field (1, whats_missing, 0);

        {
          int x;

          for (x = 0; x < rel_n_records (whats_missing); ++x)
            {
              if (!summarized_headers)
                {
                  if (full)
                    safe_printfmt (1, "%s/%s--%s\n", archive, version, rel_peek_str (whats_missing, x, 0));
                  else
                    safe_printfmt (1, "%s\n", rel_peek_str (whats_missing, x, 0));
                }
              else
                {
                  t_uchar * revision = 0;
                  t_uchar * log = 0;
                  t_uchar * body = 0;
                  assoc_table headers = 0;

                  revision = str_alloc_cat_many (0, version, "--", rel_peek_str (whats_missing, x, 0), str_end);
                  log = arch_archive_log (arch, revision);

                  arch_parse_log (0, &headers, (const t_uchar **)&body, log);

                  if (!merges)
                    {
                      if (full)
                        safe_printfmt (1, "%s/%s--%s\n", archive, version, rel_peek_str (whats_missing, x, 0));
                      else
                        safe_printfmt (1, "%s\n", rel_peek_str (whats_missing, x, 0));
                    }
                  else
                    {
                      panic ("--merge not yet");
                    }


                  arch_print_headers_summary (1, 4, headers, summarized_headers);

                  lim_free (0, revision);
                  lim_free (0, log);
                  lim_free (0, body);
                  free_assoc_table (headers);
                }
            }
        }
      }

    arch_archive_close (arch);

    lim_free (0, vsnspec);
    lim_free (0, tree_root);
    lim_free (0, archive);
    lim_free (0, version);
    rel_free_table (whats_missing);
  }


  lim_free (0, dir);
  lim_free (0, default_archive);

  if (exit_status_significant)
    return exit_status;
  else
    return 0;
}



/* tag: Tom Lord Sat May 24 23:36:40 2003 (whats-missing.c)
 */
