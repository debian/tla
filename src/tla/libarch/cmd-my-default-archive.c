/* cmd-my-default-archive.c
 *
 ****************************************************************
 * Copyright (C) 2003 Tom Lord
 *
 * See the file "COPYING" for further information about
 * the copyright and warranty status of this work.
 */


#include "config-options.h"
#include "hackerlab/cmd/main.h"
#include "tla/libarch/my.h"
#include "tla/libarch/namespace.h"
#include "tla/libarch/cmd-my-default-archive.h"



static t_uchar * usage = "[options] [archive]";
static t_uchar * version_string = (cfg__std__package " from regexps.com\n"
                                   "\n"
                                   "Copyright 2003 Tom Lord\n"
                                   "\n"
                                   "This is free software; see the source for copying conditions.\n"
                                   "There is NO warranty; not even for MERCHANTABILITY or FITNESS FOR A\n"
                                   "PARTICULAR PURPOSE.\n"
                                   "\n"
                                   "Report bugs to " cfg__tla_bug_mail ".\n"
                                   "\n"
                                   cfg__std__release_id_string
                                   "\n");

#define OPTS(OP) \
  OP (opt_help_msg, "h", "help", 0, \
      "Display a help message and exit.") \
  OP (opt_long_help, "H", 0, 0, \
      "Display a verbose help message and exit.") \
  OP (opt_version, "V", "version", 0, \
      "Display a release identifier string\n" \
      "and exit.") \
  OP (opt_archive, "A", "archive", 1, \
      "Override `my-default-archive'") \
  OP (opt_errname, "e", "errname", 1, \
      "specify program name for errors") \
  OP (opt_delete, "d", "delete", 0, \
      "unspecify your default archive") \
  OP (opt_silent, "s", "silent", 0, \
      "suppress reassuring messages")

t_uchar arch_cmd_my_default_archive_help[] = ("print or change your default archive\n"
                                              "With no argument, and without -d, print the name of your default\n"
                                              "archive.\n"
                                              "\n"
                                              "With an argument, record ARCHIVE as your default archive\n"
                                              "in ~/.arch-params/=default-archive\n"
                                              "\n"
                                              "With the option -d (--delete) and no argument, ensure that\n"
                                              "you do not have a default archive set in ~/.arch-params.\n"
                                              "\n"
                                              "Your default archive is determined this way:\n"
                                              "\n"
                                              "If the option -A (--archive) is given and not empty, that archive\n"
                                              "is the default (which makes this script useful for processing a -A\n"
                                              "argument that was passed to another script).\n"
                                              "\n"
                                              "If -A is not given, but ~/.arch-params/=default-archive exists\n"
                                              "and is not empty, that is your default archive.\n"
                                              "\n"
                                              "Otherwise, your default archive is the name of the local archive\n"
                                              "rooted at the argument to -R (--root) or specified in the environment\n"
                                              "variable ARCHROOT.\n"
                                              "\n"
                                              "If no default archive can be found by any of these means, the\n"
                                              "program exits with status 1, printing an error message unless\n"
                                              "the -s (--silent) option is given.\n");

enum options
{
  OPTS (OPT_ENUM)
};

static struct opt_desc opts[] =
{
  OPTS (OPT_DESC)
    {-1, 0, 0, 0, 0}
};



enum operation
{
  op_print,
  op_set,
  op_delete,
};

int
arch_cmd_my_default_archive (t_uchar * program_name, int argc, char * argv[])
{
  int o;
  struct opt_parsed * option;
  char * default_archive;
  char * errname;
  enum operation op = op_print;
  int silent;

  default_archive = 0;
  errname = argv[0];
  silent = 0;

  safe_buffer_fd (1, 0, O_WRONLY, 0);

  option = 0;

  while (1)
    {
      o = opt_standard (lim_use_must_malloc, &option, opts, &argc, argv, program_name, usage, version_string, arch_cmd_my_default_archive_help, opt_help_msg, opt_long_help, opt_version);
      if (o == opt_none)
        break;
      switch (o)
        {
        default:
          safe_printfmt (2, "unhandled option `%s'\n", option->opt_string);
          panic ("internal error parsing arguments");

        usage_error:
          opt_usage (2, argv[0], program_name, usage, 1);
          exit (1);

          /* bogus_arg: */
          safe_printfmt (2, "ill-formed argument for `%s' (`%s')\n", option->opt_string, option->arg_string);
          goto usage_error;


        case opt_archive:
          {
            default_archive = str_save (0, option->arg_string);
            break;
          }

        case opt_errname:
          {
            if (!option->arg_string[0])
              errname = 0;
            else
              errname = str_save (0, option->arg_string);
            break;
          }

        case opt_delete:
          {
            op = op_delete;
            break;
          }

        case opt_silent:
          {
            silent = 1;
            break;
          }
        }
    }

  if (op == op_delete)
    {
      if (argc != 1)
        goto usage_error;
    }
  else if (argc == 2)
    {
      op = op_set;
      default_archive = argv[1];
    }
  else if (argc == 1)
    {
      op = op_print;
    }
  else
    goto usage_error;

  /****************************************************************
   * Sanity Check
   */

  if (((op == op_set) && !default_archive) || (default_archive && !arch_valid_archive_name (default_archive)))
    {
      if (errname)
        safe_printfmt (2, "%s: invalid archive name (%s)\n", errname, default_archive);
      exit (1);
    }

  /****************************************************************
   * Do It
   */

  switch (op)
    {
    default:
      {
        panic ("my-default-archive: unreachable op case");
        break;
      }

    case op_delete:
      {
        arch_delete_my_default_archive ();
        if (!silent)
          safe_printfmt (1, "default archive removed\n");
        break;
      }

    case op_print:
      {
        default_archive = arch_my_default_archive (default_archive);
        if (default_archive)
          safe_printfmt (1, "%s\n", default_archive);
        else
          {
            if (errname)
              safe_printfmt (2, "%s: no default archive\n", errname);
            exit (1);
          }
        break;
      }

    case op_set:
      {
        arch_set_my_default_archive (default_archive);
        break;
      }
    }

  return 0;
}




/* tag: Tom Lord Mon May 12 15:14:03 2003 (my-default-archive.c)
 */
