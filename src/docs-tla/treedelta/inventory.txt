  [[title
                    /6.1/ The Inventory Subsystem
  ]]

 <"*(prev chapter)*" -- $/hacking/branch-publishing.html> <"*(next)*" -- $/treedelta/naming-conventions.html>


  The `commit' operation records both changes to the contents
  of files and changes to the structure of the source tree: file
  additions, deletions, renames, and some meta-data changes are
  also recorded.

  To recognize a tree restructuring operation such as a file rename, 
  Arch makes use of the concept of a logical *inventory id* associated
  with each file.   An inventory id is an alternative name for a file
  but, unlike the ordinary filename, the inventory name does not
  change if the file is renamed.

* A List of Inventory IDs

  A list of the inventory id associated with each source file in a
  tree is obtained by the (highly versatile) `inventory' command:

  [[tty
    % tla inventory -sfd --ids 
  ]]  


* Commands Discussed

  [[tty
    % tla inventory -H
  ]]


 <"*(prev chapter)*" -- $/hacking/branch-publishing.html> <"*(next)*" -- $/treedelta/naming-conventions.html>


* Copyright

  Copyright /(C)/ 2005 Tom Lord (`lord@emf.net')

  This program is free software; you can redistribute it and/or modify
  it under the terms of the /GNU General Public License/ as published by
  the Free Software Foundation; either version 2, or (at your option)
  any later version.

  This program is distributed in the hope that it will be useful,
  but \\/WITHOUT ANY WARRANTY/\\; without even the implied warranty of
  \\/MERCHANTABILITY/\\ or \\/FITNESS FOR A PARTICULAR PURPOSE/\\.  See the
  /GNU General Public License/ for more details.

  You should have received a copy of the /GNU General Public License/
  along with this software; see the file <"`COPYING'" --
  $/COPYING.html>.  If not, write to the Free Software Foundation, 675
  Mass Ave, Cambridge, MA 02139, USA.

 <"*(prev chapter)*" -- $/hacking/branch-publishing.html> <"*(next)*" -- $/treedelta/naming-conventions.html>

 [[null
   ; arch-tag: Tom Lord Thu Mar 31 10:00:35 2005 (inventory.txt)
 ]]
