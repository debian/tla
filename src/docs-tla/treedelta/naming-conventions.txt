  [[title
        /6.2/ Naming Convention and Inventory ID Configuration
  ]]

 <"*(prev)*" -- $/treedelta/inventory.html> <"*(next)*" -- $/treedelta/add.html>


  When arch examines a source tree, it distinguishes source files from
  build byproducts, temporary files and so forth by a set of *file
  naming and inventory id assignment conventions*.

  For example, one can configure a project tree so that all `*.c'
  files are considered to be source files while no `*.o' files are.

  These configuration settings control the output of the `inventory'
  command.


* The `id-tagging-method' Parameter

  The most important inventory configuration parameter is the
  *inventory ID tagging method*.   The value of that parameter
  for a given tree is reported by:

  [[tty
    % tla id-tagging-method
  ]]

  That command produces a single line of output which is
  one of the strings:

  [[tty
    explicit
    implicit
    tagline
    names
  ]]


* Inventory Rules and the `{arch}/=tagging-method' File

  Inventory rules (the naming and inventory id configuration) of a 
  project tree is stored just below the top level directory of the 
  project tree in the file `{arch}/=tagging-method'.

  The default contents of that file contain extensive comments 
  describing the configuration parameters in detail.   Those
  contents (with the comments) are printed by:

  [[tty
    % tla id-tagging-defaults
  ]]


* Violations of Naming Conventions

  It is possible to create files or directories which "violate" the
  naming and id conventions of a tree.   In that case, some operations
  (such as `commit') may be unable to complete.

  In additions to outright violations, the naming conventions
  sometimes notice files or directories which merit a warning -- as
  possible mistakes -- but which should not prevent operations like
  `commit' from completing.

  Naming convention errors and warnings are reported by:

  [[tty
    % tla tree-lint
  ]]



* Commands Discussed

  [[tty
    % tla id-tagging-method -H
    % tla id-tagging-defaults -H
    % tla tree-lint -H
  ]]


* Configuration Files Discussed

  [[tty
    $proj/{arch}/=tagging-method
  ]]


 <"*(prev)*" -- $/treedelta/inventory.html> <"*(next)*" -- $/treedelta/add.html>

* Copyright

  Copyright /(C)/ 2005 Tom Lord (`lord@emf.net')

  This program is free software; you can redistribute it and/or modify
  it under the terms of the /GNU General Public License/ as published by
  the Free Software Foundation; either version 2, or (at your option)
  any later version.

  This program is distributed in the hope that it will be useful,
  but \\/WITHOUT ANY WARRANTY/\\; without even the implied warranty of
  \\/MERCHANTABILITY/\\ or \\/FITNESS FOR A PARTICULAR PURPOSE/\\.  See the
  /GNU General Public License/ for more details.

  You should have received a copy of the /GNU General Public License/
  along with this software; see the file <"`COPYING'" --
  $/COPYING.html>.  If not, write to the Free Software Foundation, 675
  Mass Ave, Cambridge, MA 02139, USA.

 <"*(prev)*" -- $/treedelta/inventory.html> <"*(next)*" -- $/treedelta/add.html>

 [[null
   ; arch-tag: Tom Lord Thu Mar 31 10:59:26 2005 (naming-conventions.txt)
 ]]
