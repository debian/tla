 [[title
           /3.2/ Registering Archives and Creating Mirrors
 ]]

 <"*(prev)*" -- $/archreg/coords.html> <"*(next)*" -- $/archreg/mirrormgt.html>


 Given the coordinates for an upstream project, the most usual
 course is to create a *local mirror* of that project's archive.
 This allows you update your mirror "in batch mode" and, between
 updates, access the data at local-disk speeds.

 Here is how to create mirror of one of the two GNU Arch project
 archives named in the previous section.  (To follow the examples in
 the handbook, you will need to have mirrors of both archives.)

 [[tty
    % tla register-archive lord@emf.net--2005-SOURCE \
      http://www.gnuarch.org/archives/lord@emf.net--2005

    % tla make-archive --mirror lord@emf.net--2005-SOURCE \
      $MIRROR_DIR/lord@emf.net--2005
 ]]

 You can see what archives you have already registered:

 [[tty
    % tla archives
      lord@emf.net--2005-SOURCE
          http://www.gnuarch.org/archives/lord@emf.net--2005
      lord@emf.net--2005
          /home/lord/mirrored-archives/lord@emf.net--2005
 ]]

 [[cartouche
   /Heads Up!/

   This section has /not/ taught you about "archive signing" -- a 
   technique for cryptographically verifying the contents of 
   archives.

   Some examples in this manual will generate warnings
   telling you that you are not checking signatures.
   Later examples will show you how you *can* check signatures
   (and avoid those warnings).
 ]]


* Commands Discussed

  [[tty
     % tla register-archive -H
     % tla make-archive -H
     % tla archives -H
  ]]


* See Also

  <"/2.3/ Pick a Default Mirror Location" -- $/my/mirrorloc.html>

  <"/3.1/ Coordinates for Upstream Projects" -- $/archreg/coords.html>



 <"*(prev)*" -- $/archreg/coords.html> <"*(next)*" -- $/archreg/mirrormgt.html>

* Copyright

  Copyright /(C)/ 2005 Tom Lord (`lord@emf.net')

  This program is free software; you can redistribute it and/or modify
  it under the terms of the /GNU General Public License/ as published by
  the Free Software Foundation; either version 2, or (at your option)
  any later version.

  This program is distributed in the hope that it will be useful,
  but \\/WITHOUT ANY WARRANTY/\\; without even the implied warranty of
  \\/MERCHANTABILITY/\\ or \\/FITNESS FOR A PARTICULAR PURPOSE/\\.  See the
  /GNU General Public License/ for more details.

  You should have received a copy of the /GNU General Public License/
  along with this software; see the file <"`COPYING'" --
  $/COPYING.html>.  If not, write to the Free Software Foundation, 675
  Mass Ave, Cambridge, MA 02139, USA.

 <"*(prev)*" -- $/archreg/coords.html> <"*(next)*" -- $/archreg/mirrormgt.html>

 [[null
   ; arch-tag: Tom Lord Sat Mar 26 09:28:35 2005 (register.txt)
 ]]
