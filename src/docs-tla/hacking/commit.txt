  [[title
                    /5.5/ Committing Your Changes
  ]]

 <"*(prev)*" -- $/hacking/changes.html> <"*(next)*" -- $/hacking/merge-from-upstream.html>


  Having made local changes in a project tree, you can save a record
  of those changes in your branch by *committing a revision*.
  Committing a revision, in addition to recording changes in an 
  archive, adds a *patch log entry* to the tree for those changes,
  as illustrated in the example:

  [[tty
    % tla tree-version
    lord@gnu.org--my-hacks-2005/tla--MYHACK--1.3.1

    % tla logs -s -r 
    base-0
        tag of lord@emf.net--2005/tla--fix-1--1.3.1--patch-1
    % tla commit -L 'touch up COPYING and the top level Makefile.in'

    ...

    % tla logs -s -r
    patch-1
        touch up COPYING and the top level Makefile.in
    base-0
        tag of lord@emf.net--2005/tla--fix-1--1.3.1--patch-1
  ]]



* Commands Discussed

  [[tty
    tty logs -H
    tty commit -H
  ]]

 <"*(prev)*" -- $/hacking/changes.html> <"*(next)*" -- $/hacking/merge-from-upstream.html>

* Copyright

  Copyright /(C)/ 2005 Tom Lord (`lord@emf.net')

  This program is free software; you can redistribute it and/or modify
  it under the terms of the /GNU General Public License/ as published by
  the Free Software Foundation; either version 2, or (at your option)
  any later version.

  This program is distributed in the hope that it will be useful,
  but \\/WITHOUT ANY WARRANTY/\\; without even the implied warranty of
  \\/MERCHANTABILITY/\\ or \\/FITNESS FOR A PARTICULAR PURPOSE/\\.  See the
  /GNU General Public License/ for more details.

  You should have received a copy of the /GNU General Public License/
  along with this software; see the file <"`COPYING'" --
  $/COPYING.html>.  If not, write to the Free Software Foundation, 675
  Mass Ave, Cambridge, MA 02139, USA.

 <"*(prev)*" -- $/hacking/changes.html> <"*(next)*" -- $/hacking/merge-from-upstream.html>

 [[null
   ; arch-tag: Tom Lord Wed Mar 30 13:49:53 2005 (commit.txt)
 ]]
