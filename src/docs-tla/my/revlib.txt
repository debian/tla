 [[title
                   /2.4/ Set Up a Revision Library
 ]]

 <"*(prev)*" -- $/my/mirrorloc.html> <"*(next chapter)*" -- $/archreg/coords.html>


* Local Caching

  Arch's best performance is obtained by allocating for it
  substantial on-disk storage for a *revision library*.

  [[blockquote
    /Note:/ It is not *required* that you create a revision 
    library;  it is *usually* a performance-win (trading 
    disk-space for speed) to do so.
  ]]

  Revision libraries are created by commands such as:

  [[tty
    % mkdir ~/Revlib
    % tla my-revision-library ~/Revlib
    % tla library-config --greedy --sparse ~/Revlib
  ]]


* Commands Discussed

  [[tty
    % tla my-revision-library -H
    % tla library-config -H
  ]]



 <"*(prev)*" -- $/my/mirrorloc.html> <"*(next chapter)*" -- $/archreg/coords.html>


* Copyright

  Copyright /(C)/ 2005 Tom Lord (`lord@emf.net')

  This program is free software; you can redistribute it and/or modify
  it under the terms of the /GNU General Public License/ as published by
  the Free Software Foundation; either version 2, or (at your option)
  any later version.

  This program is distributed in the hope that it will be useful,
  but \\/WITHOUT ANY WARRANTY/\\; without even the implied warranty of
  \\/MERCHANTABILITY/\\ or \\/FITNESS FOR A PARTICULAR PURPOSE/\\.  See the
  /GNU General Public License/ for more details.

  You should have received a copy of the /GNU General Public License/
  along with this software; see the file <"`COPYING'" --
  $/COPYING.html>.  If not, write to the Free Software Foundation, 675
  Mass Ave, Cambridge, MA 02139, USA.

 <"*(prev)*" -- $/my/mirrorloc.html> <"*(next chapter)*" -- $/archreg/coords.html>

 [[null
   ; arch-tag: Tom Lord Thu Mar 24 14:01:57 2005 (my/revlib.txt)
 ]]
