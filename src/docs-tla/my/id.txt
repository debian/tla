 [[title
	/2.1/ Set Your User Identity
 ]]

 <"*(prev chapter)*" -- $/build/help.html> <"*(next)*" -- $/my/archloc.html>


 You *must* set your user id.   An arch user id looks like
 a pretty-printed email address.  Here is my current arch id:

 [[tty
	Thomas Lord <lord@emf.net>
 ]]

 The part in angle brackets is mandatory.   At this time,
 you must be fairly conservative in the characters you use
 in your id (sorry -- we're working on it but don't want to 
 liberalize the standard carelessly).

* How to Do It

  [[tty
	% tla my-id "Thomas Lord <lord@emf.net>"
  ]]


* Commands Discussed

  [[tty
	% tla my-id -H
  ]]


 <"*(prev chapter)*" -- $/build/help.html> <"*(next)*" -- $/my/archloc.html>

* Copyright

  Copyright /(C)/ 2005 Tom Lord (`lord@emf.net')

  This program is free software; you can redistribute it and/or modify
  it under the terms of the /GNU General Public License/ as published by
  the Free Software Foundation; either version 2, or (at your option)
  any later version.

  This program is distributed in the hope that it will be useful,
  but \\/WITHOUT ANY WARRANTY/\\; without even the implied warranty of
  \\/MERCHANTABILITY/\\ or \\/FITNESS FOR A PARTICULAR PURPOSE/\\.  See the
  /GNU General Public License/ for more details.

  You should have received a copy of the /GNU General Public License/
  along with this software; see the file <"`COPYING'" --
  $/COPYING.html>.  If not, write to the Free Software Foundation, 675
  Mass Ave, Cambridge, MA 02139, USA.

 <"*(prev chapter)*" -- $/build/help.html> <"*(next)*" -- $/my/archloc.html>

 [[null
   ; arch-tag: Tom Lord Thu Mar 24 12:53:03 2005 (id.txt)
 ]]
