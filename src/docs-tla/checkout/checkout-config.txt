 [[title
                 /4.2/ Check Out a Multi-Project Tree
 ]]

 <"*(prev)*" -- $/checkout/checkout.html> <"*(next)*" -- $/checkout/tree-state.html>


 The `get' command checks out a single *arch project* which, as the 
 previous section illustrates, may be less than a complete tree.

 Complete trees are often comprised of *multiple project trees*
 which are *combined into a single tree by an arch configuration*.

 Upstream maintainers create `config'-files: instructions for
 building a multi-project tree from sources controlled by arch.

 Typically (but not always) the config files are themselves 
 controlled by `arch', as illustrated by this example which 
 builds a complete (multi-project) arch source tree:

 [[tty
   % tla get lord@emf.net--2005/rel--tla--1.3.1  tla-dist
   % cd tla-dist
   % tla buildcfg ./config
 ]]

 /Suggestion:/ Examine the files `./config' and `./config.template'
 in the `tla-dist' directory to see some example config files.

 
* Commands Discussed

  [[tty
    % tla buildcfg -H
  ]]

* See Also

  <"/\Appendix A:\ Arch Terminology and the Global Namespace of Arch Revisions/ " -- $/glossary/glossary.html>


 <"*(prev)*" -- $/checkout/checkout.html> <"*(next)*" -- $/checkout/tree-state.html>

* Copyright

  Copyright /(C)/ 2005 Tom Lord (`lord@emf.net')

  This program is free software; you can redistribute it and/or modify
  it under the terms of the /GNU General Public License/ as published by
  the Free Software Foundation; either version 2, or (at your option)
  any later version.

  This program is distributed in the hope that it will be useful,
  but \\/WITHOUT ANY WARRANTY/\\; without even the implied warranty of
  \\/MERCHANTABILITY/\\ or \\/FITNESS FOR A PARTICULAR PURPOSE/\\.  See the
  /GNU General Public License/ for more details.

  You should have received a copy of the /GNU General Public License/
  along with this software; see the file <"`COPYING'" --
  $/COPYING.html>.  If not, write to the Free Software Foundation, 675
  Mass Ave, Cambridge, MA 02139, USA.

 <"*(prev)*" -- $/checkout/checkout.html> <"*(next)*" -- $/checkout/tree-state.html>

 [[null
   ; arch-tag: Tom Lord Sat Mar 26 18:37:40 2005 (checkout-config.txt)
 ]]
