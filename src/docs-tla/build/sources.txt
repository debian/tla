 [[title
                       /1.2/ Obtaining Sources
 ]]

 <"*(prev)*" -- $/build/why.html> <"*(next)*" -- $/build/othertools.html>

 Check <ftp://ftp.gnu.org/gnu/gnu-arch> and use the highest numbered
 version that doesn't have a suffix of the form "pre<N>".  E.g., 
 pick `1.5' rather than `1.6pre5'.

 See also <http://www.gnu.org/software/gnu-arch>

 See also <http://www.gnuarch.org>

 See also <http://wiki.gnuarch.org>

 See also <http://savannah.gnu.org/projects/gnu-arch>


 <"*(prev)*" -- $/build/why.html> <"*(next)*" -- $/build/othertools.html>

* Copyright

  Copyright /(C)/ 2005 Tom Lord (`lord@emf.net')

  This program is free software; you can redistribute it and/or modify
  it under the terms of the /GNU General Public License/ as published by
  the Free Software Foundation; either version 2, or (at your option)
  any later version.

  This program is distributed in the hope that it will be useful,
  but \\/WITHOUT ANY WARRANTY/\\; without even the implied warranty of
  \\/MERCHANTABILITY/\\ or \\/FITNESS FOR A PARTICULAR PURPOSE/\\.  See the
  /GNU General Public License/ for more details.

  You should have received a copy of the /GNU General Public License/
  along with this software; see the file <"`COPYING'" --
  $/COPYING.html>.  If not, write to the Free Software Foundation, 675
  Mass Ave, Cambridge, MA 02139, USA.

 <"*(prev)*" -- $/build/why.html> <"*(next)*" -- $/build/othertools.html>


 [[null
   ; arch-tag: Tom Lord Thu Mar 24 12:21:33 2005 (sources.txt)
 ]]
