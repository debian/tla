<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
    <title>Tom Lord's Hackery</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link href="../../../gnuarch.css" rel="stylesheet" type="text/css">
  </head>

  <body>

    <h1 class="page-title"><u>Tom Lord's Hackery</u></h1>

    <div class="mainContent">

<h1 class=essay-title>
  Inter-Call State in <code>libarch</code><br>
  
</h1>

<p>
  Suppose that you write a <code>libarch</code> client which calls a function
  to set the <i>default archive</i>, overriding the setting in
  <code>~/.arch-params</code> (for your client only).




<p>
  <code>libarch</code> returns from that call and your client makes another, this
  time to the librified equivalent of the <code>categories</code> command.




<p>
  The second call to <code>libarch</code> will have to &quot;remember&quot; the default
  archive setting recorded by the first call: there is <i><b>inter-call
  state</b></i> implied by some familiar arch primitives.




<p>
  This note is about the data structures I'm coding for inter-call
  state in librified libarch.  The description here is in terms of a
  pseudo-code language; the transcription into the <b>C</b> API is
  straightforward.







  <h2>Scalars
</h2>
  <p>
    <b>String constants:</b> <code>&quot;hello world&quot;</code>




  <p>
    <b>Integers:</b> <code>-5</code>, <code>10</code>




  <p>
    <b>Identifiers:</b> <code>xyzzy</code>




  <p>
    <b>Nil:</b> <code>#nil</code>








  <h2>Module Names and Module Variables
</h2>
  <p>
    Each module has a unique (to the process) name.




  <p>
    A module can create a &quot;global&quot; variables whose state persists across 
  <code>libarch</code> calls:




  
  <pre>
        module user_params
        {
          var default_archive;
          var history_limit;
        }

        default_archive = &quot;gnu@gnu.gnu--2001&quot;;
        history_limit = 5;

  </pre>




  <h2>Lists, Arrays, and Associative Tables
</h2>
  <p>
    The system supports lists, arrays, and associative tables of scalar
  values:




  
  <pre>
      module user_params
      {
        list library_path;
        array aliases;
        list new_alias;
        table aliases_map;
      }

      push library_path &quot;~/{revlib}&quot;;
      push library_path &quot;/usr2/gnu/{revlib}&quot;;

      aliases[0] = list &quot;commit&quot; &quot;cmt&quot;;
      aliases[1] = list &quot;update&quot; &quot;up&quot;;

      new_alias = list &quot;replay&quot; &quot;r&quot;;
      push aliases new_alias;

      aliases_map[&quot;cmt&quot;] = &quot;commit&quot;;
      aliases_map[&quot;up&quot;] = &quot;update&quot;;


      --------------------------------

      library_path      =&gt;      (&quot;~/{revlib}&quot; &quot;/usr2/gnu/{revlib}&quot;)

      aliases           =&gt;      ((&quot;commit&quot; &quot;cmt&quot;)
                                 (&quot;update&quot; &quot;up&quot;)
                                 (&quot;replay&quot; r&quot;))

      new_alias         =&gt;      (&quot;replay&quot; &quot;r&quot;)

      library_path[0]   =&gt;      &quot;~/{revlib}&quot;

      aliases[0][1]     =&gt;      &quot;cmt&quot;;

      aliases_map[&quot;up&quot;] =&gt;      &quot;update&quot;;

      aliases_map[&quot;frob&quot;] =&gt;    #nil;

  </pre>




  <h2>Namespace Variables
</h2>
  <p>
    A <i><b>namespace variable</b></i> is similar to a table except that bindings
  in a namespace are not limited to being scalars (they may be of any
  type).




  
  <pre>
        module user_params
        {
          namespace perms_defaults;
        }

        list perms_defaults[&quot;gnu.org&quot;];
        push perms_defaults[&quot;gnu.org&quot;] = 'rw-;
        push perms_defaults[&quot;gnu.org&quot;] = 'r--;
        push perms_defaults[&quot;gnu.org&quot;] = 'r--;

        --------------------------------

        perms_defaults[&quot;gnu.org&quot;] =&gt; (rw-- r-- r--)

        perms_defaults[&quot;gnu.org&quot;][0] =&gt; rw--

  </pre>

  <p>
    <b>Note:</b> Namespaces are not <i>values</i> -- they are properties of
  <i>variables</i>.  For example, the above example could not contain the
  statement <code>tmp = perms_defaults</code> because that would be an attempt to
  use the entire namespace (<code>perms_defaults</code>) as a value (to be stored
  in <code>tmp</code>).  Namespaces may be <i>nested</i>: The statement <code>namespace
  perms_default[&quot;subdir&quot;]</code> creates a nested, initially-empty namespace
  named <code>&quot;subdir&quot;</code> in <code>perms_default</code>.








  <h2>Records
</h2>
  <p>
    A <i><b>record variable</b></i> is a nested namespace of variables.  Records 
  have a fixed structure which must be declared before use:




  
  <pre>
        record user_id
        {
          var full_id;
          var uid_part;
          table host_id_map;
        }

        module user_params
        {
          record user_id id;
        }

        id.full_id = &quot;Tom Lord &lt;lord@emf.net&gt;&quot;;
        id.uid_part = &quot;lord@emf.net&quot;
        id.host_id_map[&quot;emf.net&quot;] = &quot;lord&quot;;
        id.host_id_map[&quot;gnu.org&quot;] = &quot;tomlord&quot;;

  </pre>

  <p>
    <b>Note:</b> Records, like namespaces, are not <i>values</i> but are
  properties of <i>variables</i>.  A record can contain another
  record or a namespace, an array, list or table -- but
  records can not be copied from variable to variable.








  <h2>Example: A Database of Archive Locations
</h2>
  <p>
    It's useful to make a namespace of records:




  
  <pre>
        record archive_location
        {
          var archive_name;
          var primary;
          var mirror;
        }

        record user_params
        {
          namespace archive_registry;
        };


        record archive_location archive_registry[&quot;lord@emf.net--2005&quot;];

        archive_registry[&quot;lord@emf.net--2005&quot;].arch_name = &quot;lord@emf.net--2005&quot;;
        archive_registry[&quot;lord@emf.net--2005&quot;].primary = &quot;~/archives/CURRENT&quot;;
        archive_registry[&quot;lord@emf.net--2005&quot;].mirror = &quot; ... &quot;;

        archive_registry[&quot;lord@emf.net--2004&quot;].arch_name = &quot;lord@emf.net--2004&quot;;
        archive_registry[&quot;lord@emf.net--2004&quot;].primary = &quot;~/archives/PREV&quot;;
        archive_registry[&quot;lord@emf.net--2004&quot;].mirror = &quot; ... &quot;;

  </pre>




  <h2>Observation: It's A Tree (With Some Sharing)
</h2>
  <p>
    The global state of an arch client (represented in <b>C</b> as a <code>t_arch</code>
  value) forms a tree.  At the root is a <i>namespace of records</i>: the
  root namespace has one entry for each module; the private variables
  of a module are the fields of its corresponding record type.




  <p>
    Namespaces and records can contain namespaces and records and form a
  tree.




  <p>
    That tree contains subtrees, each of which is a list, array, table,
  or scalar.   Arrays are subtrees of lists.  Lists and tables are
  subtrees of scalars.  All leaf nodes are scalars.




  <p>
    (One pleasing consequence is that all of these data structures can
  be reference counted since a tree contains no cycles.)




  <p>
    A single list, array, or table may occur at multiple locations in
  the tree.  This matters only when mutations are considered: changing
  a list at one place in the tree might change another part of the
  tree where that list also occurs.   Module implementations should
  use this facility for sharing with caution for obvious reasons.








  <h2>A Nice Debugging Aid
</h2>
  <p>
    Among other benefits, organizing the inter-call state of <code>libarch</code>
  this way gives rise to a nice debugging aid: the entire state of
  a <code>t_arch</code> instance can be usefully printed by a generic printer:




  
  <pre>
    module user_params
      {
        var user_id = &quot;Tom Lord &lt;lord@emf.net&gt;&quot;;
        var default = &quot;lord@emf.net--2005&quot;;
      }

    module revision_libraries
      {
        list path = (&quot;~/{revlib}&quot; &quot;/usr2/share/{revlib}&quot;);
        namespace libprops
          {
            record library_properties &quot;~/{revlib}&quot;
              {
                greedy = 1;
                sparse = 1;
              }
            record library_properties &quot;/usr2/share/{revlib}&quot;
              {
                greedy = 1;
                sparse = 0;
              }
          }
      }

      ...etc...

  </pre>

  <p>
    (The printer is what I'm currently working on.)








    </div>

    <div class="navLeft">

<p>
  <a href="../../../index.html">home</a>





  <h2>GNU Arch
</h2>
  <p>
    <a href="../../../web/gnu-arch/what-is.html">what is GNU Arch?</a>




  <p>
    <a href="../../../bugs/index.html">GNU Arch Bug Database</a>




  <p>
    <a href="../../../index.html#external GNU Arch links">popular GNU Arch links</a>




  <p>
    <a href="../../../web/communications/gnu-arch-roadmap.html">GNU Arch development roadmap</a>




  <p>
    <a href="../../../web/gnu-arch/popular-writings.html">popular GNU Arch writings</a>






    </div>

  </body>
</html>
