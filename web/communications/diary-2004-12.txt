[[title

  Tom's Work Diary (December 2004)

]]

[[blockquote
  //\*^<<<"`<--' November 2004" -- ./diary-2004-11.html>>>^*\//
  //\*^<<<"January 2005 `-->'" -- ./diary-2005-01.html>>>^*\//
]]

[[cartouche

  /`Thu Dec 23 12:02 2004'/

  \// Today's goals //\

  * Progress on the `tla 2.0' todo list

  /`Thu Dec 23 12:02 2004'/

  A bit of code shuffling (bringing down prototype from rafters to the
  2.0 mainline) and there's <"progress on the `libarch' API" --
  ./src/tla2/guide.html>.

]]

[[cartouche

  /`Fri Dec 17 11:01 2004'/

  \// Today's goals //\

  /Really, really!  Overdue TODO Items!/

  * Deal with the FSF copyright assignment paperwork

  * Make a mirror of `lord@regexps.com--2002' available
    somewhere where jblack can snarf it.

  /Routine todo items:/

  * Progress on the `tla 2.0' todo list

  /`Fri Dec 17 12:21 2004'/

  Made ample progress on `tla 2.0'.

  Finally got jblack his mirror and even sent mail about it!

  Finally mailed back FSF paperwork!

   /`Fri Dec 17 11:03 2004'/

   `tla 2.0' continues to <"progress nicely" -- $/src/tla2/guide.html>.

   It's probably not controversial but <"here is a standard for the
   lexical syntax of subcommand names" --
   $/src/tla2/stds/arch-cmd-name.html>

   Now I must catch up on some overdue <"non-tla 2.0 TODO items" -- 
   $/web/communications/diary-2004-12.html>
  
]]

[[cartouche

  /`Thu Dec 16 13:12:11 2004'/

  \// Today's goals //\

  * ^Finish^ the tla 2.0 error code stuff
 
  * Deal with the FSF copyright assignment paperwork

  * Make a mirror of `lord@emf.net--2002' available
    somewhere where jblack can snarf it.

   /`Thu Dec 16 13:13 2004'/

   Hey, boys and girls, I hit a */milestone/*.... crazy, eh?
   The error code system for `tla2' is now officially (I assert)
   in decent enough shape to move on to the next `tla2' subtask.

    <"read all about it...." -- $/src/tla2/guide.html#Error Subsystems>
]]

[[cartouche

  /`Wed Dec 15 12:00 2004'/

  \// Today's goals //\

  * Write up the tla-2.0

  * Update this web site

  * Deal with the FSF copyright assignment paperwork

  * Freshmeat about 1.3pre3

  * Make a mirror of `lord@emf.net--2002' available
    somewhere where jblack can snarf it.

  /`Wed Dec 15 12:01 2004'/

  Coding of `tla 2.0' has begun.   Currently, attention is being 
  given to the mechanisms for managing error codes.

  You can read about the status of that at <$/src/tla2/guide.html>.

]]


[[cartouche

  /`Mon Dec 13 08:33 2004'/

  \// Today's goals //\

  * Write up the tla-2.0

  * Update this web site

  * Deal with the FSF copyright assignment paperwork

  * Freshmeat about 1.3pre3

  * Make a mirror of `lord@emf.net--2002' available
    somewhere where jblack can snarf it.


  /'Mon Dec 13 08:18 2004'/

    It's been a busy two weeks since my last web site update.
    
    /Awiki now using css -- / As you can see, the awiki tool that
    generates these web pages now has some rudimentary CSS hacks.  A
    friend "forced" me to learn a little bit about CSS recently and,
    indeed, the core functionality of it seems to be clean, sensible,
    and useful.   The next major revision to the awiki source code
    (which is not an *immediate* priority) will (a) make the wiki
    syntax it recognizes entirely "table driven" --- you can create
    custom wiki syntaxes by writing a new table;  (b) make the 
    translation of a wiki parse syntax tree to xml table driven -- 
    choose your own element tags and `CLASS' attributes.


    /tla-1.3pre3 finally at ftp.gnu.org -- / Freshmeat announcement
    going out today.

    /clarity achieved (relatively speaking) about archive and project
    tree formats -- / recent discussions on those topics on the dev list
    have been very enlightening, at least for me.

    /tla 2.0 source base started;  `libawk' largely replace -- /
    The `tla-2.0' project is officially underway.

]]


[[null

  ; arch-tag: Tom Lord Mon Dec 13 09:28:33 2004 (communications/diary-2004-12.txt)

]]
