<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
    <title>Tom Lord's Hackery</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link href="../../gnuarch.css" rel="stylesheet" type="text/css">
  </head>

  <body>

    <h1 class="page-title"><u>Tom Lord's Hackery</u></h1>

    <div class="mainContent">

<h1 class=essay-title>
  Anatomy of a Librified <code>libarch</code> Function<br>
  <hr align="left" width="14%">
  <small><b><code>#include &lt;tla/libarch/arch.h&gt;</code></b><br>
  </small>
</h1>

<blockquote class="cartouche">
  
  <p>
    It may be helpful to read this document in parallel with the
        <a href="../../src/tla/Template-fn">template <b>C</b> code for <code>libarch</code> functions</a>




  <p>
    The template is described in detail in the appendix
        <a href="#Template Details">Template Details</a>.



</blockquote>


  <h2><code>t_arch const arch</code> -- The Universal Function Parameter
</h2>
  <p>
    Every ordinary function in <code>libarch</code> accepts a parameter of type
  <code>t_arch</code>, conventionally named <code>arch</code>.  Some very low level
  functions are an exception to this rule.




  <p>
    By convention, the output parameters to a function are first
  in its parameter list, then the <code>t_arch const arch</code> parameter,
  and then any input parameters.








  <h2>Arch Sessions and Arch Calls
</h2>
  <p>
    The <code>arch</code> parameter holds the state, including dynamically
  allocated resources, of a single &quot;arch session&quot; and of all &quot;arch
  calls&quot; within that session.




  <p>
    An <b>arch call</b> is the invocation and execution of a single,
  client-facing, ordinary <code>libarch</code> function.  Typically an arch
  call performs a primitive arch operation or calculation.  A call may
  temporarily allocate resources as it runs; these resources are
  released before the call returns to the client.   During a single
  call, temporary resources are &quot;attached&quot; to the <code>t_arch const arch</code> 
  and, when a call returns, are automatically released.




  <p>
    An <b>arch session</b> is a sequence of calls which may share 
  persistent state among themselves.   A <code>t_arch const arch</code> value
  is the name for a single context in which such persistent state
  can be recorded.   For example, the location of a user's
  <code>.arch-params</code> directory defaults to <code>~/</code> but it might as easily be
  an element of the persistent state of a <code>t_arch const arch</code> object.
  A client can allocate a <code>t_arch</code>, set its <code>.arch-params</code> location
  to a non-default location, and then perform a series of arch 
  calls with that <code>t_arch</code>.   Those arch calls will use the
  non-default <code>.arch-params</code> location.   Other calls in the 
  same process, using a different <code>t_arch</code>, might use a 
  different <code>.arch-params</code> location.








  <h2>Anatomy of an Arch Function
</h2>
  
    <h3>Entry and Exit
</h3>
    <p>
      <code>libarch</code> extends the standard <b>C</b> function prolog and
  postlog with its own conventions.   Every <code>libarch</code> function
  must be of the general form (here illustrated for a function
  which can not raise errors):




    
    <pre>

    int
    some_fn (t_arch const arch)
    {
      int answer = -1;

      arch_enter_call (arch);

      /* ... compute the real answer.  Set `answer'.  ... */

      arch_leave_call (arch);
      return answer;
    }


    </pre>

    <p>
      Unbalanced calls to <code>arch_leave_call</code> constitute an invariant
  violation resulting from a coding error and cause a
  call to <code>panic</code>.





  
  
  
    <h3>Error Codes and Raising Errors
</h3>
    <p>
      <code>libarch</code> uses dynamically assigned <i><b>positive</b></i><b>integer error codes 
  when reporting error conditions to callers.   Must functions which
  can indicate an error do so by returning a <i><big>negative</big></i></b><i>integer
  whose magnitude (additive inverse) is an error code.

</i>


    <p>
      In addition to signalling errors via return value, functions must
  explicitly <i>raise</i> errors, which records then in the <code>t_arch const
  arch</code> parameter.  Typical code looks (schematically) like this:





    
    <pre>
    ssize_t
    some_fn (t_arch const arch)
    {
      ssize_t answer = -1;
      ssize_t errcode = 0;

      arch_enter_call (arch);

      /* ... compute the real value for `answer' but:
       */
      if (... an XYZZY error has been detected ...)
        {
          errcode = error_arch_XYZZY ();
          goto error_exit;
        }

      /* ... assuming that `answer' is now set 
       */
      invariant (answer &gt;= 0);  /* normal return can't be negative --
                                 * that's reserved for errors.
                                 */
      arch_leave_call (arch);
      return answer;


     error_exit:
       invariant (errcode &gt; 0);
       arch_RAISE (arch, errcode, &quot;some_fn&quot;);
       answer = -errcode;
       arch_leave_call (arch);
       return answer;
    }


    </pre>

    <p>
      See also <a href="#Error State Transition Table">Error State Transition Table</a>.





  
  
  
    <h3>Error Messages -- The <code>msgbuf</code>
</h3>
    <p>
      Between a call to <code>arch_enter_call</code> and a call to <code>arch_RAISE</code>,
  clients can format arbitrary text to be associated with the
  raised error.   (The intent is that this is text which
  might be displayed to a user.)




    <p>
      The exmaple above might be modified to contain:




    
    <pre>
      if (... an XYZZY error has been detected ...)
        {
          arch_msgbuf_printfmt (arch, &quot;an xyzzy error caused by %s\n&quot;, xyzzy_errstr());
          errcode = error_arch_XYZZY ();
          goto error_exit;
        }

    </pre>

  
  
  
    <h3>Managing Allocted Local State
</h3>
    <p>
      Commonly, <code>libarch</code> functions need to allocate temporary strings,
  lists, and tables to contain the results of intermediate
  computations.   Those data structures must be reliably freed
  before returning to the caller.   The situation is common enough
  that <code>libarch</code> includes help for it.




    <p>
      Local allocated values, when allocated, are &quot;attached&quot; to a <code>t_arch
  const arch</code> value.  When the call is exited (indicated by a call to
  <code>arch_leave_call</code>), those attached resources are freed:




    <p>
      <b>WARNING:</b> this interface is not fully implemented yet and is
  subject to minor changes as it is finalized.




    
    <pre>
    ssize_t
    some_fn (t_arch const arch)
    {
      ARCH_FN_DECL;
      ssize_t answer = -1;
      t_local_val tmp_str = LOCAL_VAL_NIL_INIT;
      ssize_t status = -1;

      arch_enter_call (arch);


      /* ... */

      /* e.g.: allocate a local string
       */
      status = val_make_str (addr_local_val (tmp_str, arch),
                             arch,
                             &quot;hello world&quot;);
      if (status &lt; 0)
        {
          errcode = arch_error_ENOMEM ();
          goto error_exit;
        }

      /* ... */

      arch_leave_call (arch);
      /*
       * The call to `arch_leave_call' above will implicitly 
       * release the storage allocated for `tmp_str'.
       */

      return answer;


     error_exit:
       invariant (errcode &gt; 0);
       arch_RAISE (arch, errcode, &quot;some_fn&quot;);
       answer = -errcode;
       arch_leave_call (arch);
       return answer;
    }


    </pre>

  
  



  <h2>Appendix: <a name="Template Details"><b><code>{Template Details}</code></b></a>
</h2>
  <p>
    The file <a href="../../src/tla/Template-fn">src/tla/Template-fn</a> contains
  a template for librified <code>libarch</code> functions.   The template begins:




  
    <h3>Function Prototype
</h3>
    
    <pre>
    /** function: FN_NAME
     *
     */
    ssize_t
    FN_NAME (t_arch const arch)

    </pre>

    <p>
      there and elsewhere, replace <code>FN_NAME</code> with the name of the function
  you are defining.




    <p>
      Most functions should return a <code>ssize_t</code> value, a negative integer
  indicating an error (whose error code is the magnitude of that
  negative integer).   Functions with no other use for the <code>ssize_t</code>
  return value should return <code>0</code> upon success.   Other functions may
  return any non-negative value upon success.




    <p>
      Output parameters should be declared prior to the <code>arch</code> parameter;
  other parameters following the <code>arch</code> parameter.   All parameters
  should be declared const.  Normally, each parameter should be 
  declared on a separate line.  For example:




    
    <pre>
    ssize_t
    some_fn (int * const n_ret,
             t_arch const arch,
             int const x,
             int const y)

    </pre>

  
  
  
    <h3>Local Variable
</h3>
    <p>
      All non-const local variables <b>must</b> be declared in the outermost 
  block of a function.  The outermost block in the template begins: 




    
    <pre>
    {
      ARCH_FN_DECLS;
      ssize_t answer = -1;
      int pc = 0;
      ssize_t errcode = 0;

    </pre>

    <p>
      Additional local variables may be added to that list.




    <p>
      The sole exception to this role is local variables which are
  <code>const</code>.  Such variables may be declared (and necessarily
  initialized) at the top of nested blocks.




    <p>
      No local variable in the outermost block may have a non-constant
  initializer or may have an initializer which makes a call to 
  a function (even a <code>const</code> function).





  
  
  
    <h3>Function Entry
</h3>
    <p>
      The body of the template reads, schematically:




    
    <pre>
    if (pc == 0)
      {
        /* init */
        arch_enter_call (arch);
        goto body;
      }
    else
      {
        [... error handling code ...]

      normal_exit:
        invariant (answer &gt;= 0);
        goto exit_with_cleanups;

      exit_with_cleanups:
        goto exit_immediately;

      exit_immediately:
        arch_leave_call (arch);
        return answer;
      }

    body:

    [... main body of the function ...]

    goto normal_exit;


    </pre>

    <p>
      Programs may add code to the section labeled <code>/* init */</code>
  between the call to <code>arch_enter_call</code> and the 
  <code>goto body</code> statement.   Coded added in that location
  should be solely for the purpose of initializing local
  variables to values which are not constant.  For example, 
  a local variable declared:




    
    <pre>
     time_t now = 0;

    </pre>

    <p>
      might be initialized in the <code>/* init */</code> section with:




    
    <pre>
     time_t now = time (0);

    </pre>

    <p>
      It could not have been declared these ways:




    
    <pre>
     time_t now = time (0);             /* illegal (by convention) */
     time_t const now = time (0);       /* illegal (by convention) */

    </pre>

    <p>
      because local variable initializers at the top of a <code>libarch</code>
  function may not make function calls.




  
  
  
    <h3>Single Entry, Single Exit
</h3>
    <p>
      The template contains the code:




    
    <pre>
      exit_immediately:
        arch_leave_call (arch);
        return answer;

    </pre>

    <p>
      That is the only <code>return</code> statement in the template and programmers
  <b>must not</b> add addition <code>return</code> statements to the function
  definition.   All exit paths from the function must pass through the
  code labeled <code>exit_immediately</code>.




    <p>
      The call to <code>arch_leave_call</code> provides run-time enforcement of an
  open ended set of calling convention invarients.  It has has
  responsibility for releasing temporary resources allocated by this
  function call.





  
  
  
    <h3>Error and Normal Returns but a Single Cleanup Path
</h3>
    <p>
      The template contains:




    
    <pre>

    error_exit:
      invariant (errcode &gt; 0);
      arch_RAISE (arch, errcode, &quot;FN_NAME&quot;);
      answer = -errcode;
      goto exit_with_cleanups;

    efatal_exit:
      errcode = error_arch_EFATAL ();
      goto error_exit;

    eparam_exit:
      errcode = error_arch_EPARAM ();
      goto error_exit;

    normal_exit:
      invariant (answer &gt;= 0);
      goto exit_with_cleanups;

    exit_with_cleanups:
      goto exit_immediately;


    </pre>

    <p>
      Programmers <b>may</b> delete the code labeled <code>efatal_exit</code> and
  <code>eparam_exit</code> if those exit paths are not needed.




    <p>
      All error exit paths from the function should involve setting
  <code>errcode</code> to a positive integer value and transfering to
  <code>error_exit</code>.  




    <p>
      All ordinary exit paths from the function should involve setting the
  variable <code>answer</code> to the return value from the function and
  transfering to <code>normal_exit</code>.




    <p>
      When those conventions are followed, execution passes through the 
  code labled <code>exit_with_cleanups</code> and finally to`exit_immediately'.




    <p>
      Programmers may (have to) customize the template in two ways:
  (1) By adding resource-releasing code to the section labled
  <code>exit_with_cleanups</code>;  (2) by modifying the <code>invariant</code>'s in
  <code>error_exit</code> and <code>normal_exit</code> to more precisely reflect the 
  range of expected return values from the function.





  
  
  
    <h3>The Main Function Body
</h3>
    <p>
      The template finishes with:




    
    <pre>

   body:

    goto normal_exit;


    </pre>

    <p>
      Programmers must customize that part of the template by filling in 
  code implementing the function between the <code>body</code> and the <code>goto</code>.
  In some cases, programmer's must replace the label in the <code>goto</code> or
  replace the entire <code>goto</code> with <code>/* not reached */</code>.






  
  



  <h2>Appendix: <a name="Error State Transition Table"><b><code>{Error State Transition Table}</code></b></a>
</h2>
  <p>
    Any <code>libarch</code> function (client facing or ordinary internal function)
  can raise an error.   Caller's <i>must</i> respond to all errors raised.




  <p>
    To an imperfect but practical extent, <code>libarch</code> enforces that
  restriction.   In particular, every <code>t_libarch</code> is partly
  characterized by an <i>error state</i>.   The error state is 
  described by three integers, according to the state diagram below.




  <p>
    The gist is that if a function you call raises an error, your client 
  must either re-raise the error before returning to <i>its</i> caller, or
  must &quot;clear&quot; the error state.




  
  <pre>

    NORMAL state:
          depth = D
          err_depth = -1;
          last_raise_depth = -1;

      invariants:
          0 &lt;= D

      transitions:

         * arch_enter_call (arch)

            goto NORMAL with
              new_depth := D + 1

         * arch_leave_call (arch)

            if (depth == 0)
              FATAL ERROR
            else
              goto NORMAL with
                new_depth := D - 1

         * arch_RAISE (arch, errcode, tag)

            goto ERROR_HERE with
              new_err_depth := D
              new_last_raise_depth := D


         * arch_clear_error (arch)

            goto NORMAL with
              err_depth = -1;
              last_raise_depth = -1;


    ----

    ERROR_HERE state:

          depth = D
          err_depth = E;
          last_raise_depth = L;

      invariants:
          D &gt;= 0
          D &lt;= E
          D == L

      transitions:

         * arch_enter_call (arch)

            FATAL ERROR

         * arch_leave_call (arch)

            if (depth == 0)
              FATAL ERROR
            else
              goto ERROR_CALLEE with
                new_depth := D - 1

         * arch_RAISE (arch, errcode, tag)

           goto ERROR_HERE

         * arch_clear_error (arch)

            goto NORMAL with
              err_depth = -1;
              last_raise_depth = -1;

    ----

    ERROR_CALLEE state:

          depth = D
          err_depth = E;
          last_raise_depth = L;

      invariants:
          D &gt;= 0
          D &lt; E
          D == (L - 1)

      transitions:

         * arch_enter_call (arch)

            FATAL ERROR

          * arch_leave_call (arch)

            FATAL ERROR

          * arch_RAISE (arch, errcode, tag)

            goto ERROR_HERE with
              new_last_raise_depth = D

         * arch_clear_error (arch)

            goto NORMAL with
              err_depth = -1;
              last_raise_depth = -1;


  </pre>




  <h2>Copyright
</h2>
  <p>
    <b>Copyright (C) 2004 Tom Lord</b>




  <p>
    This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.




  <p>
    This program is distributed in the hope that it will be useful,
 but <b>WITHOUT ANY WARRANTY</b>; without even the implied warranty of
 <b>MERCHANTABILITY</b> or <b>FITNESS FOR A PARTICULAR PURPOSE</b>.  See the
 GNU General Public License for more details.




  <p>
    You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software Foundation,
 Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.




  <p>
    See the file <code>COPYING</code> for further information about
 the copyright and warranty status of this work.







    </div>

    <div class="navLeft">

<p>
  <a href="../../index.html">home</a>





  <h2>GNU Arch
</h2>
  <p>
    <a href="../../web/gnu-arch/what-is.html">what is GNU Arch?</a>




  <p>
    <a href="../../bugs/index.html">GNU Arch Bug Database</a>




  <p>
    <a href="../../index.html#external GNU Arch links">popular GNU Arch links</a>




  <p>
    <a href="../../web/communications/gnu-arch-roadmap.html">GNU Arch development roadmap</a>




  <p>
    <a href="../../web/gnu-arch/popular-writings.html">popular GNU Arch writings</a>






    </div>

  </body>
</html>
