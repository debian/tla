[[title

  Tom's Work Diary (February 2005)

]]

[[blockquote
  //\*^<<<"`<--' January 2005" -- ./diary-2005-01.html>>>^*\//
  //\*^<<<"March 2005 `-->'" -- ./diary-2005-03.html>>>^*\//
]]

[[cartouche
  /`Mon Feb 21 16:53 2005'/

  I've been working on an experiment regarding librifying the `1.3.1'
  code base.

  Here's my <"week-1 report" -- $/web/communications/progress-2005021.html>
]]

[[cartouche
  /`Mon Feb 14 12:19 2005'

  I've implemented changes in `1.3.1' which result in relational
  table entries and keys and values in associative tables (both
  `libawk' data structures) can be shared and are, in fact, shared
  opportunistically.

  Crude initial testing suggests that these changes result in 
  fairly substantial memory savings, especially on large trees.

  Speed is roughly unchanged, with a silght *degredation*.  On the
  other hand, the new code hasn't been tuned and profiling of it
  suggests that obtaining a net speed *improvement* is much simpler
  after these changes than before.

  The nitty gritty details are available in <"a bug report" --
  $/bugs--devo--2005/all-bugs/libawk-api-bogosities.html>.

]]


[[cartouche
  /`Fri Feb 11 16:56 2005'/

  There are two new patches (`patch-1' and `patch-2', in fact) in:

  [[tty
        lord@emf.net--2005/tla--devo--1.3.1

  ]]

  which are summarized:

  [[tty
    patch-2
      Remove "libawk/trim.[ch]"
    patch-1
      remove libawk/numbers.[ch]
  ]]

  Use of these patches requires the latest revision of libhackerlab
  in:


  [[tty
        lord@emf.net--2005/hackerlab--devo--1.1
  ]]

  (<"archive locations..." -- $/web/gnu-arch/famous-archives.html>)

  Merging these changes into branches which wish to remain somewhat 
  in sync with GNU Arch 1.3.1 is recommended.   These ought to be 
  low impact changes for all branches.

  The short description is that these changes remove the source files
  matching the patterns:

  [[tty
        ./src/tla/libawk/numbers.[ch]
        ./src/tla/libawk/trim.[ch]
  ]]

  The two functions defined in those files have been removed and all
  callers updated to reflect that.

  The changes are documented in detail in the log messages of

  [[blockquote
    [[styled-lines
        /Archive:/ `lord@emf.net--libawk-exp-2005'
        /Version:/ tla--libawk-exp--1.3.1'
        /Revs:/ `patch-1' ... `patch-9'
    ]]
  ]]
]]


[[cartouche
  /`Wed Feb  9 11:37 2005'/

  The new system for recording bugs in GNU Arch releases seems to work
  reasonably well so far and hopefully will be officially in operation
  before too long.  Currently it is still an unofficial experiment,
  temporarily housed at <http://www.gnuarch.org/bugs--devo--2005>.

  In other news, I am experimentating and exploring a 
  <"performance question concerning string allocation and copying
  practices in arch" -- $/web/gnu-arch/string-copying-notes.html>.

]]


[[cartouche
  /`Sun Feb  6 13:00 2005'/ 

  Well, `tla-1.4pre1' looks terrible: it seems it will not work very
  well to just "pass through" `baz' changes as GNU Arch.

  Therefore, work on `tla-2.0' has been de-prioritized, and work in
  persuit of a new <"GNU Arch Roadmap" --
  ./web/communications/gnu-arch-roadmap.html> has begun.

  /`Sun Feb  6 13:11 2005'/ Pika and XL are not dead or even entirely
  dormant.   But to maintain the freedom to work on them, I must
  concentrate heavily on `tla 1.x' right now.  I have removed
  links about them from this page because I don't want to take the
  time to maintain those links properly right now.

]]

* Copyright

 /Copyright (C) 2004 Tom Lord/

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 This program is distributed in the hope that it will be useful,
 but /WITHOUT ANY WARRANTY/; without even the implied warranty of
 /MERCHANTABILITY/ or /FITNESS FOR A PARTICULAR PURPOSE/.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software Foundation,
 Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

 See the file `COPYING' for further information about
 the copyright and warranty status of this work.



[[null
   ; arch-tag: Tom Lord Wed Feb  9 09:11:24 2005 (communications/diary-2005-02.txt)
]]

