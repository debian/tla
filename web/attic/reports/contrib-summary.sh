#!/usr/local/bin/bash

set -e


printf '[[title\n'
printf "             Contrib Branches Summary\n"
printf ']]\n'
printf '\n'
printf '\n'

archive="$1"

feature_versions=$(tla abrowse --versions lord@emf.net--gnu-arch-ws-2004 \
     		   | grep -E '.--feature.*--' \
		   | sed -e 's/^[ 	]*//' \
		   | sort -u)

for v in $feature_versions ; do
  printf "* \`%s\'\n" $v
  printf '\n'
  latest_rev=$(tla revisions --full -r $1/$v | head -1)
  root=$(tla library-find "$latest_rev")
  category=$(tla parse-package-name --category "$latest_rev")
  mainline=$(gtla mainline-cfg | awk '{print $2}' | grep -E "/$category--" | head -1)
  printf '  The mainline for this contribution branch is\n'
  if [ -z "$mainline" ] ; then
    printf '  */unknown/*.\n'
  else
    printf "  \`%s\'.\\n" "$mainline"
    printf '\n'
    latest="$( cd $root ; tla logs -r -s $mainline | head -1 )"
    if [ -z "$latest" ] ; then
      printf '  This contribution has no patch logs for the mainline.\n'
    else
      printf "  This contribution branch is up-to-date with the mainline revision \`%s\'\\n" $latest
    fi
  fi
  printf '\n'


  if [ ! -e $root/CONTRIBUTION-LOG ] ; then
     printf "  *no \`CONTRIBUTION-LOG\' found in \`%s\'*\n" $(tla parse-package-name --lvl "$latest_rev")
  else
     printf "  *from \`./CONTRIBUTION-LOG\' in \`%s\'*\n" $(tla parse-package-name --lvl "$latest_rev")
     printf '\n'
     printf '    [[cartouche\n'
     printf '\n'
     grep -i -E '^summary:' $root/CONTRIBUTION-LOG | head -1 | sed -e 's,^[^:]*:,      /Summary: /,'
     printf '\n'
     printf '\n'
     cat $root/CONTRIBUTION-LOG \
     | awk '
            BEGIN { past_headers = 0; 
                    lines_printed = -1;
                  }


            (!past_headers && match($0,"^[[:space:]]*$")) { past_headers = 1; }

            ((lines_printed < 0) && past_headers && !match($0,"^[[:space:]]*$")) { lines_printed = 0; }

            (lines_printed >= 0) { 
                		   print $0; 
                		   lines_printed = lines_printed + 1;
                                   if (lines_printed >= 10)
                                     exit(0);
                                 }
           ' \
      | sed -e 's/^/        /'
     printf '       /*[...]*/\n'
     printf '\n'
     printf '    ]]\n'
     printf '\n'

  fi
  printf '\n'
  printf '\n'
  printf '  /Recent Commits:/\n'
  printf '\n'
  printf '  [[log-summary\n\n'
    tla revisions --full --summary --creator --date --reverse -A "$archive" "$v" | head -20 | sed -e 's/^/    /'
  printf '  ]]\n'
  printf '\n'
  printf '\n'
  printf '\n'
done


# arch-tag: Tom Lord Tue Nov 23 09:22:30 2004 (reports/contrib-summary.sh)
